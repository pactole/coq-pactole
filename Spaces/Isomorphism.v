(**************************************************************************)
(**   Mechanised Framework for Local Interactions & Distributed Algorithms  
      T. Balabonski, P. Courtieu, R. Pelle, L. Rieg, X. Urbain              
                                                                            
      PACTOLE project                                                       
                                                                            
      This file is distributed under the terms of the CeCILL-C licence.     
                                                                          *)
(**************************************************************************)


Require Import Utf8.
Require Import SetoidDec.
Require Import Rbase Rbasic_fun.
Require Import Psatz.
Require Import Pactole.Util.Coqlib.
Require Import Pactole.Util.Bijection.
Require Import Pactole.Core.Configuration.
Require Import Pactole.Spaces.Graph.
Set Implicit Arguments.


(****************************)
(** *  Graph Isomorphisms  **)
(****************************)


Section Isomorphism.
Context {V E : Type}.
Context {G : Graph V E}.

Record isomorphism := {
  iso_V :> bijection V;
  iso_E : bijection E;
  iso_morphism : ∀ e, iso_V (src e) == src (iso_E e)
                   /\ iso_V (tgt e) == tgt (iso_E e) }.

Global Instance isomorphism_Setoid : Setoid isomorphism.
Proof using .
  simple refine {|
    equiv := λ iso1 iso2, iso1.(iso_V) == iso2.(iso_V)
                       /\ iso1.(iso_E) == iso2.(iso_E) |}; autoclass. split.
  + intro f. now repeat split.
  + intros f g Hfg; destruct Hfg as [HV HE]. split; now symmetry.
  + intros f g h Hfg Hgh. destruct Hfg as [? ?], Hgh as [? ?].
    split; etransitivity; eauto.
Defined.

Instance iso_V_compat : Proper (equiv ==> equiv) iso_V.
Proof using . intros ? ? Heq ?. now apply Heq. Qed.

Instance iso_E_compat : Proper (equiv ==> equiv) iso_E.
Proof using . intros ? ? Heq ?. now apply Heq. Qed.

Lemma equiv_iso_V_to_iso_E iso1 iso2:
  iso_V iso1 == iso_V iso2 -> iso_E iso1 == iso_E iso2.
Proof using .
  intros Heqiso_V e. apply simple_graph. rewrite <-! (proj1 (iso_morphism _ _)),
  <-! (proj2 (iso_morphism _ _)), <- Heqiso_V. split. all: reflexivity.
Qed.

Definition id : isomorphism.
Proof using .
  refine {| iso_V := id;
            iso_E := id |}.
  now intros.
Defined.

Definition comp (f g : isomorphism) : isomorphism.
Proof using .
  refine {|
      iso_V := compose f.(iso_V) g.(iso_V);
      iso_E := compose f.(iso_E) g.(iso_E) |}.
  intro. simpl. split; now rewrite <- 2 (proj1 (iso_morphism _ _)) || rewrite <- 2 (proj2 (iso_morphism _ _)).
Defined.

Global Instance IsoComposition : Composition isomorphism.
refine {| compose := comp |}.
Proof. intros f1 f2 Hf g1 g2 Hg. repeat split; intro; simpl; now rewrite Hf, Hg. Defined.

(* Global Instance compose_compat : Proper (equiv ==> equiv ==> equiv) compose.
Proof. intros f1 f2 Hf g1 g2 Hg. repeat split; intro; simpl; now rewrite Hf, Hg. Qed. *)

Lemma compvE : ∀ iso1 iso2 : isomorphism,
  iso_V (iso1 ∘ iso2) = iso_V iso1 ∘ iso_V iso2 :> bijection V.
Proof using . reflexivity. Qed.

Lemma compeE : ∀ iso1 iso2 : isomorphism,
  iso_E (iso1 ∘ iso2) = iso_E iso1 ∘ iso_E iso2 :> bijection E.
Proof using . reflexivity. Qed.

Lemma compose_assoc : ∀ f g h, f ∘ (g ∘ h) == (f ∘ g) ∘ h.
Proof using . intros f g h; repeat split; simpl; reflexivity. Qed.

Definition inv (iso : isomorphism) : isomorphism.
Proof using .
  refine {| iso_V := inverse iso.(iso_V);
            iso_E := inverse iso.(iso_E) |}.
  intro. simpl. rewrite <- 2 Inversion, (proj1 (iso_morphism _ _)), (proj2 (iso_morphism _ _)).
  split; apply src_compat || apply tgt_compat; now rewrite Inversion.
Defined.

Global Instance IsoInverse : Inverse isomorphism.
Proof using .
  refine {| inverse := inv |}.
  intros f g [? ?]. repeat split; intro; simpl; change eq with (@equiv R _); f_equiv; auto.
Defined.

Lemma id_inv : id⁻¹ == id.
Proof using . split. all: setoid_rewrite id_inv. all: reflexivity. Qed.

Lemma id_comp_l : ∀ iso : isomorphism, id ∘ iso == iso.
Proof using. intros. split. all: setoid_rewrite id_comp_l. all: reflexivity. Qed.

Lemma id_comp_r : ∀ iso : isomorphism, iso ∘ id == iso.
Proof using. intros. split. all: setoid_rewrite id_comp_r. all: reflexivity. Qed.

Lemma inv_inv : ∀ iso : isomorphism, iso⁻¹⁻¹ == iso.
Proof using . intros. split. all: setoid_rewrite inv_inv. all: reflexivity. Qed.

(* Global Instance inverse_compat : Proper (equiv ==> equiv) inverse.
Proof.
intros f g [? [? ?]].
repeat split; intro; simpl; change eq with (@equiv R _); f_equiv; auto.
Qed. *)

Lemma compose_inverse_l : ∀ iso : isomorphism, iso ⁻¹ ∘ iso == id.
Proof using . intro. repeat split; intro; simpl; try now rewrite retraction_section; autoclass. Qed.

Lemma compose_inverse_r : ∀ iso : isomorphism, iso ∘ (iso ⁻¹) == id.
Proof using . intro. repeat split; intro; simpl; try now rewrite section_retraction; autoclass. Qed.

Lemma inverse_compose : ∀ f g : isomorphism, (f ∘ g) ⁻¹ == (g ⁻¹) ∘ (f ⁻¹).
Proof using . intros f g; repeat split; intro; simpl; reflexivity. Qed.

End Isomorphism.

Arguments isomorphism {V} {E} G.

Lemma find_edge_iso_Some `{G : Graph} : ∀ (iso : isomorphism G) src tgt e,
  @find_edge _ _ G (iso src) (iso tgt) == Some (iso.(iso_E) e)
  <-> @find_edge _ _ G src tgt == Some e.
Proof using .
intros iso src tgt e.
assert (strong_and : forall T U V W (A B : T -> U -> V -> W -> Prop),
          (forall x y z t, B x y z t) ->
          ((forall x y z t, B x y z t) -> (forall x y z t, A x y z t)) ->
          forall x y z t, A x y z t /\ B x y z t) by intuition.
revert iso src tgt e. apply strong_and.
+ intros iso src tgt e. rewrite 2 find_edge_Some. intro Hfind.
  rewrite (proj1 Hfind), (proj2 Hfind). apply iso_morphism.
+ intros Hstep iso src tgt e H.
  specialize (Hstep (inverse iso) (iso src) (iso tgt) (iso_E iso e) H).
  simpl in Hstep. now rewrite 3 Bijection.retraction_section in Hstep.
Qed.

Lemma find_edge_iso_None `{G : Graph} : ∀ (iso : isomorphism G) src tgt,
  @find_edge _ _ G (iso src) (iso tgt) == None <-> @find_edge _ _ G src tgt == None.
Proof using .
intros iso src tgt. destruct (find_edge src tgt) eqn:Hcase.
+ apply (eq_subrelation (R := equiv)) in Hcase; autoclass; [].
  rewrite <- find_edge_iso_Some in Hcase. rewrite Hcase. tauto.
+ intuition; []. destruct (find_edge (iso src) (iso tgt)) eqn:Hcase'; try reflexivity; [].
  exfalso. apply (eq_subrelation (R := equiv)) in Hcase'; autoclass; [].
  rewrite <- (find_edge_iso_Some (inverse iso)) in Hcase'. cbn -[equiv] in Hcase'.
  rewrite 2 Bijection.retraction_section in Hcase'. rewrite Hcase in Hcase'. tauto.
Qed.
