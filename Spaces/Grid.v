(**************************************************************************)
(**   Mechanised Framework for Local Interactions & Distributed Algorithms  
      T. Balabonski, P. Courtieu, R. Pelle, L. Rieg, X. Urbain              
                                                                            
      PACTOLE project                                                       
                                                                            
      This file is distributed under the terms of the CeCILL-C licence      
                                                                          *)
(**************************************************************************)


Require Import Psatz SetoidDec ZArith Rbase.
Require Import Pactole.Util.Coqlib.
Require Import Pactole.Util.Bijection.
Require Export Pactole.Spaces.Graph.


Typeclasses eauto := (bfs).
Remove Hints eq_setoid : typeclass_instances.
Open Scope Z_scope.


(** Four cardinal points + self loop *)
Inductive direction :=
  North | South | East | West | Self.

#[export] Instance direction_Setoid : Setoid direction := eq_setoid _.

#[export] Instance direction_EqDec : EqDec direction_Setoid.
Proof using .
intros x y. simpl. change (x = y -> False) with (x <> y). decide equality.
Defined.

(** Vertices are points in Z² and edges are defined by their origin point plus a direction. *)
Notation node := (Z*Z)%type.
Notation edge := (Z*Z*direction)%type.

#[export] Instance node_Setoid : Setoid node := eq_setoid _.
#[export] Instance node_EqDec : EqDec node_Setoid.
Proof using .
intros x y.
destruct (fst x =?= fst y).
+ destruct (snd x =?= snd y).
  - left. abstract (destruct x, y; cbn in *; subst; reflexivity).
  - right. abstract (destruct x, y; injection; auto).
+ right. abstract (destruct x, y; injection; auto).
Defined.

#[export] Instance edge_Setoid : Setoid edge := eq_setoid _.
#[export] Instance edge_EqDec : EqDec edge_Setoid.
Proof using .
intros x y.
destruct (fst x =?= fst y).
+ destruct (snd x =?= snd y).
  - left. abstract (destruct x, y; cbn in *; subst; reflexivity).
  - right. abstract (destruct x, y; injection; auto).
+ right. abstract (destruct x, y; injection; auto).
Defined.

Definition edge_tgt (e : edge) : node :=
  match snd e with
    | North => (fst (fst e), snd (fst e) + 1)
    | South => (fst (fst e), snd (fst e) - 1)
    | East  => (fst (fst e) + 1, snd (fst e))
    | West  => (fst (fst e) - 1, snd (fst e))
    | Self  => fst e
  end.
Arguments edge_tgt !e.

(** The Z² grid is a graph. *)
#[export] Instance Z2 : Graph node edge.
simple refine {| V_EqDec := node_EqDec;
                 E_EqDec := edge_EqDec;
                 src := fst;
                 tgt := edge_tgt; |}.
Proof using .
* (* find_edge *)
  exact (fun x y : node => if equiv_dec (EqDec := node_EqDec) y x then Some (x, Self) else
                           if y =?= (fst x + 1, snd x) then Some (x, East) else
                           if y =?= (fst x, snd x + 1) then Some (x, North) else
                           if y =?= (fst x - 1, snd x) then Some (x, West) else
                           if y =?= (fst x, snd x - 1) then Some (x, South) else None).
* (* find_edge_Some *)
  abstract (intros [p d] x y; cbn-[equiv];
            assert (forall x : Z, x <> x + 1) by lia;
            assert (forall x : Z, x <> x - 1) by lia;
            assert (forall x : Z, x + 1 <> x - 1) by lia;
            repeat destruct_match; destruct x, y, p, d; cbn in *; intuition congruence).
Defined.

(** **  Change of frame of reference in Z²  **)

Require Pactole.Util.Bijection.
Require Import Pactole.Core.State.
Require Import Pactole.Core.Formalism.

#[export] Instance Loc : Location := {| location := node |}.
(** angle: anglei represents the possible angles for a rotation of reflection:
    - for a rotation: angle i/2 * pi;
    - for a reflection: angle i/4 * pi *)
Inductive angle := angle0 | angle1 | angle2 | angle3.
#[export] Instance angle_Setoid : Setoid angle := eq_setoid _.
#[export] Instance angle_EqDec : EqDec angle_Setoid.
Proof using .
intros x y. simpl.
change (x = y -> False) with (x <> y).
decide equality.
Defined.

Definition opp_angle (r : angle) :=
  match r with
    | angle0 => angle0
    | angle1 => angle3
    | angle2 => angle2
    | angle3 => angle1
  end.

(** ***  Some bijections used in changes of frame of reference  **)

(** Translation *)
Definition translation (v : Z*Z) : Bijection.bijection (Z*Z).
 refine {| Bijection.section := fun x => (fst x + fst v, snd x + snd v);
           Bijection.retraction := fun x => (fst x - fst v, snd x - snd v) |}.
Proof using .
intros x y. simpl.
abstract (split; intro; subst; destruct x || destruct y; f_equal; simpl; lia).
Defined.

#[export] Instance translation_compat : Proper (equiv ==> equiv) translation.
Proof. intros ? ? Heq x. Fail timeout 2 now rewrite Heq. cbn in Heq. now subst. Qed.

(** Rotation *)
Definition mk_rotation r : Z*Z -> Z*Z :=
  match r with
    | angle0 => fun x => x
    | angle1 => fun x => (- snd x, fst x)
    | angle2 => fun x => (- fst x, - snd x)
    | angle3 => fun x => (snd x, - fst x)
  end.

Lemma mk_rotation_compat : forall r, Proper (equiv ==> equiv) (mk_rotation r).
Proof. intros [] [] [] Heq; cbn; congruence. Qed.

Definition rotation (r : angle) : Bijection.bijection (Z*Z).
  refine {| Bijection.section := mk_rotation r;
            Bijection.retraction := mk_rotation (opp_angle r);
            Bijection.section_compat := mk_rotation_compat r |}.
Proof using .
intros x y. cbn.
abstract (split; intro; subst; destruct r; cbn; destruct x || destruct y; cbn; f_equal; lia).
Defined.

(* #[export] Instance rotation_compat : Proper (equiv ==> equiv) rotation := reflexive_proper _. *)

(** Reflection *)
Definition mk_reflection r : Z*Z -> Z*Z :=
  match r with
    | angle0 => fun x => (fst x, - snd x)
    | angle1 => fun x => (snd x, fst x)
    | angle2 => fun x => (- fst x, snd x)
    | angle3 => fun x => (- snd x, - fst x)
  end.

Lemma mk_reflection_compat : forall r, Proper (equiv ==> equiv) (mk_reflection r).
Proof. intros [] [] [] Heq; cbn; congruence. Qed.

Definition reflection (r : angle) : Bijection.bijection (Z*Z).
 refine {| Bijection.section := mk_reflection r;
           Bijection.retraction := mk_reflection r;
            Bijection.section_compat := mk_reflection_compat r |}.
Proof using .
intros x y. cbn.
abstract (split; intro; subst; destruct r; cbn; destruct x || destruct y; cbn; f_equal; lia).
Defined.

(* #[export] Instance reflection_compat : Proper (equiv ==> equiv) reflection := reflexive_proper _. *)

(** ***  Change of frame of reference  **)

(** Translation  **)
#[export] Instance FCTranslation : frame_choice (Z*Z) := {|
  frame_choice_bijection := translation;
  frame_choice_Setoid := _;
  frame_choice_bijection_compat := _ |}.

(** Rigid Motion  **)
#[export] Instance rigid_motion_compat :
  Proper (equiv ==> equiv) (fun rm => rotation (snd rm) ∘ translation (fst rm)).
Proof using . intros ? ? [Hv Ha]; do 2 f_equiv; assumption. Qed.

#[export] Instance FCRigidMotion : frame_choice (Z*Z*angle) := {|
  frame_choice_bijection := fun rm => rotation (snd rm) ∘ translation (fst rm);
  frame_choice_Setoid := prod_Setoid node_Setoid angle_Setoid;
  frame_choice_bijection_compat := rigid_motion_compat |}.

(** Similarities *)
#[export] Instance FCSimilarity : frame_choice (bool*(Z*Z)*angle)%type.
simple refine {|
  frame_choice_bijection := fun '(b, v, a) =>
   rotation a ∘ translation v ∘ (if b : bool then reflection angle0 else @Bijection.id node _);
  frame_choice_Setoid := prod_Setoid (prod_Setoid bool_Setoid node_Setoid) angle_Setoid |}.
Proof.
abstract (intros [[b1 v1] a1] [[b2 v2] a2] [[Hb Hv] Ha]; cbn -[equiv] in *;
          repeat f_equiv; trivial; []; now rewrite Hb).
Defined.
