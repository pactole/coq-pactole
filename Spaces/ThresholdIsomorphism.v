Require Import Utf8 SetoidDec Rbase Rbasic_fun Psatz.
From Pactole Require Import Util.Coqlib Util.Bijection Util.Ratio.
From Pactole Require Import Spaces.Graph Spaces.Isomorphism.
Set Implicit Arguments.

Section ThresholdIsomorphism.

Context {V E : Type}.
Context {G : ThresholdGraph V E}.

Record threshold_isomorphism := {
  iso_VE :> isomorphism G;
  iso_T : bijection strict_ratio;
  iso_threshold : ∀ e : E, iso_T (threshold e) == threshold (iso_E iso_VE e) :> strict_ratio;
  iso_incr : ∀ a b : strict_ratio, (a < b)%R -> (iso_T a < iso_T b)%R }.

Global Instance threshold_isomorphism_Setoid : Setoid threshold_isomorphism.
Proof using .
  simple refine {|
    equiv := λ iso1 iso2, iso1.(iso_VE) == iso2.(iso_VE)
                       /\ iso1.(iso_T) == iso2.(iso_T) |}; autoclass. split.
  + intro f. now repeat split.
  + intros f g Hfg; destruct Hfg as [HVE HT]. split; now symmetry.
  + intros f g h Hfg Hgh. destruct Hfg as [? ?], Hgh as [? ?].
    split; etransitivity; eauto.
Defined.

Instance iso_T_compat : Proper (equiv ==> equiv) iso_T.
Proof using . intros ? ? Heq ?. now apply Heq. Qed.

Definition id : threshold_isomorphism.
Proof using .
  refine {| iso_VE := id;
            iso_T := Bijection.id |}.
  + now intros.
  + now intros.
Defined.

Definition comp (f g : threshold_isomorphism) : threshold_isomorphism.
Proof using .
  refine {|
      iso_VE := compose f.(iso_VE) g.(iso_VE);
      iso_T := compose f.(iso_T) g.(iso_T) |}.
  + intro. simpl. now rewrite 2 iso_threshold.
  + intros. simpl. now do 2 apply iso_incr.
Defined.

Global Instance TIsoComposition : Composition threshold_isomorphism.
Proof using .
  refine {| compose := comp |}.
  intros f1 f2 Hf g1 g2 Hg. split. all: apply compose_compat.
  3,4: f_equiv. 1,3: apply Hf. all: apply Hg.
Defined.

Lemma compose_assoc : ∀ f g h, f ∘ (g ∘ h) == (f ∘ g) ∘ h.
Proof using . intros f g h; repeat split; simpl; reflexivity. Qed.

Definition inv (tiso : threshold_isomorphism) : threshold_isomorphism.
Proof using .
  refine {| iso_VE := inverse tiso.(iso_VE);
            iso_T := inverse tiso.(iso_T) |}.
  + intros e. cbn-[equiv]. rewrite <- Inversion, iso_threshold,
    section_retraction. reflexivity.
  + intros a b Hab. apply Rnot_le_lt. intros [Hd | Hd].
    all: apply (Rlt_not_le b a Hab). left. erewrite <- (section_retraction _ b),
    <- (section_retraction _ a). apply iso_incr, Hd. right.
    apply proj_ratio_compat, proj_strict_ratio_compat,
    (injective (iso_T tiso ⁻¹)), proj_strict_ratio_inj, proj_ratio_inj, Hd.
Defined.

Global Instance TIsoInverse : Inverse threshold_isomorphism.
Proof using .
  refine {| inverse := inv |}.
  intros f g [? ?]. split. all: apply inverse_compat. all: assumption.
Defined.

Lemma id_inv : id⁻¹ == id.
Proof using . split. apply id_inv. setoid_rewrite Bijection.id_inv. reflexivity. Qed.

Lemma id_comp_l : ∀ tiso : threshold_isomorphism, id ∘ tiso == tiso.
Proof using. intros. split. apply id_comp_l. setoid_rewrite Bijection.id_comp_l. reflexivity. Qed.

Lemma id_comp_r : ∀ tiso : threshold_isomorphism, tiso ∘ id == tiso.
Proof using. intros. split. apply id_comp_r. setoid_rewrite Bijection.id_comp_r. reflexivity. Qed.

Lemma inv_inv : ∀ tiso : threshold_isomorphism, tiso⁻¹⁻¹ == tiso.
Proof using . intros. split. apply inv_inv. setoid_rewrite Bijection.inv_inv. reflexivity. Qed.

Lemma compose_inverse_l : ∀ tiso : threshold_isomorphism, tiso ⁻¹ ∘ tiso == id.
Proof using . intro. split. apply compose_inverse_l. setoid_rewrite Bijection.compose_inverse_l. reflexivity. Qed.

Lemma compose_inverse_r : ∀ tiso : threshold_isomorphism, tiso ∘ (tiso ⁻¹) == id.
Proof using . intro. split. apply compose_inverse_r. setoid_rewrite Bijection.compose_inverse_r. reflexivity. Qed.

Lemma inverse_compose : ∀ f g : threshold_isomorphism, (f ∘ g) ⁻¹ == (g ⁻¹) ∘ (f ⁻¹).
Proof using . intros f g. split. apply inverse_compose. setoid_rewrite Bijection.inverse_compose. reflexivity. Qed.

End ThresholdIsomorphism.

Arguments threshold_isomorphism {V} {E} G.
