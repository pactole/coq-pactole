(**************************************************************************)
(**   Mechanised Framework for Local Interactions & Distributed Algorithms  
      T. Balabonski, P. Courtieu, R. Pelle, L. Rieg, X. Urbain              
                                                                            
      PACTOLE project                                                       
                                                                            
      This file is distributed under the terms of the CeCILL-C licence      
                                                                          *)
(**************************************************************************)

Require Import Utf8 Lia Psatz SetoidDec Rbase.
From Pactole Require Import Util.Coqlib Util.Bijection Spaces.Graph
  Spaces.Isomorphism Models.NoByzantine Util.Fin.

(** ** A ring  **)

Inductive direction := Forward | Backward | SelfLoop.

Global Instance direction_Setoid : Setoid direction := eq_setoid direction.

Lemma direction_eq_dec_subproof : ∀ d1 d2 : direction, {d1 = d2} + {d1 <> d2}.
Proof using . decide equality. Defined.

Global Instance direction_EqDec : EqDec direction_Setoid
  := direction_eq_dec_subproof.

(* Returns the nat to give as parameter to
   addm to translate in the direction dir *)
Definition dir2nat {n : nat} {ltc_2_n : 2 <c n} (d : direction) : nat :=
  match d with
    | SelfLoop => 0
    | Forward => 1
    | Backward => Nat.pred n
  end.

(* ''inverse'' of dir2nat *)
Definition nat2Odir {n : nat} {ltc_2_n : 2 <c n} (m : nat) : option direction :=
  match m with
    | 0 => Some SelfLoop
    | 1 => Some Forward
    | _ => if m =? Nat.pred n then Some Backward else None
  end.

Global Instance ltc_1_n {n : nat} {ltc_2_n : 2 <c n} : 1 <c n
  := (lt_s_u 1 ltac:(auto)).

Definition ring_edge {n : nat} {ltc_2_n : 2 <c n} := (fin n * direction)%type.

Section Ring.

Context {n : nat} {ltc_2_n : 2 <c n}.

Global Instance ring_edge_Setoid : Setoid ring_edge := eq_setoid ring_edge.

Lemma ring_edge_dec : ∀ e1 e2 : ring_edge, {e1 = e2} + {e1 ≠ e2}.
Proof using . apply pair_dec. apply fin_EqDec. apply direction_EqDec. Qed.

Global Instance ring_edge_EqDec : EqDec ring_edge_Setoid := ring_edge_dec.

Definition dir2nat_compat : Proper (equiv ==> equiv) dir2nat := _.

Lemma dir20 : dir2nat SelfLoop = 0.
Proof using . reflexivity. Qed.

Lemma dir21 : dir2nat Forward = 1.
Proof using . reflexivity. Qed.

Lemma dir2pred_n : dir2nat Backward = Nat.pred n.
Proof using . reflexivity. Qed.

Lemma dir2nat_lt : ∀ d : direction, dir2nat d < n.
Proof using .
  intros. destruct d. all: cbn. 2: apply lt_pred_u.
  all: apply lt_s_u. all: auto.
Qed.

Lemma dir2natI : Util.Preliminary.injective equiv equiv dir2nat.
Proof using .
  intros d1 d2 H'. destruct d1, d2. all: inversion H' as [H].
  all: try reflexivity. all: exfalso. 1,4: symmetry in H.
  all: eapply neq_pred_u_s. 2,4,6,8: exact H. all: auto.
Qed.

Definition nat2Odir_compat : Proper (equiv ==> equiv) nat2Odir := _.

Lemma nat2Odir_Some : ∀ (d : direction) (m : nat),
  nat2Odir m = Some d <-> dir2nat d = m.
Proof using .
  unfold dir2nat, nat2Odir. intros d m. split; intros H.
  all: destruct d, m as [|p]. all: try inversion H. all: try reflexivity.
  4:{ exfalso. eapply neq_pred_u_s. 2: exact H. auto. }
  4: rewrite Nat.eqb_refl. all: destruct p. all: try inversion H.
  all: try reflexivity. 4:{ exfalso. eapply neq_pred_u_s. 2: exact H.
  auto. } all: destruct (S (S p) =? Nat.pred n) eqn:Hd.
  all: try inversion H. symmetry. apply Nat.eqb_eq, Hd.
Qed.

Lemma nat2Odir_None : ∀ m : nat,
  nat2Odir m = None <-> (∀ d : direction, dir2nat d ≠ m).
Proof using .
  intros. split; intros H.
  - intros d Habs. apply nat2Odir_Some in Habs. rewrite H in Habs.
    inversion Habs.
  - destruct (nat2Odir m) eqn:Hd. 2: reflexivity. exfalso.
    apply (H d), nat2Odir_Some, Hd.
Qed.

Lemma nat2Odir_pick : ∀ m : nat,
  pick_spec (λ d : direction, dir2nat d = m) (nat2Odir m).
Proof using .
  intros. apply pick_Some_None. intros d.
  all: rewrite opt_Setoid_eq by reflexivity.
  apply nat2Odir_Some. apply nat2Odir_None.
Qed.

Definition nat2dir (m : nat) (H : ∃ d, dir2nat d = m) : direction.
Proof using .
  destruct (nat2Odir_pick m) as [d Hd | Hd]. exact d.
  abstract (exfalso; destruct H as [d H]; apply (Hd d), H).
Defined.

Lemma dir2natK : ∀ (d1 : direction) (H : ∃ d2, dir2nat d2 = dir2nat d1),
  nat2dir (dir2nat d1) H = d1.
Proof using .
  intros. unfold nat2dir. destruct (nat2Odir_pick (dir2nat d1)) as [m Hd | Hd].
  eapply dir2natI, Hd. contradiction (Hd d1). reflexivity.
Qed.

Lemma nat2dirK :
  ∀ (m : nat) (H : ∃ d, dir2nat d = m), dir2nat (nat2dir m H) = m.
Proof using .
  intros. unfold nat2dir. destruct (nat2Odir_pick m) as [g Hd | Hd].
  exact Hd. exfalso. destruct H as [d H]. apply (Hd d). exact H.
Qed.

(* Returns the fin n to give as parameter to
   addf to translate in the direction dir *)
Definition dir2nod (d : direction) : fin n := Fin (dir2nat_lt d).

(* ''inverse'' of dir2nat *)
Definition nod2Odir (v : fin n) : option direction := nat2Odir v.

Definition dir2nod_compat : Proper (equiv ==> equiv) dir2nod := _.

Definition nod2Odir_compat : Proper (equiv ==> equiv) nod2Odir := _.

Lemma dir2nod2nat : ∀ d : direction, dir2nod d = dir2nat d :> nat.
Proof using . reflexivity. Qed.

Lemma dir2nodE : ∀ d : direction, dir2nod d = mod2fin (dir2nat d).
Proof using .
  intros. apply fin2natI. rewrite dir2nod2nat.
  symmetry. apply mod2fin_small, dir2nat_lt.
Qed.

Lemma nod2OdirE : ∀ v : fin n, nod2Odir v = nat2Odir v.
Proof using . reflexivity. Qed.

Lemma dir2fin0 : dir2nod SelfLoop = fin0.
Proof using . apply fin2natI. rewrite dir2nod2nat. apply dir20. Qed.

Lemma dir2fin1 : dir2nod Forward = fin1.
Proof using .
  apply fin2natI. symmetry. rewrite dir2nod2nat, dir21. apply fin12nat.
Qed.

Lemma dir2max : dir2nod Backward = fin_max.
Proof using . apply fin2natI. rewrite dir2nod2nat. apply dir2pred_n. Qed.

Lemma dir2nodI : Util.Preliminary.injective equiv equiv dir2nod.
Proof using .
  intros d1 d2 H. apply dir2natI. rewrite <-2 dir2nod2nat, H. reflexivity.
Qed.

Lemma nod2Odir_Some : ∀ (d : direction) (v : fin n),
  nod2Odir v = Some d <-> dir2nod d = v.
Proof using .
  intros. rewrite nod2OdirE. split. all: intros H. apply fin2natI,
  nat2Odir_Some, H. apply nat2Odir_Some. rewrite <- dir2nod2nat, H. reflexivity.
Qed.

Lemma nod2Odir_None : ∀ v : fin n,
  nod2Odir v = None <-> (∀ d : direction, dir2nod d ≠ v).
Proof using .
  intros. rewrite nod2OdirE. split.
  - intros H d Habs. eapply (proj1 (nat2Odir_None _)). apply H.
    rewrite <- Habs. symmetry. apply dir2nod2nat.
  - intros H. apply nat2Odir_None. intros d Habs. apply (H d), fin2natI, Habs.
Qed.

Lemma nod2Odir_pick : ∀ v : fin n,
  pick_spec (λ d : direction, dir2nod d = v) (nod2Odir v).
Proof using .
  intros. apply pick_Some_None. intros d.
  all: rewrite opt_Setoid_eq by reflexivity.
  apply nod2Odir_Some. apply nod2Odir_None.
Qed.

Definition nod2dir (v : fin n) (H : ∃ d, dir2nod d = v) : direction.
Proof using .
  apply (nat2dir v). abstract (destruct H as [d H]; exists d;
  rewrite <- H; symmetry; apply dir2nod2nat).
Defined.

Lemma dir2nodK : ∀ (d1 : direction) (H : ∃ d2, dir2nod d2 = dir2nod d1),
  nod2dir (dir2nod d1) H = d1.
Proof using . intros. erewrite <- (dir2natK d1). reflexivity. Qed.

Lemma nod2dirK :
  ∀ (v : fin n) (H : ∃ d, dir2nod d = v), dir2nod (nod2dir v H) = v.
Proof using .
  intros. apply fin2natI. erewrite <- (nat2dirK v). reflexivity.
Qed.

(** From a node, if we move in one direction, get get to another node. *)
Definition move_along (v : fin n) (d : direction)
  :=  addf v (dir2nod d).

Definition move_along_compat :
  Proper (equiv ==> equiv ==> equiv) move_along := _.

Lemma move_alongE : ∀ (v : fin n) (d : direction),
  move_along v d = addf v (dir2nod d).
Proof using . intros. reflexivity. Qed.

Definition move_along2nat : ∀ (v : fin n) (d : direction),
  fin2nat (move_along v d) = (v + dir2nat d) mod n.
Proof using . intros. apply addf2nat. Qed.

Lemma move_alongI_ : ∀ d : direction,
  Util.Preliminary.injective equiv equiv (λ v, move_along v d).
Proof using . intros. apply addIf. Qed.

Lemma move_along_I : ∀ v : fin n,
  Util.Preliminary.injective equiv equiv (move_along v).
Proof using . intros v d1 d2 H. eapply dir2nodI, addfI, H. Qed.

Lemma move_along_0 : ∀ v : fin n, move_along v SelfLoop = v.
Proof using . apply addf0. Qed.

Lemma move_along0_ : ∀ d : direction, move_along fin0 d = dir2nod d.
Proof using . intros. rewrite move_alongE. apply add0f. Qed.

Lemma move_alongAC : ∀ (v : fin n) (d1 d2 : direction),
  move_along (move_along v d2) d1 = move_along (move_along v d1) d2.
Proof using . intros. rewrite 4 move_alongE. apply addfAC. Qed.

Lemma addm_move_along : ∀ (v : fin n) (m : nat) (d : direction),
  addm (move_along v d) m = move_along (addm v m) d.
Proof using . intros. rewrite 2 move_alongE, 2 addm_addf. apply addfAC. Qed.

Lemma subm_move_along : ∀ (v : fin n) (m : nat) (d : direction),
  subm (move_along v d) m = move_along (subm v m) d.
Proof using . intros. rewrite 2 move_alongE, 2 submE. apply addfAC. Qed.

Lemma addf_move_along : ∀ (v1 v2 : fin n) (d : direction),
  addf (move_along v1 d) v2 = move_along (addf v1 v2) d.
Proof using . intros. rewrite 2 move_alongE. apply addfAC. Qed.

Lemma subf_move_along : ∀ (v1 v2 : fin n) (d : direction),
  subf v2 (move_along v1 d) = subf (subf v2 v1) (dir2nod d).
Proof using . intros. rewrite move_alongE. apply subf_addf. Qed.

Lemma subf_move_along' : ∀ (v1 v2 : fin n) (d : direction),
  subf (move_along v1 d) v2 = move_along (subf v1 v2) d.
Proof using . intros. rewrite 2 move_alongE, 2 subfE. apply addfAC. Qed.

(* returns the ''opposite'' direction *)
Definition swap_direction (d : direction) :=
  match d with
    | Forward => Backward
    | Backward => Forward
    | SelfLoop => SelfLoop
  end.

Definition swap_direction_compat :
  Proper (equiv ==> equiv) swap_direction := _.

Lemma swap_directionI :
  Util.Preliminary.injective equiv equiv swap_direction.
Proof using .
  unfold swap_direction. intros d1 d2 H. destruct d1, d2.
  all: try inversion H. all: reflexivity.
Qed.

Lemma swap_directionK : ∀ d : direction,
  swap_direction (swap_direction d) == d.
Proof using .
  intros d. unfold swap_direction. destruct d. all: reflexivity.
Qed.

Definition swbd : bijection direction
  := cancel_bijection swap_direction _ swap_directionI
                      swap_directionK swap_directionK.

Lemma swbdE : swbd = swap_direction :> (direction -> direction).
Proof using . reflexivity. Qed.

Lemma swbdVE : swbd⁻¹ = swap_direction :> (direction -> direction).
Proof using . reflexivity. Qed.

Lemma swbdV : swbd⁻¹ == swbd.
Proof using . intros dir. rewrite swbdE, swbdVE. reflexivity. Qed.

Lemma swbdK : swbd ∘ swbd == Bijection.id.
Proof using . intros dir. rewrite compE, swbdE. apply swap_directionK. Qed.

Lemma dir2nod_swap_direction : ∀ d : direction,
  dir2nod (swap_direction d) = oppf (dir2nod d).
Proof using .
  intros. destruct d. all: cbn. all: symmetry. 1,2: rewrite dir2max, dir2fin1.
  apply oppf1. apply oppf_max. rewrite dir2fin0. apply oppf0.
Qed.

Lemma move_along_swap_direction : ∀ (v : fin n) (d : direction),
  move_along v (swap_direction d) = subf v (dir2nod d).
Proof using .
  intros. rewrite move_alongE, dir2nod_swap_direction. symmetry. apply subfE.
Qed.

Lemma move_alongK' : ∀ (v : fin n) (d : direction),
  subf (move_along v d) (dir2nod d) = v.
Proof using . intros. rewrite move_alongE. apply addfVKV. Qed.

Lemma submVKV' : ∀ (v : fin n) (d : direction),
  move_along (subf v (dir2nod d)) d = v.
Proof using . intros. rewrite move_alongE. apply subfVKV. Qed.

Lemma move_alongK : ∀ (v : fin n) (d : direction),
  move_along (move_along v d) (swap_direction d) = v.
Proof using .
  intros. rewrite move_along_swap_direction. apply move_alongK'.
Qed.

Lemma move_along_dir2nod_subf : ∀ (v1 v2 : fin n) (d : direction),
  dir2nod d = subf v2 v1 <-> move_along v1 d = v2.
Proof using .
  intros. rewrite move_alongE. split; intros H. rewrite H. apply subfVK.
  rewrite <- H. symmetry. apply addfKV.
Qed.

Lemma symf_move_along : ∀ (c v : fin n) (d : direction),
  symf c (move_along v d) = move_along (symf c v) (swap_direction d).
Proof using .
  intros. rewrite move_along_swap_direction, move_alongE. apply symf_addf.
Qed.

Lemma move_along_symf : ∀ (v1 v2 : fin n) (d : direction),
  move_along (symf v1 v2) d = symf v1 (move_along v2 (swap_direction d)).
Proof using .
  intros. rewrite symf_move_along, swap_directionK. reflexivity.
Qed.

Lemma nod2Odir_subf_Some :
  ∀ (v1 v2 : fin n) (d : direction), nod2Odir (subf v1 v2) = Some d
  <-> nod2Odir (subf v2 v1) = Some (swap_direction d).
Proof using .
  intros. rewrite 2 nod2Odir_Some, dir2nod_swap_direction, <- (oppf_subf v2).
  symmetry. apply injective_eq_iff, oppfI.
Qed.

Lemma nod2Odir_subf_None : ∀ v1 v2 : fin n,
  nod2Odir (subf v2 v1) = None <-> ∀ d : direction, move_along v1 d ≠ v2.
Proof using .
  intros. rewrite nod2Odir_None. split. all: intros H d Heq. all: apply (H d).
  all: apply move_along_dir2nod_subf, Heq.
Qed.

Definition find_edge_subterm (v1 v2 : fin n) : option ring_edge
  := match (nod2Odir (subf v2 v1)) with
      | Some d1 => Some (v1, d1)
      | None => None
     end.

Lemma find_edge_subproof :
  ∀ (e : ring_edge) (v1 v2 : fin n), find_edge_subterm v1 v2 == Some e
  <-> v1 == fst e /\ v2 == move_along (fst e) (snd e).
Proof using .
  intros. rewrite opt_Setoid_eq by reflexivity. unfold find_edge_subterm.
  cbn. rewrite (eq_sym_iff v2), <- move_along_dir2nod_subf.
   destruct (nod2Odir_pick (subf v2 v1)) as [d Hd|Hd].
  - rewrite (injective_eq_iff Some_eq_inj), <- (pair_eqE (v1, d)). cbn[fst snd].
    split. all: intros [H1 H2]. all: subst. all: split; [reflexivity |].
    apply Hd. apply dir2nodI. rewrite H2. apply Hd.
  - split. intros H. inversion H. intros [? H]. subst. exfalso. eapply Hd, H.
Qed.

Global Instance Ring : Graph (fin n) ring_edge := {|
  V_EqDec := @fin_EqDec n;
  E_EqDec := ring_edge_EqDec;
  src := fst;
  tgt := λ e, move_along (fst e) (snd e);
  find_edge := find_edge_subterm;
  find_edge_Some := find_edge_subproof |}.

Global Instance Ring_isomorphism_Setoid :
  Setoid (isomorphism Ring) := isomorphism_Setoid.

Global Instance Ring_isomorphism_Inverse : Inverse (isomorphism Ring)
  := @IsoInverse _ _ Ring.

Global Instance Ring_isomorphism_Composition
  : Composition (isomorphism Ring) := @IsoComposition _ _ Ring.

(** **  Ring operations **)

Lemma srcE : ∀ e : ring_edge, src e = fst e.
Proof using . reflexivity. Qed.

Lemma tgtE : ∀ e : ring_edge, tgt e = move_along (fst e) (snd e).
Proof using . reflexivity. Qed.

Lemma find_edgeE : ∀ v1 v2 : fin n,
  find_edge v1 v2 = match (nod2Odir (subf v2 v1)) with
                        | Some d1 => Some (v1, d1)
                        | None => None
                       end.
Proof using . reflexivity. Qed.

Lemma find_edge_move_along : ∀ (v : fin n) (d : direction),
  find_edge v (move_along v d) = Some (v, d).
Proof using .
  intros. rewrite <- opt_Setoid_eq. setoid_rewrite find_edge_Some.
  split. all: reflexivity.
Qed.

Definition trans (v : fin n) : isomorphism Ring.
Proof using .
  refine {|
    iso_V := asbf v⁻¹;
    iso_E := prod_eq_bij (asbf v⁻¹) Bijection.id |}.
  abstract (intros; split; [reflexivity | apply subf_move_along']).
Defined.
Global Opaque trans.

Definition trans_compat : Proper (equiv ==> equiv) trans := _.

Lemma transvE : ∀ v : fin n, trans v = asbf v⁻¹ :> bijection (fin n).
Proof using . reflexivity. Qed.

Lemma transvVE : ∀ v : fin n, trans v⁻¹ == asbf v :> bijection (fin n).
Proof using . intros. rewrite <- (Bijection.inv_inv (asbf v)). reflexivity. Qed.

Lemma transeE :
  ∀ v : fin n, iso_E (trans v) = prod_eq_bij (asbf v⁻¹) Bijection.id.
Proof using . reflexivity. Qed.

Lemma transeVE :
  ∀ v : fin n, iso_E (trans v⁻¹) == prod_eq_bij (asbf v) Bijection.id.
Proof using .
  intros. rewrite <- (Bijection.inv_inv (asbf v)),
  <- Bijection.id_inv, <- prod_eq_bij_inv. reflexivity.
Qed.

Lemma trans0 : trans fin0 == id.
Proof using .
  split. rewrite transvE. 2: rewrite transeE. all: rewrite asbf0,
  Bijection.id_inv. reflexivity. apply prod_eq_bij_id.
Qed.

Lemma transI : ∀ v1 v2 : fin n, trans v1 == trans v2 → v1 = v2.
Proof using . intros * H. apply subfI with (f1:=v1), H. Qed.

Lemma transVI : ∀ v1 v2 : fin n, (trans v1)⁻¹ == (trans v2)⁻¹ → v1 = v2.
Proof using . intros * H. apply addfI with (f1:=v1), H. Qed.

Lemma transAC :
  ∀ v1 v2 : fin n, trans v2 ∘ (trans v1) == trans v1 ∘ (trans v2).
Proof using .
  intros. split. rewrite 2 (compvE (trans _)), 2 transvE. apply asbfVAC.
  rewrite 2 (compeE (trans _)), 2 transeE. setoid_rewrite prod_eq_bij_comp.
  rewrite (asbfVAC v2). reflexivity.
Qed.

Lemma transVAC : ∀ v1 v2 : fin n,
  (trans v2)⁻¹ ∘ (trans v1)⁻¹ == (trans v1)⁻¹ ∘ (trans v2)⁻¹.
Proof using .
  intros. split. rewrite 2 (compvE (trans _⁻¹)), 2 transvVE. apply asbfAC.
  rewrite 2 (compeE (trans _⁻¹)), 2 transeVE. setoid_rewrite prod_eq_bij_comp.
  rewrite (asbfAC v2). reflexivity.
Qed.

Lemma transCV : ∀ v1 v2 : fin n,
  trans v1 ∘ (trans v2)⁻¹ == (trans v2)⁻¹ ∘ (trans v1).
Proof using .
  intros. split. rewrite (compvE (trans _)), (compvE (trans _⁻¹)), transvE,
  transvVE. symmetry. apply asbfCV. rewrite (compeE (trans _)),
  (compeE (trans _⁻¹)), transeE, transeVE. setoid_rewrite prod_eq_bij_comp.
  rewrite (asbfCV v2). reflexivity.
Qed.

Lemma transA :
  ∀ v1 v2 : fin n, trans (trans v1 v2) == (trans v1⁻¹) ∘ (trans v2).
Proof using .
  intros. split. rewrite (compvE (trans _⁻¹)), 3 transvE, transvVE.
  apply asbfVAV. rewrite (compeE (trans _⁻¹)), transvE, 2 transeE, transeVE.
  setoid_rewrite prod_eq_bij_comp. rewrite asbfVAV, Bijection.id_comp_l.
  reflexivity.
Qed.

Lemma transAV :
  ∀ v1 v2 : fin n, trans ((trans v1)⁻¹ v2) == (trans v1) ∘ (trans v2).
Proof using .
  intros. split. rewrite (compvE (trans _)), 3 transvE, transvVE.
  apply asbfVA. rewrite (compeE (trans _)), transvVE, 3 transeE.
  setoid_rewrite prod_eq_bij_comp. rewrite asbfVA, Bijection.id_comp_l.
  reflexivity.
Qed.

Lemma transVA :
  ∀ v1 v2 : fin n, trans (trans v1 v2)⁻¹ == (trans v1) ∘ (trans v2⁻¹).
Proof using .
  intros. split. rewrite (compvE (trans _)), transvE, 2 transvVE.
  apply asbfAV. rewrite (compeE (trans _)), transvE, transeE, 2 transeVE.
  setoid_rewrite prod_eq_bij_comp. rewrite asbfAV, Bijection.id_comp_l.
  reflexivity.
Qed.

Lemma transVAV :
  ∀ v1 v2 : fin n, trans ((trans v1)⁻¹ v2)⁻¹ == (trans v1⁻¹) ∘ (trans v2⁻¹).
Proof using .
  intros. split. rewrite (compvE (trans _⁻¹)), 3 transvVE.
  apply asbfA. rewrite (compeE (trans _⁻¹)), transvVE, 3 transeVE.
  setoid_rewrite prod_eq_bij_comp.
  rewrite asbfA, Bijection.id_comp_l. reflexivity.
Qed.

Lemma move_along_trans : ∀ (v1 v2 : fin n) (d : direction),
  move_along (trans v1 v2) d == trans v1 (move_along v2 d).
Proof using .
  intros. rewrite transvE, (asbfVE v1). symmetry. apply subf_move_along'.
Qed.

Lemma trans_inj : Preliminary.injective equiv equiv trans.
Proof using .
  intros v1 v2 [H _]. eapply addfI. rewrite (addfC _ v2).
  apply inverse_compat in H. rewrite (transvVE v1), (transvVE v2) in H.
  specialize (H v2). rewrite (asbfE v1), (asbfE v2) in H. apply H.
Qed.

Lemma transs : ∀ v : fin n, trans v v = fin0.
Proof using . intros. rewrite transvE. apply asbfVf. Qed.

Definition sym (v : fin n) : isomorphism Ring.
Proof using .
  refine {|
    iso_V := sybf v;
    iso_E := prod_eq_bij (sybf v) swbd |}.
  (* iso_morphism *)
  abstract (intros; split; [reflexivity | apply symf_move_along]).
Defined.
Global Opaque sym.

Definition sym_compat : Proper (equiv ==> equiv) sym := _.

Lemma symvE : ∀ v : fin n, sym v = sybf v :> bijection (fin n).
Proof using . reflexivity. Qed.

Lemma symeE : ∀ v : fin n, iso_E (sym v) = prod_eq_bij (sybf v) swbd.
Proof using . reflexivity. Qed.

Lemma symvVE : ∀ v : fin n, sym v⁻¹ == sybf v :> bijection (fin n).
Proof using . intros v1 v2. rewrite <- (sybfV v1). reflexivity. Qed.

Lemma symeVE : ∀ v : fin n, iso_E (sym v⁻¹) == prod_eq_bij (sybf v) swbd.
Proof using .
  intros. rewrite <- sybfV, <- swbdV, <- prod_eq_bij_inv. reflexivity.
Qed.

Lemma symK : ∀ v : fin n, (sym v) ∘ (sym v) == id.
Proof using .
  intros. split. rewrite (compvE (sym v)), symvE. apply sybfK.
  rewrite (compeE (sym v)), symeE. setoid_rewrite prod_eq_bij_comp.
  rewrite (sybfK v), swbdK, prod_eq_bij_id. reflexivity.
Qed.

Lemma move_along_sym : ∀ (v1 v2 : fin n) (d : direction),
  move_along (sym v1 v2) d = sym v1 (move_along v2 (swap_direction d)).
Proof using . intros. rewrite symvE, (sybfE v1). apply move_along_symf. Qed.

Lemma symm : ∀ v : fin n, sym v v = v.
Proof using . intros. rewrite symvE. apply sybff. Qed.

End Ring.
