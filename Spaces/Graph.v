(**************************************************************************)
(**   Mechanised Framework for Local Interactions & Distributed Algorithms  
      T. Balabonski, P. Courtieu, R. Pelle, L. Rieg, X. Urbain              
                                                                            
      PACTOLE project                                                       
                                                                            
      This file is distributed under the terms of the CeCILL-C licence      
                                                                          *)
(**************************************************************************)

Require Import Utf8 Rbase SetoidDec.
Require Import Pactole.Util.Coqlib.
Require Import Pactole.Util.Fin.
Require Import Pactole.Util.Ratio.
Require Import Pactole.Core.State.
Set Implicit Arguments.


Class Graph (V E : Type) := {
  #[global] V_Setoid :: Setoid V;
  #[global] E_Setoid :: Setoid E;
  #[global] V_EqDec :: EqDec V_Setoid;
  #[global] E_EqDec :: EqDec E_Setoid;

  src : E -> V; (* source and target of an edge *)
  tgt : E -> V;
  #[global] src_compat :: Proper (equiv ==> equiv) src;
  #[global] tgt_compat :: Proper (equiv ==> equiv) tgt;

  find_edge : V -> V -> option E;
  #[global] find_edge_compat :: Proper (equiv ==> equiv ==> opt_eq equiv) find_edge;
  find_edge_Some : ∀ e v1 v2, find_edge v1 v2 == Some e <-> v1 == src e /\ v2 == tgt e}.

Class ThresholdGraph (V E : Type) := {
  nothreshold_graph : Graph V E;
  threshold : E -> strict_ratio;
  #[global] threshold_compat :: Proper (equiv ==> equiv) threshold}.

Coercion nothreshold_graph : ThresholdGraph >-> Graph.
Global Existing Instance nothreshold_graph.
Global Opaque src_compat tgt_compat threshold_compat find_edge_compat find_edge_Some.

Section some_lemmas.

Context {V E : Type} {G : Graph V E}.

Lemma find_edge_None : ∀ v1 v2 : V,
  @find_edge _ _ G v1 v2 == None <-> ∀ e : E, ¬ (v1 == src e /\ v2 == tgt e).
Proof using .
  intros. rewrite <- (Decidable.not_not_iff _ (option_decidable _)).
  setoid_rewrite not_None_iff. split.
  - intros H1 e H2. apply H1. exists e. apply find_edge_Some, H2.
  - intros H1 [e H2]. apply (H1 e), find_edge_Some, H2.
Qed.

Lemma find_edge_pick : ∀ v1 v2 : V,
  pick_spec (λ e : E, v1 == src e /\ v2 == tgt e) (find_edge v1 v2).
Proof using .
  intros. apply pick_Some_None. intros e.
  apply find_edge_Some. apply find_edge_None.
Qed.

(* The specifications of find_edge make the graph simple *)
Lemma simple_graph : ∀ e1 e2 : E,
  @src _ _ G e1 == src e2 /\ @tgt _ _ G e1 == tgt e2 -> e1 == e2.
Proof using .
  intros * [Hs Ht]. apply Some_inj. erewrite <-2 (proj2 (find_edge_Some _ _ _)).
  apply find_edge_compat. 3,4: split. 1,2,5,6: symmetry.
  1,3,5: apply Hs. all: apply Ht.
Qed.

Lemma simple_graph_iff : ∀ e1 e2 : E,
  @src _ _ G e1 == src e2 /\ @tgt _ _ G e1 == tgt e2 <-> e1 == e2.
Proof using .
  intros. split. apply simple_graph. intros <-. split. all: reflexivity.
Qed.

End some_lemmas.

(** A finite graph ia a graph where the set [V] of vertices is a prefix of N. *)
(* FIXME: nothing prevents E from being infinite! *)
(* Definition FiniteGraph (n : nat) E := Graph (fin n) E.
Existing Class FiniteGraph.
Global Identity Coercion proj_graph : FiniteGraph >-> Graph. *)

Definition NodesLoc {V E : Type} {G : Graph V E} : Location
  := make_Location V.
