(**************************************************************************)
(*   Mechanised Framework for Local Interactions & Distributed Algorithms *)
(*   T. Balabonski, P. Courtieu, L. Rieg, X. Urbain                       *)
(*   PACTOLE project                                                      *)
(*                                                                        *)
(*   This file is distributed under the terms of the CeCILL-C licence.    *)
(*                                                                        *)
(**************************************************************************)

(**************************************************************************)
(**   Mechanised Framework for Local Interactions & Distributed Algorithms  
   T. Balabonski, P. Courtieu, L. Rieg, X. Urbain                           
   PACTOLE project                                                          
                                                                            
   This file is distributed under the terms of the CeCILL-C licence         
                                                                          *)
(**************************************************************************)


Require Import Utf8.
Require Import SetoidDec.
Require Import Rbase Rbasic_fun.
Require Import Lra.
Require Import Pactole.Util.Coqlib.
Require Import Pactole.Util.Bijection.
Require Import Pactole.Spaces.EuclideanSpace.
Require Pactole.Spaces.Similarity.
Set Implicit Arguments.


(********************)
(** *  Isometries  **)
(********************)

Open Scope R_scope.

Section IsometryDefinition.

Context {T : Type}.
Context `{RealMetricSpace T}.

(** Isometries are functions that preserve distances.
    Unlike bijections that only need a setoid, we need here a metric space. *)
Record isometry := {
  iso_f :> bijection T;
  dist_prop : forall x y, dist (iso_f x) (iso_f y) = dist x y}.

Global Instance isometry_Setoid : Setoid isometry.
simple refine {| equiv := fun sim1 sim2 => equiv (iso_f sim1) (iso_f sim2) |}.
Proof using .
* apply bij_Setoid.
* split.
  + repeat intro. reflexivity.
  + repeat intro. now symmetry.
  + repeat intro. etransitivity; eauto.
Defined.

Global Instance f_compat : Proper (equiv ==> equiv) iso_f.
Proof using . intros sim1 sim2 Hsim ?. now apply Hsim. Qed.

Theorem injective : forall iso : isometry, Preliminary.injective equiv equiv iso.
Proof using .
intros iso z t Heqf.
rewrite <- dist_defined in Heqf |- *.
now rewrite iso.(dist_prop) in Heqf.
Qed.

(** The identity isometry *)
Definition id : isometry := {|
  iso_f := @Bijection.id T _;
  dist_prop := ltac:(reflexivity) |}.

(** Composition of isometries *)
Definition comp (f g : isometry) : isometry.
refine {| iso_f := @compose (Bijection.bijection T) _ _ f g |}.
Proof using . abstract (intros; simpl; now rewrite f.(dist_prop), g.(dist_prop)). Defined.

Global Instance IsometryComposition : Composition isometry.
refine {| compose := comp |}.
Proof using . intros f1 f2 Hf g1 g2 Hg x. cbn. now rewrite Hf, Hg. Defined.

Lemma compose_assoc : forall f g h, f ∘ (g ∘ h) == (f ∘ g) ∘ h.
Proof using . repeat intro. reflexivity. Qed.

Lemma compose_id_l : forall sim, id ∘ sim == sim.
Proof using . intros sim x. simpl. reflexivity. Qed.

Lemma compose_id_r : forall sim, sim ∘ id == sim.
Proof using . intros sim x. simpl. reflexivity. Qed.

(** Inverse of an isometry *)
Definition inv (iso : isometry) : isometry.
refine {| iso_f := inverse iso.(iso_f) |}.
Proof using .
intros x y.
rewrite <- iso.(dist_prop). simpl. now repeat rewrite section_retraction.
Defined.

Global Instance IsometryInverse : Inverse isometry.
refine {| inverse := inv |}.
Proof using . intros f g Hfg x. simpl. now f_equiv. Defined.

Lemma compose_inverse_l : forall iso : isometry, (iso ⁻¹ ∘ iso) == id.
Proof using . intros iso x. simpl. now rewrite retraction_section; autoclass. Qed.

Lemma compose_inverse_r : forall iso : isometry, iso ∘ (iso ⁻¹) == id.
Proof using . intros iso x. simpl. now rewrite section_retraction; autoclass. Qed.

Lemma inverse_compose : forall f g : isometry, (f ∘ g) ⁻¹ == (g ⁻¹) ∘ (f ⁻¹).
Proof using . intros f g x. simpl. reflexivity. Qed.

Lemma simpl_inverse_l : forall (iso : isometry) x, iso ⁻¹ (iso x) == x.
Proof using . intros iso x. apply compose_inverse_l. Qed.

Lemma simpl_inverse_r : forall (iso : isometry) x, iso (iso ⁻¹ x) == x.
Proof using . intros iso x. apply compose_inverse_r. Qed.

Lemma dist_swap : forall (iso : isometry) x y, dist (iso⁻¹ x) y == dist (iso y) x.
Proof using.
intros iso x y. rewrite <- (compose_inverse_r iso x) at 2.
cbn. now rewrite dist_prop, dist_sym.
Qed.

(** Center of a isometry, that is, the point that gets mapped to the origin. *)
Definition center (iso : isometry) : T := iso⁻¹ origin.

Lemma center_prop : forall iso : isometry, iso (center iso) == origin.
Proof using . intro. unfold center. apply compose_inverse_r. Qed.

Global Instance center_compat : Proper (equiv ==> equiv) center.
Proof using . intros iso ? Hiso. apply (injective iso). now rewrite center_prop, Hiso, center_prop. Qed.

Definition isometry_similarity (iso : isometry) : Similarity.similarity T := {|
  Similarity.sim_f := iso_f iso;
  Similarity.zoom := 1%R;
  Similarity.dist_prop := ltac:(intros; rewrite Rmult_1_l; apply dist_prop) |}.

Definition similarity_isometry (sim : Similarity.similarity T)
                               (Hzoom : Similarity.zoom sim = 1) : isometry := {|
  iso_f := Similarity.sim_f sim;
  dist_prop := ltac:(now intros; rewrite Similarity.dist_prop, Hzoom, Rmult_1_l) |}.

Instance isometry_similarity_compat : Proper (equiv ==> equiv) isometry_similarity.
Proof using . intros ? ? Heq. apply Heq. Qed.

Lemma similarity_isometry_compat : forall sim1 sim2 (Hsim : sim1 == sim2) Hsim1 Hsim2,
  similarity_isometry sim1 Hsim1 == similarity_isometry sim2 Hsim2.
Proof using . intros ? ? Heq ? ?. apply Heq. Qed.

Lemma isometry_similarity_zoom : forall iso, Similarity.zoom (isometry_similarity iso) = 1.
Proof using . reflexivity. Qed.

Lemma isometry_similarity_inv : forall iso Hsim,
  similarity_isometry (isometry_similarity iso) Hsim == iso.
Proof using . intros iso Hiso x. reflexivity. Qed.

Lemma similarity_isometry_inv : forall sim Hsim,
  isometry_similarity (similarity_isometry sim Hsim) == sim.
Proof using . intros sim Hsim x. reflexivity. Qed.

(* TODO: prove that isometries preserve barycenters *)
End IsometryDefinition.
Arguments isometry T {_} {_} {_} {_}.


Section Translation.
Context {T : Type}.
Context `{rnsT : RealNormedSpace T}.

(** The translation isometry *)
Lemma bij_translation_Inversion : forall v x y : T, add x v == y ↔ add y (opp v) == x.
Proof using .
intros. split; intro Heq; rewrite Heq || rewrite <- Heq; rewrite <- add_assoc.
- now rewrite add_opp, add_origin.
- setoid_rewrite add_comm at 2. now rewrite add_opp, add_origin.
Qed.

Definition bij_translation (v : T) : @bijection T _.
refine {|
  section := fun x => add x v;
  retraction := fun x => add x (opp v) |}.
Proof using VS.
+ now repeat intro; apply add_compat.
+ apply bij_translation_Inversion.
Defined.

Lemma translation_zoom : forall v x y : T, dist (add x v) (add y v) = 1 * dist x y.
Proof using . intros. ring_simplify. apply dist_translation. Qed.

Definition translation (v : T) : isometry T.
refine {| iso_f := bij_translation v |}.
Proof using . cbn -[dist]. abstract (now intros; rewrite dist_translation). Defined.

Global Instance translation_compat : Proper (equiv ==> equiv) translation.
Proof using . intros u v Huv x. simpl. now rewrite Huv. Qed.

Lemma translation_origin : translation origin == id.
Proof using . intro. simpl. now rewrite add_origin. Qed.

Lemma translation_inverse : forall t, inverse (translation t) == translation (opp t).
Proof using . intros t x. simpl. reflexivity. Qed.

End Translation.

