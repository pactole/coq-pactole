The following people have contributed to the development of the Pactole library during the indicated periods of time:

- Cédric Auger (2013)
- Thibaut Balabonski (2016-2017)
- Sebastien Bouchard (2021- now)
- Pierre Courtieu (2013- now)
- Robin Pelle (2016-2020)
- Lionel Rieg (2014- now)
- Xavier Urbain (2013-now)
