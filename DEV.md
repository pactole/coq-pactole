The fundamental principle is that the MASTER branch must remain
compilable at all times and should never contain incomplete
developments.

Development work on the master branch must be avoided. Instead, each
feature or topic should have its own dedicated branch. Developers
should create new branches as needed, and all work—including
collaborative efforts—should occur within these topic-specific
branches.

Before merging changes into master, ensure that your code:

-   Is complete and thoroughly tested
-   Contains clear and comprehensive comments
-   Features proper indentation
-   Uses descriptive hypothesis names rather than auto-generated ones
-   Meets all project quality standards

When your changes are ready (such as a completed case study), you have
two options:

1.  Submit a pull request for review
2.  Merge directly into master after squashing your commits into
    meaningful, well-organized units (typically one or a few commits
    that clearly represent the changes)
