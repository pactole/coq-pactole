(**************************************************************************)
(*   Mechanised Framework for Local Interactions & Distributed Algorithms *)
(*   P. Courtieu, L. Rieg, X. Urbain                                      *)
(*   PACTOLE project                                                      *)
(*                                                                        *)
(*   This file is distributed under the terms of the CeCILL-C licence.    *)
(*                                                                        *)
(**************************************************************************)

(**************************************************************************)
(**   Mechanised Framework for Local Interactions & Distributed Algorithms 

   P. Courtieu, L. Rieg, X. Urbain                                      

   PACTOLE project                                                      
                                                                        
   This file is distributed under the terms of the CeCILL-C licence     
                                                                          *)
(**************************************************************************)

Require Import Utf8_core.
Require Import Lia.
Require Import SetoidList.
Require Import SetoidDec.
Require Import SetoidClass.
Require Import Pactole.Util.Coqlib.
Require Import Pactole.Core.Identifiers.
Require Import Pactole.Core.State.
Require Import Pactole.Core.Configuration.
Require Import Pactole.Observations.Definition.


(** Pairing two observations into one *)

Section PairObservation.

Context `{Names}.
Context `{Location}.
Context `{State}.
Variables Obs1 Obs2 : Observation.

Implicit Type config : configuration.

Local Instance pair_observation : Observation.
simple refine {|
  observation := observation (Observation := Obs1) * observation (Observation := Obs2);
  observation_Setoid := prod_Setoid _ _;
  
  obs_from_config config pt := (obs_from_config (Observation := Obs1) config pt,
                                obs_from_config (Observation := Obs2) config pt);
  
  obs_is_ok s config pt := obs_is_ok (Observation := Obs1) (fst s) config pt
                        /\ obs_is_ok (Observation := Obs2) (snd s) config pt |};
autoclass; [|].
Proof.
+ repeat intro. split; cbn; apply obs_from_config_compat; assumption.
+ intros config state. cbn. split; apply obs_from_config_spec.
Defined.

End PairObservation.
