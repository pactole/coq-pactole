Require Import Reals Morphisms Setoid.
Require Import Decidable.
Require Import SetoidDec SetoidList SetoidClass.
Require  Pactole.Util.Stream.
Require Import Pactole.Util.SetoidDefs Pactole.Util.NumberComplements.

Open Scope R_scope.

Inductive ident:Type := A | B. (* Nous avons seulement 2 robots. *)

Definition position := (R*R)%type. (*les coordonnées dans le plan euclidien.*)
Unset Automatic Proposition Inductives.
Class State_info {info:Type}:Type := mkStI { }. (* Paramètre du modèle *)
Class State_pos {pos:Type}:Type := mkStP { }. (* Paramètre du modèle *)

(* À partir des paramètres on a un type State. *)
Class State {I:Type} {L:Type} {info:@State_info I} {position:@State_pos L}
  : Type :=
  mkSt {
      pos: L;
      info: I
    }.
(* Point technique: le dernier argument de pos et info n'est pas implicite. *)
Arguments pos {I}%_type {L}%_type {info} {position} State.
Arguments info {I}%_type {L}%_type {info} {position} State.

Module Minipactole. (* comme si c'était dans un autre fichier. *)

  Section minipactole.
    (* On laisse Info comme paramètre *)
    Context {info} {Info: @State_info info}. (* À définir ou bien faire varier selon
                                                les algorithmes considérés:
                                                couleur etc *)
    (* Mais on fixe pos *)
    Context {pos} {Pos: @State_pos pos}.
    (* Local Instance Pos: State_pos := (mkStP position). *)

    (* config = fonction retournant l'état de n'importe quel robot. *)
    Definition configuration := ident -> State.

    (* Comme une configuration est une fonction, l'égalité "=" de coq
       ne convient pas: deux fonctions équivalentes mais définie
       différemment ne seraient pas considérées comme "=". On définit
       donc une autre égalité: "equiv". Deux configurations sont
       "equiv" si elles ont le même comportement. Ceci devrait
       permettre d'utiliser "rewrite" de façon plus agréable. *)
    Local Instance configuration_Setoid : Setoid configuration := fun_Setoid ident _.

    (* Le robogram est le programme qui tourne localement sur le
       robot: il prend la configuration (observation parfaite de la
       configuration). et retourne le nouvel état (position + infos
       couleur etc). Il est compatible avec le "equiv" de
       configuration et de state. *)
    Record robogram  := {
        pgm :> configuration -> ident -> State;
        pgm_compat :> Proper (equiv ==> eq ==> eq) pgm
      }.

    (* Une demonic_action détermine qui est activé au round courant. Le
       démon sera une suite infinie de demonic_action. *)
    Record demonic_action := {
        activate : ident -> bool; (** Select which robots are activated *)
      }.

    (** A [demon] is just a stream of [demonic_action]s. *)
    Definition demon := Stream.t demonic_action.

    (* Un round consiste à fabriquer la configuration suivante à
       partir de la configuration donnée (config) en appliquant le
       robogram sur les robots activés. *)
    Definition round (r:robogram) da config :=
      fun id =>
        if da.(activate) id then r config id (* on applique le robogram *)
        else config id (* on garde le même état que dans config *)
    .


    Global Instance round_compat : Proper (equiv ==> equiv ==> equiv ==> equiv) round.
    Proof using Type.
      intros r1 r2 Hr da1 da2 Hda config1 config2 Hconfig id.
      unfold round. rewrite Hda.
      destruct (activate da2 id).
      - (* active robot *)
        destruct id.
        + rewrite Hr.
          apply pgm_compat;auto.
        + rewrite Hr.
          apply pgm_compat;auto.
      - apply Hconfig.
    Qed.

    (** Executions are simply streams of configurations. *)
    Definition execution := Stream.t configuration.

    (** [execute r d config] returns an (infinite) execution from an
        initial global configuration [config], a demon [d] and a
        robogram [r] running on each good robot. *)
    Definition execute (r : robogram) : demon -> configuration -> execution :=
      cofix execute d config :=
        Stream.cons config (execute (Stream.tl d) (round r (Stream.hd d) config)).

  End minipactole.
End Minipactole.

(* On développe en parallèle l'exemple le plus simple *)
Section Vig2Cols.

  Inductive Couleur:Type := Black | White.

  Local Instance Pos: State_pos := (mkStP position).
  Local Instance Info: State_info := (mkStI Couleur).

  Definition theother id :=
    match id with
      A => B
    | B => A
    end.

  Lemma RRdec : forall x y : position, {x = y} + {x <> y}.
  Proof.
    intros.
    case x;case y.
    intros r r0 r1 r2. 
    case (Req_dec_T r1 r);intros; subst.
    - case (Req_dec_T r2 r0);intros;cbn [equiv] in *;subst.
      + left; reflexivity.
      + right.
        intro abs.
        injection abs.
        intros H. 
        contradiction.
    - right.
      intro abs.
      injection abs.
      intros H H0.  
      contradiction.
  Qed.

  Definition middle (p1 p2: position) := ((fst p1 + fst p2)/2, (snd p1 + snd p2)/2).

  (* (BLACK, BLACK)  ->  WHITE, STAY
   (BLACK, WHITE)  ->  skip
   (WHITE, BLACK)  ->  –, M2O
   (WHITE, WHITE)  ->  BLACK, M2H *)
  Definition rVig2Cols (conf:Minipactole.configuration) id :=
    let me := conf id in
    let other := conf (theother id) in
    match me.(info),other.(info) with
      Black,Black => {| pos := me.(pos) ; info := White |} (* (BLACK, BLACK)  ->  WHITE, STAY *)
    | Black,White => me (* skip *)
    | White,Black => {| pos := other.(pos) ; info := White |}(*white?*)(* (WHITE, BLACK)  ->  –, M2O *)
    | White,White => {| pos := middle me.(pos) other.(pos) ; info := Black |} (* BLACK, M2H *)
    end.

  Definition rbmVig2Cols : Minipactole.robogram.
  Proof.
    apply (Minipactole.Build_robogram rVig2Cols).
    repeat red;simpl.
    intros x y H x0 y0 H0. 
    rewrite H0.
    unfold rVig2Cols.
    repeat rewrite H.
    subst.
    reflexivity.
  Defined. (* Important *)

  (* Exemple de théorème prouvable sur l'algo concret. *)
  (* une fois le RDV atteint et A=White et B=black, personne ne bouge plus. *)
  Lemma once_RDV1 : forall c da,
      (c A).(info) = Black -> (c B).(info) = White ->
      (c A).(pos) = (c B).(pos) -> 
      Minipactole.round rbmVig2Cols da c == c.
  Proof.
    intros c da HcA HcB HcPos.
    simpl.
    intros x. 
    unfold rbmVig2Cols , Minipactole.round;simpl.
    destruct (Minipactole.activate da x);auto.
    unfold rVig2Cols.
    destruct x;simpl.
    - rewrite HcA.
      rewrite HcB.
      reflexivity.
    - rewrite HcA.
      rewrite HcPos.
      rewrite HcB at 1.
      rewrite <- HcB.
      destruct (c B);auto.
  Qed.

  (* une fois le RDV atteint et A=White et B=black, personne ne bouge plus. *)
  Lemma once_RDV2 : forall c da,
      (c A).(info) = White -> (c B).(info) = Black ->
      (c A).(pos) = (c B).(pos) -> 
      Minipactole.round rbmVig2Cols da c == c.
  Proof.
    intros c da HcA HcB HcPos.
    simpl.
    intros x. 
    unfold rbmVig2Cols , Minipactole.round;simpl.
    destruct (Minipactole.activate da x);auto.
    unfold rVig2Cols.
    destruct x;simpl.
    - rewrite HcA.
      rewrite HcB.
      rewrite <- HcA.
      rewrite <- HcPos.
      destruct (c A);auto.
    - rewrite HcA.
      rewrite HcB at 1.
      reflexivity.
  Qed.

  Lemma once_RDV: forall c da,
      (c A).(info) <> (c B).(info) -> 
      (c A).(pos) = (c B).(pos) -> 
      Minipactole.round rbmVig2Cols da c == c.
  Proof.
    intros c da. 
    destruct ((c A).(info)) eqn:hA;
    destruct ((c B).(info)) eqn:hB; intros; try contradiction.
    - apply once_RDV1;auto.
    - apply once_RDV2;auto.
  Qed.


End Vig2Cols.
