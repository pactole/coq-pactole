
# Requirements

- Coq 8.19 or 8.20 (including the executable `coqc`, `coqdep`, `coq_makefile`)
- GNU `make`

# Configuration

You should perform once the following command to generate the Makefile:

```bash
coq_makefile -f _CoqProject -o Makefile
```

# Compilation

To compile the whole projet including case studies:

``` bash
make
```

# Use in your coq development

Suppose you have compiled Pactole in a directory calle `pactole` and
you want to use it in your own development called `myproject`. You
should use a `_CoqProject` file containing:

```
-Q pactole Pactole
.. # your own project options
```

and use `coq_makefile -f _CoqProject -o Makefile` to generate your
makefile. Note that ProofGeneral and other IDE also read the
`_CoqProject`.

From now on you can use Pactole in your coq files by importing the
relevant modules, e.g.:

``` coq
Require Import Pactole.Core.Formalism.
```

See the file README.md for the directory structure of the library.

# NOTES FOR DEVELOPPERS

Pactole provides fast (and unsafe) compilation target. This is
explained in this section.


## build (slow)

The default makefile target may take more than 10mn to compile due to
some case studies. You may want to comment them in the _CoqProject
file before the configure step above if you are not interested in
these case studies. Another solution is to follow the fast compilation
recipes below.

Doing this once a week when developing is good practice. This is the
only way to make a really safe compilation including all
xxx_Assumption.v. You should always do this once in a while to make
sure some universe constraints aren't failing and to check if you did
not miss any remaining axiom.

But during the development process this makes the compilation when
switching between files too slow. Hence the following less safe
compilation processes:

## unsafe builds when developing

During development you can benefit from coq's "vos/vok" generation to
speed up compilation. The only specificity of Pactole concerns
compilation of the files named xxx_Assumptions.v. This files print the
sets of assumptions used in the proofs of final lemmas in Case
studies. Compiling these files for vos/vok target would raise errors.
We provide adapted targets below.

## build (slow, almost safe, but very parallelisable)

```
make [-j] vok
```
or better when developing
```
make [-j] vok-nocheck
```
Doing this once a day when developing is good practice.

##  build (Very fast, unsafe)

```
make [-j] vos
```
or better when developing
```
make [-j] vos-nocheck
```

This should be your prefered way of compiling when developing. It is
much faster. It is unsafe but in most situations no bad surprise is to
be expected.

You should do real compilations from time to time as explained above.

## Proofgeneral

For easy use of this feature (vos) you can use the auto compilation
feature of proofgeneral. menu:

menu: Coq / Auto Compilation / Compile Before Require
and then: Coq / Auto Compilation / vos compilation

Now you can transparently script in any buffer, all needed file will
be compiled quickly. Don't forget to make a big fat "make" from time
to time.
