Require Import Utf8.
From Pactole Require Export Setting Spaces.Graph Util.Bijection
  Spaces.Isomorphism Util.Fin Spaces.Ring Observations.MultisetObservation.

Set Implicit Arguments.

Section RingSSync.

Context {n : nat} {ltc_2_n : 2 <c n} {Robots : Names}.

Global Existing Instance NodesLoc.

Global Instance St : State location := OnlyLocation (fun _ => True).

Global Existing Instance multiset_observation.

Global Instance RC : robot_choice direction :=
  { robot_choice_Setoid := direction_Setoid }.

(** Demon's frame choice:
      we move back the robot to the origin with a translation
      and we can choose the orientation of the ring. *)
Global Instance FC : frame_choice (location * bool) := {
  frame_choice_bijection :=
    λ nb, if snd nb then (trans (fst nb)) ∘ (sym (fst nb))
                    else trans (fst nb);
  frame_choice_Setoid := eq_setoid _ }.

Global Existing Instance NoChoice.
Global Existing Instance NoChoiceIna.
Global Existing Instance NoChoiceInaFun.

Global Instance UpdFun : update_function direction (location * bool) unit := {
  update := λ config g _ dir _, move_along (config (Good g)) dir;
  update_compat := ltac:(repeat intro; subst; now apply move_along_compat) }.

End RingSSync.
