(**************************************************************************)
(*   Mechanised Framework for Local Interactions & Distributed Algorithms *)
(*   T. Balabonski, P. Courtieu, L. Rieg, X. Urbain                       *)
(*   PACTOLE project                                                      *)
(*                                                                        *)
(*   This file is distributed under the terms of the CeCILL-C licence.    *)
(*                                                                        *)
(**************************************************************************)

(**************************************************************************)
(**  Mechanised Framework for Local Interactions & Distributed Algorithms   
     T. Balabonski, P. Courtieu, L. Rieg, X. Urbain                         
     PACTOLE project                                                        
                                                                            
     This file is distributed under the terms of the CeCILL-C licence.      
                                                                          *)
(**************************************************************************)

Set Implicit Arguments.
Require Import Utf8.
Require Import SetoidDec.
Require Import Reals.
Require Import Pactole.Setting.
Require Import Pactole.Spaces.RealMetricSpace.
Require Import Pactole.Spaces.Isometry.


Typeclasses eauto := (bfs).


Section IsometryCenter.

Context `{Observation}.
Context {VS : RealVectorSpace location}.
Context {RMS : RealMetricSpace location}.
Context `{robot_choice}.
Context `{update_choice}.
Context `{inactive_choice}.

(** Similarities as a frame choice, inside real metric spaces *)
Global Instance FrameChoiceIsometry : frame_choice (isometry location) := {|
  frame_choice_bijection := @iso_f location _ _ _ _;
  frame_choice_Setoid := isometry_Setoid;
  frame_choice_bijection_compat := f_compat |}.

Definition isometry_da_prop da :=
  forall config g, center (change_frame da config g) == get_location (config (Good g)).

(** When using isometries to change the frame of reference, we need to ensure that
    the center of the isometry is the location of the robot. *)
Definition isometry_da := { iso_da : demonic_action | isometry_da_prop iso_da }.

Instance isometry_da_Setoid : Setoid isometry_da := sig_Setoid _.

Definition proj_iso_da : isometry_da -> demonic_action := @proj1_sig _ _.

Global Instance proj_iso_da_compat : Proper (equiv ==> equiv) proj_iso_da.
Proof using . intros ? ? Heq. apply Heq. Qed.

Definition isometry_center : forall da config g,
  center (change_frame (proj_iso_da da) config g) == get_location (config (Good g))
  := @proj2_sig _ _.


(** Demons are now stream of [isometry_da]s.*)
Definition isometry_demon := Stream.t isometry_da.


(** From a [isometry_demon], we can recover the demon] and its property [isometry_da_prop]. *)
Definition isometry_demon2demon : isometry_demon -> demon := Stream.map proj_iso_da.

Definition isometry_demon2prop : forall d,
  Stream.forever (Stream.instant isometry_da_prop) (isometry_demon2demon d).
Proof. coinduction Hcorec. simpl. unfold proj_iso_da. apply proj2_sig. Defined.

Global Instance isometry_demon2demon_compat :
  Proper (@equiv _ Stream.stream_Setoid ==> @equiv _ Stream.stream_Setoid) isometry_demon2demon.
Proof using . unfold isometry_demon2demon. autoclass. Qed.

Lemma isometry2demon_hd : forall d, Stream.hd (isometry_demon2demon d) = proj_iso_da (Stream.hd d).
Proof using . now intros []. Qed.

Lemma isometry2demon_tl : forall d, Stream.tl (isometry_demon2demon d) = isometry_demon2demon (Stream.tl d).
Proof using . now intros []. Qed.

Lemma isometry2demon_cons : forall da d,
  @equiv _ Stream.stream_Setoid
    (isometry_demon2demon (Stream.cons da d))
    (Stream.cons (proj_iso_da da) (isometry_demon2demon d)).
Proof using . intros. unfold isometry_demon2demon. now rewrite Stream.map_cons. Qed.

(** Conversely, from a [demon] and a proof that [isometry_da_prop] is always true,
    we can build a [isometry_demon]. *)

CoFixpoint demon2isometry_demon : forall (d : demon) (Hd : Stream.forever (Stream.instant isometry_da_prop) d),
  isometry_demon := fun d Hd =>
  Stream.cons
    (exist _ (Stream.hd d) match Hd with | Stream.Always x _ => x end)
    (@demon2isometry_demon (Stream.tl d) (match Hd with | Stream.Always _ x => x end)).

Arguments demon2isometry_demon d Hd : clear implicits.

Lemma demon2isometry_hd : forall d Hd,
  Stream.hd (demon2isometry_demon d Hd) == exist _ (Stream.hd d) (match Hd with Stream.Always x _ => x end).
Proof using . now intros [] []. Qed.

Lemma demon2isometry_tl : forall d Hd,
  Stream.tl (demon2isometry_demon d Hd) == demon2isometry_demon (Stream.tl d) (match Hd with Stream.Always _ x => x end).
Proof using . now intros [] []. Qed.

Theorem demon2demon : forall d Hd,
  @equiv _ Stream.stream_Setoid (isometry_demon2demon (demon2isometry_demon d Hd)) d.
Proof using . coinduction Hcorec. unfold Stream.instant2. now rewrite isometry2demon_hd, demon2isometry_hd. Qed.

End IsometryCenter.

Coercion proj_iso_da : isometry_da >-> demonic_action.
Coercion isometry_demon2demon : isometry_demon >-> demon.

(*
 *** Local Variables: ***
 *** coq-prog-name: "coqtop" ***
 *** fill-column: 117 ***
 *** End: ***
 *)
