(**************************************************************************)
(*  Mechanised Framework for Local Interactions & Distributed Algorithms  *)
(*  T. Balabonski, P. Courtieu, L. Rieg, X. Urbain                        *)
(*  PACTOLE project                                                       *)
(*                                                                        *)
(*  This file is distributed under the terms of the CeCILL-C licence.     *)
(*                                                                        *)
(**************************************************************************)

(**************************************************************************)
(** Mechanised Framework for Local Interactions & Distributed Algorithms    
                                                                            
    T. Balabonski, P. Courtieu, L. Rieg, X. Urbain                          
                                                                            
    PACTOLE project                                                         
                                                                            
    This file is distributed under the terms of the CeCILL-C licence.       
                                                                          *)
(**************************************************************************)

Set Implicit Arguments.
Require Import Utf8.
Require Import SetoidDec.
Require Import Reals.
Require Import Pactole.Util.Preliminary.
Require Import Pactole.Setting.

Typeclasses eauto := (bfs).
Section RigidFormalism.

Context {Tframe : Type}.
Context `{Observation}.
Context {Robot : robot_choice location}.
Context {Frame : frame_choice Tframe}.
Context `{update_choice}.
Context {Upd : update_function location _ _}.


(** Rigid moves are a special case of state updates where the updated location of the robot
    is the one chosen by the robogram. *)
Class RigidSetting := {
  rigid_update : forall config frame g target choice,
    get_location (update config g frame target choice) == target }.

(** Rigid moves are a special case of state updates where the updated location of the robot
    is the one chosen by the robogram. *)

End RigidFormalism.

(* Same notion for cases where the return type (Trobot) of the robogram is info. Here we
define rigidity as the fact that the final state of the robot is exactly the info computed
by the robogram. *)
Section RigidFormalismInfo.
  Context {info : Type} {Loc : Location} {St : State info} {N : Names} {Obs : Observation} 
          {Tactive Tframe Tinactive : Type} {RC : robot_choice info} {FC : frame_choice Tframe} 
          {UC : update_choice Tactive} {IC : inactive_choice Tinactive}.

  Context {Upd : update_function info _ Tactive}.

  Definition xxx := forall config frame g target choice,
      (@update info Loc St N info Tframe Tactive RC FC UC Upd config g frame target choice == target).

  Class RigidSettingInfo := {
      rigid_update_Info : forall config frame g target choice,
        (update config g frame target choice) == target }.

End RigidFormalismInfo.

(*
 *** Local Variables: ***
 *** coq-prog-name: "coqtop" ***
 *** fill-column: 90 ***
 *** End: ***
 *)
