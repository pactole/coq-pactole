Require Import Utf8 SetoidClass.
From Pactole Require Import Core.Identifiers Core.State Core.Configuration.

(** To use these results, just provide an instance of the [NoByzantine] class. *)

Section NoByzantine.

Context {N : Names}.
Context {L : Location} {info : Type} {St : State info}.

Class NoByzantine := { nB_eq_0 : nB = 0 }.

Context {NoByz : NoByzantine}.

(** There is no byzantine robot so we can simplify properties
    about identifiers and configurations. *)

Lemma Bnames_nil : Bnames = nil.
Proof using NoByz.
  now rewrite <- List.length_zero_iff_nil, Bnames_length, nB_eq_0.
Qed.

Lemma no_byz : ∀ (id : ident) (P : ident -> Type), (∀ g, P (Good g)) -> P id.
Proof using NoByz.
  intros [g | b] P HP. { apply HP. } exfalso. apply (@List.in_nil _ b).
  rewrite <- Bnames_nil. apply In_Bnames.
Qed.

Lemma no_byz_inv : ∀ (P : ident -> Type),
  (∀ id : ident, P id) -> ∀ g, P (Good g).
Proof using . intros * H *. apply H. Qed.

Lemma no_byz_eq : ∀ config1 config2 : configuration,
  (∀ g, config1 (Good g) == config2 (Good g)) -> config1 == config2.
Proof using NoByz.
  intros config1 config2 Heq id. apply (no_byz id). intro g. apply Heq.
Qed.

Lemma no_byz_fun {T : Type} : B -> T.
Proof using NoByz.
  intros b. exfalso. apply (@List.in_nil B b). rewrite <- Bnames_nil.
  apply In_Bnames.
Qed.

Lemma no_byz_ex : ∀ (P : ident → Prop),
  (∃ id : ident, P id) <-> (∃ g : G, P (Good g)).
Proof using NoByz.
  intros. split.
  - intros [[g | b] H]. { exists g. apply H. } exfalso.
    apply (@List.in_nil B b). rewrite <- Bnames_nil. apply In_Bnames.
  - intros [g H]. exists (Good g). apply H.
Qed.

End NoByzantine.
