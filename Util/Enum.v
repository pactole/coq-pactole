Require Import Utf8 SetoidDec SetoidList Arith_base Lia.
From Pactole Require Import Util.Coqlib Util.Fin.

Set Implicit Arguments.

(* TODO: should we add a fold operator? *)
(* FIXME: change the equalities to use equiv and the Setoid class *)

Program Fixpoint build_enum N k (Hle : k <= N) acc : list (fin N) :=
  match k with
    | 0 => acc
    | S m => @build_enum N m _ (@Fin _ m _ :: acc)
  end.
Next Obligation.
  lia.
Qed.

(** A list containing all elements of [fin N]. *)
Definition enum N : list (fin N) := build_enum (Nat.le_refl N) nil.

(** Specification of [enum]. *)
Lemma In_build_enum : ∀ N k (Hle : k <= N) l x,
  In x (build_enum Hle l) <-> In x l \/ fin2nat x < k.
Proof using .
  intros N k. induction k; intros Hle l x; simpl.
  + intuition auto with *.
  + rewrite IHk. simpl. split; intro Hin.
    - destruct Hin as [[Hin | Hin] | Hin]; intuition; [].
      subst. simpl. right. lia.
    - destruct Hin as [Hin | Hin]; intuition; [].
      assert (Hcase : fin2nat x < k \/ fin2nat x = k) by lia.
      destruct Hcase as [Hcase | Hcase]; intuition; [].
      subst. do 2 left. destruct x; f_equal; simpl in *. apply le_unique.
Qed.

Lemma In_enum : ∀ N x, In x (enum N) <-> fin2nat x < N.
Proof using . intros. unfold enum. rewrite In_build_enum. simpl. intuition. Qed.

(** Length of [enum]. *)
Lemma build_enum_length : ∀ N k (Hle : k <= N) l,
  length (build_enum Hle l) = k + length l.
Proof using .
  intros N k. induction k; intros Hle l; simpl.
  + reflexivity.
  + rewrite IHk. simpl. lia.
Qed.

Lemma enum_length : ∀ N, length (enum N) = N.
Proof using . intro. unfold enum. now rewrite build_enum_length. Qed.

(** [enum] does not contain duplicates. *)
Lemma build_enum_NoDup : ∀ N k (Hle : k <= N) l,
  (∀ x, In x l -> k <= fin2nat x) -> NoDup l -> NoDup (build_enum Hle l).
Proof using .
  intros N k. induction k; intros Hle l Hin Hl; simpl; auto; [].
  apply IHk.
  + intros x [Hx | Hx].
    - now subst.
    - apply Hin in Hx. lia.
  + constructor; trivial; [].
    intro Habs. apply Hin in Habs. simpl in Habs. lia.
Qed.

Lemma enum_NoDup : ∀ N, NoDup (enum N).
Proof using .
  intro. unfold enum.
  apply build_enum_NoDup; simpl; intuition; constructor.
Qed.

(** [enum] is sorted in increasing order. *)
Notation Flt := (fun x y => lt (fin2nat x) (fin2nat y)).

Lemma build_enum_Sorted :
  ∀ N k (Hle : k <= N) l, (∀ x, In x l -> k <= fin2nat x)
  -> Sorted Flt l -> Sorted Flt (build_enum Hle l).
Proof using .
  intros N k. induction k; intros Hle l Hin Hl; simpl; auto; [].
  apply IHk.
  + intros x [Hx | Hx].
    - now subst.
    - apply Hin in Hx. lia.
  + constructor; trivial; [].
    destruct l; constructor; []. simpl. apply Hin. now left.
Qed.

Lemma enum_Sorted : ∀ N, Sorted Flt (enum N).
Proof using .
  intro. unfold enum.
  apply build_enum_Sorted; simpl; intuition.
Qed.

(** Extensional equality of functions is decidable over finite domains. *)
Lemma build_enum_app_nil : ∀ N k (Hle : k <= N) l,
  build_enum Hle l = build_enum Hle nil ++ l.
Proof using .
  intros N k. induction k; intros Hle l; simpl.
  + reflexivity.
  + now rewrite (IHk _ (_ :: nil)), IHk, <- app_assoc.
Qed.

Theorem build_enum_eq : ∀ {A} eqA N (f g : fin N -> A) k (Hle : k <= N) l,
  eqlistA eqA (List.map f (build_enum Hle l)) (List.map g (build_enum Hle l)) ->
  ∀ x, fin2nat x < k -> eqA (f x) (g x).
Proof using .
  intros A eqA N f g k. induction k; intros Hle l Heq x Hx; simpl.
  * destruct x; simpl in *; lia.
  * assert (Hlt : k <= N) by lia.
    assert (Hcase : fin2nat x < k \/ fin2nat x = k) by lia.
    destruct Hcase as [Hcase | Hcase].
    + apply IHk with (x := x) in Heq; auto.
    + subst k. cbn in Heq. rewrite build_enum_app_nil, map_app, map_app in Heq.
      destruct (eqlistA_app_split _ _ _ _ Heq) as [_ Heq'].
      - now do 2 rewrite map_length, build_enum_length.
      - simpl in Heq'. inv Heq'.
        assert (Heqx : x = Fin Hle).
        { clear. destruct x; simpl. f_equal. apply le_unique. }
        now rewrite Heqx.
Qed.

Corollary enum_eq : ∀ {A} eqA N (f g : fin N -> A),
  eqlistA eqA (List.map f (enum N)) (List.map g (enum N))
  -> ∀ x, eqA (f x) (g x).
Proof using .
  unfold enum. intros A eqA N f g Heq x.
  apply build_enum_eq with (x := x) in Heq; auto; []. apply fin_lt.
Qed.

(** Cutting [enum] after some number of elements. *)
Lemma firstn_build_enum_le : ∀ N k (Hle : k <= N) l k' (Hk : k' <= N),
  k' <= k -> firstn k' (build_enum Hle l) = @build_enum N k' Hk nil.
Proof using .
  intros N k. induction k; intros Hk l k' Hk' Hle.
  * assert (k' = 0) by lia. now subst.
  * rewrite build_enum_app_nil, firstn_app, build_enum_length.
    replace (k' - (S k + length (@nil (fin N)))) with 0 by lia.
    rewrite app_nil_r.
    destruct (Nat.eq_dec k' (S k)) as [Heq | Heq].
    + subst k'. rewrite firstn_all2.
      - f_equal. apply le_unique.
      - rewrite build_enum_length. simpl. lia.
    + simpl build_enum. erewrite IHk.
      - f_equal.
      - lia.
Qed.

Lemma firstn_build_enum_lt : ∀ N k (Hle : k <= N) l k', k <= k' ->
  firstn k' (build_enum Hle l) = build_enum Hle (firstn (k' - k) l).
Proof using .
  intros N k. induction k; intros Hle l k' Hk.
  + now rewrite Nat.sub_0_r.
  + rewrite build_enum_app_nil, firstn_app, build_enum_length, Nat.add_0_r.
    rewrite firstn_all2, <- build_enum_app_nil; trivial; [].
    rewrite build_enum_length. simpl. lia.
Qed.

Lemma firstn_enum_le : ∀ N k (Hle : k <= N),
  firstn k (enum N) = build_enum Hle nil.
Proof using . intros. unfold enum. now apply firstn_build_enum_le. Qed.

Lemma firstn_enum_lt : ∀ N k, N <= k -> firstn k (enum N) = enum N.
Proof using .
  intros. unfold enum. now rewrite firstn_build_enum_lt, firstn_nil.
Qed.

Lemma firstn_enum_spec : ∀ N k x, In x (firstn k (enum N)) <-> fin2nat x < k.
Proof using .
  intros N k x. destruct (le_lt_dec k N) as [Hle | Hlt].
  + rewrite (firstn_enum_le Hle), In_build_enum. simpl. intuition.
  + rewrite (firstn_enum_lt (Nat.lt_le_incl _ _ Hlt)).
    split; intro Hin.
    - transitivity N; trivial; []. apply fin_lt.
    - apply In_enum, fin_lt.
Qed.

(** Removing some number of elements from the head of [enum]. *)
Lemma skipn_build_enum_lt : ∀ N k (Hle : k <= N) l k', k <= k' ->
  skipn k' (build_enum Hle l) = skipn (k' - k) l.
Proof using .
  intros N k Hle l k' Hk'. apply app_inv_head with
  (firstn k' (build_enum Hle l)). rewrite firstn_skipn, firstn_build_enum_lt;
  trivial; []. rewrite (build_enum_app_nil Hle (firstn _ _)).
  now rewrite build_enum_app_nil, <- app_assoc, firstn_skipn.
Qed.

Lemma skipn_enum_lt : ∀ N k, N <= k -> skipn k (enum N) = nil.
Proof using .
  intros. unfold enum. now rewrite skipn_build_enum_lt, skipn_nil.
Qed.

Lemma skipn_enum_spec :
  ∀ N k x, In x (skipn k (enum N)) <-> k <= fin2nat x < N.
Proof using .
  intros N k x. split; intro Hin.
  + assert (Hin' : ~In x (firstn k (enum N))).
    { intro Habs. rewrite <- InA_Leibniz in *. revert x Habs Hin.
      apply NoDupA_app_iff; autoclass; [].
      rewrite firstn_skipn. rewrite NoDupA_Leibniz. apply enum_NoDup. }
    rewrite firstn_enum_spec in Hin'. split; auto with zarith; [].
    apply fin_lt.
  + assert (Hin' : In x (enum N)) by apply In_enum, fin_lt.
    rewrite <- (firstn_skipn k), in_app_iff, firstn_enum_spec in Hin'.
    intuition lia.
Qed.
