(**************************************************************************)
(*   Mechanised Framework for Local Interactions & Distributed Algorithms *)
(*   P. Courtieu, L. Rieg, X. Urbain                                      *)
(*   PACTOLE project                                                      *)
(*                                                                        *)
(*   This file is distributed under the terms of the CeCILL-C licence     *)
(*                                                                        *)
(**************************************************************************)

(**************************************************************************)
(**  Mechanised Framework for Local Interactions & Distributed Algorithms   
                                                                            
     P. Courtieu, L. Rieg, X. Urbain                                        
                                                                            
     PACTOLE project                                                        
                                                                            
     This file is distributed under the terms of the CeCILL-C licence       
                                                                          *)
(**************************************************************************)


Require Import Utf8 Rbase RelationPairs SetoidDec.
Require Import Pactole.Util.Preliminary.
Set Implicit Arguments.


(* To avoid infinite loops, we use a breadth-first search... *)
Typeclasses eauto := (bfs) 20.
(* but we need to remove [eq_setoid] as it matches everything... *)
#[export] Remove Hints eq_setoid : Setoid.
(* while still declaring it for the types for which we still want to use it. *)
#[export]Instance R_Setoid : Setoid R := eq_setoid R.
#[export]Instance Z_Setoid : Setoid Z := eq_setoid Z.
#[export]Instance nat_Setoid : Setoid nat := eq_setoid nat.
#[export]Instance bool_Setoid : Setoid bool := eq_setoid bool.
#[export]Instance unit_Setoid : Setoid unit := eq_setoid unit.

#[export]Instance R_EqDec : EqDec R_Setoid := Req_dec_T.
#[export]Instance Z_EqDec : EqDec Z_Setoid := Z.eq_dec.
#[export]Instance nat_EqDec : EqDec nat_Setoid := Nat.eq_dec.
#[export]Instance bool_EqDec : EqDec bool_Setoid := Bool.bool_dec.
#[export]Instance unit_EqDec : EqDec unit_Setoid := fun x y => match x, y with tt, tt => left eq_refl end.

Notation "x == y" := (equiv x y).
Notation "x == y :> A" := (@equiv A _ x y) (at level 70, y at next level, no associativity, only parsing).
Arguments complement A R x y /.
Arguments Proper {A}%_type R%_signature m.

Lemma equiv_dec_refl {T U} {S : Setoid T} {E : EqDec S} :
  forall (e : T) (A B : U), (if equiv_dec e e then A else B) = A.
Proof using . intros. destruct_match; intuition auto with *. Qed.

Definition equiv_decb_refl [T] [S : Setoid T] (E : EqDec S) : forall x : T, x ==b x = true :=
  fun x => equiv_dec_refl x true false.

(** **  Setoid Definitions  **)

Instance fun_Setoid A B `(Setoid B) : Setoid (A -> B) | 4.
Proof. exists (fun f g : A -> B => forall x, f x == g x).
split.
+ repeat intro. reflexivity.
+ intros ? ? Heq ?. symmetry. apply Heq.
+ repeat intro. etransitivity; eauto.
Defined.

#[export]Instance fun_Setoid_respectful_compat (A B : Type) (SB : Setoid B) :
  subrelation (@equiv (A->B) (fun_Setoid A SB))
    (respectful (@eq A) (@equiv B SB)).
Proof using . intros ?? H ???. subst. apply H. Qed.

#[export]Instance fun_Setoid_pointwise_compat A B `{Setoid B} :
  subrelation (@equiv _ (fun_Setoid A _)) (pointwise_relation _ equiv).
Proof using . intros f g Hfg x. apply Hfg. Qed.

#[export]Instance eq_Setoid_eq_compat (T : Type) :
  subrelation (@equiv T (eq_setoid T)) (@eq T).
Proof using . apply subrelation_refl. Qed.

#[export]Instance eq_eq_Setoid_compat (T : Type) :
  subrelation (@eq T) (@equiv T (eq_setoid T)).
Proof using . apply subrelation_refl. Qed.

Notation "x =?= y" := (equiv_dec x y) (at level 70, no associativity).

(** Lifting an equivalence relation to an option type. *)
Definition opt_eq {T} (eqT : T -> T -> Prop) (xo yo : option T) :=
  match xo, yo with
    | None, None => True
    | None, Some _ | Some _, None => False
    | Some x, Some y => eqT x y
  end.

#[export]Instance opt_eq_refl : ∀ T (R : T -> T -> Prop), Reflexive R -> Reflexive (opt_eq R).
Proof using . intros T R HR [x |]; simpl; auto. Qed.

#[export]Instance opt_eq_sym : ∀ T (R : T -> T -> Prop), Symmetric R -> Symmetric (opt_eq R).
Proof using . intros T R HR [x |] [y |]; simpl; auto. Qed.

#[export]Instance opt_eq_trans : ∀ T (R : T -> T -> Prop), Transitive R -> Transitive (opt_eq R).
Proof using . intros T R HR [x |] [y |] [z |]; simpl; intros; eauto; contradiction. Qed.

#[export]Instance opt_equiv T eqT (HeqT : @Equivalence T eqT) : Equivalence (opt_eq eqT).
Proof using . split; auto with typeclass_instances. Qed.

#[export]Instance opt_Setoid T (S : Setoid T) : Setoid (option T) := {| equiv := opt_eq equiv |}.

#[export]Instance Some_compat `(Setoid) : Proper (equiv ==> @equiv _ (opt_Setoid _)) Some.
Proof using . intros ? ? Heq. apply Heq. Qed.

Lemma Some_inj {T : Type} {ST : Setoid T} :
  injective (@equiv _ ST) (@equiv _ (opt_Setoid ST)) Some.
Proof using . intros t1 t2 H. exact H. Qed.

Lemma Some_eq_inj {T : Type} : @injective T _ eq eq Some.
Proof using . intros t1 t2 H1. inversion_clear H1. reflexivity. Qed.

Lemma not_None {T : Type} {ST : Setoid T} :
  ∀ ot : option T, ¬ (@equiv _ (opt_Setoid ST) ot None)
  -> {t : T | @equiv _ (opt_Setoid ST) ot (Some t)}.
Proof using .
  intros * H. destruct ot as [t|]. constructor 1 with t.
  reflexivity. contradict H. reflexivity.
Qed.

Lemma not_Some {T : Type} {ST : Setoid T} : ∀ ot : option T,
  (∀ t : T, ¬ (@equiv _ (opt_Setoid ST) ot (Some t)))
  -> @equiv _ (opt_Setoid ST) ot None.
Proof using .
  intros * H. destruct ot as [t|]. exfalso. apply (H t). all: reflexivity.
Qed.

Lemma not_None_iff {T : Type} {ST : Setoid T} :
  ∀ ot : option T, ¬ (@equiv _ (opt_Setoid ST) ot None)
  <-> ∃ t : T, @equiv _ (opt_Setoid ST) ot (Some t).
Proof using .
  intros. etransitivity. 2: split. 2: apply inhabited_sig_to_exists.
  2: apply exists_to_inhabited_sig. split.
  - intros H. constructor. apply not_None, H.
  - intros [[t H]] Hn. rewrite H in Hn. inversion Hn.
Qed.

Lemma option_decidable {T : Type} {ST : Setoid T} : ∀ ot : option T,
  Decidable.decidable (@equiv _ (opt_Setoid ST) ot None).
Proof using .
  intros. destruct ot as [t|]. right. intros H. inversion H. left. reflexivity.
Qed.

Lemma pick_Some_None :
  ∀ {T : Type} {ST : Setoid T} (P : T -> Prop) (oT : option T),
  (∀ t : T, @equiv _ (opt_Setoid ST) oT (Some t) <-> P t)
  -> (@equiv _ (opt_Setoid ST) oT None <-> (∀ t : T, ¬ (P t)))
  -> pick_spec P oT.
Proof using .
  intros T ST * Hs Hn. destruct oT as [t|]. apply Pick, Hs.
  2: apply Nopick, Hn. all: reflexivity.
Qed.

Lemma opt_Setoid_eq : ∀ {T : Type} {ST : Setoid T} (o1 o2 : option T),
  (∀ t1 t2 : T, @equiv _ ST t1 t2 <-> t1 = t2)
  -> @equiv _ (opt_Setoid ST) o1 o2 <-> o1 = o2.
Proof using .
  intros * H1. destruct o1 as [t1|], o2 as [t2|].
  4: split; intros; reflexivity. 2,3: split; intros H2; inversion H2.
  rewrite (injective_compat_iff Some_inj), (injective_compat_iff Some_eq_inj).
  apply H1.
Qed.

(* This Setoid could be written using RelProd in which case,
   revealing the "and" inside would be more troublesome than
   with this simple definition. *)
#[export]Instance prod_Setoid A B (SA : Setoid A) (SB : Setoid B) : Setoid (A * B).
simple refine {| equiv := fun xn yp => fst xn == fst yp /\ snd xn == snd yp |}; auto; [].
Proof. split.
+ repeat intro; now split.
+ repeat intro; split; now symmetry.
+ intros ? ? ? [? ?] [? ?]; split; etransitivity; eauto.
Defined.

#[export]Instance prod_EqDec A B (SA : Setoid A) (SB : Setoid B) (EDA : EqDec SA) (EDB : EqDec SB)
  : EqDec (prod_Setoid SA SB).
refine (fun xn yp => if equiv_dec (fst xn) (fst yp) then
                     if equiv_dec (snd xn) (snd yp) then left _ else right _
                     else right _).
Proof.
- now split.
- abstract (intros [? ?]; contradiction).
- abstract (intros [? ?]; contradiction).
Defined.

(* Local Instance fst_relprod_compat {A B} : forall R S, Proper (R * S ==> R) (@fst A B) := fst_compat. *)
(* Local Instance snd_relprod_compat {A B} : forall R S, Proper (R * S ==> S) (@snd A B) := snd_compat. *)

#[export] Instance fst_compat_pactole {A B : Type} {SA : Setoid A} {SB : Setoid B} :
  Proper (@equiv _ (prod_Setoid SA SB) ==> equiv) fst.
Proof. now intros [] [] []. Qed.

#[export] Instance snd_compat_pactole {A B : Type} {SA : Setoid A} {SB : Setoid B} :
  Proper (@equiv _ (prod_Setoid SA SB) ==> equiv) snd.
Proof. now intros [] [] []. Qed.

#[export] Instance pair_compat_pactole {A B : Type} {SA : Setoid A} {SB : Setoid B} :
  Proper (equiv ==> equiv ==> @equiv _ (prod_Setoid SA SB)) pair.
Proof. repeat intro. now split. Qed.

Lemma prod_Setoid_eq : ∀ {A B : Type} {SA : Setoid A} {SB : Setoid B}
  (p1 p2 : A * B), (∀ a1 a2 : A, @equiv _ SA a1 a2 <-> a1 = a2)
  -> (∀ b1 b2 : B, @equiv _ SB b1 b2 <-> b1 = b2)
  -> @equiv _ (prod_Setoid SA SB) p1 p2 <-> p1 = p2.
Proof using .
  intros * Ha Hb. rewrite <- pair_eqE, <- Ha, <- Hb. reflexivity.
Qed.

(* Setoid over [sig] types *)
#[export]Instance sig_Setoid {T} (S : Setoid T) {P : T -> Prop} : Setoid (sig P).
simple refine {| equiv := fun x y => proj1_sig x == proj1_sig y |}; auto; [].
Proof. split.
+ intro. reflexivity.
+ intros ? ?. now symmetry.
+ intros ? ? ? ? ?. etransitivity; eauto.
Defined.

#[export]Instance sig_EqDec {T} {S : Setoid T} (E : EqDec S) (P : T -> Prop) : EqDec (@sig_Setoid T S P).
Proof. intros ? ?. simpl. apply equiv_dec. Defined.

#[export]Instance proj1_sig_compat {T} {S : Setoid T} (E : EqDec S) (P : T -> Prop) :
  Proper (@equiv _ (sig_Setoid S) ==> equiv) (@proj1_sig T P).
Proof using . intros ?? H. apply H. Qed.

Lemma proj1_sig_inj : ∀ {T : Type} {S : Setoid T} (P : T -> Prop),
  injective (@equiv (sig P) (sig_Setoid S)) (@equiv T S) (@proj1_sig T P).
Proof using . intros * ?? H. cbn. apply H. Qed.

#[export]Instance sigT_Setoid {T} (S : Setoid T) {P : T -> Type} : Setoid (sigT P).
simple refine {| equiv := fun x y => projT1 x == projT1 y |}; auto; [].
Proof. split.
+ intro. reflexivity.
+ intros ? ?. now symmetry.
+ intros ? ? ? ? ?. etransitivity; eauto.
Defined.

#[export]Instance sigT_EqDec {T} {S : Setoid T} (E : EqDec S) (P : T -> Type) : EqDec (@sigT_Setoid T S P).
Proof. intros ? ?. simpl. apply equiv_dec. Defined.

#[export]Instance projT1_compat {T} {S : Setoid T} (E : EqDec S) (P : T -> Type) :
  Proper (@equiv _ (sigT_Setoid S) ==> equiv) (@projT1 T P).
Proof using . intros ?? H. apply H. Qed.

(** The intersection of equivalence relations is still an equivalence relation. *)
Lemma inter_equivalence T R1 R2 (E1 : Equivalence R1) (E2 : Equivalence R2)
  : Equivalence (fun x y : T => R1 x y /\ R2 x y).
Proof using . split.
+ split; reflexivity.
+ now split; symmetry.
+ intros ? ? ? [] []. split; etransitivity; eauto.
Qed.

(* TODO: set it as an instance and fix all the typeclass search loops that appear *)
Definition inter_Setoid {T} (S1 : Setoid T) (S2 : Setoid T) : Setoid T := {|
  equiv := fun x y => @equiv T S1 x y /\ @equiv T S2 x y;
  setoid_equiv := inter_equivalence setoid_equiv setoid_equiv |}.

Definition inter_EqDec {T} {S1 S2 : Setoid T} (E1 : EqDec S1) (E2 : EqDec S2)
  : EqDec (inter_Setoid S1 S2).
Proof using .
intros x y. destruct (E1 x y), (E2 x y); (now left; split) || (right; intros []; contradiction).
Defined.

Definition inter_subrelation_l : forall {T} {S1 S2 : Setoid T},
  subrelation (@equiv T (inter_Setoid S1 S2)) (@equiv T S1).
Proof using . now intros ? ? ? ? ? []. Qed.

Definition inter_subrelation_r : forall {T} {S1 S2 : Setoid T},
  subrelation (@equiv T (inter_Setoid S1 S2)) (@equiv T S2).
Proof using . now intros ? ? ? ? ? []. Qed.

Definition inter_compat_l {T U} {S1 S2 : Setoid T} `{Setoid U} : forall f : T -> U,
  Proper (@equiv T S1 ==> equiv) f -> Proper (@equiv T (inter_Setoid S1 S2) ==> equiv) f.
Proof using . intros f Hf x y Heq. apply Hf, Heq. Qed.

Definition inter_compat_r {T U} {S1 S2 : Setoid T} `{Setoid U} : forall f : T -> U,
  Proper (@equiv T S2 ==> equiv) f -> Proper (@equiv T (inter_Setoid S1 S2) ==> equiv) f.
Proof using . intros f Hf x y Heq. apply Hf, Heq. Qed.

(** Setoid by precomposition *)
Definition precompose_Equivalence T U R (E : @Equivalence U R) :
  forall f : T -> U, Equivalence (fun x y => R (f x) (f y)).
Proof using . split.
+ intro. reflexivity.
+ repeat intro. now symmetry.
+ repeat intro. now transitivity (f y).
Qed.

(* TODO: set it as an instance and fix all the typeclass search loops that appear *)
Definition precompose_Setoid {T U} (f : T -> U) {S : Setoid U} : Setoid T := {|
  equiv := fun x y => f x == f y;
  setoid_equiv := precompose_Equivalence setoid_equiv f |}.

Definition precompose_EqDec {T U} (f : T -> U) `{EqDec U}
  : EqDec (precompose_Setoid f) := fun x y => f x =?= f y.

Definition precompose_compat {T U} (f : T -> U) `{Setoid U} :
  Proper (@equiv T (precompose_Setoid f) ==> equiv) f.
Proof using . intros x y Heq. apply Heq. Qed.

(** Setoids for lists *)
Definition eqlistA_Setoid {T} (S : Setoid T) : Setoid (list T) := {|
  equiv := SetoidList.eqlistA equiv;
  setoid_equiv := SetoidList.eqlistA_equiv setoid_equiv |}.

Fixpoint eqlistA_dec {T} {eqT} {E : Equivalence eqT} (dec : forall x y : T, {eqT x y} + {~eqT x y}) l1 l2
  : {SetoidList.eqlistA eqT l1 l2} + {~ SetoidList.eqlistA eqT l1 l2}.
Proof.
refine (
  match l1, l2 with
    | nil, nil => left (reflexivity nil)
    | nil, cons e2 l2 => right _
    | cons _ _, nil => right _
    | cons e1 l1, cons e2 l2 => _
  end).
+ abstract (intro Habs; inv Habs).
+ abstract (intro Habs; inv Habs).
+ destruct (dec e1 e2); [destruct (eqlistA_dec _ _ _ dec l1 l2) |].
  - abstract (left; now constructor).
  - abstract (right; intro Habs; now inv Habs).
  - abstract (right; intro Habs; now inv Habs).
Defined.

Definition eqlistA_EqDec {T} {S : Setoid T} (E : EqDec S) : EqDec (eqlistA_Setoid S) :=
  eqlistA_dec equiv_dec.

(* Definition PermutationA_Setoid {T} {S : Setoid T} : Setoid (list T) := {|
  equiv := PermutationA equiv;
  setoid_equiv := PermutationA_Equivalence setoid_equiv |}. *)


Lemma equiv_decb_spec: forall A (SA:Setoid A) (Aeq:EqDec SA) (a b:A), (a ==b b = true) <-> a == b.
Proof.
  unfold equiv_decb.
  intros A SA Aeq a b.
  destruct (a =?= b).
  - tauto.
  - cbn in c.
    intuition.
Qed. 

Corollary equiv_decb_false A (SA:Setoid A) (Aeq:EqDec SA) : forall a b : A,(a ==b b = false) <-> a =/= b.
Proof.
intros. unfold complement.
rewrite <- equiv_decb_spec with (Aeq := Aeq), <- Bool.not_true_iff_false.
tauto.
Qed.
