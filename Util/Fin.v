Require Import Utf8 Arith RelationPairs SetoidDec.
Require Import Lia.
Require Import Pactole.Util.Preliminary.
Require Import Pactole.Util.SetoidDefs.
Require Import Pactole.Util.NumberComplements.
Require Import Pactole.Util.Bijection.
Set Implicit Arguments.
Unset Strict Implicit.
Unset Printing Implicit Defensive.

(*
 *
 *            The type fin u with u any nat ≠ 0
 *            consisting of the nats smaller than u
 *
 *)

Section Fin.

Context (u : nat).

Inductive fin : Type :=  Fin : ∀ j : nat, j < u -> fin.

Global Instance fin_Setoid : Setoid fin := eq_setoid fin.

Lemma fin_dec : ∀ f1 f2 : fin, {f1 = f2} + {f1 <> f2}.
Proof using .
  intros [j1 H1] [j2 H2]. destruct (Nat.eq_dec j1 j2).
  - subst. left. f_equal. apply le_unique.
  - right. intro Habs. inv Habs. auto.
Qed.

Global Instance fin_EqDec : EqDec fin_Setoid := fin_dec.

Global Coercion fin2nat (f : fin) : nat := match f with @Fin m _ => m end.

Definition fin2nat_compat : Proper (equiv ==> equiv) fin2nat := _.

Lemma fin_lt : ∀ f : fin, f < u.
Proof using . intros. destruct f as [j p]. exact p. Qed.

Lemma fin_le : ∀ f : fin, f <= Nat.pred u.
Proof using . intros. apply Nat.lt_le_pred, fin_lt. Qed.

Lemma fin_between : ∀ f : fin, 0 <= f < u.
Proof using . intros. split. apply Nat.le_0_l. apply fin_lt. Qed.

Lemma fin2natI : Preliminary.injective equiv equiv fin2nat.
Proof using .
  intros [j1 H1] [j2 H2] Heq. cbn in Heq. subst. cbn. f_equal. apply le_unique.
Qed.

Lemma lt_gt_cases : ∀ f1 f2 : fin, f1 ≠ f2 <-> f1 < f2 ∨ f2 < f1.
Proof using .
  intros. erewrite not_iff_compat. apply Nat.lt_gt_cases.
  symmetry. apply (injective_eq_iff fin2natI).
Qed.

Lemma le_lt_eq_dec : ∀ f1 f2 : fin, f1 <= f2 -> {f1 < f2} + {f1 = f2}.
Proof using .
  intros * H. destruct (le_lt_eq_dec f1 f2 H) as [Hd | Hd].
  left. apply Hd. right. apply fin2natI, Hd.
Qed.

Lemma lt_eq_lt_dec : ∀ f1 f2 : fin, {f1 < f2} + {f1 = f2} + {f2 < f1}.
Proof using .
  intros. destruct (lt_eq_lt_dec f1 f2) as [[Hd | Hd] | Hd].
  left. left. apply Hd. left. right. apply fin2natI, Hd. right. apply Hd.
Qed.

End Fin.

Section mod2fin.

Context {l u : nat} {ltc_l_u : l <c u}.

(*
 *
 *            The fin u whose underlying nat is 0
 *
 *)

Definition fin0 : fin u := Fin lt_0_u.

Lemma fin02nat : fin0 = 0 :> nat.
Proof using . reflexivity. Qed.

Lemma all_fin0 : ∀ f : fin u, u = 1 -> f = fin0.
Proof using .
  intros f H. apply fin2natI. rewrite fin02nat. subst.
  pose proof (fin_lt f) as H. inversion H as [H0 | n H0 H1].
  apply H0. inversion H0.
Qed.

Lemma all_eq : ∀ f1 f2 : fin u, u = 1 -> f1 = f2.
Proof using ltc_l_u.
  intros * H. rewrite (all_fin0 f1), (all_fin0 f2). reflexivity. all: apply H.
Qed.

Lemma fin0_le : ∀ f : fin u, fin0 <= f.
Proof using . intros. rewrite fin02nat. apply Nat.le_0_l. Qed.

Lemma neq_fin0_lt_fin0 : ∀ f : fin u, f ≠ fin0 <-> fin0 < f.
Proof using .
  intros. rewrite <- Nat.neq_0_lt_0, <- fin02nat. symmetry.
  apply not_iff_compat, injective_eq_iff, fin2natI.
Qed.

Lemma eq_fin0_lt_dec : ∀ f : fin u, {f = fin0} + {fin0 < f}.
Proof using .
  intros. destruct (eq_0_lt_dec f) as [Hd | Hd].
  left. apply fin2natI, Hd. right. apply Hd.
Qed.

Lemma le_fin0 : ∀ f : fin u, f <= fin0 <-> f = fin0.
Proof using .
  intros. rewrite fin02nat, Nat.le_0_r, <- fin02nat.
  apply injective_eq_iff, fin2natI.
Qed.

Lemma lt_fin0 : ∀ f1 f2 : fin u, f1 < f2 -> fin0 < f2.
Proof using . intros * H. eapply Nat.lt_lt_0, H. Qed.

(*
 *
 *            The fin u whose underlying nat is Nat.pred u
 *
 *)

Definition fin_max : fin u := Fin lt_pred_u.

Lemma fin_max2nat : fin_max = Nat.pred u :> nat.
Proof using . reflexivity. Qed.

Lemma le_max : ∀ f : fin u, f <= fin_max.
Proof using .
  intros. apply Nat.lt_succ_r. rewrite fin_max2nat, S_pred_u. apply fin_lt.
Qed.

Lemma max_le : ∀ f : fin u, fin_max <= f <-> f = fin_max.
Proof using .
  intros. rewrite Nat.le_lteq, Nat.lt_nge. etransitivity. split.
  - intros [H | H]. contradict H. apply le_max. apply H.
  - intros H. right. apply H.
  - rewrite Nat.eq_sym_iff. apply injective_eq_iff, fin2natI.
Qed.

Lemma neq_max_lt_max : ∀ f : fin u, f ≠ fin_max <-> f < fin_max.
Proof using . intros. rewrite <- max_le. apply Nat.nle_gt. Qed.

Lemma eq_max_lt_dec : ∀ f : fin u, {f = fin_max} + {f < fin_max}.
Proof using .
  intros. destruct (fin_dec f fin_max) as [Hd | Hd].
  left. apply Hd. right. apply neq_max_lt_max, Hd.
Qed.

Lemma max_lt : ∀ f1 f2 : fin u, f2 < f1 -> f2 < fin_max.
Proof using .
  intros * H. rewrite Nat.lt_nge, max_le. intros Ha.
  subst. eapply Nat.lt_nge. apply H. apply le_max.
Qed.

(*
 *
 *            Building the fin u whose underlying nat
 *            is j mod u with j the input of the function
 *
 *)

Lemma mod2fin_lt : ∀ j : nat, j mod u < u.
Proof using ltc_l_u. intros. apply Nat.mod_upper_bound, neq_u_0. Qed.

Lemma mod2fin_le : ∀ j : nat, j mod u <= Nat.pred u.
Proof using ltc_l_u. intros. apply Nat.lt_le_pred, mod2fin_lt. Qed.

Definition mod2fin (j : nat) : fin u := Fin (mod2fin_lt j).

Definition mod2fin_compat : Proper (equiv ==> equiv) mod2fin := _.

Lemma mod2fin2nat : ∀ j : nat, mod2fin j = j mod u :> nat.
Proof using . intros. reflexivity. Qed.

Lemma mod2fin_mod : ∀ j : nat, mod2fin (j mod u) = mod2fin j.
Proof using .
  intros. apply fin2natI. rewrite 2 mod2fin2nat. apply Nat.Div0.mod_mod.
Qed.

Lemma mod2fin_mod2fin : ∀ j : nat, mod2fin (mod2fin j) = mod2fin j.
Proof using . intros. rewrite mod2fin2nat. apply mod2fin_mod. Qed.

Lemma mod2finK : ∀ f : fin u, mod2fin f = f.
Proof using . intros. apply fin2natI, Nat.mod_small, fin_lt. Qed.

Lemma mod2fin_small : ∀ j : nat, j < u -> mod2fin j = j :> nat.
Proof using . intros * H. rewrite mod2fin2nat. apply Nat.mod_small, H. Qed.

Lemma mod2fin_bounded_diffI : ∀ j1 j2 : nat, j1 <= j2
  -> j2 - j1 < u -> mod2fin j1 = mod2fin j2 -> j1 = j2.
Proof using .
  intros * Hleq Hsub Heq. eapply mod_bounded_diffI. apply neq_u_0.
  exact Hleq. exact Hsub. rewrite <-2 mod2fin2nat, Heq. reflexivity.
Qed.

Lemma mod2fin_betweenI : ∀ p j1 j2 : nat, p <= j1 < p + u
  -> p <= j2 < p + u -> mod2fin j1 = mod2fin j2 -> j1 = j2.
Proof using .
  intros p. eapply (bounded_diff_between _
  (inj_sym _ _ nat_Setoid (eq_setoid (fin u)) mod2fin)).
  intros * H1 H2. apply mod2fin_bounded_diffI. all: assumption.
Qed.

Lemma mod2fin_muln : ∀ j : nat, mod2fin (u * j) = fin0.
Proof using .
  intros. apply fin2natI. rewrite mod2fin2nat, fin02nat, Nat.mul_comm.
  apply Nat.Div0.mod_mul.
Qed.

Lemma mod2fin_le_between_compat : ∀ p j1 j2 : nat, u * p <= j1 < u * p + u
  -> u * p <= j2 < u * p + u -> j1 <= j2 -> mod2fin j1 <= mod2fin j2.
Proof using .
  intros * Hb1 Hb2 Hle. rewrite 2 mod2fin2nat.
  eapply mod_le_between_compat. all: eassumption.
Qed.

Lemma mod2fin_lt_between_compat : ∀ p j1 j2 : nat, u * p <= j1 < u * p + u
  -> u * p <= j2 < u * p + u -> j1 < j2 -> mod2fin j1 < mod2fin j2.
Proof using .
  intros * Hb1 Hb2 Hlt. rewrite 2 mod2fin2nat.
  eapply mod_lt_between_compat. all: eassumption.
Qed.

Lemma addn_mod2fin_idemp_l : ∀ j1 j2 : nat,
  mod2fin (mod2fin j1 + j2) = mod2fin (j1 + j2).
Proof using .
  intros. apply fin2natI. rewrite 3 mod2fin2nat.
  apply Nat.Div0.add_mod_idemp_l.
Qed.

Lemma addn_mod2fin_idemp_r : ∀ j1 j2 : nat,
  mod2fin (j1 + mod2fin j2) = mod2fin (j1 + j2).
Proof using .
  intros. apply fin2natI. rewrite 3 mod2fin2nat.
  apply Nat.Div0.add_mod_idemp_r.
Qed.

Lemma divide_fin0_mod2fin : ∀ j : nat, Nat.divide u j -> mod2fin j = fin0.
Proof using .
  intros * H. apply fin2natI. rewrite mod2fin2nat.
  apply Nat.Lcm0.mod_divide, H.
Qed.

Lemma mod2fin0 : mod2fin 0 = fin0.
Proof using . apply divide_fin0_mod2fin, Nat.divide_0_r. Qed.

Lemma mod2fin_max : mod2fin (Nat.pred u) = fin_max.
Proof using . apply fin2natI, mod2fin_small, fin_lt. Qed.

Lemma mod2fin_u : mod2fin u = fin0.
Proof using . apply divide_fin0_mod2fin, Nat.divide_refl. Qed.

(*
 *
 *            The successor of fin0
 *
 *)

Definition fin1 : fin u := mod2fin 1.

Lemma fin1E : fin1 = mod2fin 1.
Proof using . reflexivity. Qed.

Lemma fin12nat {ltc_1_u : 1 <c u} : fin1 = 1 :> nat.
Proof using . rewrite fin1E. apply mod2fin_small, ltc_1_u. Qed.

(*
 *
 *            Adding either a nat (addm) or
 *            a fin u (addf) to a fin u
 *
 *)

Definition addm (f : fin u) (j : nat) : fin u := mod2fin (f + j).

Definition addm_compat : Proper (equiv ==> equiv ==> equiv) addm := _.

Lemma addmE : ∀ (f : fin u) (j : nat), addm f j = mod2fin (f + j).
Proof using . reflexivity. Qed.

Lemma addm2nat : ∀ (f : fin u) (j : nat), addm f j = (f + j) mod u :> nat.
Proof using . intros. rewrite addmE. apply mod2fin2nat. Qed.

Lemma addm_mod : ∀ (f : fin u) (j : nat), addm f (j mod u) = addm f j.
Proof using .
  intros. rewrite 2 addmE, <- mod2fin2nat. apply addn_mod2fin_idemp_r.
Qed.

Lemma addm_mod2fin : ∀ (f : fin u) (j : nat), addm f (mod2fin j) = addm f j.
Proof using . intros. rewrite mod2fin2nat. apply addm_mod. Qed.

Definition addf (f1 f2 : fin u) : fin u := addm f1 f2.

Definition addf_compat : Proper (equiv ==> equiv ==> equiv) addf := _.

Lemma addfE : ∀ f1 f2 : fin u, addf f1 f2 = mod2fin (f1 + f2).
Proof using . reflexivity. Qed.

Lemma addf2nat : ∀ f1 f2 : fin u, addf f1 f2 = (f1 + f2) mod u :> nat.
Proof using . intros. apply addm2nat. Qed.

Lemma addf_addm : ∀ f1 f2 : fin u, addf f1 f2 = addm f1 f2.
Proof using . reflexivity. Qed.

Lemma addm_addf : ∀ (f : fin u) (j : nat), addm f j = addf f (mod2fin j).
Proof using . intros. rewrite addf_addm. symmetry. apply addm_mod2fin. Qed.

Lemma addfC : ∀ f1 f2 : fin u, addf f1 f2 = addf f2 f1.
Proof using . intros. rewrite 2 addfE, Nat.add_comm. reflexivity. Qed.

Lemma addmC : ∀ (f : fin u) (j : nat), addm f j = addm (mod2fin j) f.
Proof using . intros. rewrite 2 addm_addf, mod2finK. apply addfC. Qed.

Lemma addmA : ∀ (f1 f2 : fin u) (j : nat),
  addm f1 (addm f2 j) = addm (addm f1 f2) j.
Proof using .
  intros. rewrite 4 addmE, addn_mod2fin_idemp_l,
  addn_mod2fin_idemp_r, Nat.add_assoc. reflexivity.
Qed.

Lemma addfA : ∀ f1 f2 f3 : fin u,
  addf f1 (addf f2 f3) = addf (addf f1 f2) f3.
Proof using . intros. rewrite 2 (addf_addm _ f3). apply addmA. Qed.

Lemma addmAC : ∀ (f : fin u) (j1 j2 : nat),
  addm (addm f j1) j2 = addm (addm f j2) j1.
Proof using .
  intros. rewrite 4 addm_addf, <-2 addfA, (addfC (mod2fin _)). reflexivity.
Qed.

Lemma addfAC : ∀ f1 f2 f3 : fin u,
  addf (addf f1 f2) f3 = addf (addf f1 f3) f2.
Proof using . intros. rewrite 4 addf_addm. apply addmAC. Qed.

Lemma addfCA : ∀ f1 f2 f3 : fin u,
  addf f1 (addf f2 f3) = addf f2 (addf f1 f3).
Proof using .
  intros. rewrite (addfC f2 f3), addfA, (addfC (addf _ _) _). reflexivity.
Qed.

Lemma addmCA : ∀ (f1 f2 : fin u) (j : nat),
  addm f1 (addm f2 j) = addm f2 (addm f1 j).
Proof using .
  intros. rewrite 2 (addm_addf _ j), <-2 addf_addm. apply addfCA.
Qed.

Lemma addfACA : ∀ f1 f2 f3 f4 : fin u,
  addf (addf f1 f2) (addf f3 f4) = addf (addf f1 f3) (addf f2 f4).
Proof using . intros. rewrite 2 addfA, (addfAC f1 f3 f2). reflexivity. Qed.

Lemma addmACA : ∀ (f1 f2 f3 : fin u) (j : nat),
  addm (addm f1 f2) (addm f3 j) = addm (addm f1 f3) (addm f2 j).
Proof using .
  intros. rewrite 2 (addm_addf _ j), <- 4 addf_addm. apply addfACA.
Qed.

Lemma addIm : ∀ j : nat,
  Preliminary.injective equiv equiv (λ f : fin u, addm f j).
Proof using .
  intros * f1 f2 H. eapply fin2natI, (Nat.add_cancel_r _ _ j), mod2fin_betweenI.
  1,2: rewrite Nat.add_comm. 1,2: split. 1,3: erewrite <- Nat.add_le_mono_l.
  1,2: apply Nat.le_0_l. 1,2: rewrite Nat.add_0_r, <- Nat.add_lt_mono_l.
  1,2: apply fin_lt. apply H.
Qed.

Lemma addm_bounded_diffI :
  ∀ (f : fin u) (j1 j2 : nat), j1 <= j2 -> j2 - j1 < u
  -> addm f j1 = addm f j2 -> j1 = j2.
Proof using .
  intros * Hle Hsu H. rewrite 2 addmE in H.
  eapply Nat.add_cancel_l, mod2fin_bounded_diffI; try eassumption; lia.
Qed.

Lemma addm_betweenI : ∀ (p : nat) (f : fin u) (j1 j2 : nat),
  p <= j1 < p + u -> p <= j2 < p + u -> addm f j1 = addm f j2 -> j1 = j2.
Proof using .
  intros p f. eapply (bounded_diff_between _
  (inj_sym _ _ nat_Setoid (eq_setoid (fin u)) (λ x, addm f x))).
  intros *. apply addm_bounded_diffI.
Qed.

Lemma addIf : ∀ f1 : fin u,
  Preliminary.injective equiv equiv (λ f2, addf f2 f1).
Proof using . intros f1 f2 f3. rewrite 2 addf_addm. apply addIm. Qed.

Lemma addfI : ∀ f1 : fin u, Preliminary.injective equiv equiv (addf f1).
Proof using .
  intros f1 f2 f3 H. eapply addIf. setoid_rewrite addfC. apply H.
Qed.

Lemma addm0 : ∀ f : fin u, addm f 0 = f.
Proof using . intros. rewrite addmE, Nat.add_0_r. apply mod2finK. Qed.

Lemma add0m : ∀ j : nat, addm fin0 j = mod2fin j.
Proof using . intros. rewrite addmE, Nat.add_0_l. reflexivity. Qed.

Lemma add0f : ∀ f : fin u, addf fin0 f = f.
Proof using . intros. rewrite addf_addm, add0m. apply mod2finK. Qed.

Lemma addf0 : ∀ f : fin u, addf f fin0 = f.
Proof using . intros. rewrite addf_addm. apply addm0. Qed.

(*
 *
 *            The successor of a fin u
 *
 *)

Definition sucf (f : fin u) : fin u := addf f fin1.

Definition sucf_compat : Proper (equiv ==> equiv) sucf := _.

Lemma sucfE : ∀ f : fin u, sucf f = addf f fin1.
Proof using . reflexivity. Qed.

Lemma sucfEmod : ∀ f : fin u, sucf f = mod2fin (S f).
Proof using .
  intros. destruct (@eq_S_l_lt_dec 0 u _) as [Hd | Hd]. apply all_eq, Hd.
  unshelve erewrite sucfE, addfE, fin12nat, Nat.add_1_r. apply Hd. reflexivity.
Qed.

Lemma sucfI : Preliminary.injective equiv equiv sucf.
Proof using . apply addIf. Qed.

Lemma mod2fin_S_sucf : ∀ j : nat, mod2fin (S j) = sucf (mod2fin j).
Proof using .
  intros. rewrite sucfE, addfE, fin1E, addn_mod2fin_idemp_l,
  addn_mod2fin_idemp_r, Nat.add_1_r. reflexivity.
Qed.

Lemma sucf_max : sucf fin_max = fin0.
Proof using . rewrite sucfEmod, fin_max2nat, S_pred_u. apply mod2fin_u. Qed.

Lemma sucf_addf : ∀ f1 f2 : fin u, sucf (addf f1 f2) = addf (sucf f1) f2.
Proof using . intros. rewrite 2 sucfE. apply addfAC. Qed.

Lemma sucf_addm : ∀ (f : fin u) (j : nat), sucf (addm f j) = addm (sucf f) j.
Proof using . intros. rewrite 2 addm_addf. apply sucf_addf. Qed.

Lemma addf_sucf : ∀ f1 f2 : fin u, addf (sucf f1) f2 = addf f1 (sucf f2).
Proof using .
  intros. rewrite (addfC f1), <-2 sucf_addf, addfC. reflexivity.
Qed.

Lemma addf_mod2fin_S : ∀ j1 j2 : nat,
  addf (mod2fin (S j1)) (mod2fin j2) = addf (mod2fin j1) (mod2fin (S j2)).
Proof using .
  intros. rewrite mod2fin_S_sucf, addf_sucf, mod2fin_S_sucf. reflexivity.
Qed.

Lemma S_sucf : ∀ f : fin u, f < fin_max -> S f = sucf f.
Proof using .
  intros * H. symmetry. rewrite sucfEmod.
  apply mod2fin_small, Nat.lt_succ_lt_pred, H.
Qed.

Lemma lt_sucf : ∀ f : fin u, f < fin_max -> f < sucf f.
Proof using .
  intros * H. rewrite <- S_sucf. apply Nat.lt_succ_diag_r. apply H.
Qed.

Lemma fin1_sucf_fin0 : fin1 = sucf fin0.
Proof using . rewrite sucfEmod, fin02nat. apply fin1E. Qed.

Lemma lt_sucf_le : ∀ f1 f2 : fin u, f1 < sucf f2 -> f1 <= f2.
Proof using .
  intros * H1. apply Nat.lt_succ_r. rewrite S_sucf.
  apply H1. apply neq_max_lt_max. intros H2. subst.
  eapply Nat.lt_irrefl, lt_fin0. rewrite <- sucf_max. apply H1.
Qed.

(*
 *
 *            The complement to fin_max of either j mod u
 *            (revm whose input is a nat j) or of a fin u (revf)
 *
 *)

Lemma revf_subproof : ∀ f : fin u, Nat.pred u - f < u.
Proof using ltc_l_u.
  intros. eapply Nat.le_lt_trans. apply Nat.le_sub_l. apply lt_pred_u.
Qed.

Definition revf (f : fin u) : fin u := Fin (revf_subproof f).

Definition revf_compat : Proper (equiv ==> equiv) revf := _.

Lemma revf2nat : ∀ f : fin u, revf f = Nat.pred u - f :> nat.
Proof using . reflexivity. Qed.

Lemma revfK : ∀ f : fin u, revf (revf f) = f.
Proof using .
intros. apply fin2natI. rewrite 2 revf2nat, sub_sub.
- hnf. lia.
- apply fin_le.
Qed.

Lemma revfI : Preliminary.injective equiv equiv revf.
Proof using .
  intros f1 f2 H. setoid_rewrite <- revfK. rewrite H. reflexivity.
Qed.

Definition revm (j : nat) : fin u := revf (mod2fin j).

Definition revm_compat : Proper (equiv ==> equiv) revm := _.

Lemma revm_revf : ∀ j : nat, revm j = revf (mod2fin j).
Proof using . reflexivity. Qed.

Lemma revf_revm : ∀ f : fin u, revf f = revm f.
Proof using . intros. rewrite revm_revf, mod2finK. reflexivity. Qed.

Lemma revm2nat : ∀ j : nat, revm j = Nat.pred u - (j mod u) :> nat.
Proof using .
  intros. rewrite revm_revf, revf2nat, mod2fin2nat. reflexivity.
Qed.

Lemma revm_mod : ∀ j : nat, revm (j mod u) = revm j.
Proof using . intros. rewrite 2 revm_revf, mod2fin_mod. reflexivity. Qed.

Lemma revm_mod2fin : ∀ j : nat, revm (mod2fin j) = revm j.
Proof using . intros. rewrite mod2fin2nat. apply revm_mod. Qed.

Lemma revmK : ∀ j : nat, revm (revm j) = mod2fin j.
Proof using . intros. rewrite 2 revm_revf, mod2finK. apply revfK. Qed.

Lemma revm_bounded_diffI :
  ∀ j1 j2 : nat, j1 <= j2 -> j2 - j1 < u -> revm j1 = revm j2 -> j1 = j2.
Proof using .
  intros. eapply mod_bounded_diffI, sub_cancel_l;
  try apply mod2fin_lt || apply neq_u_0; trivial; [].
  rewrite <- (Nat.lt_succ_pred l u) at 1 3.
  rewrite <- Nat.add_1_r, 2 Nat.add_sub_swap, <-2 revm2nat, H1; trivial;
  apply Nat.lt_le_pred, mod2fin_lt. apply lt_l_u.
Qed.

Lemma revm_betweenI : ∀ p j1 j2 : nat,
  p <= j1 < p + u -> p <= j2 < p + u -> revm j1 = revm j2 -> j1 = j2.
Proof using .
  rewrite <- bounded_diff_between. apply revm_bounded_diffI.
  apply (@inj_sym _ _ (eq_setoid _) (eq_setoid _)).
Qed.

Lemma revm0 : revm 0 = fin_max.
Proof using .
  apply fin2natI. rewrite revm2nat, fin_max2nat, Nat.Div0.mod_0_l.
  apply Nat.sub_0_r.
Qed.

Lemma revf0 : revf fin0 = fin_max.
Proof using . rewrite revf_revm, fin02nat. apply revm0. Qed.

Lemma revf_le_compat : ∀ f1 f2 : fin u, f1 <= f2 -> revf f2 <= revf f1.
Proof using . intros * H. rewrite 2 revf2nat. apply Nat.sub_le_mono_l, H. Qed.

Lemma revf_lt_compat : ∀ f1 f2 : fin u, f1 < f2 -> revf f2 < revf f1.
Proof using .
  intros * H. rewrite 2 revf2nat. apply sub_lt_mono_l. apply fin_le. apply H.
Qed.

Lemma revm_le_between_compat : ∀ p j1 j2 : nat, u * p <= j1 < u * p + u
  -> u * p <= j2 < u * p + u -> j1 <= j2 -> revm j2 <= revm j1.
Proof using .
  intros * Hb1 Hb2 Hle. rewrite 2 revm2nat. eapply Nat.sub_le_mono_l,
  mod_le_between_compat. all: eassumption.
Qed.

Lemma revm_lt_between_compat : ∀ p j1 j2 : nat, u * p <= j1 < u * p + u
  -> u * p <= j2 < u * p + u -> j1 < j2 -> revm j2 < revm j1.
Proof using .
  intros * Hb1 Hb2 Hle. rewrite 2 revm2nat. apply sub_lt_mono_l.
  apply mod2fin_le. eapply mod_lt_between_compat. all: eassumption.
Qed.

Lemma revf_fin_max : revf fin_max = fin0.
Proof using . symmetry. apply revfI. rewrite revfK. apply revf0. Qed.

Lemma revm_fin_max : revm fin_max = fin0.
Proof using . rewrite <- revf_revm. apply revf_fin_max. Qed.

Lemma addf_revf : ∀ f : fin u, addf (revf f) f = fin_max.
Proof using .
  intros. rewrite addfE, revf2nat, Nat.sub_add. apply mod2fin_max. apply fin_le.
Qed.

(*
 *
 *            The complement to fin0 of either j mod u
 *            (oppm whose input is a nat j) or of a fin u (oppf)
 *
 *)

Definition oppf (f : fin u) : fin u := sucf (revf f).

Definition oppf_compat : Proper (equiv ==> equiv) oppf := _.

Lemma oppfE : ∀ f : fin u, oppf f = sucf (revf f).
Proof using . reflexivity. Qed.

Lemma oppfEmod : ∀ f : fin u, oppf f = mod2fin (u - f).
Proof using .
  intros. rewrite oppfE, sucfEmod, revf2nat, <- Nat.sub_succ_l, S_pred_u;
  auto using fin_le.
Qed.

Lemma oppf2nat : ∀ f : fin u, oppf f = (u - f) mod u :> nat.
Proof using . intros. rewrite oppfEmod. apply mod2fin2nat. Qed.

Definition oppm (j : nat) : fin u := oppf (mod2fin j).

Definition oppm_compat : Proper (equiv ==> equiv) oppm := _.

Lemma oppm_oppf : ∀ j : nat, oppm j = oppf (mod2fin j).
Proof using . reflexivity. Qed.

Lemma oppf_oppm : ∀ f : fin u, oppf f = oppm f.
Proof using . intros. rewrite oppm_oppf, mod2finK. reflexivity. Qed.

Lemma oppmE : ∀ j : nat, oppm j = sucf (revm j).
Proof using . intros. rewrite oppm_oppf, oppfE, revm_revf. reflexivity. Qed.

Lemma oppmEmod : ∀ j : nat, oppm j = mod2fin (u - (j mod u)).
Proof using .
  intros. rewrite oppm_oppf, oppfEmod, mod2fin2nat. reflexivity.
Qed.

Lemma oppm2nat : ∀ j : nat, oppm j = (u - (j mod u)) mod u :> nat.
Proof using . intros. rewrite oppmEmod. apply mod2fin2nat. Qed.

Lemma oppm_mod : ∀ j : nat, oppm (j mod u) = oppm j.
Proof using . intros. rewrite 2 oppm_oppf, mod2fin_mod. reflexivity. Qed.

Lemma addfKoppf : ∀ f : fin u, addf (oppf f) f = fin0.
Proof using .
  intros. rewrite oppfE, <- sucf_addf, addf_revf. apply sucf_max.
Qed.

Lemma addmKoppm : ∀ j : nat, addm (oppm j) j = fin0.
Proof using . intros. rewrite oppm_oppf, addm_addf. apply addfKoppf. Qed.

Lemma oppfKaddf : ∀ f : fin u, addf f (oppf f) = fin0.
Proof using . intros. rewrite addfC. apply addfKoppf. Qed.

Lemma addfOoppf : ∀ f1 f2 : fin u, addf (oppf f1) (addf f1 f2) = f2.
Proof using . intros. rewrite addfA, addfKoppf, add0f. reflexivity. Qed.

Lemma oppfOaddf : ∀ f1 f2 : fin u, addf (addf f2 f1) (oppf f1) = f2.
Proof using . intros. rewrite addfC, (addfC f2 f1). apply addfOoppf. Qed.

Lemma addfOVoppf : ∀ f1 f2 : fin u, addf f1 (addf (oppf f1) f2) = f2.
Proof using . intros. rewrite (addfC _ f2), addfCA, oppfKaddf. apply addf0. Qed.

Lemma oppfOVaddf : ∀ f1 f2 : fin u, addf (addf f2 (oppf f1)) f1 = f2.
Proof using . intros. rewrite addfC, (addfC f2 _). apply addfOVoppf. Qed.

Lemma oppmOVaddm : ∀ (f : fin u) (j : nat), addm (addf f (oppm j)) j = f.
Proof using . intros. rewrite oppm_oppf, addm_addf. apply oppfOVaddf. Qed.

Lemma oppfI : Preliminary.injective equiv equiv oppf.
Proof using . intros f1 f2 H. eapply revfI, sucfI, H. Qed.

Lemma oppm_bounded_diffI : ∀ j1 j2 : nat, j1 <= j2
  -> j2 - j1 < u -> oppm j1 = oppm j2 -> j1 = j2.
Proof using .
  intros * Hleq Hsub H. rewrite 2 oppmE in H.
  eapply revm_bounded_diffI, sucfI. all: eassumption.
Qed.

Lemma oppm_betweenI : ∀ (p : nat) (j1 j2 : nat),
  p <= j1 < p + u -> p <= j2 < p + u -> oppm j1 = oppm j2 -> j1 = j2.
Proof using .
  rewrite <- bounded_diff_between. apply oppm_bounded_diffI.
  apply (@inj_sym _ _ (eq_setoid _) (eq_setoid _)).
Qed.

Lemma oppf0 : oppf fin0 = fin0.
Proof using . rewrite oppfE, revf0. apply sucf_max. Qed.

Lemma oppm0 : oppm 0 = fin0.
Proof using . rewrite oppm_oppf, mod2fin0. apply oppf0. Qed.

Lemma addn_oppf : ∀ f : fin u, fin0 < f -> f + oppf f = u.
Proof using .
intros * H. rewrite oppf2nat, Nat.mod_small, Nat.add_comm, Nat.sub_add.
- reflexivity.
- apply Nat.lt_le_incl, fin_lt.
- apply lt_sub_u, H.
Qed.

Lemma divide_addn_oppf : ∀ f : fin u, Nat.divide u (f + oppf f).
Proof using .
  intros. destruct (eq_fin0_lt_dec f) as [Hd | Hd]. subst. rewrite oppf0,
  Nat.add_0_r. apply Nat.divide_0_r. rewrite addn_oppf. reflexivity. apply Hd.
Qed.

Lemma divide_addn_oppm : ∀ j : nat, Nat.divide u (j + oppm j).
Proof using .
  intros. rewrite (Nat.div_mod j) at 1. rewrite <- Nat.add_assoc,
  <- mod2fin2nat, oppm_oppf. apply Nat.divide_add_r. apply Nat.divide_factor_l.
  apply divide_addn_oppf. apply neq_u_0.
Qed.

(*
 *
 *            Substracting either a nat (subm) or
 *            a fin u (subf) to a fin u
 *
 *)

Definition subm (f : fin u) (j : nat) : fin u := addm f (oppm j).

Definition subm_compat : Proper (equiv ==> equiv ==> equiv) subm := _.

Definition subf (f1 f2 : fin u) : fin u := addf f1 (oppf f2).

Definition subf_compat : Proper (equiv ==> equiv ==> equiv) subf := _.

Lemma subfE : ∀ f1 f2 : fin u, subf f1 f2 = addf f1 (oppf f2).
Proof using . reflexivity. Qed.

Lemma submE : ∀ (f : fin u) (j : nat), subm f j = addf f (oppm j).
Proof using . reflexivity. Qed.

Lemma subf_subm : ∀ f1 f2 : fin u, subf f1 f2 = subm f1 f2.
Proof using . intros. rewrite submE, subfE, oppf_oppm. reflexivity. Qed.

Lemma subm_subf : ∀ (f : fin u) (j : nat), subm f j = subf f (mod2fin j).
Proof using . intros. rewrite submE, subfE, oppm_oppf. reflexivity. Qed.

Lemma submEmod : ∀ (f : fin u) (j : nat),
  subm f j = mod2fin (f + (u - (j mod u))).
Proof using .
  intros. rewrite submE, addfE, oppm2nat,
  <- mod2fin2nat. apply addn_mod2fin_idemp_r.
Qed.

Lemma subm2nat : ∀ (f : fin u) (j : nat),
  subm f j = (f + (u - (j mod u))) mod u :> nat.
Proof using . intros. rewrite submEmod. apply mod2fin2nat. Qed.

Lemma subfEmod : ∀ f1 f2 : fin u, subf f1 f2 = mod2fin (f1 + (u - f2)).
Proof using .
  intros. rewrite subf_subm, submEmod, Nat.mod_small. reflexivity. apply fin_lt.
Qed.

Lemma subf2nat : ∀ f1 f2 : fin u,
  subf f1 f2 = (f1 + (u - f2)) mod u :> nat.
Proof using . intros. rewrite subfEmod. apply mod2fin2nat. Qed.

Lemma subm_mod : ∀ (f : fin u) (j : nat), subm f (j mod u) = subm f j.
Proof using . intros. rewrite 2 subm_subf, mod2fin_mod. reflexivity. Qed.

Lemma subm_mod2fin : ∀ (f : fin u) (j : nat), subm f (mod2fin j) = subm f j.
Proof using . intros. rewrite mod2fin2nat. apply subm_mod. Qed.

Lemma subIf : ∀ f1 : fin u,
  Preliminary.injective equiv equiv (λ f2, subf f2 f1).
Proof using . intros f1 f2 f3 H. eapply addIf, H. Qed.

Lemma subfI : ∀ f1 : fin u,
  Preliminary.injective equiv equiv (subf f1).
Proof using . intros f1 f2 f3 H. eapply oppfI, addfI, H. Qed.

Lemma subIm : ∀ j : nat,
  Preliminary.injective equiv equiv (λ f, subm f j).
Proof using . intros j f1 f2. rewrite 2 subm_subf. apply subIf. Qed.

Lemma subm_bounded_diffI : ∀ (f : fin u) (j1 j2 : nat),
  j1 <= j2 -> j2 - j1 < u -> subm f j1 = subm f j2 -> j1 = j2.
Proof using .
  intros * Hle Hsu H. rewrite 2 submE in H.
  eapply oppm_bounded_diffI, addfI. all: eassumption.
Qed.

Lemma subm_betweenI : ∀ (p : nat) (f : fin u) (j1 j2 : nat),
  p <= j1 < p + u -> p <= j2 < p + u -> subm f j1 = subm f j2 -> j1 = j2.
Proof using .
  intros p f. eapply (bounded_diff_between _
  (inj_sym _ _ nat_Setoid (eq_setoid (fin u)) (λ x, subm f x))).
  intros *. apply subm_bounded_diffI.
Qed.

Lemma sub0f : ∀ f : fin u, subf fin0 f = oppf f.
Proof using . intros. rewrite subfE. apply add0f. Qed.

Lemma sub0m : ∀ j : nat, subm fin0 j = oppm j.
Proof using . intros. rewrite submE. apply add0f. Qed.

Lemma subf0 : ∀ f : fin u, subf f fin0 = f.
Proof using . intros. rewrite subfE, oppf0. apply addf0. Qed.

Lemma subm0 : ∀ f : fin u, subm f 0 = f.
Proof using . intros. rewrite subm_subf, mod2fin0. apply subf0. Qed.

Lemma subff : ∀ f : fin u, subf f f = fin0.
Proof using . intros. rewrite subfE. apply oppfKaddf. Qed.

Lemma submm : ∀ j : nat, subm (mod2fin j) (mod2fin j) = fin0.
Proof using . intros. rewrite subm_subf, mod2finK. apply subff. Qed.

Lemma subfAC : ∀ f1 f2 f3 : fin u,
  subf (subf f1 f2) f3 = subf (subf f1 f3) f2.
Proof using . intros. rewrite 4 subfE. apply addfAC. Qed.

Lemma submAC : ∀ (f : fin u) (j1 j2 : nat),
  subm (subm f j1) j2 = subm (subm f j2) j1.
Proof using . intros. rewrite 4 submE. apply addmAC. Qed.

Lemma addf_subf : ∀ f1 f2 f3 : fin u,
  addf (subf f1 f2) f3 = subf (addf f1 f3) f2.
Proof using . intros. rewrite 2 subfE. apply addfAC. Qed.

Lemma addm_subm : ∀ (f : fin u) (j1 j2 : nat),
  addm (subm f j2) j1 = subm (addm f j1) j2.
Proof using . intros. rewrite 2 addm_addf, 2 subm_subf. apply addf_subf. Qed.

Lemma addfKV : ∀ f1 f2 : fin u, subf (addf f1 f2) f1 = f2.
Proof using . intros. rewrite addfC, subfE. apply oppfOaddf. Qed.

Lemma addfVKV : ∀ f1 f2 : fin u, subf (addf f2 f1) f1 = f2.
Proof using . intros. rewrite addfC. apply addfKV. Qed.

Lemma addmVKV : ∀ (j : nat) (f : fin u), subm (addm f j) j = f.
Proof using . intros. rewrite subm_subf, addm_addf. apply addfVKV. Qed.

Lemma subfVK : ∀ f1 f2 : fin u, addf f1 (subf f2 f1) = f2.
Proof using . intros. eapply subIf. rewrite addfKV. reflexivity. Qed.

Lemma subfVKV : ∀ f1 f2 : fin u, addf (subf f2 f1) f1 = f2.
Proof using . intros. rewrite addfC. apply subfVK. Qed.

Lemma submVKV : ∀ (j : nat) (f : fin u), addm (subm f j) j = f.
Proof using . intros. rewrite addm_addf, subm_subf. apply subfVKV. Qed.

(*
 *
 *            The predecessor of a fin u
 *
 *)

Definition pref (f : fin u) : fin u := subf f fin1.

Definition pref_compat : Proper (equiv ==> equiv) pref := _.

Lemma prefE : ∀ f : fin u, pref f = subf f fin1.
Proof using . reflexivity. Qed.

Lemma prefI : Preliminary.injective equiv equiv pref.
Proof using . apply subIf. Qed.

Lemma sucfK : ∀ f : fin u, pref (sucf f) = f.
Proof using . intros. rewrite prefE, sucfE. apply addfVKV. Qed.

Lemma prefK : ∀ f : fin u, sucf (pref f) = f.
Proof using . intros. rewrite sucfE, prefE. apply subfVKV. Qed.

Lemma prefEmod : ∀ f : fin u, pref f = mod2fin (f + Nat.pred u).
Proof using .
  intros. apply sucfI. symmetry. rewrite prefK, <- mod2fin_S_sucf, plus_n_Sm,
  S_pred_u, <- addn_mod2fin_idemp_r, mod2fin_u, Nat.add_0_r. apply mod2finK.
Qed.

Lemma revfE : ∀ f : fin u, revf f = pref (oppf f).
Proof using . intros. rewrite oppfE. symmetry. apply sucfK. Qed.

Lemma pref0 : pref fin0 = fin_max.
Proof using . rewrite <- sucf_max. apply sucfK. Qed.

Lemma pred_pref : ∀ f : fin u, fin0 < f -> Nat.pred f = pref f.
Proof using .
  intros * H. symmetry. apply Nat.succ_inj. erewrite Nat.lt_succ_pred, S_sucf.
  apply fin2nat_compat, prefK. rewrite <- neq_max_lt_max, <- pref0.
  eapply not_iff_compat. 2: apply neq_fin0_lt_fin0. 2,3: apply H.
  split. apply prefI. apply pref_compat.
Qed.

Lemma pref_subf : ∀ f1 f2 : fin u, pref (subf f1 f2) = subf (pref f1) f2.
Proof using . intros. rewrite 2 prefE. apply subfAC. Qed.

Lemma pref_subm : ∀ (f : fin u) (j : nat), pref (subm f j) = subm (pref f) j.
Proof using . intros. rewrite 2 subm_subf. apply pref_subf. Qed.

Lemma sucf_subf : ∀ f1 f2 : fin u, sucf (subf f1 f2) = subf (sucf f1) f2.
Proof using . intros. rewrite 2 sucfE. apply addf_subf. Qed.

Lemma sucf_subm : ∀ (f : fin u) (j : nat), sucf (subm f j) = subm (sucf f) j.
Proof using . intros. rewrite 2 subm_subf. apply sucf_subf. Qed.

Lemma pref_addf : ∀ f1 f2 : fin u, pref (addf f1 f2) = addf (pref f1) f2.
Proof using . intros. rewrite 2 prefE. symmetry. apply addf_subf. Qed.

Lemma pref_addm : ∀ (f : fin u) (j : nat), pref (addm f j) = addm (pref f) j.
Proof using . intros. rewrite 2 addm_addf. apply pref_addf. Qed.

Lemma addf_pref : ∀ f1 f2 : fin u, addf (pref f1) f2 = addf f1 (pref f2).
Proof using .
  intros. rewrite (addfC f1), <- 2 pref_addf, addfC. reflexivity.
Qed.

Lemma lt_pref : ∀ f : fin u, fin0 < f -> pref f < f.
Proof using .
  intros * H. rewrite <- pred_pref. eapply lt_pred. all: apply H.
Qed.

Lemma fin0_pref_fin1 : fin0 = pref fin1.
Proof using. symmetry. apply sucfI. rewrite prefK. apply fin1_sucf_fin0. Qed.

Lemma lt_pref_le : ∀ f1 f2 : fin u, pref f1 < f2 -> f1 <= f2.
Proof using .
  intros * H1. apply Nat.lt_pred_le. rewrite pred_pref.
  apply H1. apply neq_fin0_lt_fin0. intros H2. subst.
  eapply Nat.lt_irrefl, max_lt. rewrite <- pref0. apply H1.
Qed.

(*
 *
 *            The complementaries (either to fin_max or fin0)
 *            or the output of several other functions
 *            (then used to prove other lemmas)
 *
 *)

Lemma revf_addf : ∀ f1 f2 : fin u, revf (addf f1 f2) = subf (revf f1) f2.
Proof using .
  intros. apply fin2natI. erewrite subf2nat, 2 revf2nat, addf2nat,
  Nat.add_sub_assoc, <- Nat.add_sub_swap, <- (Nat.mod_small (Nat.pred _ - _)),
  <- Nat.sub_add_distr, <- (Nat.Div0.mod_add (_ - ((_ + _) mod _)) 1), Nat.mul_1_l,
  <- Nat.add_sub_swap, (Nat.Div0.mod_eq (_ + _)), sub_sub,
  (Nat.add_sub_swap _ (_ * _)), Nat.mul_comm, Nat.Div0.mod_add. reflexivity.
  - apply Nat.add_le_mono.
    + apply Nat.lt_le_pred.
      apply fin_lt.
    + apply Nat.lt_le_incl.
      apply fin_lt.
  - apply Nat.Div0.mul_div_le.
  - apply Nat.lt_le_pred.
    apply mod2fin_lt.
  - apply lt_sub_lt_add_l.
    + apply Nat.lt_le_pred.
      apply mod2fin_lt.
    + apply Nat.lt_lt_add_l.
      apply lt_pred_u.
  - apply Nat.lt_le_pred.
    apply fin_lt.
  - apply Nat.lt_le_incl.
    apply fin_lt.
Qed.

Lemma revf_addm : ∀ (f : fin u) (j : nat), revf (addm f j) = subm (revf f) j.
Proof using . intros. rewrite addm_addf. apply revf_addf. Qed.

Lemma revf_sucf : ∀ f : fin u, revf (sucf f) = pref (revf f).
Proof using . intros. rewrite sucfE, prefE. apply revf_addf. Qed.

Lemma oppf_addf : ∀ f1 f2 : fin u, oppf (addf f1 f2) = subf (oppf f1) f2.
Proof using . intros. rewrite 2 oppfE, revf_addf. apply sucf_subf. Qed.

Lemma oppf_addm : ∀ (f : fin u) (j : nat), oppf (addm f j) = subm (oppf f) j.
Proof using . intros. rewrite addm_addf, subm_subf. apply oppf_addf. Qed.

Lemma oppf_sucf : ∀ f : fin u, oppf (sucf f) = pref (oppf f).
Proof using . intros. rewrite sucfE, prefE. apply oppf_addf. Qed.

Lemma revf_subf : ∀ f1 f2 : fin u, revf (subf f1 f2) = addf (revf f1) f2.
Proof using .
  intros. eapply subIf. rewrite <- revf_addf, addfVKV, subfVKV. reflexivity.
Qed.

Lemma revf_subm : ∀ (f : fin u) (j : nat), revf (subm f j) = addm (revf f) j.
Proof using . intros. rewrite subm_subf, addm_addf. apply revf_subf. Qed.

Lemma revf_pref : ∀ f : fin u, revf (pref f) = sucf (revf f).
Proof using . intros. rewrite sucfE, prefE. apply revf_subf. Qed.

Lemma oppf_subf : ∀ f1 f2 : fin u, oppf (subf f2 f1) = subf f1 f2.
Proof using .
  intros. rewrite oppfE, revf_subf, sucf_addf,
  <- oppfE, addfC. symmetry. apply subfE.
Qed.

Lemma oppf_subm : ∀ (f : fin u) (j : nat),
  oppf (subm f j) = subf (mod2fin j) f.
Proof using . intros. rewrite subm_subf. apply oppf_subf. Qed.

Lemma oppf_pref : ∀ f : fin u, oppf (pref f) = sucf (oppf f).
Proof using . intros. rewrite 2 oppfE, revf_pref. reflexivity. Qed.

Lemma revf_oppf : ∀ f : fin u, revf (oppf f) = pref f.
Proof using . intros. rewrite oppfE, revf_sucf, revfK. reflexivity. Qed.

Lemma oppfK : ∀ f : fin u, oppf (oppf f) = f.
Proof using . intros. rewrite oppfE, revf_oppf. apply prefK. Qed.

Lemma oppmK : ∀ j : nat, oppf (oppm j) = mod2fin j.
Proof using . intros. rewrite oppm_oppf. apply oppfK. Qed.

Lemma oppf_revf : ∀ f : fin u, oppf (revf f) = sucf f.
Proof using . intros. rewrite oppfE, revfK. reflexivity. Qed.

Lemma addf_subf_oppf : ∀ f1 f2 : fin u, addf f1 f2 = subf f1 (oppf f2).
Proof using . intros. rewrite subfE, oppfK. reflexivity. Qed.

Lemma addm_subf_oppm : ∀ (f : fin u) (j : nat), addm f j = subf f (oppm j).
Proof using . intros. rewrite subfE, oppmK. apply addm_addf. Qed.

Lemma subf_subf : ∀ f1 f2 f3 : fin u,
  subf f1 (subf f2 f3) = subf (addf f1 f3) f2.
Proof using .
  intros. rewrite 3subfE, oppf_addf, subfE, oppfK, addfA. apply addfAC.
Qed.

Lemma subm_subm : ∀ (f1 f2 : fin u) (j : nat),
  subm f1 (subm f2 j) = subm (addm f1 j) f2.
Proof using .
  intros. rewrite <-2 subf_subm, subm_subf, addm_addf. apply subf_subf.
Qed.

Lemma subf_addf : ∀ f1 f2 f3 : fin u,
  subf f1 (addf f2 f3) = subf (subf f1 f2) f3.
Proof using .
  intros. rewrite 3 subfE, <- addfA, oppf_addf, subfE. reflexivity.
Qed.

Lemma subf_addf_addf : ∀ f1 f2 f3 : fin u,
  subf (addf f1 f3) (addf f2 f3) = subf f1 f2.
Proof using . intros. rewrite <- subf_subf, addfVKV. reflexivity. Qed.

Lemma subf_addm_addm : ∀ (f1 f2 : fin u) (j : nat),
  subf (addm f1 j) (addm f2 j) = subf f1 f2.
Proof using . intros. rewrite 2 addm_addf. apply subf_addf_addf. Qed.

Lemma subf_addf_addfC : ∀ f1 f2 f3 : fin u,
  subf (addf f1 f2) (addf f1 f3) = subf f2 f3.
Proof using . intros. rewrite 2 (addfC f1). apply subf_addf_addf. Qed.

Lemma subfCAC : ∀ f1 f2 f3 f4 : fin u,
  subf (subf f1 f2) (subf f3 f4) = subf (subf f1 f3) (subf f2 f4).
Proof using .
  intros. rewrite <- subf_addf, addfC, addf_subf, addfC,
  <- addf_subf, addfC, subf_addf. reflexivity.
Qed.

Lemma subf_sucf : ∀ f1 f2 : fin u, subf (sucf f1) f2 = subf f1 (pref f2).
Proof using . intros. rewrite sucfE, prefE. symmetry. apply subf_subf. Qed.

Lemma subf_pref : ∀ f1 f2 : fin u, subf (pref f1) f2 = subf f1 (sucf f2).
Proof using .
  intros. rewrite sucfE, prefE, subfAC. symmetry. apply subf_addf.
Qed.

Lemma oppf1 : oppf fin1 = fin_max.
Proof using . rewrite fin1_sucf_fin0, oppf_sucf, oppf0. apply pref0. Qed.

Lemma oppm1 : oppm 1 = fin_max.
Proof using . rewrite oppm_oppf, <- fin1E. apply oppf1. Qed.

Lemma oppf_max : oppf fin_max = fin1.
Proof using . rewrite <- oppf1. apply oppfK. Qed.

(*
 *
 *            The symmetric of f by the center c
 *
 *)

Definition symf (c f : fin u) : fin u := addf c (subf c f).

Definition symf_compat : Proper (equiv ==> equiv ==> equiv) symf := _.

Lemma symfE : ∀ c f : fin u, symf c f = addf c (subf c f).
Proof using . reflexivity. Qed.

Lemma symfEmod : ∀ c f : fin u, symf c f = mod2fin (u - f + c + c).
Proof using .
  intros. rewrite symfE, addfE, subfEmod, addn_mod2fin_idemp_r, Nat.add_assoc,
  (Nat.add_comm _ (_ - _)), Nat.add_assoc. reflexivity.
Qed.

Lemma symf2nat : ∀ c f : fin u, symf c f = (u - f + c + c) mod u :> nat.
Proof using . intros. rewrite symfEmod. apply mod2fin2nat. Qed.

Lemma symfI : ∀ c : fin u, Preliminary.injective equiv equiv (symf c).
Proof using . intros c f1 f2 H. eapply subfI, addfI, H. Qed.

Lemma symfK : ∀ c f : fin u, symf c (symf c f) = f.
Proof using .
  intros. rewrite 2 symfE, subf_addf, subff, sub0f, oppf_subf. apply subfVK.
Qed.

Lemma sym0f : ∀ f : fin u, symf fin0 f = oppf f.
Proof using . intros. rewrite symfE, add0f. apply sub0f. Qed.

Lemma symf0 : ∀ c : fin u, symf c fin0 = addf c c.
Proof using . intros. rewrite symfE, subf0. reflexivity. Qed.

Lemma symff : ∀ f : fin u, symf f f = f.
Proof using . intros. rewrite symfE, subff. apply addf0. Qed.

Lemma symf_addf : ∀ c f1 f2 : fin u, symf c (addf f1 f2) = subf (symf c f1) f2.
Proof using .
  intros. rewrite 2 symfE, subf_addf, addfC, addf_subf, addfC. reflexivity.
Qed.

Lemma symf_subf : ∀ c f1 f2 : fin u, symf c (subf f1 f2) = addf (symf c f1) f2.
Proof using .
  intros. rewrite 2 symfE, subf_subf, <- addf_subf, addfA. reflexivity.
Qed.

Lemma symf_sucf : ∀ c f : fin u, symf c (sucf f) = pref (symf c f).
Proof using . intros. rewrite sucfE, prefE. apply symf_addf. Qed.

Lemma symf_pref : ∀ c f : fin u, symf c (pref f) = sucf (symf c f).
Proof using . intros. rewrite prefE, sucfE. apply symf_subf. Qed.

(*
 *
 *            The compatibility between various functions
 *            on fin u and both < and <=
 *
 *)

Lemma addm_lt_large : ∀ (f : fin u) (j : nat),
  fin0 < f -> oppf f <= j mod u -> addm f j < f.
Proof using .
  intros * Hlt Hle. erewrite <- (Nat.mod_small f), <- Nat.Div0.mod_add, <- addm_mod,
  addm2nat, Nat.mul_1_l. eapply mod_lt_between_compat; try rewrite Nat.mul_1_r.
  - split.
    + eapply Nat.le_trans.
      all:swap 1 2.
      * apply Nat.add_le_mono_l, Hle.
      * rewrite addn_oppf. reflexivity. apply Hlt.
    + apply Nat.add_lt_mono;try apply fin_lt.
      apply mod2fin_lt.
  - split.
    + apply Nat.le_add_l.
    + apply Nat.add_lt_mono_r.
      apply fin_lt.
  - apply Nat.add_lt_mono_l.
    apply mod2fin_lt.
  - apply fin_lt.
Qed.

Lemma addm_le_small :
  ∀ (f : fin u) (j : nat), j mod u < oppf f -> f <= addm f j.
Proof using .
  intros * H. destruct (eq_fin0_lt_dec f) as [| Hd]. subst. apply fin0_le.
  erewrite <- (Nat.mod_small f), <- addm_mod, addm2nat.
  eapply mod_le_between_compat. 1,2: rewrite Nat.mul_0_r. apply fin_between.
  2: apply Nat.le_add_r. 2: apply fin_lt. split. apply Nat.le_0_l.
  eapply Nat.lt_le_trans. apply Nat.add_lt_mono_l, H.
  rewrite addn_oppf. reflexivity. apply Hd.
Qed.

Lemma addm_lt_small :
  ∀ (f : fin u) (j : nat), fin0 < j mod u < oppf f -> f < addm f j.
Proof using .
  intros * [H1 H2]. apply Nat.le_neq. split. apply addm_le_small, H2.
  intros Habs. apply <- Nat.neq_0_lt_0. apply H1. eapply addm_betweenI.
  1,2: rewrite Nat.add_0_l. 1,2: split. 1,3: apply Nat.le_0_l. apply mod2fin_lt.
  apply lt_0_u. symmetry. rewrite addm0, addm_mod. apply fin2natI, Habs.
Qed.

Lemma addf_lt_large :
  ∀ f1 f2 : fin u, fin0 < f1 -> oppf f1 <= f2 -> addf f1 f2 < f1.
Proof using .
  intros * Hlt Hle. rewrite addf_addm. apply addm_lt_large. apply Hlt.
  rewrite Nat.mod_small by apply fin_lt. apply Hle.
Qed.

Lemma addf_le_small :
  ∀ f1 f2 : fin u, f2 < oppf f1 -> f1 <= addf f1 f2.
Proof using .
  intros * H. rewrite addf_addm. apply addm_le_small.
  rewrite Nat.mod_small by apply fin_lt. apply H.
Qed.

Lemma addf_lt_small : ∀ f1 f2 : fin u, fin0 < f2 < oppf f1 -> f1 < addf f1 f2.
Proof using .
  intros * H. rewrite addf_addm. apply addm_lt_small.
  rewrite Nat.mod_small. apply H. apply fin_lt.
Qed.

Lemma addm_le_compat_large : ∀ (f : fin u) (j1 j2 : nat),
  oppf f <= j1 mod u <= j2 mod u -> addm f j1 <= addm f j2.
Proof using .
  intros * [H1 H2]. destruct (eq_fin0_lt_dec f) as [| Hd].
  - subst. rewrite (add0m j1), (add0m j2), 2 mod2fin2nat. apply H2.
  - rewrite <- (addm_mod f j1), <- (addm_mod f j2), 2 addmE.
    eapply mod2fin_le_between_compat. 3: apply Nat.add_le_mono_l, H2.
    all: rewrite Nat.mul_1_r. all: split. 2,4: apply Nat.add_lt_mono, mod2fin_lt.
    2,3: apply fin_lt. all: eapply Nat.le_trans. 2,4: apply Nat.add_le_mono_l.
    3: eapply Nat.le_trans. 2,3: apply H1. 2: apply H2. all: rewrite addn_oppf.
    1,3: reflexivity. all: apply Hd.
Qed.

Lemma addm_le_compat_small : ∀ (f : fin u) (j1 j2 : nat),
  j1 mod u <= j2 mod u < oppf f -> addm f j1 <= addm f j2.
Proof using .
  intros * [H1 H2]. destruct (eq_fin0_lt_dec f) as [| Hd].
  - subst. rewrite (add0m j1), (add0m j2), 2 mod2fin2nat. apply H1.
  - rewrite <- (addm_mod f j1), <- (addm_mod f j2), 2 addmE.
    eapply mod2fin_le_between_compat. 3: apply Nat.add_le_mono_l, H1.
    all: rewrite Nat.mul_0_r, Nat.add_0_l. all: split. 1,3: apply Nat.le_0_l.
    eapply Nat.le_lt_trans. apply Nat.add_le_mono_l, H1. all: eapply Nat.lt_le_trans.
    1,3: apply Nat.add_lt_mono_l, H2. all: rewrite addn_oppf.
    1,3: reflexivity. all: apply Hd.
Qed.

Lemma addf_le_compat_large : ∀ f1 f2 f3 : fin u,
  oppf f1 <= f2 <= f3 -> addf f1 f2 <= addf f1 f3.
Proof using .
  intros * H. rewrite 2 addf_addm. apply addm_le_compat_large.
  rewrite 2 Nat.mod_small by apply fin_lt. apply H.
Qed.

Lemma addf_le_compat_small : ∀ f1 f2 f3 : fin u,
  f2 <= f3 < oppf f1 -> addf f1 f2 <= addf f1 f3.
Proof using .
  intros * H. rewrite 2 addf_addm. apply addm_le_compat_small.
  rewrite 2 Nat.mod_small by apply fin_lt. apply H.
Qed.

Lemma addm_lt_compat_large : ∀ (f : fin u) (j1 j2 : nat),
  oppf f <= j1 mod u < j2 mod u -> addm f j1 < addm f j2.
Proof using .
  intros * [H1 H2]. apply Nat.le_neq. split. apply addm_le_compat_large.
  split. apply H1. apply Nat.lt_le_incl, H2. rewrite <- (addm_mod _ j1),
  <- (addm_mod _ j2). intros Habs. eapply Nat.lt_irrefl.
  erewrite addm_betweenI. apply H2. 3: apply fin2natI, Habs.
  all: split. 1,3: apply Nat.le_0_l. all: apply mod2fin_lt.
Qed.

Lemma addm_lt_compat_small : ∀ (f : fin u) (j1 j2 : nat),
  j1 mod u < j2 mod u < oppf f -> addm f j1 < addm f j2.
Proof using .
  intros * [H1 H2]. apply Nat.le_neq. split. apply addm_le_compat_small.
  split. apply Nat.lt_le_incl, H1. apply H2. rewrite <- (addm_mod _ j1),
  <- (addm_mod _ j2). intros Habs. eapply Nat.lt_irrefl.
  erewrite addm_betweenI. apply H1. 3: apply fin2natI, Habs.
  all: split. 1,3: apply Nat.le_0_l. all: apply mod2fin_lt.
Qed.

Lemma addf_lt_compat_large :
  ∀ f1 f2 f3 : fin u, oppf f1 <= f2 < f3 -> addf f1 f2 < addf f1 f3.
Proof using .
  intros * H. rewrite 2 addf_addm. apply addm_lt_compat_large.
  rewrite 2 Nat.mod_small. apply H. all: apply fin_lt.
Qed.

Lemma addf_lt_compat_small :
  ∀ f1 f2 f3 : fin u, f2 < f3 < oppf f1 -> addf f1 f2 < addf f1 f3.
Proof using .
  intros * H. rewrite 2 addf_addm. apply addm_lt_compat_small.
  rewrite 2 Nat.mod_small. apply H. all: apply fin_lt.
Qed.

Lemma lt_fin0_oppf : ∀ f : fin u, fin0 < f -> fin0 < oppf f.
Proof using .
  intros * H1. apply neq_fin0_lt_fin0 in H1. apply neq_fin0_lt_fin0. intros H2.
  apply H1. apply oppfI. rewrite oppf0. apply H2.
Qed.

Lemma oppf_le_compat : ∀ f1 f2 : fin u, fin0 < f1 <= f2 -> oppf f2 <= oppf f1.
Proof using .
  intros * [H1 H2]. rewrite 2 oppfEmod. eapply mod2fin_le_between_compat.
  1,2: rewrite Nat.mul_0_r, Nat.add_0_l. 1,2: split. 1,3: apply Nat.le_0_l.
  3: apply Nat.sub_le_mono_l, H2. all: apply lt_sub_u.
  eapply Nat.lt_le_trans. 1,3: apply H1. apply H2.
Qed.

Lemma oppf_lt_compat : ∀ f1 f2 : fin u, fin0 < f1 < f2 -> oppf f2 < oppf f1.
Proof using .
  intros * [H1 H2]. rewrite <- (Nat.lt_succ_pred 0 (oppf f1)). apply Nat.lt_succ_r.
  rewrite pred_pref, <- oppf_sucf. apply oppf_le_compat. rewrite <- S_sucf.
  split. apply Nat.lt_0_succ. 2: eapply Nat.lt_le_trans. 1,2: apply H2.
  apply le_max. all: apply lt_fin0_oppf, H1.
Qed.

Lemma oppf_le_inj : ∀ f1 f2 : fin u, fin0 < oppf f2 <= oppf f1 -> f1 <= f2.
Proof using .
  intros * H. rewrite <- (oppfK f1), <- (oppfK f2). apply oppf_le_compat, H.
Qed.

Lemma oppf_lt_inj : ∀ f1 f2 : fin u, fin0 < oppf f2 < oppf f1 -> f1 < f2.
Proof using .
  intros * H. rewrite <- (oppfK f1), <- (oppfK f2). apply oppf_lt_compat, H.
Qed.

Lemma opmm_le_compat :
  ∀ j1 j2 : nat, fin0 < j1 mod u <= j2 mod u -> oppm j2 <= oppm j1.
Proof using .
  intros * H. rewrite 2 oppm_oppf. apply oppf_le_compat.
  rewrite 2 mod2fin2nat. apply H.
Qed.

Lemma oppm_lt_compat :
  ∀ j1 j2 : nat, fin0 < j1 mod u < j2 mod u -> oppm j2 < oppm j1.
Proof using .
  intros * H. rewrite 2 oppm_oppf. apply oppf_lt_compat.
  rewrite 2 mod2fin2nat. apply H.
Qed.

Lemma oppm_le_inj :
  ∀ j1 j2 : nat, fin0 < oppm j2 <= oppm j1 -> j1 mod u <= j2 mod u.
Proof using .
  intros * H. rewrite <-2 mod2fin2nat. apply oppf_le_inj.
  rewrite <-2 oppm_oppf. apply H.
Qed.

Lemma oppm_lt_inj :
  ∀ j1 j2 : nat, fin0 < oppm j2 < oppm j1 -> j1 mod u < j2 mod u.
Proof using .
  intros * H. rewrite <-2 mod2fin2nat. apply oppf_lt_inj.
  rewrite <-2 oppm_oppf. apply H.
Qed.

Lemma lt_fin0_subf : ∀ f1 f2 : fin u, f1 ≠ f2 -> fin0 < subf f1 f2.
Proof using .
  intros * H1. apply neq_fin0_lt_fin0. intros H2. apply H1.
  eapply subIf. rewrite (subff f2). apply H2.
Qed.

Lemma subf_lt_small : ∀ f1 f2 : fin u, fin0 < f2 <= f1 -> subf f1 f2 < f1.
Proof using .
  intros * H. rewrite subfE. apply addf_lt_large.
  2: apply oppf_le_compat, H. eapply Nat.lt_le_trans. all: apply H.
Qed.

Lemma subf_le_small :
  ∀ f1 f2 : fin u, f2 <= f1 -> subf f1 f2 <= f1.
Proof using .
  intros * H. destruct (eq_fin0_lt_dec f1) as [| H1]. subst.
  rewrite sub0f, le_fin0, <- oppf0. f_equal. apply le_fin0, H.
  destruct (eq_fin0_lt_dec f2) as [| H2]. subst. rewrite subf0.
  reflexivity. rewrite subfE. apply Nat.lt_le_incl, addf_lt_large.
  apply H1. apply oppf_le_compat. split. apply H2. apply H.
Qed.

Lemma subf_lt_large : ∀ f1 f2 : fin u, fin0 < f1 < f2 -> f1 < subf f1 f2.
Proof using .
  intros * H. rewrite subfE. apply addf_lt_small. split.
  2: apply oppf_lt_compat, H. eapply lt_fin0_oppf, Nat.lt_trans. all: apply H.
Qed.

Lemma subf_le_large : ∀ f1 f2 : fin u, f1 < f2 -> f1 <= subf f1 f2.
Proof using .
  intros * H. destruct (eq_fin0_lt_dec f1) as [| Hd]. subst. apply fin0_le.
  rewrite subfE. apply addf_le_small, oppf_lt_compat. split. apply Hd. apply H.
Qed.

Lemma subm_lt_small :
  ∀ (f : fin u) (j : nat), fin0 < j mod u <= f -> subm f j < f.
Proof using .
  intros * H. rewrite subm_subf. apply subf_lt_small.
  rewrite mod2fin2nat. apply H.
Qed.

Lemma subm_le_small :
  ∀ (f : fin u) (j : nat), j mod u <= f -> subm f j <= f.
Proof using .
  intros * H. rewrite subm_subf. apply subf_le_small.
  rewrite mod2fin2nat. apply H.
Qed.

Lemma subm_lt_large :
  ∀ (f : fin u) (j : nat), fin0 < f < j mod u -> f < subm f j.
Proof using .
  intros * H. rewrite subm_subf. apply subf_lt_large.
  rewrite mod2fin2nat. apply H.
Qed.

Lemma subm_le_large : ∀ (f : fin u) (j : nat), f < j mod u -> f <= subm f j.
Proof using .
  intros * H. rewrite subm_subf. apply subf_le_large.
  rewrite mod2fin2nat. apply H.
Qed.

Lemma subf_le_compat_small :
  ∀ f1 f2 f3 : fin u, f3 <= f2 <= f1 -> subf f1 f2 <= subf f1 f3.
Proof using .
  intros * H. destruct (eq_fin0_lt_dec f3) as [| Hd]. subst.
  rewrite subf0. apply subf_le_small, H. rewrite 2 subfE.
  apply addf_le_compat_large. split. all: apply oppf_le_compat.
  all: split. eapply Nat.lt_le_trans. 1,4: apply Hd. all: apply H.
Qed.

Lemma subf_le_compat_large :
  ∀ f1 f2 f3 : fin u, f1 < f3 <= f2 -> subf f1 f2 <= subf f1 f3.
Proof using .
  intros * H. destruct (eq_fin0_lt_dec f1) as [| Hd]. subst.
  rewrite 2 sub0f. apply oppf_le_compat, H. rewrite 2 subfE.
  apply addf_le_compat_small. split. apply oppf_le_compat. split.
  eapply lt_fin0. 1,2: apply H. apply oppf_lt_compat. split. apply Hd. apply H.
Qed.

Lemma subf_lt_compat_small :
  ∀ f1 f2 f3 : fin u, f3 < f2 <= f1 -> subf f1 f2 < subf f1 f3.
Proof using .
  intros * H. destruct (eq_fin0_lt_dec f3) as [| Hd]. subst.
  rewrite subf0. apply subf_lt_small, H. rewrite 2 subfE.
  apply addf_lt_compat_large. split. apply oppf_le_compat. split.
  eapply lt_fin0. 1,2: apply H. apply oppf_lt_compat. split. apply Hd. apply H.
Qed.

Lemma subf_lt_compat_large :
  ∀ f1 f2 f3 : fin u, f1 < f3 < f2 -> subf f1 f2 < subf f1 f3.
Proof using .
  intros * H. destruct (eq_fin0_lt_dec f1) as [| Hd]. subst.
  rewrite 2 sub0f. apply oppf_lt_compat, H. rewrite 2 subfE.
  apply addf_lt_compat_small. split. all: apply oppf_lt_compat.
  all: split. eapply lt_fin0. 1,2,4: apply H. apply Hd.
Qed.

Lemma subm_le_compat_small : ∀ (f : fin u) (j1 j2 : nat),
  j2 mod u <= j1 mod u <= f -> subm f j1 <= subm f j2.
Proof using .
  intros * H. rewrite 2 subm_subf. apply subf_le_compat_small.
  rewrite 2 mod2fin2nat. apply H.
Qed.

Lemma subm_le_compat_large : ∀ (f : fin u) (j1 j2 : nat),
  f < j2 mod u <= j1 mod u -> subm f j1 <= subm f j2.
Proof using .
  intros * H. rewrite 2 subm_subf. apply subf_le_compat_large.
  rewrite 2 mod2fin2nat. apply H.
Qed.

Lemma subm_lt_compat_small : ∀ (f : fin u) (j1 j2 : nat),
  j2 mod u < j1 mod u <= f -> subm f j1 < subm f j2.
Proof using .
  intros * H. rewrite 2 subm_subf. apply subf_lt_compat_small.
  rewrite 2 mod2fin2nat. apply H.
Qed.

Lemma subm_lt_compat_large : ∀ (f : fin u) (j1 j2 : nat),
  f < j2 mod u < j1 mod u -> subm f j1 < subm f j2.
Proof using .
  intros * H. rewrite 2 subm_subf. apply subf_lt_compat_large.
  rewrite 2 mod2fin2nat. apply H.
Qed.

Lemma subf_lt_smallV : ∀ f1 f2 : fin u, fin0 < f2 < f1 -> subf f1 f2 < oppf f2.
Proof using .
  intros * H. apply oppf_lt_inj. rewrite oppfK, oppf_subf.
  split. apply H. apply subf_lt_large, H.
Qed.

Lemma subf_le_smallV :
  ∀ f1 f2 : fin u, fin0 < f2 < f1 -> subf f1 f2 <= oppf f2.
Proof using .
  intros * H. apply oppf_le_inj. rewrite oppfK, oppf_subf.
  split. apply H. apply subf_le_large, H.
Qed.

Lemma subf_lt_largeV : ∀ f1 f2 : fin u, fin0 < f1 < f2 -> oppf f2 < subf f1 f2.
Proof using .
  intros * H. apply oppf_lt_inj. rewrite oppfK, oppf_subf. split.
  apply lt_fin0_subf, lt_gt_cases. right. 2: apply subf_lt_small.
  2: split. 3: apply Nat.lt_le_incl. all: apply H.
Qed.

Lemma subf_le_largeV : ∀ f1 f2 : fin u, f1 < f2 -> oppf f2 <= subf f1 f2.
Proof using .
  intros * H. apply oppf_le_inj. rewrite oppfK, oppf_subf. split.
  apply lt_fin0_subf, lt_gt_cases. right. 2: apply subf_le_small.
  2: apply Nat.lt_le_incl. all: apply H.
Qed.

Lemma subf_le_compat_largeV :
  ∀ f1 f2 f3 : fin u, f1 <= f3 <= f2 -> subf f3 f1 <= subf f2 f1.
Proof using .
  intros * [H1 H2]. destruct (le_lt_eq_dec H1) as [Hd |]. 2:{ subst.
  rewrite subff. apply fin0_le. } apply oppf_le_inj. rewrite 2 oppf_subf.
  split. apply lt_fin0_subf, lt_gt_cases. left. eapply Nat.lt_le_trans.
  3: apply subf_le_compat_large. 3: split. 1,3: apply Hd. all: apply H2.
Qed.

Lemma subf_le_compat_smallV :
  ∀ f1 f2 f3 : fin u, f3 <= f2 < f1 -> subf f3 f1 <= subf f2 f1.
Proof using .
  intros * H. apply oppf_le_inj. rewrite 2 oppf_subf. split.
  apply lt_fin0_subf, lt_gt_cases. right. 2: apply subf_le_compat_small.
  2: split. 3: apply Nat.lt_le_incl. all: apply H.
Qed.

Lemma subf_lt_compat_largeV :
  ∀ f1 f2 f3 : fin u, f1 <= f3 < f2 -> subf f3 f1 < subf f2 f1.
Proof using .
  intros * [H1 H2]. destruct (le_lt_eq_dec H1) as [Hd |]. 2: subst.
  2: rewrite subff. apply oppf_lt_inj. rewrite 2 oppf_subf. split.
  1,3: apply lt_fin0_subf, lt_gt_cases. 3: apply subf_lt_compat_large.
  left. 2: right. 3: split. eapply Nat.lt_trans. 1,4: apply Hd. all: apply H2.
Qed.

Lemma subf_lt_compat_smallV :
  ∀ f1 f2 f3 : fin u, f3 < f2 < f1 -> subf f3 f1 < subf f2 f1.
Proof using .
  intros * H. apply oppf_lt_inj. rewrite 2 oppf_subf. split.
  apply lt_fin0_subf, lt_gt_cases. right. 2: apply subf_lt_compat_small.
  2: split. 3: apply Nat.lt_le_incl. all: apply H.
Qed.

Lemma sucf_le_compat :
  ∀ f1 f2 : fin u, f1 <= f2 < fin_max -> sucf f1 <= sucf f2.
Proof using .
  intros * H. rewrite 2 sucfE, (addfC f1), (addfC f2).
  apply addf_le_compat_small. rewrite oppf1. apply H.
Qed.

Lemma sucf_lt_compat :
  ∀ f1 f2 : fin u, f1 < f2 < fin_max -> sucf f1 < sucf f2.
Proof using .
  intros * H. rewrite 2 sucfE, (addfC f1), (addfC f2).
  apply addf_lt_compat_small. rewrite oppf1. apply H.
Qed.

Lemma pref_le_compat :
  ∀ f1 f2 : fin u, fin0 < f1 <= f2 -> pref f1 <= pref f2.
Proof using .
  intros * H. rewrite 2 prefE. apply subf_le_compat_largeV. split.
  2: apply H. apply lt_pref_le. rewrite <- fin0_pref_fin1. apply H.
Qed.

Lemma pref_lt_compat : ∀ f1 f2 : fin u, fin0 < f1 < f2 -> pref f1 < pref f2.
Proof using .
  intros * H. rewrite 2 prefE. apply subf_lt_compat_largeV. split.
  2: apply H. apply lt_pref_le. rewrite <- fin0_pref_fin1. apply H.
Qed.

(*
 *
 *            The bijections on fin u whose section adds
 *            and whose retraction substracts
 *
 *)

Definition asbf (f : fin u) : bijection (fin u) :=
  cancel_bijection (λ f2 : fin u, subf f2 f) _ (@addIf f)
                   (addfVKV f) (subfVKV f).
Global Opaque asbf.

Definition asbm (j : nat) : bijection (fin u) :=
  cancel_bijection (λ f : fin u, subm f j) _ (@addIm j)
                   (addmVKV j) (submVKV j).
Global Opaque asbm.

Lemma asbfE : ∀ f1 : fin u, asbf f1 = (λ f2, addf f2 f1) :> (fin u -> fin u).
Proof using . reflexivity. Qed.

Lemma asbfVE :
  ∀ f1 : fin u, (asbf f1)⁻¹ = (λ f2, subf f2 f1) :> (fin u -> fin u).
Proof using . reflexivity. Qed.

Lemma asbmE : ∀ j : nat, asbm j = (λ f, addm f j) :> (fin u -> fin u).
Proof using . reflexivity. Qed.

Lemma asbmVE : ∀ j : nat, (asbm j)⁻¹ = (λ f, subm f j) :> (fin u -> fin u).
Proof using . reflexivity. Qed.

Lemma asbm_asbf : ∀ j : nat, asbm j == asbf (mod2fin j).
Proof using . intros j f. rewrite asbmE, asbfE. apply addm_addf. Qed.

Lemma asbf_asbm : ∀ f1 : fin u, asbf f1 == asbm f1.
Proof using . intros f1 f2. rewrite asbmE, asbfE. apply addf_addm. Qed.

Lemma asbmV_asbfV : ∀ j : nat, (asbm j)⁻¹ == (asbf (mod2fin j))⁻¹.
Proof using . intros j f. rewrite asbmVE, asbfVE. apply subm_subf. Qed.

Lemma asbfV_asbmV : ∀ f1 : fin u, (asbf f1)⁻¹ == (asbm f1)⁻¹.
Proof using . intros f1 f2. rewrite asbmVE, asbfVE. apply subf_subm. Qed.

Lemma asbm_mod : ∀ j : nat, asbm (j mod u) == asbm j.
Proof using . intros j f. rewrite 2 asbmE. apply addm_mod. Qed.

Lemma asbmV_mod : ∀ j : nat, (asbm (j mod u))⁻¹ == (asbm j)⁻¹.
Proof using . intros j f. rewrite 2 asbmVE. apply subm_mod. Qed.

Lemma asbm0 : asbm 0 == id.
Proof using . intros f. rewrite asbmE. apply addm0. Qed.

Lemma asbf0 : asbf fin0 == id.
Proof using . intros f. rewrite asbfE. apply addf0. Qed.

Lemma asbfAC : ∀ f1 f2 : fin u, asbf f2 ∘ (asbf f1) == asbf f1 ∘ (asbf f2).
Proof using . intros * f3. rewrite 2 compE, 2 asbfE. apply addfAC. Qed.

Lemma asbmAC : ∀ j1 j2 : nat, asbm j2 ∘ (asbm j1) == asbm j1 ∘ (asbm j2).
Proof using . intros * f. rewrite 2 compE, 2 asbmE. apply addmAC. Qed.

Lemma asbmVAC :
  ∀ j1 j2 : nat, (asbm j2)⁻¹ ∘ (asbm j1)⁻¹ == (asbm j1)⁻¹ ∘ (asbm j2)⁻¹.
Proof using . intros * f. rewrite 2 compE, 2 asbmVE. apply submAC. Qed.

Lemma asbfVAC :
  ∀ f1 f2 : fin u, (asbf f2)⁻¹ ∘ (asbf f1)⁻¹ == (asbf f1)⁻¹ ∘ (asbf f2)⁻¹.
Proof using . intros * f3. rewrite 2 compE, 2 asbfVE. apply subfAC. Qed.

Lemma asbmCV : ∀ j1 j2 : nat, asbm j1 ∘ (asbm j2)⁻¹ == (asbm j2)⁻¹ ∘ (asbm j1).
Proof using . intros * f. rewrite 2 compE, asbmVE, asbmE. apply addm_subm. Qed.

Lemma asbfCV :
  ∀ f1 f2 : fin u, asbf f1 ∘ (asbf f2)⁻¹ == (asbf f2)⁻¹ ∘ (asbf f1).
Proof using . intros * f3. rewrite 2 compE, asbfVE, asbfE. apply addf_subf. Qed.

Lemma asbfA : ∀ f1 f2 : fin u, asbf (asbf f1 f2) == asbf f1 ∘ (asbf f2).
Proof using . intros * f3. rewrite compE, 3 asbfE. apply addfA. Qed.

Lemma asbfAV :
  ∀ f1 f2 : fin u, asbf ((asbf f1)⁻¹ f2) == (asbf f1)⁻¹ ∘ (asbf f2).
Proof using .
  intros * f3. rewrite compE, 2 asbfE,
  asbfVE, addfC, (addfC f3). apply addf_subf.
Qed.

Lemma asbfVA :
  ∀ f1 f2 : fin u, (asbf (asbf f1 f2))⁻¹ == (asbf f1)⁻¹ ∘ (asbf f2)⁻¹.
Proof using .
  intros * f3. rewrite compE, 3 asbfVE, asbfE. apply subf_addf.
Qed.

Lemma asbfVAV :
  ∀ f1 f2 : fin u, (asbf ((asbf f1)⁻¹ f2))⁻¹ == asbf f1 ∘ (asbf f2)⁻¹.
Proof using .
  intros * f3. rewrite compE, 3 asbfVE, asbfE, addf_subf. apply subf_subf.
Qed.

Lemma asbfVf : ∀ f : fin u, (asbf f)⁻¹ f = fin0.
Proof using . intros. rewrite asbfVE. apply subff. Qed.

(*
 *
 *            The bijection on fin u whose section is sucf
 *            and whose retraction is pref
 *
 *)

Definition spbf : bijection (fin u)
  := cancel_bijection pref _ sucfI sucfK prefK.
Global Opaque spbf.

Lemma spbfE : spbf = sucf :> (fin u -> fin u).
Proof using . reflexivity. Qed.

Lemma spbfVE : spbf⁻¹ = pref :> (fin u -> fin u).
Proof using . reflexivity. Qed.

Lemma asbf1 : asbf fin1 == spbf.
Proof using . intros f. rewrite asbfE, spbfE. symmetry. apply sucfE. Qed.

Lemma asbm1 : asbm 1 == spbf.
Proof using . rewrite asbm_asbf, <- fin1E. apply asbf1. Qed.

(*
 *
 *            The bijection on fin u whose section and retraction are revf
 *
 *)

Definition rebf : bijection (fin u)
  := cancel_bijection revf _ revfI revfK revfK.
Global Opaque rebf.

Lemma rebfE : rebf = revf :> (fin u -> fin u).
Proof using . reflexivity. Qed.

Lemma rebfVE : rebf⁻¹ = revf :> (fin u -> fin u).
Proof using . reflexivity. Qed.

Lemma rebfV : rebf⁻¹ == rebf.
Proof using . intros f. rewrite rebfE, rebfVE. reflexivity. Qed.

(*
 *
 *            The bijection on fin u whose section and retraction are oppf
 *
 *)

Definition opbf : bijection (fin u)
  := cancel_bijection oppf _ oppfI oppfK oppfK.
Global Opaque opbf.

Lemma opbfE : opbf = oppf :> (fin u -> fin u).
Proof using . reflexivity. Qed.

Lemma opbfVE : opbf⁻¹ = oppf :> (fin u -> fin u).
Proof using . reflexivity. Qed.

Lemma opbfV : opbf⁻¹ == opbf.
Proof using . intros f. rewrite opbfE, opbfVE. reflexivity. Qed.

Lemma spbf_rebf : spbf ∘ rebf == opbf.
Proof using .
  intros f. rewrite compE, spbfE, rebfE, opbfE. symmetry. apply oppfE.
Qed.

Lemma spbfV_opbf : spbf⁻¹ ∘ opbf == rebf.
Proof using.
  rewrite <- spbf_rebf, compose_assoc, compose_inverse_l. apply id_comp_l.
Qed.

(*
 *
 *            The bijection on fin u whose section and retraction are symf
 *
 *)

Definition sybf (c : fin u) : bijection (fin u) :=
  cancel_bijection (symf c) _ (@symfI c) (symfK c) (symfK c).
Global Opaque sybf.

Lemma sybfE : ∀ c : fin u, sybf c = symf c :> (fin u -> fin u).
Proof using . reflexivity. Qed.

Lemma sybfVE : ∀ c : fin u, sybf c⁻¹ = symf c :> (fin u -> fin u).
Proof using . reflexivity. Qed.

Lemma sybfV : ∀ c : fin u, sybf c⁻¹ == sybf c.
Proof using . intros c f. rewrite sybfE, sybfVE. reflexivity. Qed.

Lemma sybfK : ∀ c : fin u, sybf c ∘ sybf c == id.
Proof using . intros c f. rewrite compE, sybfE. apply symfK. Qed.

Lemma sybff : ∀ c : fin u, sybf c c = c.
Proof using . intros. rewrite sybfE. apply symff. Qed.

End mod2fin.
