(**************************************************************************)
(*   Mechanised Framework for Local Interactions & Distributed Algorithms *)
(*   P. Courtieu, L. Rieg, X. Urbain                                      *)
(*   PACTOLE project                                                      *)
(*                                                                        *)
(*   This file is distributed under the terms of the CeCILL-C licence     *)
(*                                                                        *)
(**************************************************************************)

(**************************************************************************)
(**  Mechanised Framework for Local Interactions & Distributed Algorithms   
                                                                            
     P. Courtieu, L. Rieg, X. Urbain                                        
                                                                            
     PACTOLE project                                                        
                                                                            
     This file is distributed under the terms of the CeCILL-C licence       
                                                                          *)
(**************************************************************************)


Require Import Utf8 Relations Morphisms SetoidClass.
Require Logic.Decidable.
Set Implicit Arguments.

Ltac autoclass := eauto with typeclass_instances.
Ltac inv H := inversion H; subst; clear H.
Hint Extern 1 (equiv ?x ?x) => reflexivity : core.
Hint Extern 2 (equiv ?y ?x) => now symmetry : core.
Hint Unfold complement : core.

(** **  Tactics  **)

(** A tactic simplifying coinduction proofs. *)
Global Ltac coinduction proof :=
  cofix proof; intros; constructor;
   [ clear proof | try (apply proof; clear proof) ].

(** Destruct matches from innermost to outermost, with or without keeping the associated equality. *)
Ltac destr_match_eq name A :=
  match A with | context[match ?x with | _ => _ end] =>
    destr_match_eq name x (* recursive call *)
    || (* let H := fresh name in *) destruct x eqn:name (* if innermost match, destruct it *)
  end.

Ltac destruct_match_eq name := match goal with | |- ?A => destr_match_eq name A end.

Ltac destr_match A :=
  match A with context[match ?x with | _ => _ end] =>
    destr_match x (* recursive call *)
    || match type of x with  (* if innermost match, destruct it *)
         | sumbool _ _ => let H := fresh in destruct x as [H | H]
         | _ => let H := fresh in destruct x eqn:H
       end
  end.

Ltac destruct_match := match goal with | |- ?A => destr_match A end.

(** Tactics to revert hypotheses based on their head symbol *)
Ltac get_head_symbol term :=
  match term with
    | ?f _ => get_head_symbol f
    | _ => term
  end.

(* Revert one hypothesis whose head symbol is [sym]. *)
Ltac revert_one sym :=
  match goal with
    | H : ?A |- _ => let f := get_head_symbol A in let g := get_head_symbol sym in unify f g; revert H
    | |- _ => fail "Symbol" sym "not found"
  end.

(* Revert all hypotheses with the same head symbol as the goal. *)
Ltac revert_all :=
  match goal with
    | |- ?B => repeat revert_one B
  end.

Definition injective {A B : Type} (eqA : relation A) (eqB : relation B) (f : A -> B) : Prop :=
  forall x y, eqB (f x) (f y) -> eqA x y.

Definition monotonic {A B : Type} (RA : relation A) (RB : relation B) (f : A -> B) :=
  Proper (RA ==> RB) f \/ Proper (RA --> RB) f.

Definition full_relation {A : Type} := fun _ _ : A => True.

#[export] Hint Extern 0 (full_relation _ _) => exact I : core.

Global Instance relation_equivalence_subrelation {A} :
  forall R R' : relation A, relation_equivalence R R' -> subrelation R R'.
Proof. intros R R' Heq x y Hxy. now apply Heq. Qed.

Global Instance neq_Symmetric {A} : Symmetric (fun x y : A => x <> y).
Proof. intros ? ? Hneq ?. apply Hneq. now symmetry. Qed.

Global Hint Extern 3 (relation_equivalence _ _) => symmetry : core.

(** Notations for composition and inverse *)

Class Composition T `{Setoid T} := {
  compose : T -> T -> T;
  #[global]compose_compat :: Proper (equiv ==> equiv ==> equiv) compose }.
Infix "∘" := compose (left associativity, at level 40).
Arguments Composition T {_}.

(* TODO: generic properties of inverse should be proven here, not in each file
         (Bijection, Similarity, Isometry, etc.) *)
Class Inverse T `{Setoid T} := {
  inverse : T -> T;
  #[global]inverse_compat :: Proper (equiv ==> equiv) inverse }.
Notation "bij ⁻¹" := (inverse bij) (at level 39).
Arguments Inverse T {_}.

(* pick_spec can be useful when you want to test whether some 'oT : option T' is
   'Some t' or 'None'. Destructing 'pick_spec P oT' gives you directly 'P t'
   in the first case and '∀ t : T, ¬ (P t)' in the other one *)
Variant pick_spec (T : Type) (P : T -> Prop) : option T -> Type :=
    Pick : ∀ x : T, P x → pick_spec P (Some x)
  | Nopick : (∀ x : T, ¬ (P x)) → pick_spec P None.
Arguments pick_spec [T] _ _.
Arguments Pick [T P x] _.
Arguments Nopick [T P] _.

Lemma injective_compat_iff :
  ∀ {A B : Type} {eqA : relation A} {eqB : relation B} (f : A -> B)
  {compat : Proper (eqA ==> eqB) f}, injective eqA eqB f
  -> ∀ a1 a2 : A, eqB (f a1) (f a2) <-> eqA a1 a2.
Proof using .
  intros * Hcompat Hinj *. split. apply Hinj. apply Hcompat.
Qed.

Lemma injective_eq_iff : ∀ {A B : Type} (f : A -> B),
  injective eq eq f -> ∀ a1 a2 : A, f a1 = f a2 <-> a1 = a2.
Proof using . intros *. apply injective_compat_iff. autoclass. Qed.

Lemma eq_sym_iff : ∀ (A : Type) (x y : A), x = y <-> y = x.
Proof using . intros. split. all: apply eq_sym. Qed.

Lemma decidable_not_and_iff :
  ∀ P Q : Prop, Decidable.decidable P -> ¬ (P ∧ Q) <-> ¬ P ∨ ¬ Q.
Proof using .
  intros * Hd. split. apply Decidable.not_and, Hd. intros Ho Ha.
  destruct Ho as [Ho | Ho]. all: apply Ho, Ha.
Qed.

Lemma decidable_not_not_iff : ∀ P : Prop, Decidable.decidable P -> ¬ ¬ P <-> P.
Proof using .
  intros * Hd. split. apply Decidable.not_not, Hd. intros H Hn. apply Hn, H.
Qed.

Lemma sumbool_iff_compat :
  ∀ P Q R S: Prop, P <-> R -> Q <-> S -> {P} + {Q} -> {R} + {S}.
Proof using .
  intros * H1 H2 [Hd | Hd]. left. apply H1, Hd. right. apply H2, Hd.
Qed.

Lemma sumbool_not_iff_compat :
  ∀ P Q : Prop, P <-> Q -> {P} + {¬ P} -> {Q} + {¬ Q}.
Proof using .
  intros * H Hd. eapply sumbool_iff_compat.
  2: apply not_iff_compat. 1,2: apply H. apply Hd.
Qed.

Lemma sumbool_and_compat :
  ∀ P Q R S: Prop, {P} + {Q} -> {R} + {S} -> {P ∧ R} + {Q ∨ S}.
Proof using .
  intros * [H1 | H1] [H2 | H2]. left. split. apply H1. apply H2.
  all: right. right. apply H2. all: left. all: apply H1.
Qed.

Lemma sumbool_decidable : ∀ P : Prop, {P} + {¬ P} -> Decidable.decidable P.
Proof using . intros P [H | H]. left. apply H. right. apply H. Qed.

Lemma pair_eqE :
  ∀ {A B : Type} (p1 p2 : A * B), fst p1 = fst p2 ∧ snd p1 = snd p2 <-> p1 = p2.
Proof using .
  intros. rewrite (surjective_pairing p1), (surjective_pairing p2).
  symmetry. apply pair_equal_spec.
Qed.

Lemma pair_dec : ∀ {A B : Type},
  (∀ a1 a2 : A, {a1 = a2} + {a1 ≠ a2}) -> (∀ b1 b2 : B, {b1 = b2} + {b1 ≠ b2})
  -> ∀ p1 p2 : A * B, {p1 = p2} + {p1 ≠ p2}.
Proof using .
  intros * Ha Hb *. eapply sumbool_not_iff_compat. apply pair_eqE.
  eapply sumbool_iff_compat. reflexivity. symmetry. apply decidable_not_and_iff.
  2: apply sumbool_and_compat. 2: apply Ha. 2: apply Hb.
  apply sumbool_decidable, Ha.
Qed.

Lemma and_cancel : ∀ P : Prop, P -> P ∧ P.
Proof using . intros * H. split. all: apply H. Qed.
