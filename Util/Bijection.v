(**************************************************************************)
(**  Mechanised Framework for Local Interactions & Distributed Algorithms   
     T. Balabonski, P. Courtieu, R. Pelle, L. Rieg, X. Urbain               
                                                                            
     PACTOLE project                                                        
                                                                            
     This file is distributed under the terms of the CeCILL-C licence.      
                                                                          *)
(**************************************************************************)


Require Import Utf8.
Require Import SetoidDec.
Require Import Pactole.Util.Coqlib.
Set Implicit Arguments.


(********************)
(** *  Bijections  **)
(********************)

(** Bijections on a type [T] with an equivalence relation [eqT] *)

Section Bijections.
Context {T : Type}.
Context {HeqT : Setoid T}.

Record bijection := {
 #[export] section :> T → T;
  retraction : T → T;
  section_compat : Proper (equiv ==> equiv) section;
  Inversion : ∀ x y, section x == y ↔ retraction y == x}.
#[export]Existing Instance section_compat.

#[export]Instance bij_Setoid : Setoid bijection.
simple refine {| equiv := λ f g, ∀ x, f.(section) x == g x |}; auto; [].
Proof. split.
+ repeat intro. reflexivity.
+ repeat intro. now symmetry.
+ repeat intro. etransitivity; eauto.
Defined.

(* Use fun_Setoid directly in the definition of bij_Setoid? *)
#[export]Instance bij_Setoid_eq_compat : Proper ((@equiv bijection bij_Setoid)
  ==> (@equiv (T -> T) (fun_Setoid T HeqT))) section.
Proof using . intros ?? H. apply H. Qed.

#[export]Instance section_full_compat : Proper (equiv ==> (equiv ==> equiv)) section.
Proof using . intros f g Hfg x y Hxy. rewrite Hxy. now apply Hfg. Qed.

#[export]Instance retraction_compat : Proper (equiv ==> (equiv ==> equiv)) retraction.
Proof using . intros f g Hfg x y Hxy. now rewrite <- f.(Inversion), Hxy, Hfg, g.(Inversion). Qed.

(** The identity bijection *)
Definition id := {|
  section := fun x => x;
  retraction := fun x => x;
  section_compat := fun x y Heq => Heq;
  Inversion := ltac:(easy) |}.

(** Composition of bijections *)
Definition comp (f g : bijection) : bijection.
refine {| section := fun x => f (g x);
          retraction := fun x => g.(retraction) (f.(retraction) x) |}.
Proof.
+ abstract (intros x y Hxy; now apply f.(section_compat), g.(section_compat)).
+ abstract (intros x y; now rewrite f.(Inversion), <- g.(Inversion)).
Defined.

#[export]Instance BijectionComposition : Composition bijection.
refine {| compose := comp |}.
Proof.
intros f1 f2 Hf g1 g2 Hg x. cbn -[equiv].
rewrite (Hf (g1 x)). f_equiv. apply Hg.
Defined.

Lemma compE : ∀ f g : bijection, f ∘ g = (λ t, f (g t)) :> (T -> T).
Proof using . reflexivity. Qed.

(*
#[export]Instance compose_compat : Proper (equiv ==> equiv ==> equiv) compose.
Proof.
intros f1 f2 Hf g1 g2 Hg x. cbn -[equiv].
rewrite (Hf (g1 x)). f_equiv. apply Hg.
Qed.
*)
Lemma compose_assoc : forall f g h : bijection, f ∘ (g ∘ h) == (f ∘ g) ∘ h.
Proof using . repeat intro. reflexivity. Qed.

(** Properties about inverse functions *)
Definition inv (bij : bijection) : bijection.
refine {| section := bij.(retraction);
          retraction := bij.(section) |}.
Proof. abstract (intros; rewrite bij.(Inversion); reflexivity). Defined.

#[export]Instance BijectionInverse : Inverse bijection.
refine {| inverse := inv |}.
Proof. repeat intro. simpl. now f_equiv. Defined.

Lemma id_inv : id⁻¹ == id.
Proof using . intros t. reflexivity. Qed.

Lemma id_comp_l : ∀ b : bijection, id ∘ b == b.
Proof using. intros. cbn. reflexivity. Qed.

Lemma id_comp_r : ∀ b : bijection, b ∘ id == b.
Proof using. intros. cbn. reflexivity. Qed.

Lemma inv_inv : ∀ b : bijection, b⁻¹⁻¹ == b.
Proof using . intros. cbn. reflexivity. Qed.

(*
#[export]Instance inverse_compat : Proper (equiv ==> equiv) inverse.
Proof. repeat intro. simpl. now f_equiv. Qed.
*)
Lemma retraction_section : forall (bij : bijection) x, bij.(retraction) (bij.(section) x) == x.
Proof using . intros bij x. simpl. rewrite <- bij.(Inversion). now apply section_compat. Qed.

Corollary compose_inverse_l : forall (bij : bijection), bij ⁻¹ ∘ bij == id.
Proof using . repeat intro. simpl. now rewrite retraction_section. Qed.

Lemma section_retraction : forall (bij : bijection) x, bij.(section) (bij.(retraction) x) == x.
Proof using . intros bij x. rewrite bij.(Inversion). now apply retraction_compat. Qed.

Corollary compose_inverse_r : forall (bij : bijection), bij ∘ bij ⁻¹ == id.
Proof using . repeat intro. simpl. now rewrite section_retraction. Qed.

Lemma inverse_compose : forall f g : bijection, (f ∘ g)⁻¹ == (g ⁻¹) ∘ (f ⁻¹).
Proof using . repeat intro. reflexivity. Qed.

Definition cancel_bijection (f g : T -> T)
  (fP : Proper (equiv ==> equiv) f) (f_inj : injective equiv equiv f)
  (fK : ∀ t, g (f t) == t) (gK : ∀ t, f (g t) == t) : bijection.
Proof using .
  refine {|
    section := f;
    retraction := g;
    section_compat := fP |}.
  abstract (intros t1 t2; split; intros H; [rewrite <- (gK t2) in H;
    apply f_inj in H; rewrite H; reflexivity | rewrite <- H; apply gK]).
Defined.

(** Bijections are in particular injective. *)
Lemma injective : forall bij : bijection, injective equiv equiv bij.
Proof using . intros bij x y Heq. now rewrite <- (retraction_section bij x), Heq, retraction_section. Qed.

End Bijections.

Arguments bijection T {_}.
Arguments section {_} {_} !_ x.
Arguments retraction {_} {_} !_ x.

Section prod_bij.

Context {A B : Type} (SA : Setoid A) (SB : Setoid B).

Definition prod_bij (BA : bijection A) (BB : bijection B) : bijection (A * B).
Proof using .
  refine {|
    section := λ p, (BA (fst p), BB (snd p));
    retraction := λ p, (BA⁻¹ (fst p), BB⁻¹ (snd p)) |}.
  abstract (intros ?? H; rewrite H; reflexivity).
  abstract (intros; split; intros H; rewrite <- H; split;
    apply Inversion; reflexivity).
Defined.

Lemma prod_bijE : ∀ (BA : bijection A) (BB : bijection B),
  prod_bij BA BB = (λ p : A * B, (BA (fst p), BB (snd p))) :> (A * B -> A * B).
Proof using . reflexivity. Qed.

Lemma prod_bijVE : ∀ (BA : bijection A) (BB : bijection B),
  prod_bij BA BB⁻¹ = (λ p, (BA⁻¹ (fst p), BB⁻¹ (snd p))) :> (A * B -> A * B).
Proof using . reflexivity. Qed.

#[export]Instance prod_bij_compat : Proper (equiv ==> equiv ==> equiv) prod_bij.
Proof using .
  intros * BA1 BA2 HA BB1 BB2 HB p. rewrite 2 prod_bijE. split.
  rewrite HA. reflexivity. rewrite HB. reflexivity.
Qed.

Lemma prod_bij_id : prod_bij (@id A SA) (@id B SB) == id.
Proof using . intros * p. rewrite prod_bijE. split. all: reflexivity. Qed.

Lemma prod_bij_comp : ∀ (BA1 BA2 : bijection A) (BB1 BB2 : bijection B),
  prod_bij BA1 BB1 ∘ (prod_bij BA2 BB2) == prod_bij (BA1 ∘ BA2) (BB1 ∘ BB2).
Proof using . intros * p. rewrite compE, 3 prod_bijE, 2 compE. reflexivity. Qed.

Lemma prod_bij_inv : ∀ (BA : bijection A) (BB : bijection B),
  (prod_bij BA BB)⁻¹ == prod_bij (BA⁻¹) (BB⁻¹).
Proof using . intros * p. rewrite prod_bijVE, prod_bijE. reflexivity. Qed.

Definition prod_eq_bij (BA : @bijection A (eq_setoid A))
  (BB : @bijection B (eq_setoid B)) : @bijection (A * B) (eq_setoid (A * B)).
Proof using .
  refine {|
    section := λ p, (BA (fst p), BB (snd p));
    retraction := λ p, (BA⁻¹ (fst p), BB⁻¹ (snd p)) |}.
  abstract (intros; split; cbn; intros H; rewrite <- H, <- pair_eqE;
    cbn[fst snd]; [rewrite 2 retraction_section | rewrite 2 section_retraction];
    split; reflexivity).
Defined.

Lemma prod_eq_bijE : ∀ (BA : bijection A) (BB : bijection B), prod_eq_bij BA BB
  = (λ p : A * B, (BA (fst p), BB (snd p))) :> (A * B -> A * B).
Proof using . reflexivity. Qed.

Lemma prod_eq_bijVE : ∀ (BA : bijection A) (BB : bijection B),
  prod_eq_bij BA BB⁻¹ = (λ p, (BA⁻¹ (fst p), BB⁻¹ (snd p))) :> (A * B -> A * B).
Proof using . reflexivity. Qed.

#[export]Instance prod_eq_bij_compat :
  Proper (equiv ==> equiv ==> equiv) prod_eq_bij.
Proof using .
  cbn. intros * BA1 BA2 HA BB1 BB2 HB p. rewrite 2 prod_eq_bijE, <- pair_eqE.
  split. rewrite HA. reflexivity. rewrite HB. reflexivity.
Qed.

Lemma prod_eq_bij_id : prod_eq_bij id id == id.
Proof using .
  intros * p. rewrite prod_eq_bijE. apply pair_eqE. split. all: reflexivity.
Qed.

Lemma prod_eq_bij_comp : ∀ (BA1 BA2 : bijection A) (BB1 BB2 : bijection B),
  prod_eq_bij BA1 BB1 ∘ (prod_eq_bij BA2 BB2)
  == prod_eq_bij (BA1 ∘ BA2) (BB1 ∘ BB2).
Proof using .
  intros * p. rewrite compE, 3 prod_eq_bijE, 2 compE. reflexivity.
Qed.

Lemma prod_eq_bij_inv : ∀ (BA : bijection A) (BB : bijection B),
  (prod_eq_bij BA BB)⁻¹ == prod_eq_bij (BA⁻¹) (BB⁻¹).
Proof using . intros * p. rewrite prod_eq_bijVE, prod_eq_bijE. reflexivity. Qed.

End prod_bij.

Section equiv_bij.

Context {T : Type} [S1 S2 : Setoid T].
Context (H : ∀ t1 t2 : T, @equiv T S1 t1 t2 <-> @equiv T S2 t1 t2).

Definition equiv_bij : @bijection T S1 -> @bijection T S2.
Proof using H.
  intros [s r c p]. refine {| section := s; retraction := r |}.
  abstract (intros ?? Heq; apply H, c, H, Heq).
  abstract (intros; rewrite <-2 H; apply p).
Defined.

Lemma equiv_bijE : ∀ b : @bijection T S1, equiv_bij b = b :> (T -> T).
Proof using . intros []. reflexivity. Qed.

Lemma equiv_bijVE : ∀ b : @bijection T S1, equiv_bij b⁻¹ = b⁻¹ :> (T -> T).
Proof using . intros []. reflexivity. Qed.

Lemma equiv_bij_id : equiv_bij (@id T S1) == (@id T S2).
Proof using . intros t. rewrite equiv_bijE. reflexivity. Qed.

End equiv_bij.
