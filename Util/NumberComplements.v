(**************************************************************************)
(*   Mechanised Framework for Local Interactions & Distributed Algorithms *)
(*   P. Courtieu, L. Rieg, X. Urbain                                      *)
(*   PACTOLE project                                                      *)
(*                                                                        *)
(*   This file is distributed under the terms of the CeCILL-C licence     *)
(*                                                                        *)
(**************************************************************************)

(**************************************************************************)
(**  Mechanised Framework for Local Interactions & Distributed Algorithms   
                                                                            
     P. Courtieu, L. Rieg, X. Urbain                                        
                                                                            
     PACTOLE project                                                        
                                                                            
     This file is distributed under the terms of the CeCILL-C licence       
                                                                          *)
(**************************************************************************)

Require Import Utf8 SetoidDec Reals Lia Psatz.
Require Import Pactole.Util.SetoidDefs.
Set Implicit Arguments.

(* ******************************** *)
(** *  Results about real numbers  **)
(* ******************************** *)

Open Scope R_scope.

(* Should be in Reals from the the std lib! *)
Global Instance Rle_preorder : PreOrder Rle.
Proof. split.
- intro. apply Rle_refl.
- intro. apply Rle_trans.
Qed.

Global Instance Rle_partialorder : PartialOrder eq Rle.
Proof.
intros x y. unfold Basics.flip. cbn. split; intro Hxy.
- now subst.
- destruct Hxy. now apply Rle_antisym.
Qed.

Global Instance Rle_partialorder_equiv : PartialOrder equiv Rle := Rle_partialorder.

Global Instance Rlt_SO : StrictOrder Rlt.
Proof. split.
+ intro. apply Rlt_irrefl.
+ intros ? ? ?. apply Rlt_trans.
Qed.

Global Instance R_EqDec : @EqDec R _ := Req_dec_T.
(* #[export]  *)
Notation Rdec := R_EqDec.

Lemma Rdiv_le_0_compat : forall a b, 0 <= a -> 0 < b -> 0 <= a / b.
Proof. intros a b ? ?. now apply Fourier_util.Rle_mult_inv_pos. Qed.

Lemma Rdiv_le_1 : forall a b, 0 < b -> a <= b -> a / b <= 1.
Proof.
intros a b Hb Ha. rewrite <- (Rinv_r b ltac:(lra)).
apply Rmult_le_compat_r; trivial; [].
now apply Rlt_le, Rinv_0_lt_compat.
Qed.


(* Lemma Rdiv_le_compat : forall a b c, (0 <= a -> a <= b -> 0 < c -> a / c <= b / c)%R.
Proof.
intros a b c ? ? ?. unfold Rdiv. apply Rmult_le_compat; try lra.
rewrite <- Rmult_1_l. apply Fourier_util.Rle_mult_inv_pos; lra.
Qed. *)

Lemma Zup_lt : forall x y, x <= y - 1 -> (up x < up y)%Z.
Proof.
intros x y Hle. apply lt_IZR. apply Rle_lt_trans with (x + 1).
- generalize (proj2 (archimed x)). lra.
- apply Rle_lt_trans with (y - 1 + 1); try lra; [].
  generalize (proj1 (archimed y)). lra.
Qed.

Lemma up_le_0_compat : forall x, 0 <= x -> (0 <= up x)%Z.
Proof. intros x ?. apply le_0_IZR, Rlt_le, Rle_lt_trans with x; trivial; []. now destruct (archimed x). Qed.

(** A boolean equality test. *)
Definition Rdec_bool x y := match Rdec x y with left _ => true | right _ => false end.

Global Instance Rdec_bool_compat : Proper (eq ==> eq ==> eq) Rdec_bool.
Proof. repeat intro. subst. reflexivity. Qed.

Lemma Rdec_bool_true_iff : forall x y, Rdec_bool x y = true <-> x = y.
Proof. intros. unfold Rdec_bool. destruct (Rdec x y); now split. Qed.

Lemma Rdec_bool_false_iff : forall x y, Rdec_bool x y = false <-> x <> y.
Proof. intros. unfold Rdec_bool. destruct (Rdec x y); now split. Qed.

Lemma if_Rdec : forall A x y (l r : A), (if Rdec x y then l else r) = if Rdec_bool x y then l else r.
Proof. intros. unfold Rdec_bool. now destruct Rdec. Qed.

Lemma Rdec_bool_plus_l : forall k r r', Rdec_bool (k + r) (k + r') = Rdec_bool r r'.
Proof.
intros k r r'.
destruct (Rdec_bool (k + r) (k + r')) eqn:Heq1, (Rdec_bool r r') eqn:Heq2; trivial;
rewrite ?Rdec_bool_true_iff, ?Rdec_bool_false_iff in *.
- contradiction Heq2. eapply Rplus_eq_reg_l; eassumption.
- subst. auto.
Qed.

Lemma Rdec_bool_mult_l : forall k r r', (k <> 0)%R -> Rdec_bool (k * r) (k * r') = Rdec_bool r r'.
Proof.
intros k r r' Hk.
destruct (Rdec_bool r r') eqn:Heq1, (Rdec_bool (k * r) (k * r')) eqn:Heq2; trivial;
rewrite ?Rdec_bool_true_iff, ?Rdec_bool_false_iff in *.
- subst. auto.
- contradiction Heq1. eapply Rmult_eq_reg_l; eassumption.
Qed.

Corollary Rdec_bool_plus_r : forall k r r', Rdec_bool (r + k) (r' + k) = Rdec_bool r r'.
Proof. intros. setoid_rewrite Rplus_comm. apply Rdec_bool_plus_l. Qed.

Corollary Rdec_bool_mult_r : forall k r r', (k <> 0)%R -> Rdec_bool (r * k) (r' * k) = Rdec_bool r r'.
Proof. intros. setoid_rewrite Rmult_comm. now apply Rdec_bool_mult_l. Qed.

(** A boolean inequality test. *)
Definition Rle_bool x y :=
  match Rle_dec x y with
    | left _ => true
    | right _ => false
  end.

Lemma Rle_bool_true_iff : forall x y, Rle_bool x y = true <-> Rle x y.
Proof. intros x y. unfold Rle_bool. destruct (Rle_dec x y); intuition discriminate. Qed.

Lemma Rle_bool_false_iff : forall x y, Rle_bool x y = false <-> ~Rle x y.
Proof. intros x y. unfold Rle_bool. destruct (Rle_dec x y); intuition discriminate. Qed.

Lemma Rle_bool_mult_l : forall k x y, (0 < k)%R -> Rle_bool (k * x) (k * y) = Rle_bool x y.
Proof.
intros k x y Hk. destruct (Rle_bool x y) eqn:Hle.
- rewrite Rle_bool_true_iff in *. now apply Rmult_le_compat_l; trivial; apply Rlt_le.
- rewrite Rle_bool_false_iff in *. intro Habs. apply Hle. eapply Rmult_le_reg_l; eassumption.
Qed.

Lemma Rle_bool_plus_l : forall k x y, Rle_bool (k + x) (k + y) = Rle_bool x y.
Proof.
intros k x y. destruct (Rle_bool x y) eqn:Hle.
- rewrite Rle_bool_true_iff in *. now apply Rplus_le_compat_l.
- rewrite Rle_bool_false_iff in *. intro Habs. apply Hle. eapply Rplus_le_reg_l; eassumption.
Qed.

Corollary Rle_bool_plus_r : forall k r r', Rle_bool (r + k) (r' + k) = Rle_bool r r'.
Proof. intros. setoid_rewrite Rplus_comm. apply Rle_bool_plus_l. Qed.

Corollary Rle_bool_mult_r : forall k r r', (0 < k)%R -> Rle_bool (r * k) (r' * k) = Rle_bool r r'.
Proof. intros. setoid_rewrite Rmult_comm. now apply Rle_bool_mult_l. Qed.

Lemma Rle_neq_lt : forall x y : R, x <= y -> x <> y -> x < y.
Proof. intros ? ? Hle ?. now destruct (Rle_lt_or_eq_dec _ _ Hle). Qed.

Unset Implicit Arguments.
Lemma succ_neq : forall x, x <> x+1.
Proof.
intros x Habs. assert (Heq : x = x + 0) by ring. rewrite Heq in Habs at 1. clear Heq.
apply Rplus_eq_reg_l in Habs. symmetry in Habs. revert Habs. exact R1_neq_R0.
Qed.

Lemma sqrt_square : forall x, sqrt (x * x) = Rabs x.
Proof.
intro x. unfold Rabs. destruct (Rcase_abs x).
+ replace (x * x) with ((-x) * (-x)) by lra. apply sqrt_square; lra.
+ apply sqrt_square; lra.
Qed.

(** Some results about [R]. *)
Lemma sqrt_subadditive : forall x y, 0 <= x -> 0 <= y -> sqrt (x + y) <= sqrt x + sqrt y.
Proof.
intros x y Hx Hy. apply R_sqr.Rsqr_incr_0.
- repeat rewrite Rsqr_sqrt, ?R_sqr.Rsqr_plus; try lra.
  assert (0 <= 2 * sqrt x * sqrt y).
  { repeat apply Rmult_le_pos; try lra; now apply sqrt_positivity. }
  lra.
- apply sqrt_positivity. lra.
- apply Rplus_le_le_0_compat; now apply sqrt_positivity.
Qed.

Lemma pos_Rsqr_eq : forall x y, 0 <= x -> 0 <= y -> x² = y² -> x = y.
Proof. intros. setoid_rewrite <- sqrt_Rsqr; trivial. now f_equal. Qed.

Lemma pos_Rsqr_le : forall x y, 0 <= x -> 0 <= y -> (x² <= y² <-> x <= y).
Proof. intros. split; intro; try now apply R_sqr.Rsqr_incr_0 + apply R_sqr.Rsqr_incr_1. Qed.

Lemma pos_Rsqr_lt : forall x y, 0 <= x -> 0 <= y -> (x² < y² <-> x < y).
Proof. intros. split; intro; try now apply R_sqr.Rsqr_incrst_0 + apply R_sqr.Rsqr_incrst_1. Qed.

Lemma pos_Rsqr_le_impl : forall x y, 0 <= y -> x² <= y² -> x <= y.
Proof.
intros x y Hle Hsqr.
destruct (Rle_dec 0 x).
- now apply R_sqr.Rsqr_incr_0.
- transitivity 0; lra.
Qed.

Lemma Rabs_bounds : forall x y, 0 <= y -> (Rabs x <= y <-> -y <= x <= y).
Proof.
intros x y Hy. split; intro Hle.
+ split.
  - cut (-x <= y); try lra; []. etransitivity; eauto; [].
    rewrite <- Rabs_Ropp. apply Rle_abs.
  - etransitivity; eauto; []. apply Rle_abs.
+ now apply Rabs_le.
Qed.

Close Scope R_scope.

(** *  Results about integers  **)

Lemma nat_compare_Eq_comm : forall n m, Nat.compare n m = Eq <-> Nat.compare m n = Eq.
Proof. intros n m. do 2 rewrite Nat.compare_eq_iff. now split. Qed.

Lemma nat_compare_Lt_Gt : forall n m, Nat.compare n m = Lt <-> Nat.compare m n = Gt.
Proof. intros n m. rewrite <- nat_compare_lt, <- nat_compare_gt. now split. Qed.

Definition nat_ind2 P P0 P1 PSS :=
  fix F (n : nat) : P n :=
    match n as n0 return (P n0) with
      | 0 => P0
      | S 0 => P1
      | S (S m) => PSS m (F m)
    end.

Lemma div2_le_compat : Proper (le ==> le) Nat.div2.
Proof.
intro n. induction n using nat_ind2; intros m Heq; auto with arith.
destruct m as [| [| m]]; try now inversion Heq.
simpl. do 2 apply le_S_n in Heq. apply IHn in Heq. lia.
Qed.

Lemma even_div2 : forall n, Nat.Even n -> Nat.div2 n + Nat.div2 n = n.
Proof.
intros n Hn. replace (Nat.div2 n + Nat.div2 n) with (2 * Nat.div2 n) by lia.
rewrite <- Nat.double_twice. symmetry. now apply Nat.Even_double.
Qed.

Lemma eq_0_lt_dec : ∀ n : nat, {n = 0} + {0 < n}.
Proof using .
  intros. destruct (le_lt_dec n 0) as [Hd | Hd].
  left. apply Nat.le_0_r, Hd. right. apply Hd.
Qed.

Lemma sub_sub : ∀ m n p : nat, p <= n → m - (n - p) = m + p - n.
Proof using . lia. Qed.

Lemma sub_cancel_l : ∀ p m n : nat, m < p -> p - m = p - n <-> m = n.
Proof using . lia. Qed.

Lemma add_mul_lt : ∀ a b c d : nat, a < d -> b < c -> d * b + a < d * c.
Proof using . nia. Qed.

Lemma le_neq_lt : forall m n : nat, n <= m -> n ≠ m -> n < m.
Proof using . lia. Qed.

Lemma lt_sub_lt_add_l : ∀ n m p : nat, m <= n -> n < m + p -> n - m < p.
Proof using . lia. Qed.

Lemma lt_sub_lt_add_r : ∀ n m p : nat, p <= n -> n < m + p -> n - p < m.
Proof using . lia. Qed.

Lemma sub_le_mono : ∀ n m p q : nat, n <= m -> p <= q -> n - q <= m - p.
Proof using . lia. Qed.

Lemma lt_sub : ∀ n m : nat, 0 < n -> 0 < m -> n - m < n.
Proof using . lia. Qed.

Lemma sub_lt_mono : ∀ n m p q : nat, p <= q -> p < m -> n < m -> n - q < m - p.
Proof using . lia. Qed.

(* Statement improvable? Weaker precondition than m <= p? *)
Lemma sub_lt_mono_l : ∀ m n p : nat, m <= p -> n < m -> p - m < p - n.
Proof using . lia. Qed.

Lemma add_sub_le_sub_add : ∀ n m p : nat, n + m - p <= n - p + m.
Proof using . lia. Qed.

Lemma S_pred2 : ∀ n : nat, 1 < n -> n = S (S (Nat.pred (Nat.pred n))).
Proof using . lia. Qed.

Lemma S_pred3 : ∀ n : nat,
  2 < n -> n = S (S (S (Nat.pred (Nat.pred (Nat.pred n))))).
Proof using . lia. Qed.

Lemma mod_bounded_diffI : ∀ p m n : nat, p ≠ 0 -> m <= n
  -> n - m < p -> m mod p = n mod p -> m = n.
Proof using .
intros * Hne Hle Hsu Heq.
rewrite (Nat.div_mod m p), (Nat.div_mod n p), Heq; trivial; [].
apply Nat.add_cancel_r, Nat.mul_cancel_l; trivial; [].
apply Nat.le_antisymm.
+ apply Nat.Div0.div_le_mono. exact Hle.
+ apply Nat.sub_0_le, Nat.lt_1_r. rewrite (Nat.mul_lt_mono_pos_l p); try lia; [].
  rewrite Nat.mul_1_r, Nat.mul_sub_distr_l.
  assert (Hmod : ∀ x y : nat, y ≠ 0 -> x - x mod y = y * (x / y)).
  { intros. rewrite Nat.Div0.mod_eq; trivial; [].
    cut (y * (x / y) <= x); try lia; [].
    rewrite (Nat.div_mod_eq x y) at 2. assert (HH := Nat.Div0.mod_le x y). lia. }
  setoid_rewrite <- Hmod; lia.
Qed.

Lemma between_1 : ∀ m : nat, m <= m < m +1.
Proof using . lia. Qed.

Lemma between_neq_0 : ∀ n p m : nat, p <= m < p + n -> n ≠ 0.
Proof using . lia. Qed.

Lemma between_addn : ∀ n1 n2 p1 p2 m1 m2 : nat, p1 <= m1 < p1 + n1
  -> p2 <= m2 < p2 + n2 -> p1 + p2 <= m1 + m2 < p1 + p2 + Nat.pred (n1 + n2).
Proof using . lia. Qed.

Lemma between_subn : ∀ n1 n2 p1 p2 m1 m2 : nat, p1 <= m1 < p1 + n1
  -> p2 <= m2 < p2 + n2 -> p1 - Nat.pred (p2 + n2)
  <= m1 - m2 < p1 - Nat.pred (p2 + n2) + Nat.pred (n1 + n2).
Proof using . lia. Qed.

Lemma bounded_diff_between : ∀ P : nat -> nat -> Prop, Symmetric P
  -> ∀ n : nat, (∀ m1 m2 : nat, m1 <= m2 -> m2 - m1 < n -> P m1 m2)
  <-> ∀ (p m1 m2: nat), p <= m1 < p + n -> p <= m2 < p + n -> P m1 m2.
Proof using .
intros * HS *. split.
+ intros H * [H11 H12] [H21 H22].
  destruct (le_ge_dec m1 m2) as [Hd | Hd]; [| symmetry]; apply H; trivial; lia.
+ intros H m1 m2 Hle Hlt. apply (H m1); split; lia.
Qed.

Lemma inj_sym : ∀ (A B : Type) (SA : Setoid A) (SB : Setoid B) (f : A -> B),
  Symmetric (λ a1 a2, f a1 == f a2 -> a1 == a2).
Proof using .
  intros * a1 a2 H1 H2. symmetry. apply H1. symmetry. exact H2.
Qed.

Lemma mod_betweenI : ∀ p n m1 m2 : nat, p <= m1 < p + n -> p <= m2 < p + n
  -> m1 mod n = m2 mod n -> m1 = m2.
Proof using .
  intros * H. pose proof (between_neq_0 _ _ _ H) as Hn. revert m1 m2 H.
  eapply (bounded_diff_between _ (inj_sym _ _ (eq_setoid nat) (eq_setoid nat)
  (λ x, x mod n))). intros * H1 H2 H3. eapply mod_bounded_diffI. all: eassumption.
Qed.

Lemma between_muln : ∀ p n m : nat, n * p <= m < n * p + n -> m / n = p.
Proof using . intros * []. symmetry. apply Nat.div_unique with (m - n * p); nia. Qed.

Lemma mod_le_between_compat : ∀ p n m1 m2 : nat, n * p <= m1 < n * p + n
  -> n * p <= m2 < n * p + n -> m1 <= m2 -> m1 mod n <= m2 mod n.
Proof using .
  intros * H1 H2 Hle. erewrite 2 Nat.Div0.mod_eq, 2 between_muln,
  Nat.add_le_mono_r, 2 Nat.sub_add;try assumption;try eassumption.
  - apply H2.
  - apply H1.
Qed.

Lemma mod_lt_between_compat : ∀ p n m1 m2 : nat, n * p <= m1 < n * p + n
  -> n * p <= m2 < n * p + n -> m1 < m2 -> m1 mod n < m2 mod n.
Proof using .
  intros * H1 H2 Hlt. erewrite 2 Nat.Div0.mod_eq, 2 between_muln,
    Nat.add_lt_mono_r, 2 Nat.sub_add;try assumption; try eassumption.
  - apply H2.
  - apply H1.
Qed.

Lemma divide_neq : ∀ a b : nat, b ≠ 0 -> Nat.divide a b -> a ≠ 0.
Proof using . intros * Hn Hd H. subst. apply Hn, Nat.divide_0_l, Hd. Qed.

Lemma divide_div_neq : ∀ a b : nat, b ≠ 0 -> Nat.divide a b -> b / a ≠ 0.
Proof using .
  intros * Hn Hd H. apply Hn. unshelve erewrite (Nat.div_mod b a),
  (proj2 (Nat.Lcm0.mod_divide b a)), Nat.add_0_r, H; try eassumption.
  - apply Nat.mul_0_r.
  - eapply divide_neq; eassumption.
  Qed.

Lemma addn_muln_divn : ∀ n q1 q2 r1 r2 : nat, n ≠ 0 -> r1 < n -> r2 < n
  -> n * q1 + r1 = n * q2 + r2 -> q1 = q2 ∧ r1 = r2.
Proof using .
  intros  * Hn H1 H2 H. apply Nat.div_unique in H as H'. rewrite Nat.mul_comm,
  Nat.div_add_l, Nat.div_small, Nat.add_0_r in H'. subst. split. reflexivity.
  eapply Nat.add_cancel_l. all: eassumption.
Qed.

Lemma divide_mul : ∀ a b : nat, b ≠ 0 -> Nat.divide a b -> b = a * (b / a).
Proof using .
  intros * Hn Hd. erewrite (Nat.div_mod b a) at 1.
  unshelve erewrite (proj2 (Nat.Lcm0.mod_divide b a )); try eassumption.
  - apply Nat.add_0_r.
  - eapply divide_neq;eassumption.
Qed.

Lemma divide_mod :
  ∀ a b c : nat, b ≠ 0 -> Nat.divide a b -> (c mod b) mod a = c mod a.
Proof using .
  intros * Hn Hd. rewrite (divide_mul a b), Nat.Div0.mod_mul_r, Nat.mul_comm,
  Nat.Div0.mod_add; try assumption.
  apply Nat.Div0.mod_mod.
Qed.

Lemma mul_add : ∀ a b : nat,
  b ≠ 0 -> a * b = a + Nat.pred b * a.
Proof using .
  intros * Hb. destruct (Nat.eq_dec a 0) as [?|Ha].
  - subst. rewrite Nat.mul_0_r. apply Nat.mul_0_l.
  - rewrite <- (Nat.mul_1_l a) at 2. rewrite <- Nat.mul_add_distr_r,
    Nat.add_1_l, Nat.succ_pred. apply Nat.mul_comm. apply Hb.
Qed.

Lemma pred_mul_add : ∀ a b : nat,
  b ≠ 0 -> Nat.pred (a * b) = Nat.pred a + Nat.pred b * a.
Proof using .
  intros * Hb. destruct (Nat.eq_dec a 0) as [?|Ha].
  - subst. rewrite Nat.mul_0_r. apply Nat.mul_0_l.
  - rewrite mul_add by apply Hb. symmetry. apply Nat.add_pred_l, Ha.
Qed.

Lemma lt_pred_mul : ∀ a b c d : nat,
  a < b -> c < d -> Nat.pred ((S a) * (S c)) < b * d.
Proof using .
  intros * Hab Hcd. erewrite <-  Nat.le_succ_l, Nat.lt_succ_pred.
  - apply Nat.mul_le_mono. apply Hab. apply Hcd.
  - apply Nat.mul_pos_pos. all: apply Nat.lt_0_succ.
Qed.

Fact mul_le_lt_compat : ∀ (n m p q : nat),
  0 < n -> n <= m -> p < q -> n * p < m * q.
Proof using.
  intros.
  apply Nat.le_lt_trans with (m * p).
  - apply Nat.mul_le_mono_r; assumption.
  - apply Nat.mul_lt_mono_pos_l; try assumption.
    apply Nat.lt_le_trans with n; assumption.
Qed.

Corollary mul_lt_compat : ∀ (m p q : nat), 0 < m -> p < q -> p < m * q.
Proof using.
  intros.
  rewrite <- (Nat.mul_1_l p).
  apply (@mul_le_lt_compat 1); try assumption.
  apply Nat.lt_0_1.
Qed.

Lemma neq_lt : ∀ a b : nat, a < b -> b ≠ a.
Proof using .
  intros * Hlt Heq. apply (Nat.lt_irrefl a). rewrite <- Heq at 2. exact Hlt.
Qed.

Lemma lt_pred : ∀ a b : nat, a < b -> Nat.pred b < b.
Proof using . lia. Qed.

Lemma neq_pred : ∀ a b c : nat, a < b -> b < c -> Nat.pred c ≠ a.
Proof using . lia. Qed.

Open Scope Z.

Global Instance Z_EqDec : @EqDec Z _ := Z.eq_dec.

Lemma Zincr_mod : forall k n, 0 < n ->
  (k + 1) mod n = k mod n + 1 \/ (k + 1) mod n = 0 /\ k mod n = n - 1.
Proof.
intros k n Hn.
destruct (Z.eq_dec (k mod n) (n - 1)) as [Heq | Heq].
+ right. rewrite <- Zdiv.Zplus_mod_idemp_l, Heq.
  ring_simplify (n - 1 + 1). split; trivial; []. apply Z.mod_same. lia.
+ left. rewrite <- Zdiv.Zplus_mod_idemp_l. apply Z.mod_small.
  assert (Hle := Z.mod_pos_bound k n Hn). lia.
Qed.

Lemma Zopp_mod : forall k n, 0 <= k < n -> (-k) mod n = (n - (k mod n)) mod n.
Proof.
intros k n Hn. destruct (Z.eq_dec k 0).
- subst. rewrite 2 Z.mod_0_l, Z.sub_0_r, Z.mod_same; lia.
- rewrite Z.mod_opp_l_nz, Z.mod_small; try rewrite Z.mod_small; lia.
Qed.

Lemma Zadd_small_mod_non_conf : forall k n x, (0 < k < n -> (x + k) mod n <> x mod n)%Z.
Proof.
intros k n x ? Habs. assert (Hn : (0 < n)%Z) by lia.
assert (Hk : (k mod n = 0)%Z).
{ replace k with (x + k - x)%Z by ring.
  rewrite Zdiv.Zminus_mod, Habs, Zminus_diag, Z.mod_0_l; lia. }
rewrite Z.mod_small in Hk; lia.
Qed.

Lemma Zsub_mod_is_0 : forall k k' n, 0 <= k < n -> 0 <= k' < n -> ((k - k') mod n = 0 <-> k = k').
Proof.
intros k k' n Hk Hk'. split; intro Heq.
+ assert (Hbound : - n < k - k' < n) by lia.
  rewrite Zdiv.Zmod_divides in Heq; try lia; [].
  destruct Heq as [c Heq]. destruct (Z.eq_dec c 0) as [? | Hneq]; hnf in *; simpl in *; nia.
+ subst. now rewrite Z.sub_diag.
Qed.

Lemma Zmin_bounds : forall n m, n < m -> Z.min n (m - n) <= m / 2.
Proof. intros. apply Z.min_case_strong; intro; apply Zdiv.Zdiv_le_lower_bound; lia. Qed.

Close Scope Z.

Class ltc (l u : nat) :=  lt_l_u : l < u.
Infix "<c" := ltc (at level 70, no associativity).

Section ltc_l_u.

Context {l u : nat} {ltc_l_u : l <c u}.

Lemma neq_u_l : u ≠ l.
Proof using ltc_l_u. apply neq_lt, lt_l_u. Qed.

Lemma le_l_pred_u : l <= Nat.pred u.
Proof using ltc_l_u. apply Nat.lt_le_pred, lt_l_u. Qed.

Lemma lt_pred_u : Nat.pred u < u.
Proof using ltc_l_u. eapply lt_pred, lt_l_u. Qed.

Lemma S_pred_u : S (Nat.pred u) = u.
Proof using ltc_l_u. eapply Nat.lt_succ_pred, lt_l_u. Qed.

End ltc_l_u.

Section ltc_s_u.

Context {l u : nat} {ltc_l_u : l <c u}.

Lemma lt_s_u : ∀ s : nat, s <= l -> s < u.
Proof using ltc_l_u.
  intros * H. eapply Nat.le_lt_trans. exact H. exact lt_l_u.
Qed.

Lemma lt_s_pred_u : ∀ s : nat, s < l -> s < Nat.pred u.
Proof using ltc_l_u.
  intros * H. eapply Nat.lt_le_trans. exact H. exact le_l_pred_u.
Qed.

Lemma le_s_pred_u : ∀ s : nat, s <= l -> s <= Nat.pred u.
Proof using ltc_l_u.
  intros * H. eapply Nat.le_trans. exact H. exact le_l_pred_u.
Qed.

Lemma neq_pred_u_s : ∀ s : nat, s < l -> Nat.pred u ≠ s.
Proof using ltc_l_u. intros * H. eapply neq_pred. apply H. apply lt_l_u. Qed.

Lemma lt_S_l_u : u ≠ S l -> S l < u.
Proof using ltc_l_u.
  intros H. apply le_neq_lt. apply ltc_l_u. apply not_eq_sym, H.
Qed.

Lemma eq_S_l_lt_dec : {u = S l} + {S l < u}.
Proof using ltc_l_u.
  destruct (Nat.eq_dec u (S l)) as [Hd | Hd].
  left. apply Hd. right. apply lt_S_l_u, Hd.
Qed.

Global Instance lt_0_u : 0 <c u.
Proof using ltc_l_u. apply lt_s_u, Nat.le_0_l. Qed.

Lemma neq_u_0 : u ≠ 0.
Proof using ltc_l_u. apply neq_u_l. Qed.

Lemma le_0_pred_u : 0 <= Nat.pred u.
Proof using ltc_l_u. apply le_l_pred_u. Qed.

Lemma lt_l_g : ∀ g : nat, u <= g -> l < g.
Proof using ltc_l_u.
  intros * H. eapply Nat.lt_le_trans. exact lt_l_u. exact H.
Qed.

Lemma lt_sub_u : ∀ s : nat, 0 < s -> u - s < u.
Proof using ltc_l_u. intros * H. apply lt_sub, H. apply lt_0_u. Qed.

End ltc_s_u.

Section lt_pred_mul_ul_ur.

Context {ll lr ul ur : nat} {ltc_ll_ul : ll <c ul} {ltc_lr_ur : lr <c ur}.

Lemma lt_pred_mul_ul_ur : Nat.pred ((S ll) * (S lr)) < ul * ur.
Proof using ltc_ll_ul ltc_lr_ur.
  apply lt_pred_mul. all: apply lt_l_u.
Qed.

Lemma lt_ll_mul_ul_ur : ll < ul * ur.
Proof using ltc_ll_ul ltc_lr_ur.
  rewrite Nat.mul_comm. apply mul_lt_compat. apply lt_0_u. apply lt_l_u.
Qed.

Lemma lt_lr_mul_ul_ur : lr < ul * ur.
Proof using ltc_ll_ul ltc_lr_ur.
  apply mul_lt_compat. apply lt_0_u. apply lt_l_u.
Qed.

End lt_pred_mul_ul_ur.

Section lt_pred_mul_ul_ur.

Context {ll lr ul ur : nat} {ltc_ll_ul : ll <c ul} {ltc_lr_ur : lr <c ur}.

Lemma lt_pred_ul_mul_ul_ur : Nat.pred ul < ul * ur.
Proof using ltc_ll_ul ltc_lr_ur.
  eapply @lt_s_u. eapply @lt_pred_mul_ul_ur. apply lt_pred_u.
  apply lt_0_u. rewrite Nat.mul_1_r. reflexivity.
Qed.

Lemma lt_pred_ur_mul_ul_ur : Nat.pred ur < ul * ur.
Proof using ltc_ll_ul ltc_lr_ur.
  eapply @lt_s_u. eapply @lt_pred_mul_ul_ur. apply lt_0_u.
  apply lt_pred_u. rewrite Nat.mul_1_l. reflexivity.
Qed.

End lt_pred_mul_ul_ur.
