Require Export SetoidDec.
Require Export Pactole.Util.Coqlib.
Require Export Pactole.Util.SetoidDefs.
Require Export Pactole.Util.NumberComplements.
Require Export Pactole.Util.ListComplements.
Require Export Pactole.Util.Ratio.
Require Pactole.Util.Stream.
Require Pactole.Util.Lexprod.
Require Pactole.Util.Fin.
Require Pactole.Util.Enum.
Require Export Pactole.Core.Identifiers.
Require Export Pactole.Core.State.
Require Export Pactole.Core.Configuration.
Require Export Pactole.Observations.Definition.
Require Export Pactole.Core.Formalism.


Remove Hints eq_setoid : Setoid.
Coercion Bijection.section : Bijection.bijection >-> Funclass.
Coercion Similarity.sim_f : Similarity.similarity >-> Bijection.bijection.

Global Existing Instance Stream.stream_Setoid.
Global Existing Instance Stream.hd_compat.
Global Existing Instance Stream.tl_compat.
Global Existing Instance Stream.constant_compat.
Global Existing Instance Stream.aternate_compat.
Global Existing Instance Stream.instant_compat.
Global Existing Instance Stream.forever_compat.
Global Existing Instance Stream.eventually_compat.
Global Existing Instance Stream.instant2_compat.
Global Existing Instance Stream.forever2_compat.
Global Existing Instance Stream.eventually2_compat.
Global Existing Instance Stream.instant2_refl.
Global Existing Instance Stream.instant2_sym.
Global Existing Instance Stream.instant2_trans.
Global Existing Instance Stream.forever2_refl.
Global Existing Instance Stream.forever2_sym.
Global Existing Instance Stream.forever2_trans.
Global Existing Instance Stream.eventually2_refl.
Global Existing Instance Stream.eventually2_sym.
Global Existing Instance Stream.map_compat.

(* By experience, this is not very useful
(** For simplicity, we gather into one definition all the classes that must be instanciated
    in order to define a complete and working environment. *)
Class GlobalDefinitions := {
  (* Number of good and Byzantine robots *)
  glob_Names :> Names;
  (** The space in which robots evolve *)
  glob_Loc :> Location;
  (** The state of robots (must contain at least the current location) *)
  glob_info : Type;
  glob_State :> @State glob_Loc glob_info;
  (** The observation: what robots can see from their surroundings *)
  glob_obs :> @Observation _ _ glob_State glob_Names;
  (** The output type of robograms: some information that we can use to get a target location *)
  glob_Trobot : Type;
  glob_robot_choice :> robot_choice glob_Trobot;
  (** The frame decision made by the demon, used to build the frame change *)
  glob_Tframe : Type;
  glob_frame_choice :> frame_choice glob_Tframe;
  (** The influence of the demon on the state update of robots, when active *)
  glob_Tactive : Type;
  glob_update_choice :> update_choice glob_Tactive;
  (** The influence of the demon on the state update of robots, when inactive *)
  glob_Tinactive : Type;
  glob_inactive_choice :> inactive_choice glob_Tinactive;
  (** How a robots state is updated:
      - if active, using the result of the robogram and the decision from the demon
      - if inactive, using only the decision from the demon *)
  glob_update_function :> @update_function _ _ glob_State glob_Names _ _ _
                            glob_robot_choice glob_frame_choice glob_update_choice;
  glob_inactive_function :> @inactive_function _ _ glob_State glob_Names _ glob_inactive_choice
  }.
*)
