Require Import Utf8.
Require Import Bool.
Require Import Lia Field.
Require Import Rbase Rbasic_fun R_sqrt Rtrigo_def Lra.
Require Import List SetoidList SetoidDec.
Require Import Relations RelationPairs Morphisms.
Require Import Inverse_Image.
Require Import Decidable.
Require Import Pactole.Util.Preliminary.
Require Import Pactole.Util.Bijection.
Require Import Pactole.Setting.
Require Import Pactole.Observations.SetObservation.
Require Import Pactole.Spaces.R2.
Require Import Pactole.Spaces.Isometry.
Require Import Pactole.Models.Rigid.
Require Import Pactole.Models.Isometry.
Require Import Pactole.Util.Fin.
Import Permutation.
Import Datatypes. (* to recover [id] *)
Close Scope VectorSpace_scope.
Open Scope R_scope.
Set Implicit Arguments.

(* TODO: rename Dmax *)
(* Keeping connection from a fixed base to a scouting robot. All robots
(including the scout) share the same speed (max distance D in one round)
and the same detection radius (Dmax). The existence of a family
of solution is shown whenever Dmax > 7*D. No optimality is proved. *)

Section PursuitSetting.

(* We have a given finite number n of robot. The final lemma will require
that there are enough robots during the execution. *)
Variable n: nat.

(** All robots are non byzantine, except the scout
   (considered as a Byzantine as it does not follow the protocol) *)
Instance Identifiers : Names := Robots n 1.
Definition scout_B : B := Fin Nat.lt_0_1.
Definition scout : ident := Byz scout_B.

(** The only Byzantine robot is the scout. *)
Lemma byz_is_scout : ∀ b : B, b = scout_B.
Proof using .
  intros [b Hb]. change B with (fin 1) in *. apply fin2natI. cbn.
  destruct b as [|b]. reflexivity. inv Hb. inv H0.
Qed.

(** The space is the Euclidean plane *)
Instance Loc : Location := make_Location R2.
Instance VS : RealVectorSpace location := R2_VS.
Instance ES : EuclideanSpace location := R2_ES.
Remove Hints R2_VS R2_ES : typeclass_instances.
Local Opaque dist.
Local Opaque inverse.

Ltac changeR2 :=
  change R2 with location in *;
  change R2_Setoid with location_Setoid in *;
  change R2_EqDec with location_EqDec in *;
  change R2_VS with VS in *;
  change R2_ES with ES in *.

(** The robot state is the location + 2 booleans and an integer: alive, light, id *)
(* NB: To avoid confusion with [Names.ident], I use [id]. *)
Record state := {
  loc   : location;
  id    : nat;
  alive : bool;
  light : bool}.
(* Each robots have a light, which state is on or off *)
(* A robot will be alive or not (the latter meaning it has withdrawn
from the task). No collision with dead robots are considered *)
(* A robot may be still at base, waiting to be needed by the task. No
collision implying non launched robot is considered either. *)

Notation "'{{' x 'with' 'loc' ':=' y '}}'" :=
  ({| loc := y; id := x.(id); alive := x.(alive); light := x.(light) |}).
Notation "'{{' x 'with' 'alive' ':=' y '}}'" :=
  ({| loc := x.(loc); id := x.(id); alive := y; light := x.(light) |}).
Notation "'{{' x 'with' 'light' ':=' y '}}'" :=
  ({| loc := x.(loc); id := x.(id); alive := x.(alive); light := y |}).

#[refine]
Instance state_Setoid : Setoid state := {
  equiv := fun st1 st2 => st1.(loc) == st2.(loc)
                       /\ st1.(id) = st2.(id)
                       /\ st1.(alive) = st2.(alive)
                       /\ st1.(light) = st2.(light) }.
Proof using . split.
+ intros []; hnf; repeat split.
+ intros []; hnf; repeat split; symmetry; intuition.
+ intros []; hnf; repeat split; simpl in *; etransitivity; intuition auto; congruence.
Defined.

#[refine]
Instance state_EqDec : EqDec state_Setoid :=
  fun st1 st2 => if Nat.eq_dec st1.(id) st2.(id) then
                 if bool_dec st1.(alive) st2.(alive) then
                 if bool_dec st1.(light) st2.(light) then
                 if st1.(loc) =?= st2.(loc) then left _
                 else right _ else right _ else right _ else right _.
Proof using .
+ abstract (unfold equiv; cbn -[equiv]; repeat (split; trivial)).
+ abstract (revert_all; intros Hneq [Habs _]; apply Hneq, Habs).
+ abstract (intros [? [? []]]; contradiction).
+ abstract (intros [? [? []]]; contradiction).
+ abstract (intros [? [? []]]; contradiction).
Defined.

#[refine]
Instance St : State state := {
  get_location := fun st => st.(loc);
  precondition := fun _ => True;
  lift := fun f st => {| loc := projT1 f st.(loc); id := st.(id);
                         alive := st.(alive); light := st.(light) |} }.
Proof using .
+ abstract (intros ? []; repeat split; try reflexivity).
+ abstract (now intros ? ? [Heq _]).
+ abstract (intros f1 f2 Hf st1 st2 [? [? []]]; unfold equiv; cbn -[equiv]; auto).
Defined.

Instance loc_compat : Proper (equiv ==> equiv) loc.
Proof using . intros [] [] [Heq _]. apply Heq. Qed.

Instance id_compat : Proper (equiv ==> eq) id.
Proof using . intros [] [] [_ [Heq _]]. apply Heq. Qed.

Instance alive_compat : Proper (equiv ==> eq) alive.
Proof using . intros [] [] [_ [_ [Heq _]]]. apply Heq. Qed.

Instance light_compat : Proper (equiv ==> eq) light.
Proof using . intros [] [] [_ [_ [_ Heq]]]. apply Heq. Qed.

(* Trying to avoid notation problem with implicit arguments *)
Notation "!! config" :=
  (@obs_from_config location _ _ _ set_observation config origin) (at level 10).

(* Max distance D traveled by a robot between two rounds. This defines
   the "collision zone": any two robots closer than that are immediately
   considered colliding. *)
Variable D : R.
Hypothesis D_pos : D > 0.

(** Max detection radius: robots inside this radius are in range, the others are not seen. *)
Variable Dmax : R.
Hypothesis Dmax_6D : Dmax >= 6*D.

(* In the problem of connection, the free robot (numbered 0) is
   launched and alive. By definition of the problem.
   -> This is wrong as it is decided by the demain. But we don't need it. *)
(* Hypothesis byz_alive : forall config, (config scout).(alive) = true. *)

(* Dp is the distance where a robot is still visible but from which it
   becomes able to leave the visibility radius at next round. *)
Definition Dp(*ursuit*) := Dmax - D.

(** The observation is the set of location and light values
    of alive robots within range with lower identifier. *)
Definition target : Type := location * bool.
Definition tgt_loc : target -> location := fst.
Definition tgt_light : target -> bool := snd.
Instance target_Setoid : Setoid target := prod_Setoid _ (eq_setoid _).

Definition mk_tgt : state -> target := fun st => (st.(loc), st.(light)).

Instance tgt_loc_compat : Proper (equiv ==> equiv) tgt_loc.
Proof using . intros [] [] [Heq _]. apply Heq. Qed.

Instance tgt_light_compat : Proper (equiv ==> equiv) tgt_light.
Proof using . intros [] [] [_ Heq]. apply Heq. Qed.

Instance mk_tgt_compat : Proper (equiv ==> equiv) mk_tgt.
Proof using . intros [] [] [? [_ [_ ?]]]. now split. Qed.

Notation "X <=? Y" := ((Rle_bool X Y)%R) (at level 70): R_scope.

Definition visible st e :=
 e.(alive) && (dist e.(loc) st.(loc) <=? Dmax)%R
 && (e.(id) <? st.(id)).


#[refine]
Instance Obs : Observation := {|
  observation := set target;
  observation_Setoid := Set_Setoid _;
  observation_EqDec := @Set_EqDec _ target_Setoid _ _ FSetList.SetListFacts;
  obs_from_config config st :=
    SetObservation.make_set
      (List.map mk_tgt
        (List.filter (fun x => x.(alive) && ((dist x.(loc) st.(loc)) <=? Dmax)
                                         && (x.(id) <? st.(id)))
          (config_list config)));
  obs_is_ok s config st :=
    forall loclit, In loclit s
      <-> exists r, let st' := config r in
                    loclit == (st'.(loc), st'.(light))
                  ∧ st'.(alive) = true ∧ dist st'.(loc) st.(loc) <= Dmax
                  ∧ (st'.(id) < st.(id))%nat |}.
Proof. all:autoclass.
* intros config1 config2 Hconfig st1 st2 Hst.
  apply SetObservation.make_set_compat, eqlistA_PermutationA_subrelation.
  eapply map_eqlistA_compat with equiv; autoclass; []. f_equiv.
  + intros ? ? Heq. now rewrite Heq, Hst.
  + now rewrite (config_list_compat Hconfig).
* intros config st x.
  assert (Proper (equiv ==> eq)
    (fun x => alive x && Rle_bool (dist (loc x) (loc st)) Dmax && (id x <? id st))).
  { intros [] [] [Heq1 [Heq2 [Heq3 _]]]. cbn -[dist] in *. now rewrite Heq1, Heq2, Heq3. }
  rewrite SetObservation.make_set_spec, InA_map_iff; autoclass; [].
  setoid_rewrite filter_InA; trivial; []. do 2 setoid_rewrite Bool.andb_true_iff.
  setoid_rewrite Rle_bool_true_iff. setoid_rewrite Nat.ltb_lt.
  split; intro Hex.
  + destruct Hex as [st' [Heq [Hin Hst']]].
    rewrite config_list_spec, InA_map_iff in Hin; autoclass; [].
    destruct Hin as [r [Hr _]].
    exists r. cbn zeta. rewrite <- Heq, <- Hr. split; try reflexivity; [].
    now rewrite Hr.
  + destruct Hex as [r [Heq Hr]].
    exists (config r). rewrite Heq. split; try reflexivity; [].
    change state_Setoid with State.state_Setoid. rewrite config_list_InA.
    now split; try (exists r).
Defined.

Notation dist := (@dist location location_Setoid location_EqDec VS _).
Notation obs_from_config := (@obs_from_config state Loc St Identifiers Obs).

Lemma obs_from_config_spec : forall config st tgt,
  (tgt ∈ obs_from_config config st)%set
  <-> ∃ r, let st' := config r in
           tgt == (loc st', light st')
         ∧ alive st' = true ∧ dist (loc st') (loc st) <= Dmax ∧ (id st' < id st)%nat.
Proof using .
intros config st tgt.
assert (Hobs := obs_from_config_spec config st tgt). apply Hobs.
Qed.

Definition iso_precondition (iso : isometry location) : precondition iso := I.

Definition lift_target (f : location -> location) : target -> target :=
  fun tgt => (f (tgt_loc tgt), tgt_light tgt).

Instance lift_target_compat : Proper ((equiv ==> equiv) ==> equiv ==> equiv) lift_target.
Proof using . intros f g Hfg st1 st2 []. split; trivial; []. now apply Hfg. Qed.

Lemma obs_from_config_map : forall (iso : isometry location) Piso config pt,
  map (lift_target iso) (obs_from_config config pt)
  == obs_from_config (map_config (lift (existT precondition iso Piso)) config)
                     (lift (existT precondition iso Piso) pt).
Proof using .
repeat intro. rewrite map_spec; autoclass; [].
setoid_rewrite obs_from_config_spec.
split; intro Hex.
+ destruct Hex as [tgt [[r [Heq [Halive [Hdist Hid]]]] Heq']].
  exists r. split; [| repeat split].
  - rewrite Heq', Heq. cbn -[equiv]. now split.
  - cbn. assumption.
  - cbn. changeR2. now rewrite dist_prop.
  - cbn. assumption.
+ destruct Hex as [r [Heq [Halive [Hdist Hid]]]]. cbn -[equiv] in *.
  exists (mk_tgt (config r)). split; try (exists r; split; [| repeat split]; auto).
  - now rewrite dist_prop in Hdist.
  - apply Heq.
Qed.

Lemma in_range_dist : forall config st tgt,
  (tgt ∈ obs_from_config config st)%set -> dist (tgt_loc tgt) st.(loc) <= Dmax.
Proof using .
intros config st tgt Hin. rewrite obs_from_config_spec in Hin.
destruct Hin as [r [Heq [Halive [Hdist Hid]]]]. rewrite Heq. apply Hdist.
Qed.

(** The robot action consists in dying, turning its light on or moving with its light off *)
Inductive action :=
  | Die
  | LightOn
  | Move (loc : location).

#[refine]
Instance action_Setoid : Setoid action := {
  equiv := fun act1 act2 => match act1, act2 with
                              | Die, Die | LightOn, LightOn => True
                              | Move loc1, Move loc2 => loc1 == loc2
                              | _, _  => False
                            end}.
Proof. split.
+ abstract (intros []; auto).
+ abstract (intros [] []; cbn -[equiv]; auto).
+ abstract (intros [] [] [] ? ?; cbn -[equiv] in *; auto; etransitivity; eauto).
Defined.

Instance Action : robot_choice action := { robot_choice_Setoid := action_Setoid }.

(** The demon's only choices are the location of the scout and the local frame of reference.
    In particular, it does not perform any choice to update the state of a robot *)
Instance Upt : update_choice unit := NoChoice.
Instance Ina : inactive_choice unit := NoChoiceIna.

(** The moves are rigid and are of distance at most D *)
#[refine]
Instance InaFun : inactive_function unit := {inactive := fun config id _ => config id }.
Proof. abstract (intros ? ? Heq ? ? ? ? ? ?; subst; apply Heq). Defined.

#[refine]
Instance UpdFun : update_function action (isometry location) unit := {
  update := fun config g _ act _ =>
              match act with
                | Die     => {{ config (Good g) with alive := false }}
                | LightOn => {{ config (Good g) with light := true }}
                | Move pt => if Rle_dec (dist pt origin) D then
                               {| loc := pt;
                                  id := (config (Good g)).(id);
                                  alive := (config (Good g)).(alive);
                                  light := false |}
                             else config (Good g)
              end }.
Proof.
intros config1 config2 Hconfig ? g ? ? ? ? [] [] Hact ? ? ?; subst; try tauto; [| |].
+ repeat split; cbn -[dist equiv]; simpl in Hact; auto; f_equiv; apply Hconfig.
+ repeat split; cbn -[dist equiv]; simpl in Hact; auto; f_equiv; apply Hconfig.
+ unfold equiv in Hact; cbn -[equiv] in Hact; rewrite Hact.
  destruct_match; try apply Hconfig; [].
  repeat split; trivial; cbn -[equiv]; f_equiv; apply Hconfig.
Defined.

Lemma rigid_update : forall config (frame : isometry location) g act choice,
  get_location (update config g frame act choice)
  == match act with 
       | Die     => get_location (config (Good g))
       | LightOn => get_location (config (Good g))
       | Move pt => if Rle_dec (dist pt origin) D then pt else get_location (config (Good g))
     end.
Proof using . intros config frame g [] ?; cbn -[dist equiv]; try destruct_match; reflexivity. Qed.


(** **  Definition of the abstract robogram  **)

(** ***  Parameters that the robogram relies on  **)

Class Param := {
  (* which robot to follow? *)
  choose_target : observation -> target;
  (* where to go to follow the target? *)
  choose_new_pos : observation -> location -> location;
  (* Compatibility properties *)
  choose_target_compat : Proper (equiv ==> equiv) choose_target;
  choose_new_pos_compat : Proper (equiv ==> equiv ==> equiv) choose_new_pos;
  (* Specifications *)
  choose_target_spec : forall obs,
    obs =/= {}%set ->
    let tgt := choose_target obs in
    (* the target must be in range *)
    In tgt obs
    (* the target must preferably have its light off *)
    ∧ (tgt_light tgt = true -> forall r, In r obs -> tgt_light r = true)
    (* the target must preferably be close *)
    ∧ (tgt_light tgt = true -> dist origin (tgt_loc tgt) > Dp ->
       forall r, In r obs -> dist origin (tgt_loc r) > Dp);
  (* The location should be reachable in a round and be within range Dp of the target. *)
  choose_new_pos_spec : forall obs pt,
    dist pt origin <= Dmax ->
    let new := choose_new_pos obs pt in
    dist new pt <= Dp /\ dist new origin <= D }.

Global Existing Instance choose_target_compat.
Global Existing Instance choose_new_pos_compat.


(** Is it safe to move? *)
Definition move_to obs pt :=
  negb (exists_ (fun x => Rle_bool (dist (tgt_loc x) pt) (2*D)) obs).

Lemma move_to_false : forall obs pt,
  move_to obs pt = false <-> ∃ other, (other ∈ obs)%set ∧ dist (tgt_loc other) pt <= 2*D.
Proof using .
intros obs pt. unfold move_to. rewrite Bool.negb_false_iff.
rewrite exists_spec.
+ split; intros [tgt [Hin Hdist]];
  exists tgt; split; trivial; now rewrite Rle_bool_true_iff in *.
+ intros tgt1 tgt2 Htgt. now rewrite Htgt.
Qed.

Lemma move_to_true : forall obs pt,
  move_to obs pt = true <-> forall x, In x obs -> dist (tgt_loc x) pt > 2*D.
Proof using .
intros obs pt. rewrite <- not_false_iff_true, move_to_false. split; intro Hspec.
+ intros tgt Hin. destruct (Rlt_le_dec (2*D) (dist (tgt_loc tgt) pt)) as [Hlt | Hle].
  - assumption.
  - contradiction Hspec. now exists tgt.
+ intros [tgt [Hin Habs]]. apply Hspec in Hin.
  apply (Rlt_irrefl (2*D)). eapply Rlt_le_trans; eauto.
Qed.

Instance move_to_compat : Proper (equiv ==> equiv ==> eq) move_to.
Proof using .
intros obs1 obs2 Hobs tgt1 tgt2 Htgt.
destruct (move_to obs2 tgt2) eqn:Hex.
+ rewrite move_to_true in *. intro x. rewrite Hobs, Htgt. now revert x.
+ rewrite move_to_false in *. setoid_rewrite Hobs. setoid_rewrite Htgt. assumption.
Qed.

Lemma move_to_isometry_compat : forall (iso : isometry R2) obs pt,
  move_to (map (lift_target iso) obs) (iso pt) = move_to obs pt.
Proof using .
intros iso obs pt. destruct (move_to obs pt) eqn:Hcase.
+ rewrite move_to_true in *.
  intros x' Hx'. rewrite map_spec in Hx'; autoclass; [].
  destruct Hx' as [x [Hin Hx]]. rewrite Hx. cbn -[dist]. rewrite dist_prop. auto.
+ rewrite move_to_false in *. destruct Hcase as [tgt [Hin Htgt]].
  exists (iso (tgt_loc tgt), tgt_light tgt). cbn -[dist]. rewrite dist_prop.
  split; trivial; []. rewrite map_spec; autoclass.
Qed.

(** If a robot is too close, the observing robot should die *)

Definition should_die obs pt :=
  is_empty obs ||
  exists_ (fun x => Rle_bool (dist (tgt_loc x) pt) D) obs.

Lemma should_die_true : forall obs pt,
  should_die obs pt = true
  <-> obs == {}%set ∨ ∃ tgt, (tgt ∈ obs)%set /\ dist (tgt_loc tgt) pt <= D.
Proof using .
intros obs pt. unfold should_die. rewrite Bool.orb_true_iff, is_empty_spec, exists_spec.
+ split; intros [Hempty | [tgt [Hin Hdist]]];
  (now left) || right; exists tgt; split; trivial; now rewrite Rle_bool_true_iff in *.
+ intros tgt1 tgt2 Htgt. now rewrite Htgt.
Qed.

Lemma should_die_false : forall obs pt,
  should_die obs pt = false
  <-> (∃ tgt, (tgt ∈ obs)%set) ∧ ∀ tgt, (tgt ∈ obs)%set -> dist (tgt_loc tgt) pt > D.
Proof using .
intros obs pt. rewrite <- not_true_iff_false, should_die_true. split; intro Hspec.
* destruct (choose obs) as [elt |] eqn:Hchoose.
  + split; try (now exists elt; apply choose_1); [].
    intros tgt Hin. destruct (Rlt_le_dec D (dist (tgt_loc tgt) pt)) as [Hlt | Hle].
    - assumption.
    - contradiction Hspec. right. now exists tgt.
  + contradiction Hspec. left. intro x. apply choose_2 in Hchoose. specialize (Hchoose x).
    split; intro Habs.
    - contradiction.
    - now apply empty_spec in Habs.
* intros [Hempty | [tgt [Hin Habs]]].
  + destruct Hspec as [[tgt Htgt] _].
    specialize (Hempty tgt). rewrite empty_spec in Hempty. now apply Hempty.
  + apply Hspec in Hin. apply (Rlt_irrefl D). eapply Rlt_le_trans; eauto.
Qed.

Instance should_die_compat : Proper (equiv ==> equiv ==> eq) should_die.
Proof using .
intros obs1 obs2 Hobs pt1 pt2 Hpt.
destruct (should_die obs2 pt2) eqn:Hex;
rewrite should_die_true in * || rewrite should_die_false in *;
setoid_rewrite Hobs; setoid_rewrite Hpt; assumption.
Qed.

Lemma should_die_map : forall (iso : isometry location) obs pt,
  should_die (map (lift_target iso) obs) (iso pt) = should_die obs pt.
Proof using .
intros iso obs pt. destruct (should_die obs pt) eqn:Hcase.
+ rewrite should_die_true in *. destruct Hcase as [Hcase | [tgt [Hin Hdist]]].
  - left. intro x. rewrite map_spec, empty_spec; autoclass; [].
    split; try tauto; []. intros [tgt [Hin Heq]].
    revert Hin. rewrite Hcase. apply empty_spec.
  - right. exists (lift_target iso tgt). cbn. rewrite map_spec, dist_prop; autoclass.
+ rewrite should_die_false in *. destruct Hcase as [[tgt Htgt] Hall]. split.
  - exists (lift_target iso tgt). rewrite map_spec; autoclass.
  - intros x Hin. rewrite map_spec in Hin; autoclass; []. destruct Hin as [x' [Hin' Heq]].
    rewrite Heq. cbn. rewrite dist_prop. now apply Hall.
Qed.

Context {robogram_args : Param}.

Corollary target_in_range : forall obs, obs =/= {}%set ->
  In (choose_target obs) obs.
Proof using . apply choose_target_spec. Qed.

Corollary target_has_lower_id : forall st config,
  let obs := obs_from_config config st in
  let tgt := choose_target obs in
  obs =/= {}%set ->
  exists r, mk_tgt r == tgt /\ (r.(id) < st.(id))%nat.
Proof using .
intros st config obs tgt Hobs.
assert (Hin := target_in_range Hobs).
assert (Hspec := obs_from_config_spec config st (choose_target (obs_from_config config st))).
unfold obs in Hin. rewrite Hspec in Hin. destruct Hin as [r [Heq [Halive [Hloc Hid]]]].
exists (config r). now split.
Qed.

Corollary target_is_alive : forall st config,
  let obs := obs_from_config config st in
  let tgt := choose_target obs in
  obs =/= {}%set ->
  exists r, mk_tgt r == choose_target obs /\ alive r = true.
Proof using .
intros st config obs tgt Hobs.
assert (Hin := target_in_range Hobs).
assert (Hspec := obs_from_config_spec config st (choose_target (obs_from_config config st))).
unfold obs in Hin. rewrite Hspec in Hin. destruct Hin as [r [Heq [Halive [Hloc Hid]]]].
exists (config r). now split.
Qed.

Corollary target_light_on : forall obs,
  obs =/= {}%set -> tgt_light (choose_target obs) = true ->
  forall r, In r obs -> tgt_light r = true.
Proof using . apply choose_target_spec. Qed.

Corollary target_is_close : forall obs, obs =/= {}%set ->
  tgt_light (choose_target obs) = true ->
  dist origin (tgt_loc (choose_target obs)) > Dp ->
  forall r, In r obs -> dist origin (tgt_loc r) > Dp.
Proof using . apply choose_target_spec. Qed.


(** **  Specification of the problem  **)

Definition no_collision config := forall r r' : ident, r <> r' ->
  (config r).(alive) = true -> (config r').(alive) = true ->
  get_location (config r) =/= get_location (config r').

Definition connected_path config :=
  forall g, (config (Good g)).(alive) = true ->
    ∃ r', (config r').(alive) = true
        ∧ dist (get_location (config r')) (get_location (config (Good g))) <= Dmax
        ∧ ((config r').(id) < (config (Good g)).(id))%nat.

Lemma connected_path_iff_obs_non_empty : forall config,
  connected_path config <-> forall g, (config (Good g)).(alive) = true ->
                              obs_from_config config (config (Good g)) =/= {}%set.
Proof using .
intro config. pose (obs g := obs_from_config config (config (Good g))).
split.
+ intros Hconnect g Halive. fold (obs g). apply Hconnect in Halive.
  destruct Halive as [r [Halive' [Hdist Hid]]].
  assert (Hin : (mk_tgt (config r) ∈ (obs g))%set).
  { unfold obs. rewrite obs_from_config_spec. exists r. now repeat split. }
  intro Habs. rewrite Habs in Hin. revert Hin. apply empty_spec.
+ intros Hobs g Halive. apply Hobs in Halive. fold (obs g) in Halive.
  destruct (choose_dec (obs g)).
  - unfold obs in Hin. rewrite obs_from_config_spec in Hin.
    destruct Hin as [r [Heq [Halive' [Hdist Hid]]]].
    exists r. now repeat split.
  - contradiction Halive. intro x. rewrite empty_spec. split; tauto || apply Hempty.
Qed.

Lemma connected_path_iso_compat :
  forall (iso : isometry location) (Hiso : @precondition _ _ St iso) config,
  connected_path (map_config (lift (existT precondition iso Hiso)) config)
  <-> connected_path config.
Proof using .
intros iso Hiso config. rewrite 2 connected_path_iff_obs_non_empty.
simpl alive. unfold map_config at 2. setoid_rewrite <- obs_from_config_map.
split; intros Hobs g Halive; apply Hobs in Halive; clear Hobs.
+ intro Habs. apply Halive. rewrite Habs. reflexivity.
+ intro Habs. apply Halive. intro x. rewrite empty_spec. split; try tauto; [].
  intro Hin. specialize (Habs (lift_target iso x)).
  rewrite map_spec, empty_spec in Habs; autoclass; [].
  rewrite <- Habs. eauto.
Qed.


(** **  The abstract robogram  **)

(** The abstract roborgram (protocol) is defined as follows,
    it uses the functions described above: two generic ones and two concrete ones. *)
Definition rbg_fun obs : action :=
  if should_die obs origin then Die else
  let target := choose_target obs in
  let new_pos := choose_new_pos obs (tgt_loc target) in
  if move_to obs new_pos then Move new_pos else LightOn.

Instance rbg_compat : Proper (equiv ==> equiv) rbg_fun.
Proof using .
intros obs1 obs2 Hobs. unfold rbg_fun.
repeat (rewrite Hobs; try destruct_match; try reflexivity).
Qed.

Definition rbg : robogram := {| pgm := rbg_fun |}.

(* Assumption about the environment:
   - local frame of reference are centred on the observer
   - it is FSYNC
   - the scout moves at most at speed D
   - the constant part of the scout state keep good values *)
Definition da_assumption (da : demonic_action) :=
  let reloc cfg := (da.(relocate_byz) cfg scout_B) in
  isometry_da_prop da
  ∧ FSYNC_da da
  ∧ (forall cfg, dist (reloc cfg).(loc) (cfg scout).(loc) <= D)
  ∧ (forall cfg, (reloc cfg).(alive) = true)
  ∧ (forall cfg, (reloc cfg).(light) = false)
  ∧ forall cfg, (reloc cfg).(id) = 0%nat.

Theorem round_simplify : forall da, da_assumption da -> forall config g,
  round rbg da config (Good g)
  == let new_frame := change_frame da config g in
     let local_config :=
       map_config (fun st => {{ st with loc := new_frame st.(loc) }}) config in
     let local_state := {{ config (Good g) with loc := new_frame (config (Good g)).(loc) }} in
     let obs := obs_from_config local_config local_state in
     let local_action := rbg obs in
     let new_local_state := update local_config g new_frame local_action tt in
     {{ new_local_state with loc := new_frame ⁻¹ new_local_state.(loc) }}.
Proof using . intros da [_ [Hactive _]] config g. unfold round. now rewrite Hactive. Qed.

Ltac simplify_round_at rbg da config g :=
  let Hround := fresh "Hround" in let new_frame := fresh "new_frame" in
  let local_config := fresh "local_config" in let local_state := fresh "local_state" in
  let obs := fresh "obs" in
  assert (Hround := round_simplify (da := da) ltac:(auto) config g);
  pose (new_frame := change_frame da config g);
  pose (local_config := map_config (λ st : state, {{st with loc := new_frame (loc st)}})  config);
  pose (local_state := {{config (Good g) with loc := new_frame (loc (config (Good g)))}});
  pose (obs := obs_from_config local_config local_state);
  change (
    let new_frame := change_frame da config g in
    let local_config :=
      map_config (λ st : state, {{st with loc := new_frame (loc st)}}) config in
    let local_state := {{config (Good g) with loc := new_frame (loc (config (Good g)))}} in
    let obs := obs_from_config local_config local_state in
    let local_action := rbg obs in
    let new_local_state := update local_config g new_frame local_action tt in
    {{new_local_state with loc := (new_frame ⁻¹) (loc new_local_state)}})
  with (
    let local_action := rbg obs in
    let new_local_state := update local_config g new_frame local_action tt in
    {{new_local_state with loc := (new_frame ⁻¹) (loc new_local_state)}}) in Hround;
  rewrite Hround in *.

Ltac simplify_round :=
  lazymatch goal with
    | |- context [round ?rbg ?da ?config (Good ?g)] =>
      simplify_round_at rbg da config g
    | H : context [round ?rbg ?da ?config (Good ?g)] |- _ =>
      simplify_round_at rbg da config g
  end.


Lemma update_simplify : forall da, da_assumption da ->
  forall config, connected_path config ->
  forall g, alive (config (Good g)) = true ->
  let new_frame := change_frame da config g in
  let local_config := map_config (λ st : state, {{st with loc := new_frame (loc st)}}) config in
  let local_state := {{config (Good g) with loc := new_frame (loc (config (Good g)))}} in
  let obs := obs_from_config local_config local_state in
  exists pf,
    Rle_dec (dist (choose_new_pos obs (tgt_loc (choose_target obs))) 0) D = left pf.
Proof using .
intros da Hda config Hpath g Hg new_frame local_config local_state obs.
destruct (Rle_dec (dist (choose_new_pos obs (tgt_loc (choose_target obs))) 0) D); eauto; [].
revert_one not. intro Habs. contradiction Habs.
assert (Hobs : obs =/= {}%set).
{ rewrite connected_path_iff_obs_non_empty in Hpath. apply Hpath in Hg. unfold complement in *.
  cbn -[equiv] in Hg.
  rewrite <- (map_empty_iff_empty (lift_target new_frame)) in Hg; autoclass; [].
  rewrite (obs_from_config_map _ I) in Hg. apply Hg. }
assert (Hloc : get_location (local_config (Good g)) == 0%VS).
{ now rewrite <- center_prop, (proj1 Hda). }
assert (Htgt : dist (tgt_loc (choose_target obs)) 0 <= Dmax).
{ assert (Hrange := target_in_range Hobs).
  unfold obs in Hrange. rewrite obs_from_config_spec in Hrange. fold obs in Hrange.
  destruct Hrange as [r [Heq [Halive [Hdist Hid]]]]. rewrite Heq, <- Hloc. apply Hdist. }
apply (proj2 (choose_new_pos_spec obs (tgt_loc (choose_target obs)) Htgt)).
Qed.

Ltac simplify_update_at da Hda config Hpath g Hg new_frame local_config local_state obs :=
          let Hpf := fresh "Hpf" in let Hrew := fresh "Hrew" in
          destruct (update_simplify Hda Hpath g Hg) as [Hpf Hrew];
          revert Hpf Hrew;
          changeR2; fold new_frame; fold local_config; fold local_state; fold obs;
          intros Hpf Hrew; rewrite Hrew in *.

Ltac simplify_update :=
  lazymatch goal with
    | Hda : da_assumption ?da,
      new_frame := change_frame ?da ?config ?g,
      obs := obs_from_config ?local_config ?local_state,
      Hpath : connected_path ?config,
      Hg : alive (?config (Good ?g)) = true
      |- context[Rle_dec _ D] =>
          simplify_update_at da Hda config Hpath g Hg new_frame local_config local_state obs
    | Hda : da_assumption ?da,
      new_frame := change_frame ?da ?config ?g,
      obs := obs_from_config ?local_config ?local_state,
      Hpath : connected_path ?config,
      Hg : alive (?config (Good ?g)) = true,
      H : context[Rle_dec _ D] |- _ =>
          simplify_update_at da Hda config Hpath g Hg new_frame local_config local_state obs
  end.

Lemma move_is_to_target : forall obs pt, rbg obs = Move pt ->
  pt = choose_new_pos obs (tgt_loc (choose_target obs)).
Proof using .
intros obs pt. cbn. unfold rbg_fun.
now repeat destruct_match; intro Heq; inv Heq.
Qed.

(** ***  Other invariants  **)
(* 
(** Alive robots have an observable robot in range and no robot too close.
    This is actually true after a round but not necessarily for the initial configuration,
    so we add it as an invariant. *)
Definition alive_spec (config : configuration) := forall g,
  (config (Good g)).(alive) = true
  <-> (∃ r, dist (get_location (config (Good g))) (get_location (config r)) <= Dmax
          ∧ ((config r).(id) < (config (Good g)).(id))%nat
          ∧ (config r).(alive) = true)
     ∧ ∀ r, ((config r).(id) < (config (Good g)).(id))%nat -> (config r).(alive) = true ->
            dist (get_location (config (Good g))) (get_location (config r)) > D.
*)

(* NB: as [rbg] only works in a local frame of reference,
       we use [should_die] rather than rbg directly.
       Another option would be to first change the frame of reference to a local one. *)

(** The scout is always alive, always has id 0, and always has its light off. *)
Definition scout_inv config :=
  exists pt, config scout == {| loc := pt; id := 0; alive := true; light := false |}.

(** Ids are always distinct *)
Definition distinct_id (config : configuration) :=
  forall r r', r ≠ r' -> (config r).(id) ≠ (config r').(id).

(* TODO: can be strenghten for all robots as the scout cannot die
        -> Requires alive (config' r) = false instead of should_die *)
(** If a relay robot dies, it has its light on *)
Definition killed_light_on config := forall g,
  let obs := obs_from_config config (config (Good g)) in
  (* if g dies, *)
  (config (Good g)).(alive) = true ->
  should_die obs (config (Good g)).(loc) = true ->
  (* it has its light on *)
  (config (Good g)).(light) = true.

(** If a relay robot dies, it is because of a robot with its light off. *)
Definition killer_light_off config := forall g,
  let obs := obs_from_config config (config (Good g)) in
  (* if g dies, *)
  (config (Good g)).(alive) = true ->
  should_die obs (config (Good g)).(loc) = true ->
  (* it is because of r' which has its light off *)
  exists tgt, (tgt ∈ obs)%set
            ∧ dist (config (Good g)).(loc) (tgt_loc tgt) <= D
            ∧ tgt_light tgt = false.

(** Every relay robot has an observable robot at most Dp away. *)
Definition exists_at_less_than_Dp config := forall g, alive (config (Good g)) = true ->
  let obs := obs_from_config config (config (Good g)) in
  (* if all robots within range have their light on *)
  For_all (fun x => tgt_light x = true) obs ->
  (* then one of them is at most Dp away *)
  exists tgt, (tgt ∈ obs)%set ∧ dist (tgt_loc tgt) (config (Good g)).(loc) <= Dp.

(* The specification of [choose_target] and the property [exists_at_less_than_Dp]
   entail that if the target has its light on, then it is at most Dp away. *)
Lemma target_light_on_at_most_Dp : forall local_config,
  exists_at_less_than_Dp local_config ->
  forall g, alive (local_config (Good g)) = true ->
            (local_config (Good g)).(loc) == origin ->
            let obs := obs_from_config local_config (local_config (Good g)) in
              obs =/= {}%set -> tgt_light (choose_target obs) = true ->
              dist (local_config (Good g)).(loc) (tgt_loc (choose_target obs)) <= Dp.
Proof using .
intros local_config Hex g Halive Hg obs Hobs Hlight.
assert (Hall := target_light_on Hobs Hlight).
specialize (Hex g Halive). cbn zeta in Hex. fold obs in Hex, Hall.
destruct (Hex Hall) as [tgt [Hin Hrange]].
destruct (Rle_or_lt (dist (loc (local_config (Good g))) (tgt_loc (choose_target obs))) Dp)
  as [Hle | Hlt]; trivial; [].
rewrite Hg in Hlt, Hrange.
assert (Hdist := target_is_close Hobs Hlight Hlt).
fold obs in Hdist. apply Hdist in Hin. rewrite dist_sym in Hin. lra.
Qed.

Lemma exists_at_less_than_Dp_map : forall (iso : isometry location) config,
  exists_at_less_than_Dp config ->
  exists_at_less_than_Dp
    (map_config (lift (existT precondition iso (iso_precondition iso))) config).
Proof using .
intros iso config Hex g Halive obs' Hall. subst obs'.
unfold map_config in Hall at 2. unfold map_config at 2.
assert (Proper (equiv ==> impl) (For_all (λ x : target, tgt_light x = true))).
{ apply For_all_compat. intros x y Hxy. now rewrite Hxy. }
rewrite <- (obs_from_config_map iso) in Hall.
rewrite For_all_map in Hall; autoclass; try (now intros x y Hxy; rewrite Hxy); [].
specialize (Hex g Halive). cbn zeta in Hex. apply Hex in Hall.
destruct Hall as [tgt [Hin Hdist]].
exists (lift_target iso tgt). split.
+ rewrite <- obs_from_config_map, map_spec; autoclass.
+ cbn. changeR2. now rewrite dist_prop.
Qed.

Lemma killed_light_on_map : forall (iso : isometry location) (Hiso : @precondition _ _ St iso),
  forall config, killed_light_on config ->
                 killed_light_on (map_config (lift (existT precondition iso Hiso)) config).
Proof using .
intros iso Hiso config Hkilled r obs Halive Hdie. cbn -[equiv] in Hdie |- *.
apply Hkilled; try apply Halive; []. unfold obs in Hdie. unfold map_config in Hdie at 2.
now rewrite <- obs_from_config_map, should_die_map in Hdie.
Qed.

Lemma killer_light_off_map : forall (iso : isometry location) (Hiso : @precondition _ _ St iso),
  forall config, killer_light_off config ->
                 killer_light_off (map_config (lift (existT precondition iso Hiso)) config).
Proof using .
intros iso Hiso config Hkiller r obs Halive Hdie. cbn -[equiv] in Hdie |- *.
unfold obs in Hdie. unfold map_config in Hdie at 2.
rewrite <- obs_from_config_map, should_die_map in Hdie.
apply Hkiller in Hdie; try apply Halive; []. destruct Hdie as [tgt [Hin [Hdist Hlight]]].
exists (lift_target iso tgt). repeat split.
+ unfold obs. unfold map_config at 2. rewrite <- obs_from_config_map, map_spec; autoclass.
+ cbn. now rewrite dist_prop.
+ cbn. assumption.
Qed.

Lemma scout_empty_obs : forall config, scout_inv config ->
  obs_from_config config (config scout) == {}%set.
Proof using .
intros config [pt Hscout] x. rewrite empty_spec, obs_from_config_spec.
split; try tauto; []. intros [r [_ [_ [_ Hid]]]].
rewrite Hscout in Hid. cbn in Hid. lia.
Qed.

Lemma should_die_indeed_dies : forall g config,
  should_die (obs_from_config config (config (Good g))) (config (Good g)).(loc) = true ->
  forall da, da_assumption da -> (round rbg da config (Good g)).(alive) = false.
Proof using .
intros g config Hdie da Hda. simplify_round. cbn. unfold rbg_fun.
cut (should_die obs origin = true); try (now intro Heq; rewrite Heq); [].
unfold obs. rewrite <- (center_prop new_frame).
unfold new_frame. rewrite (proj1 Hda).
rewrite <- (should_die_map new_frame) in Hdie. rewrite <- Hdie.
f_equiv. rewrite (obs_from_config_map _ I). f_equiv.
Qed.

Lemma dead_means_Good : forall config, scout_inv config ->
  forall r, alive (config r) = false -> exists g, r = Good g.
Proof using .
intros config [pt Hscout] [g | b] Hdead; eauto; [].
exfalso. rewrite (byz_is_scout b), Hscout in Hdead. discriminate.
Qed.


Section InvariantProofs.

Variable da : demonic_action.
Hypothesis Hda : da_assumption da.

Variable config : configuration.
Local Definition config' := round rbg da config.

Lemma da_iso : isometry_da_prop da.
Proof using Hda. apply Hda. Qed.

Lemma da_FSYNC : FSYNC_da da.
Proof using Hda. apply Hda. Qed.

Lemma da_relocate_scout :
  dist (da.(relocate_byz) config scout_B).(loc) (config scout).(loc) <= D.
Proof using Hda. apply Hda. Qed.

Lemma da_scout_alive : (da.(relocate_byz) config scout_B).(alive) = true.
Proof using Hda. apply Hda. Qed.

Lemma da_scout_light : (da.(relocate_byz) config scout_B).(light) = false.
Proof using Hda. apply Hda. Qed.

Lemma da_scout_id : (da.(relocate_byz) config scout_B).(id) = 0%nat.
Proof using Hda. apply Hda. Qed.

(** The scout's state *)
Hypothesis scout_has_inv : scout_inv config.

Corollary scout_alive : (config scout).(alive) = true.
Proof using scout_has_inv. destruct scout_has_inv as [pt Hpt]. now rewrite Hpt. Qed.

Corollary scout_light : (config scout).(light) = false.
Proof using scout_has_inv. destruct scout_has_inv as [pt Hpt]. now rewrite Hpt. Qed.

Corollary scout_id : (config scout).(id) = 0%nat.
Proof using scout_has_inv. destruct scout_has_inv as [pt Hpt]. now rewrite Hpt. Qed.

(** id should be unique to properly identify a robot *)
Hypothesis id_uniq : distinct_id config.

Corollary id_unique : forall r r', r ≠ r' <-> (config r).(id) ≠ (config r').(id).
Proof using id_uniq. intros. split; auto; intuition subst; tauto. Qed.

Lemma forever_same_id : forall r, (config' r).(id) = (config r).(id).
Proof using Hda scout_has_inv.
intros [g | b]; unfold config'.
+ simplify_round. destruct (rbg obs); cbn; try destruct_match; reflexivity.
+ unfold round. rewrite da_FSYNC. rewrite (byz_is_scout b). fold scout.
  now rewrite scout_id, da_scout_id.
Qed.

Corollary id'_unique : forall r r', r ≠ r' <-> (config' r).(id) ≠ (config' r').(id).
Proof using Hda id_uniq scout_has_inv. intros. rewrite 2 forever_same_id. apply id_unique. Qed.

Lemma scout_invariant : scout_inv config -> scout_inv config'.
Proof using Hda.
intros [pt Hscout]. exists (config' scout).(loc).
unfold config', round.
destruct_match; cbn; changeR2; [|].
- now rewrite da_scout_alive, da_scout_light, da_scout_id.
- now rewrite Hscout.
Qed.

Lemma distinct_id_invariant : distinct_id config'.
Proof using Hda id_uniq scout_has_inv. unfold distinct_id. apply id'_unique. Qed.


(** Why is a robot dead after a round? *)
Lemma why_dead : forall r,
  (config' r).(alive) = false ->
  (** either it was already dead *)
  (config r).(alive) = false
  (** or there is an alive robot too close *)
  ∨ (exists r', ((config r').(id) < (config r).(id))%nat
               ∧ (config r').(alive) = true
               ∧ dist (config r).(loc) (config r').(loc) <= D)
  (** or there is no alive robot in range *)
  ∨ (forall r', ((config r').(id) < (config r).(id))%nat ->
                 (config r').(alive) = true ->
                 dist (config r').(loc) (config r).(loc) > Dmax).
Proof using Hda scout_has_inv.
intros r Hdead.
assert (Hg : exists g, r = Good g).
{ destruct r as [g | b]; eauto; []. exfalso. revert Hdead. rewrite (byz_is_scout b).
  unfold config', round. destruct_match.
  - rewrite da_scout_alive. discriminate.
  - cbn. change (Byz scout_B) with scout. rewrite scout_alive. discriminate. }
destruct Hg as [g ?]. subst r.
unfold config' in Hdead. simplify_round.
destruct (rbg obs) eqn:Hrbg.
+ assert (Hdie : should_die obs origin = true).
  { revert Hrbg. cbn. unfold rbg_fun. changeR2. repeat destruct_match; trivial; discriminate. }
  rewrite should_die_true in Hdie. destruct Hdie as [Hdie | Hdie].
  - do 2 right. intros r Hid Halive.
    assert (Hobs := obs_from_config_spec local_config local_state).
    unfold obs in Hdie. setoid_rewrite Hdie in Hobs. setoid_rewrite empty_spec in Hobs.
    specialize (Hobs (mk_tgt (local_config r))). cbn -[dist equiv] in Hobs.
    setoid_rewrite dist_prop in Hobs.
    apply Rnot_le_gt. intro. rewrite Hobs. exists r. intuition.
  - right; left. destruct Hdie as [tgt [Hin Hdist]]. unfold obs in Hin.
    rewrite obs_from_config_spec in Hin. destruct Hin as [r [Heq [Halive [Hle Hid]]]].
    exists r. repeat split; trivial; [].
    rewrite Heq in Hdist. cbn -[dist] in Hdist.
    rewrite <- (center_prop (change_frame da config g)), dist_prop in Hdist.
    rewrite da_iso, dist_sym in Hdist. apply Hdist.
+ left. cbn in Hdead. assumption.
+ left. cbn in Hdead. revert Hdead. destruct_match; auto.
Qed.

Corollary dies_as_it_should : forall r,
  (config r).(alive) = true ->
  (config' r).(alive) = false ->
  should_die (obs_from_config config (config r)) (config r).(loc) = true.
Proof using Dmax_6D D_pos config da Hda scout_has_inv.
intros r Halive Halive'. rewrite should_die_true.
destruct (why_dead r Halive')
  as [Habs | [[r' [Hid [Halive_r' Hdist]]] | Hall]]; try congruence; [|].
+ right. exists (mk_tgt (config r')). split.
  - rewrite obs_from_config_spec. exists r'. repeat split; trivial; [].
    rewrite dist_sym. lra.
  - now rewrite dist_sym.
+ left. intro x. rewrite empty_spec. split; try tauto; [].
  rewrite obs_from_config_spec. intros [r' [Heq [Halive_r' [Hdist Hr']]]].
  specialize (Hall r' Hr' Halive_r'). lra.
Qed.

Corollary dies_iff_should_die : forall g, alive (config (Good g)) = true ->
  alive (config' (Good g)) = false
  <-> should_die (obs_from_config config (config (Good g))) (config (Good g)).(loc) = true.
Proof using D_pos Dmax_6D Hda scout_has_inv.
intros g Halive. split; intro Hdead.
- now apply dies_as_it_should.
- now apply should_die_indeed_dies.
Qed.

Lemma find_killer :
  killed_light_on config ->
  killer_light_off config ->
  forall r, alive (config r) = true -> alive (config' r) = false ->
  exists r', (id (config r') < id (config r))%nat
           ∧ alive (config' r') = true
           ∧ light (config r') = false
           ∧ dist (loc (config r)) (loc (config r')) <= D.
Proof using D_pos Dmax_6D Hda scout_has_inv.
intros Hkilled Hkiller r Halive_r Hdead_r.
destruct (dead_means_Good (scout_invariant scout_has_inv) _ Hdead_r) as [g ?]. subst r.
apply dies_as_it_should, Hkiller in Hdead_r; trivial; [].
destruct Hdead_r as [tgt [Hin [Hdist Hlight]]].
rewrite obs_from_config_spec in Hin. destruct Hin as [r' [Htgt [Halive [Hdist' Hid]]]].
rewrite Htgt in *. cbn in Hlight.
exists r'. repeat split; trivial; [].
rewrite <- Bool.not_false_iff_true. intro Habs.
destruct (dead_means_Good (scout_invariant scout_has_inv) _ Habs) as [g' ?]. subst r'.
apply dies_as_it_should, Hkilled in Habs; trivial; []. congruence.
Qed.

Lemma moving_means_light_off : forall r,
  (config' r).(alive) = true ->
  (config' r).(loc) =/= (config r).(loc) ->
  (config' r).(light) = false.
Proof using Hda scout_has_inv.
intros [g | b] Halive Hmoving.
* unfold config' in *. simplify_round.
  destruct (rbg obs).
  + cbn in Halive. discriminate.
  + cbn -[equiv] in Hmoving. contradiction Hmoving. apply retraction_section.
  + cbn -[equiv] in Hmoving |- *. destruct_match.
    - reflexivity.
    - contradiction Hmoving. cbn. now rewrite simpl_inverse_l.
* rewrite (byz_is_scout b). apply scout_invariant in scout_has_inv.
  destruct scout_has_inv as [pt Hpt]. now rewrite Hpt.
Qed.

Corollary light_on_same_location : forall r, alive (config' r ) = true ->
  light (config' r) = true -> loc (config' r) == loc (config r).
Proof using Hda scout_has_inv.
intros r Halive Hlight.
destruct (equiv_dec (loc (config' r)) (loc (config r))); trivial; [].
exfalso. revert Hlight. rewrite moving_means_light_off; auto.
Qed.

Lemma alive_before : forall r,
  (config' r).(alive) = true -> (config r).(alive) = true.
Proof using Hda scout_has_inv.
intros [g | b]; unfold config'.
+ simplify_round. destruct (rbg obs).
  - cbn. discriminate.
  - cbn. auto.
  - cbn. destruct_match; auto.
+ intros _. rewrite (byz_is_scout b). apply scout_alive.
Qed.

(** Because of the update function, robots can move at most D in a round. *)
Lemma move_at_most_D : forall r,
  dist (get_location (config' r)) (get_location (config r)) <= D.
Proof using D_pos Hda.
intros [g | b]; unfold config'.
* simplify_round. destruct (rbg obs); cbn -[dist].
  + rewrite simpl_inverse_l, dist_same. now apply Rlt_le.
  + rewrite simpl_inverse_l, dist_same. now apply Rlt_le.
  + destruct_match.
    - cbn -[dist]. rewrite <- da_iso.
      assert (Hcenter := center_prop new_frame).
      rewrite Inversion in Hcenter. fold new_frame. rewrite <- Hcenter.
      changeR2. change (retraction new_frame) with (section (new_frame ⁻¹)).
      rewrite dist_prop. assumption.
    - cbn -[dist]. rewrite simpl_inverse_l, dist_same. now apply Rlt_le.
* unfold round. rewrite da_FSYNC. cbn -[dist].
  rewrite (byz_is_scout b). apply da_relocate_scout.
Qed.

Lemma wlog_lt_id : forall P,
  (forall r r', P r r' <-> P r' r) ->
  (forall r r', ((config r).(id) < (config r').(id))%nat -> P r r') ->
  forall r r', r <> r' -> P r r'.
Proof using id_uniq.
intros P Hsym HP r r' Hdiff.
rewrite id_unique, Nat.lt_gt_cases in Hdiff.
destruct Hdiff.
- now apply HP.
- now apply Hsym, HP.
Qed.

Lemma no_collision_invariant : connected_path config ->
  no_collision config -> no_collision config'.
Proof using D_pos Dmax_6D Hda id_uniq scout_has_inv.
intros Hpath Hcollision r r' Hdiff.
pattern r, r'.
match goal with |- ?P _ _ => assert (Hsym : forall r r', P r r' ↔ P r' r) by (intuition auto with *) end.
cbn beta. apply (wlog_lt_id _ Hsym); trivial; []. clear r r' Hdiff Hsym.
intros r r' Hlt Halive Halive' Heq'.
(* The robot with highest id cannot be the scout. *)
assert (Hr' : exists g, r' = Good g).
{ destruct r' as [g | b]; eauto; [].
  rewrite (byz_is_scout b) in *. fold scout in Hlt. rewrite scout_id in Hlt. lia. }
destruct Hr' as [g ?]. subst r'.
(* To collide, both robots must have been at most 2*D away. *)
assert (Hg_alive : alive (config (Good g)) = true) by apply alive_before, Halive'.
assert (Hdist : dist (get_location (config r))
                     (get_location (config (Good g))) <= 2 * D).
{ etransitivity; [apply RealMetricSpace.triang_ineq |].
  replace (2 * D) with (D + D) by lra. apply Rplus_le_compat.
  + rewrite dist_sym. apply move_at_most_D.
  + rewrite Heq'. apply move_at_most_D. }
(* To collide, r was at most D away from the new position of g *)
assert (Hnext : dist (get_location (config r)) (get_location (config' (Good g))) <= D).
{ rewrite <- Heq', dist_sym. apply move_at_most_D. }
(* Thus, g cannot have moved *)
assert (Hg : get_location (config' (Good g)) == get_location (config (Good g))).
{ unfold config' in Halive', Heq' |- *. simplify_round. destruct (rbg obs) eqn:Hrbg.
  + cbn in Halive'. discriminate.
  + cbn -[equiv]. apply simpl_inverse_l.
  + assert (Haction := Hrbg). unfold rbg, rbg_fun in Haction. cbn -[equiv] in Haction.
    destruct (should_die obs) eqn:Hdie; try discriminate; [].
    destruct (move_to obs (choose_new_pos obs (tgt_loc (choose_target obs)))) eqn:Hmove;
    try discriminate; []. inv Haction.
    rewrite move_to_true in Hmove.
    assert (Hobs : (mk_tgt (local_config r) ∈ obs)%set).
    { unfold obs. rewrite obs_from_config_spec. exists r. repeat split.
      - cbn. now apply alive_before.
      - cbn. rewrite dist_prop. transitivity (2*D); trivial; []. lra.
      - cbn. apply Hlt. }
    specialize (Hmove _ Hobs). simpl tgt_loc in Hmove.
    assert (Hloc' : get_location (config' (Good g))
                    == (new_frame ⁻¹) (choose_new_pos obs (tgt_loc (choose_target obs)))).
    { unfold config'. rewrite Hround. cbn -[equiv rbg]. simplify_update. reflexivity. }
    rewrite <- dist_swap, <- Hloc', dist_sym in Hmove.
    rewrite <- Hround in Hnext. fold config' in Hnext. change loc with get_location in *.
    exfalso. clear -Hmove Hnext D_pos. lra. }
(* Then r was actually at most D away from g *)
rewrite Hg in Hnext.
(* In that case, g should have died. *)
unfold config' in Halive'. simplify_round.
assert (Hdie : should_die obs origin = true).
{ rewrite should_die_true. right. exists (mk_tgt (local_config r)). split.
  + unfold obs. rewrite obs_from_config_spec. exists r. repeat split.
    - cbn. now apply alive_before.
    - cbn. rewrite dist_prop. transitivity D; trivial; []. lra.
    - cbn. assumption.
  + cbn. rewrite <- dist_swap, <- (center_prop new_frame),
                 (compose_inverse_l new_frame (center new_frame)).
    unfold new_frame. rewrite da_iso, dist_sym. apply Hnext. }
cbn -[update] in Halive'. unfold rbg_fun in Halive'.
rewrite Hdie in Halive'. cbn in Halive'. discriminate.
Qed.

Lemma connected_path_invariant :
  connected_path config ->
  killed_light_on config ->
  killer_light_off config ->
  exists_at_less_than_Dp config ->
  connected_path config'.
Proof using D_pos Dmax_6D Hda scout_has_inv.
intros Hpath Hkilled Hkiller Hless g Halive'.
assert (Halive : alive (config (Good g)) = true) by now apply alive_before.
setoid_rewrite forever_same_id. unfold config'. simplify_round. setoid_rewrite Hround.
pose (real_target := new_frame ⁻¹ (choose_new_pos obs (tgt_loc (choose_target obs)))).
assert (Hlocal_state : loc local_state == origin).
{ unfold local_state, new_frame. cbn -[equiv]. rewrite <- da_iso. now rewrite center_prop. }
assert (Hobs : obs =/= {}%set).
{ rewrite connected_path_iff_obs_non_empty in Hpath.
  specialize (Hpath g Halive). unfold complement in *. cbn -[equiv] in Hpath.
  rewrite <- (map_empty_iff_empty (lift_target new_frame)) in Hpath; autoclass; [].
  rewrite (obs_from_config_map _ I) in Hpath. apply Hpath. }
cbn -[equiv update location] in *. unfold rbg_fun in *.
destruct (should_die obs origin) eqn:Hdie;
[| destruct (move_to obs (choose_new_pos obs (tgt_loc (choose_target obs)))) eqn:Hmove].
* cbn in Halive'. discriminate.
* cbn -[equiv location] in *. simplify_update. simpl loc in *. fold real_target.
  (* Let r be the target of g in config *)
  assert (Hrange := target_in_range Hobs). unfold obs in Hrange.
  rewrite obs_from_config_spec in Hrange. fold obs in Hrange.
  destruct Hrange as [r [Hr [Halive_r [Hdist_r Hid_r]]]].
  changeR2. fold config'.
  destruct (alive (config' r)) eqn:Halive'_r.
  + (* If r is still alive on config', we can use it *)
    exists r. repeat split; trivial; [].
    etransitivity; [apply (RealMetricSpace.triang_ineq _ (loc (config r))) |].
    replace Dmax with (D + Dp) by (unfold Dp; lra).
    apply Rplus_le_compat; try apply move_at_most_D; [].
    unfold real_target. rewrite dist_sym, <- dist_swap.
    unfold local_state in Hdist_r. rewrite Hlocal_state in Hdist_r.
    destruct (choose_new_pos_spec obs (new_frame (loc (config r))) Hdist_r) as [Hdist1 Hdist2].
    rewrite dist_sym, Hr. apply Hdist1.
  + (* If r dies, we can use its killer, because it did not move *)
    assert (Hlight_r : tgt_light (choose_target obs) = true).
    { destruct (dead_means_Good (scout_invariant scout_has_inv) _ Halive'_r) as [g' ?]. subst r.
      fold obs. rewrite Hr. cbn. now apply Hkilled, dies_as_it_should. }
    destruct (find_killer Hkilled Hkiller r Halive_r Halive'_r)
      as [r' [Hid_r' [Halive_r' [Hlight_r' Hdist_r']]]].
    exfalso. revert Hlight_r'. rewrite (Bool.not_false_iff_true (light (config r'))).
    change (tgt_light (mk_tgt (local_config r')) = true).
    eapply (target_light_on Hobs Hlight_r). unfold obs.
    rewrite obs_from_config_spec. exists r'. repeat split.
    - now apply alive_before.
    - etransitivity; [apply (RealMetricSpace.triang_ineq _ (loc (local_config r))) |].
      replace Dmax with (D + Dp) by (unfold Dp; lra). apply Rplus_le_compat.
      -- cbn. now rewrite dist_prop, dist_sym.
      -- assert (Hless' : exists_at_less_than_Dp local_config).
         { unfold local_config. now apply exists_at_less_than_Dp_map. }
         apply tgt_loc_compat in Hr. rewrite <- Hr, dist_sym. revert Hobs Hlight_r.
         now apply (target_light_on_at_most_Dp Hless'), Hlocal_state.
    - cbn in *. lia.
* (* If g cannot move, it is because of a robot r too close to the target of g *)
  assert (Hmove2 := Hmove). rewrite move_to_false in Hmove2.
  destruct Hmove2 as [tgt [Hin_r Hdist_r]].
  unfold obs in Hin_r. rewrite obs_from_config_spec in Hin_r.
  destruct Hin_r as [r [Hr [Halive_r [Hrange_r Hid_r]]]].
  destruct (alive (config' r)) eqn:Hr_alive'.
  + (* As r is alive, we can use it *)
    exists r. repeat split; trivial; [].
    cbn. changeR2. rewrite simpl_inverse_l.
    transitivity (D + 2 * D + D); try lra; []. fold (config' r).
    transitivity (
        dist (loc (config' r)) (loc (config r))
      + dist (loc (config r)) (new_frame ⁻¹ (choose_new_pos obs (tgt_loc (choose_target obs))))
      + dist (new_frame ⁻¹ (choose_new_pos obs (tgt_loc (choose_target obs))))
             (loc (config (Good g))));
    [ etransitivity; [eapply RealMetricSpace.triang_ineq |
                      apply Rplus_le_compat; [eapply RealMetricSpace.triang_ineq |]]; reflexivity
    | repeat apply Rplus_le_compat].
    - apply move_at_most_D.
    - rewrite Hr in Hdist_r. simpl tgt_loc in Hdist_r.
      now rewrite <- dist_swap, dist_sym in Hdist_r.
    - assert (Hdist : dist (tgt_loc (choose_target obs)) 0 <= Dmax).
      { rewrite <- Hlocal_state.
        change (new_frame (loc (config (Good g)))) with (loc (local_config (Good g))).
        now apply (in_range_dist local_config), target_in_range. }
      destruct (choose_new_pos_spec obs _ Hdist) as [Hdist1 Hdist2].
      rewrite dist_swap, dist_sym, <- da_iso. unfold new_frame.
      rewrite center_prop. fold obs in Hdist2. apply Hdist2.
  + (* If r dies, its killer is alive and in range of g *)
    destruct (find_killer Hkilled Hkiller _ Halive_r Hr_alive')
      as [r' [Hid_r' [Halive_r' [Hlight_r' Hdist_r']]]].
    exists r'. cbn in Hid_r. repeat split; trivial; try lia; [].
    cbn. rewrite simpl_inverse_l. changeR2.
    transitivity (D + D + 2 * D + D); try lra; []. fold (config' r').
    transitivity (dist (loc (config' r')) (loc (config r'))
                + dist (loc (config r')) (loc (config r))
                + dist (loc (config r)) real_target
                + dist real_target (loc (config (Good g))));
    [ repeat (etransitivity; [eapply RealMetricSpace.triang_ineq
                             | apply Rplus_le_compat; [| reflexivity]]); reflexivity
    | repeat apply Rplus_le_compat].
    - apply move_at_most_D.
    - now rewrite dist_sym.
    - unfold real_target. rewrite dist_sym, <- dist_swap. now rewrite Hr in Hdist_r.
    - assert (Hdist : dist (tgt_loc (choose_target obs)) 0 <= Dmax).
      { rewrite <- Hlocal_state.
        change (new_frame (loc (config (Good g)))) with (loc (local_config (Good g))).
        now apply (in_range_dist local_config), target_in_range. }
      destruct (choose_new_pos_spec obs _ Hdist) as [Hdist1 Hdist2].
      unfold real_target. rewrite dist_swap, dist_sym, <- da_iso. unfold new_frame.
      rewrite center_prop. fold obs in Hdist2. apply Hdist2.
Qed.


Lemma killed_light_on_invariant :
  killed_light_on config ->
  killer_light_off config ->
  connected_path config ->
  exists_at_less_than_Dp config ->
  killed_light_on config'.
Proof using D_pos Dmax_6D Hda scout_has_inv.
intros Hkilled Hkiller Hpath HlessDp g obs' Halive' Hdie'.
assert (Hg_alive : alive (config (Good g)) = true) by apply alive_before, Halive'.
assert (Hshould_die := Hdie'). rewrite should_die_true in Hshould_die.
destruct Hshould_die as [Hobs' | Hobs'].
* (* Absurd case: there is no robot in range now *)
  (* Since g is alive in config', it had a target r in config *)
  unfold config'. simplify_round. cbn -[update equiv location] in *. unfold rbg_fun in *.
  destruct (should_die obs 0%VS) eqn:Hdie;
  [| destruct (move_to obs (choose_new_pos obs (tgt_loc (choose_target obs)))) eqn:Hmove].
  + cbn in Hdie'. discriminate.
  + (* If g moved, its target r in config is still in range in config' *)
    cbn -[equiv] in *. simplify_update. cbn -[equiv location] in *. exfalso.
    assert (Hlocal_state : get_location local_state == origin).
    { unfold local_state, new_frame. now rewrite <- da_iso, center_prop. }
    assert (Hrange : dist (tgt_loc (choose_target obs)) 0 <= Dmax).
    { rewrite <- (center_prop new_frame). unfold new_frame at 2. rewrite da_iso.
      change (new_frame (get_location (config (Good g)))) with ((local_config (Good g)).(loc)).
      apply (in_range_dist local_config). apply target_in_range; trivial; [].
      change local_config with (map_config (lift (existT precondition new_frame I)) config).
      unfold map_config at 2. unfold complement.
      rewrite <- obs_from_config_map, map_empty_iff_empty; autoclass; [].
      rewrite connected_path_iff_obs_non_empty in Hpath. now apply Hpath. }
    destruct (choose_new_pos_spec obs _ Hrange) as [Hdist1 Hdist2]. fold obs in Hdist1, Hdist2.
    rewrite <- (dist_prop (new_frame ⁻¹)), <- (loc_compat Hround) in Hdist1.
    assert (Hstate_eq_0 : local_state == {{local_state with loc := origin}}).
    { unfold local_state. rewrite <- da_iso. destruct (config (Good g)). split; [| repeat split].
      cbn -[equiv]. unfold new_frame. now rewrite center_prop. }
    assert (Hobs : obs =/= {}%set).
    { rewrite connected_path_iff_obs_non_empty in Hpath. specialize (Hpath g Halive').
      intro Habs. apply Hpath. cbn -[equiv].
      rewrite <- (map_empty_iff_empty (lift_target new_frame)); autoclass; [].
      rewrite (obs_from_config_map _ I). apply Habs. }
    assert (Htgt := target_in_range Hobs).
    unfold obs in Htgt. rewrite obs_from_config_spec in Htgt. fold obs in Htgt.
    destruct Htgt as [r [Hr [Halive_r [Hdist_r Hid_r]]]].
    (* is r still alive in config'? *)
    destruct (alive (config' r)) eqn:Halive'_r.
    - (* if so, r is the robot we are looking for *)
      specialize (Hobs' (mk_tgt (config' r))).
      rewrite empty_spec in Hobs'. rewrite <- Hobs'. unfold obs'.
      rewrite obs_from_config_spec. exists r. split; [| repeat split].
      ++ reflexivity.
      ++ assumption.
      ++ replace Dmax with (D + Dp) by (unfold Dp; lra).
         etransitivity; [apply (RealMetricSpace.triang_ineq _ (loc (config r))) |].
         apply Rplus_le_compat.
         -- apply move_at_most_D.
         -- rewrite Hr in Hdist1. cbn in Hdist1. rewrite simpl_inverse_l in Hdist1.
            rewrite dist_sym. apply Hdist1.
      ++ now rewrite 2 forever_same_id.
    - (* otherwise, it was killed by another robot which must have been in range of g,
         thus had its light on and could not kill r *)
      destruct (find_killer Hkilled Hkiller r Halive_r Halive'_r)
        as [r' [Hid_r' [Halive_r' [Hlight_r' Hdist_r']]]].
      destruct (dead_means_Good (scout_invariant scout_has_inv) r Halive'_r) as [g' ?]. subst r.
      rewrite dies_iff_should_die in Halive'_r; auto; [].
      destruct (Hkiller _ Halive_r Halive'_r) as [tgt [Hin [Hdist Hlight]]].
      revert Hlight. change (tgt_light tgt <> false). rewrite Bool.not_false_iff_true.
      rewrite obs_from_config_spec in Hin. destruct Hin as [r [Htgt [Halive [Hd Hid]]]].
      apply Hkilled in Halive'_r; auto; [].
      change (tgt_light (mk_tgt (config (Good g'))) = true) in Halive'_r.
      assert (Hlight_on := target_light_on Hobs).
      fold obs in Hlight_on. rewrite Hr in Hlight_on.
      specialize (Hlight_on Halive'_r (mk_tgt (local_config r))).
      rewrite Htgt. apply Hlight_on. unfold obs.
      rewrite obs_from_config_spec. exists r. split; reflexivity || (repeat split).
      ++ apply Halive.
      ++ replace Dmax with (D + Dp) by (unfold Dp; lra).
         etransitivity; [ apply (RealMetricSpace.triang_ineq _ (loc (local_config (Good g')))) |].
         apply Rplus_le_compat.
         -- rewrite Htgt in Hdist. cbn. changeR2. now rewrite dist_prop, dist_sym.
         -- apply (exists_at_less_than_Dp_map new_frame) in HlessDp.
            assert (Hr_light := tgt_light_compat Hr). cbn in Hr_light, Halive'_r.
            rewrite <- Hr_light in Halive'_r.
            assert (Hless := target_light_on_at_most_Dp
                               HlessDp g Hg_alive Hlocal_state Hobs Halive'_r).
            change (dist (loc (local_state)) (tgt_loc (choose_target obs)) <= Dp) in Hless.
            rewrite Hr, dist_sym in Hless. apply Hless.
      ++ now transitivity (id (config (Good g'))).
  + reflexivity.
* (* If a robot r is close enough to kill g in config' *)
  destruct Hobs' as [tgt [Hin' Hdist']].
  unfold config'. simplify_round.
  cbn -[equiv update] in *. unfold rbg_fun in *.
  destruct (should_die obs 0%VS) eqn:Hdie;
  [| destruct (move_to obs (choose_new_pos obs (tgt_loc (choose_target obs)))) eqn:Hmove].
  + (* g cannot die in config' as it is assumed alive *)
    cbn in Halive'. discriminate.
  + (* Assume by contradiction that g moves so has its light off *)
    cbn -[equiv] in *. simplify_update. exfalso.
    simpl loc in Hdist', Hdie'. simpl alive in Halive'.
    unfold obs' in Hin'. rewrite obs_from_config_spec in Hin'.
    destruct Hin' as [r [Htgt [Halive [Hdist Hid]]]].
    rewrite move_to_true in Hmove. specialize (Hmove (lift_target new_frame (mk_tgt (config r)))).
    cbn -[equiv] in Hround.
    assert (Heq' : loc (config' (Good g))
                   == (new_frame ⁻¹) (choose_new_pos obs (tgt_loc (choose_target obs))))
      by now rewrite Hround.
    (* r was already in range of g in config *)
    assert (Hin : (lift_target new_frame (mk_tgt (config r)) ∈ obs)%set).
    { change (lift_target new_frame (mk_tgt (config r)) ∈
       obs_from_config (map_config (lift (existT precondition new_frame I)) config)
                       (lift (existT precondition new_frame I) (config (Good g))))%set.
      rewrite <- obs_from_config_map, map_spec; autoclass; [].
      exists (mk_tgt (config r)). split; try reflexivity; [].
      rewrite obs_from_config_spec. exists r. split; [| repeat split].
      + reflexivity.
      + now apply alive_before.
      + transitivity (D + D + D); try lra; [].
        transitivity (dist (loc (config r)) (loc (config' r))
                      + dist (loc (config' r)) (loc (config' (Good g)))
                      + dist (loc (config' (Good g))) (loc (config (Good g))));
        [ etransitivity; [eapply RealMetricSpace.triang_ineq
                         | apply Rplus_le_compat; [eapply RealMetricSpace.triang_ineq |]]
        | repeat apply Rplus_le_compat]; try reflexivity; [| |].
        - rewrite dist_sym. apply move_at_most_D.
        - rewrite Htgt, <- Heq' in Hdist'. apply Hdist'.
        - apply move_at_most_D.
      + now setoid_rewrite <- forever_same_id. }
    (* But r in config was not within 2 D of the location of g in config' since g moved,
       so it could not be within range D of g in config' and kill it *)
    apply Hmove in Hin. contradict Hin. apply Rle_not_gt.
    replace (2 * D) with (D + D) by lra.
    etransitivity; [apply (RealMetricSpace.triang_ineq _ (new_frame (tgt_loc tgt))) |].
    apply Rplus_le_compat.
    - cbn. changeR2. rewrite dist_prop, Htgt, dist_sym. cbn. apply move_at_most_D.
    - now rewrite <- dist_swap, dist_sym.
  + cbn. reflexivity.
Qed.

Lemma killer_light_off_invariant :
  killed_light_on config ->
  killer_light_off config ->
  connected_path config ->
  exists_at_less_than_Dp config ->
  killer_light_off config'.
Proof using id_uniq D_pos Dmax_6D Hda scout_has_inv.
intros Hkilled Hkiller Hpath HlessDp g obs' Halive' Hdie'.
assert (Halive : alive (config (Good g)) = true) by now apply alive_before.
(* If g dies it has its light on *)
assert (Hkilled' : killed_light_on config') by now apply killed_light_on_invariant.
specialize (Hkilled' g Halive' Hdie').
(* Thus g did not move *)
assert (Heq_loc : (loc (config' (Good g))) == (loc (config (Good g))))
  by now apply light_on_same_location.
assert (Hobs' : obs' =/= {}%set).
{ apply connected_path_invariant in Hpath; trivial; [].
  rewrite connected_path_iff_obs_non_empty in Hpath.
  now apply Hpath in Halive'. }
(* Let r be the robot killing g. *)
rewrite should_die_true in Hdie'.
destruct Hdie' as [Habs | [tgt [Hin Hdist]]]; try contradiction; [].
assert (Hr := Hin). unfold obs' in Hr. rewrite obs_from_config_spec in Hr.
destruct Hr as [r [Hr [Halive_r [Hrange_r Hid_r]]]].
(* Assume by contradiction that the light of r is on *)
rewrite dist_sym in Hdist.
destruct (tgt_light tgt) eqn:Hlight; eauto; [].
exfalso.
(* Then r did not move *)
assert (Heq_loc_r : (loc (config' r)) == (loc (config r))).
{ rewrite Hr in Hlight. now apply light_on_same_location. }
(* So g should have died in config *)
revert Halive'. rewrite (Bool.not_true_iff_false (alive (config' (Good g)))).
rewrite dies_iff_should_die, should_die_true; trivial; [].
right. exists (tgt_loc tgt, light (config r)). split.
+ rewrite obs_from_config_spec. exists r.
  repeat split.
  - cbn -[equiv]. now rewrite Hr, Heq_loc_r.
  - now apply alive_before.
  - now rewrite Heq_loc, Heq_loc_r in Hrange_r.
  - now rewrite 2 forever_same_id in Hid_r.
+ now rewrite dist_sym, <- Heq_loc.
Qed.

Lemma exists_at_less_than_Dp_invariant :
  killed_light_on config ->
  killer_light_off config ->
  connected_path config ->
  exists_at_less_than_Dp config ->
  exists_at_less_than_Dp config'.
Proof using D_pos Dmax_6D Hda scout_has_inv.
intros Hkilled Hkiller Hpath Hless g Halive' obs' Hall.
assert (Halive := alive_before _ Halive').
revert Halive'. unfold config'. simplify_round. fold config'. intro Halive'.
pose (real_target := new_frame ⁻¹ (choose_new_pos obs (tgt_loc (choose_target obs)))).
assert (Hobs : obs =/= {}%set).
{ rewrite connected_path_iff_obs_non_empty in Hpath.
  specialize (Hpath _ Halive). unfold complement in Hpath. cbn -[equiv] in Hpath.
  rewrite <- (map_empty_iff_empty (lift_target new_frame)),
          (obs_from_config_map _ I) in Hpath; autoclass. }
assert (Hlocal_less : exists_at_less_than_Dp local_config)
  by now apply exists_at_less_than_Dp_map.
assert (Hlocal_state : loc (local_config (Good g)) == origin).
{ cbn -[equiv]. unfold new_frame. now rewrite <- da_iso, center_prop. }
destruct (light (config' (Good g))) eqn:Hlight'.
* (* If g has its light on in config', it is because of a robot r close to its target location
     in config, thus at most 3D away from the current location of g. *)
  unfold config' in Hlight'. rewrite Hround in Hlight'.
  assert (Hmove : move_to obs (choose_new_pos obs (tgt_loc (choose_target obs))) = false).
  { cbn -[equiv] in Halive', Hround, Hlight' |-*. unfold rbg_fun in *.
    destruct (should_die obs 0%VS) eqn:Hdie;
    [| destruct (move_to obs (choose_new_pos obs (tgt_loc (choose_target obs))))].
    + cbn in Halive'. discriminate.
    + simplify_update. cbn in Hlight'. discriminate.
    + reflexivity. }
  rewrite move_to_false in Hmove. destruct Hmove as [tgt [Hin Hdist]]. unfold obs in Hin.
  rewrite obs_from_config_spec in Hin. destruct Hin as [r [Htgt [Halive_r [Hdist_r Hid_r]]]].
  (* The main result depending on distances *)
  assert (Hdist_main : dist (loc (config r)) (loc (config' (Good g))) <= 3 * D).
  { replace (3 * D) with (2 * D + D) by lra.
    etransitivity; [eapply (RealMetricSpace.triang_ineq _ real_target) | apply Rplus_le_compat].
    - unfold real_target. rewrite dist_sym, <- dist_swap.
      rewrite Htgt in Hdist. apply Hdist.
    - assert (Hrange : dist (tgt_loc (choose_target obs)) 0 <= Dmax).
      { rewrite <- Hlocal_state. now apply (in_range_dist local_config), target_in_range. }
      destruct (choose_new_pos_spec obs _ Hrange) as [Hdist1 Hdist2].
      fold obs in Hdist1, Hdist2. unfold real_target.
      assert (Heq_loc : loc (config' (Good g)) == loc (config (Good g))).
      { now apply light_on_same_location; rewrite Hround. }
      rewrite dist_swap, dist_sym, Heq_loc.
      change (new_frame (loc (config (Good g)))) with (loc (local_config (Good g))).
      rewrite Hlocal_state. apply Hdist2. }
  assert (Hprop : forall A B : Prop, (B -> A) -> B -> A ∧ B) by tauto.
  destruct (alive (config' r)) eqn:Halive'_r.
  + (* If r is still alive in config', we can use it *)
    exists (mk_tgt (config' r)). simpl tgt_loc. unfold obs'. apply Hprop.
    - intro Hdist'. rewrite obs_from_config_spec. exists r. split; [| repeat split].
      -- reflexivity.
      -- assumption.
      -- transitivity Dp; trivial; unfold Dp; lra.
      -- rewrite 2 forever_same_id. apply Hid_r.
    - transitivity (D + 3 * D); try (unfold Dp; lra); [].
      etransitivity; [ eapply (RealMetricSpace.triang_ineq _ (loc (config r)))
                     | apply Rplus_le_compat]; trivial; apply move_at_most_D.
  + (* r dies in config' so we use its killer *)
    destruct (find_killer Hkilled Hkiller r Halive_r Halive'_r)
      as [r' [Hid_r' [Halive_r' [Hlight_r' Hdist_r']]]].
    exists (mk_tgt (config' r')). apply Hprop.
    - unfold obs'. rewrite obs_from_config_spec.
      exists r'. split; [| repeat split].
      -- reflexivity.
      -- assumption.
      -- transitivity Dp; trivial; unfold Dp; lra.
      -- rewrite 2 forever_same_id. cbn in *; lia.
    - transitivity (D + D + 3 * D); try (unfold Dp; lra); [].
      transitivity (dist (loc (config' r')) (loc (config r'))
                    + dist (loc (config r')) (loc (config r))
                    + dist (loc (config r)) (loc (config' (Good g))));
      [ etransitivity; [eapply RealMetricSpace.triang_ineq
                       | apply Rplus_le_compat; [eapply RealMetricSpace.triang_ineq |]]
      | repeat apply Rplus_le_compat]; try reflexivity; [| |].
      -- apply move_at_most_D.
      -- now rewrite dist_sym.
      -- apply Hdist_main.
* (* If g has its light off, we consider its target r in config. *)
  (* The target r of g in config has its light on in config' and is at most Dp away in config'. *)
  assert (Hrange := target_in_range Hobs). unfold obs in Hrange.
  rewrite obs_from_config_spec in Hrange. fold obs in Hrange.
  destruct Hrange as [r [Hr [Halive_r [Hdist_r Hid_r]]]].
  (* If r dies in config', it killer would have its light off and be in range of g.
     Being in range of g, it should have had its light on, a contradiction *)
  assert (Halive'_r : alive (config' r) = true).
  { rewrite <- Bool.not_false_iff_true. intro Habs.
    destruct (find_killer Hkilled Hkiller r Halive_r Habs)
      as [r' [Hid_r' [Halive_r' [Hlight_r' Hdist_r']]]].
    assert (Hlight : tgt_light (choose_target obs) = true).
    { rewrite Hr. cbn.
      destruct (dead_means_Good (scout_invariant scout_has_inv) r Habs) as [g' ?]. subst r.
      assert (Hdie := dies_as_it_should _ Halive_r Habs).
      now apply Hkilled in Hdie. }
    assert (Htarget := target_light_on_at_most_Dp Hlocal_less g Halive Hlocal_state Hobs Hlight).
    change (local_config (Good g)) with local_state in Htarget. fold obs in Htarget.
    assert (Hrange_r' : (mk_tgt (local_config r') ∈ obs)%set).
    { unfold obs. rewrite obs_from_config_spec. exists r'. split; [| repeat split].
      + reflexivity.
      + now apply alive_before.
      + rewrite dist_sym.
        etransitivity; [apply (RealMetricSpace.triang_ineq _ (loc (local_config r))) |].
        replace Dmax with (Dp + D) by (unfold Dp; lra). apply Rplus_le_compat.
        - rewrite Hr in Htarget. apply Htarget.
        - cbn. rewrite dist_prop. apply Hdist_r'.
      + cbn in *; lia. }
    apply (target_light_on Hobs Hlight) in Hrange_r'.
    cbn in Hrange_r'. congruence. }
  (* Since g is alive and has its light off in config', it moved *)
  assert (Heq_loc : loc (config' (Good g)) == real_target).
  { unfold config' in Hlight' |- *. rewrite Hround in Hlight' |- *. cbn -[equiv] in *.
    unfold rbg_fun in *.
    destruct (should_die obs 0%VS);
    [| destruct (move_to obs (choose_new_pos obs (tgt_loc (choose_target obs))))].
    - cbn in Halive'. discriminate.
    - simplify_update. reflexivity.
    - cbn in Hlight'. discriminate. }
  (* The distance to this new location and the location of r in config is at most Dp *)
  assert (Hdist_main : dist (tgt_loc (choose_target obs))
                            (choose_new_pos obs (tgt_loc (choose_target obs))) <= Dp).
  { rewrite dist_sym. apply choose_new_pos_spec. rewrite Hr, <- Hlocal_state. apply Hdist_r. }
  (* Thus, r is the robot we are looking for *)
  assert (Hprop : forall A B : Prop, A -> (A -> B) -> A ∧ B) by tauto.
  exists (mk_tgt (config' r)). simpl tgt_loc. apply Hprop.
  + unfold obs'. rewrite obs_from_config_spec.
    exists r. split; [| repeat split].
    - reflexivity.
    - assumption.
    - rewrite Heq_loc. unfold real_target. rewrite dist_sym, dist_swap.
      replace Dmax with (D + Dp) by (unfold Dp; lra).
      etransitivity; [apply (RealMetricSpace.triang_ineq _ (tgt_loc (choose_target obs))) |].
      apply Rplus_le_compat.
      -- rewrite Hr. cbn. rewrite dist_prop. apply move_at_most_D.
      -- apply Hdist_main.
    - rewrite 2 forever_same_id. apply Hid_r.
  + intro Hin. apply Hall, light_on_same_location in Hin; trivial; [].
    rewrite Heq_loc, Hin. unfold real_target.
    rewrite dist_sym, dist_swap. rewrite Hr in Hdist_main at 1. apply Hdist_main.
Qed.

End InvariantProofs.

Definition full_inv config :=
    scout_inv config
  ∧ distinct_id config
  ∧ no_collision config
  ∧ killed_light_on config
  ∧ killer_light_off config
  ∧ exists_at_less_than_Dp config
  ∧ connected_path config.

Theorem global_invariant : forall d, Stream.forever (Stream.instant da_assumption) d ->
  forall config, full_inv config ->
  Stream.forever (Stream.instant full_inv) (execute rbg d config).
Proof using D_pos Dmax_6D.
coinduction Hcorec.
+ assumption.
+ now destruct H.
+ revert_all. intros [? [? [? [? [? [? ?]]]]]].
  destruct H.
  repeat split;
  apply scout_invariant ||
  apply distinct_id_invariant ||
  apply no_collision_invariant ||
  apply killed_light_on_invariant ||
  apply killer_light_off_invariant ||
  apply exists_at_less_than_Dp_invariant ||
  apply connected_path_invariant; auto.
Qed.




(** ***  Existence of a solution by instantiating the robogram parameters  **)

(** We need a choose function on sets compatible with their equality.
    Here, we pick the minimum. *)
Section Compatible_choose.

Variable A : Type.
Variables (lt : relation A) (lt_dec : forall x y, {lt x y} + {~lt x y}).
Context `{EqDec A}.
Context (lt_compat : Proper (equiv ==> equiv ==> iff) lt).
Context (St : StrictOrder lt).
Variable (trichotomy : forall x y, {lt x y} + {x == y} + {lt y x}).

Definition set_min (s : set A) : option A :=
  match choose s with
    | None => None
    | Some elt => Some (fold (fun e min => if lt_dec e min then e else min) s elt)
  end.

Lemma fold_min_spec : forall s e,
  let min := fold (fun e min => if lt_dec e min then e else min) s e in
  (min = e \/ InA equiv min (elements s))
  /\ ~lt e min /\ Forall (fun x => ~lt x min) (elements s).
Proof using A lt St lt_compat trichotomy.
intros s e min. subst min. rewrite fold_spec. revert e.
induction (elements s) as [| a l]; simpl; intro e.
* repeat split.
  + now left.
  + apply irreflexivity.
  + constructor.
* destruct_match.
  + specialize (IHl a). destruct IHl as [Hin [Hmin1 Hmin2]].
    repeat split.
    - right. destruct Hin as [Hin | Hin]; (now rewrite Hin; left) || now right.
    - intro Habs. apply Hmin1. now transitivity e.
    - now constructor.
  + specialize (IHl e). destruct IHl as [Hin [Hmin1 Hmin2]].
    repeat split.
    - destruct Hin as [Hin | Hin]; (now left) || (now right; constructor 2).
    - assumption.
    - constructor; trivial; [].
      intro Habs. destruct (trichotomy a e) as [[Hae | Hae] | Hae].
      -- contradiction.
      -- rewrite Hae in *. contradiction.
      -- apply Hmin1. now transitivity a.
Qed.

Lemma fold_indep_elt : forall s e1 e2, (e1 ∈ s)%set -> (e2 ∈ s)%set ->
  fold (fun e min => if lt_dec e min then e else min) s e1
  == fold (fun e min => if lt_dec e min then e else min) s e2.
Proof using trichotomy lt_compat St.
intros s e1 e2 He1 He2.
destruct (fold_min_spec s e1) as [Hin1 [Hmin1 Hall1]],
         (fold_min_spec s e2) as [Hin2 [Hmin2 Hall2]].
destruct (trichotomy (fold (λ e min : A, if lt_dec e min then e else min) s e1)
                     (fold (λ e min : A, if lt_dec e min then e else min) s e2))
  as [[Hlt | Heq] | Hlt].
+ exfalso. rewrite Forall_forall in Hall2.
  destruct Hin1 as [Hin1 | Hin1].
  - apply elements_1 in He1. rewrite InA_alt in He1. destruct He1 as [e1' [Heq1 He1]].
    apply Hall2 in He1. rewrite Hin1, Heq1 in *. contradiction.
  - rewrite InA_alt in Hin1. destruct Hin1 as [e1' [Heq1 Hin1]].
    apply Hall2 in Hin1. rewrite Heq1 in *. contradiction.
+ assumption.
+ exfalso. rewrite Forall_forall in Hall1.
  destruct Hin2 as [Hin2 | Hin2].
  - apply elements_1 in He2. rewrite InA_alt in He2. destruct He2 as [e2' [Heq2 He2]].
    apply Hall1 in He2. rewrite Hin2, Heq2 in *. contradiction.
  - rewrite InA_alt in Hin2. destruct Hin2 as [e2' [Heq2 Hin2]].
    apply Hall1 in Hin2. rewrite Heq2 in *. contradiction.
Qed.

Instance set_min_compat : Proper (equiv ==> equiv) set_min.
Proof using A lt St lt_compat lt_dec trichotomy.
intros s1 s2 Hs. unfold set_min.
destruct (choose s1) as [elt1 |] eqn:Hchoose1.
* assert (Hcase : exists elt2, choose s2 = Some elt2).
  { destruct (choose s2) eqn:Habs; try eauto; []. apply choose_2 in Habs.
    apply choose_1 in Hchoose1. rewrite Hs in Hchoose1. apply Habs in Hchoose1. tauto. }
  destruct Hcase as [elt2 Hcase]. rewrite Hcase. cbn.
  transitivity (fold (λ e min : A, if lt_dec e min then e else min) s1 elt2).
  + apply fold_indep_elt.
    - now apply choose_1.
    - rewrite Hs. now apply choose_1.
  + apply fold_compat.
    - intros ? ? Heq1 ? ? Heq2. do 2 destruct_match; rewrite Heq1, Heq2 in *; intuition.
    - intros ? ? ?. repeat destruct_match; auto.
      -- exfalso. eapply irreflexivity. etransitivity; eauto.
      -- exfalso. revert_one not. intro Habs. apply Habs. etransitivity; eauto.
      -- destruct (trichotomy y x) as [[] |]; auto.
      -- exfalso. revert_one not. intro Habs. apply Habs. etransitivity; eauto.
    - assumption.
* assert (Hcase : choose s2 = None).
  { destruct (choose s2) eqn:Habs; auto; []. apply choose_2 in Hchoose1.
    apply choose_1 in Habs. rewrite <- Hs in Habs. now apply Hchoose1 in Habs. }
  now rewrite Hcase.
Qed.

Lemma set_min_None : forall s, set_min s = None <-> s == {}%set.
Proof using .
intro s. unfold set_min. destruct (choose s) eqn:Hcase.
+ split; intro Hs; try discriminate;[].
  apply choose_1 in Hcase. rewrite Hs in Hcase. now apply empty_spec in Hcase.
+ split; intros _; trivial; [].
  apply choose_2 in Hcase. intro x. rewrite empty_spec. split; try tauto; []. apply Hcase.
Qed.

Lemma set_min_Some : forall s e,
  set_min s = Some e -> (e ∈ s)%set /\ forall x, (x ∈ s)%set -> ~lt x e.
Proof using St lt_compat trichotomy.
intros s e Hin. unfold set_min in Hin.
destruct (choose s) as [a |] eqn:Hcase; try discriminate; [].
inv Hin. destruct (fold_min_spec s a) as [Hin [Hmin Hall]]. split.
+ destruct Hin as [Hin | Hin].
  - rewrite Hin. now apply choose_1.
  - now apply elements_2.
+ intros x Hx. apply elements_1 in Hx. rewrite InA_alt in Hx.
  destruct Hx as [y [Hxy Hy]]. rewrite Forall_forall in Hall.
  rewrite Hxy. now apply Hall.
Qed.

Lemma set_min_spec : forall s e,
  set_min s == Some e <-> (e ∈ s)%set /\ forall x, (x ∈ s)%set -> ~lt x e.
Proof using St lt lt_compat lt_dec trichotomy.
clear Dmax_6D. clear robogram_args. clear Dmax.
intros s e. destruct (set_min s) as [a |] eqn:Hcase.
+ apply set_min_Some in Hcase. cbn. split; intro Hin.
  - rewrite <- Hin. setoid_rewrite <- Hin. apply Hcase.
  - destruct Hcase, Hin, (trichotomy a e) as [[Hlt | Heq] | Hlt]; firstorder.
+ cbn. split; try tauto; [].
  intros [Habs _]. rewrite set_min_None in Hcase. rewrite Hcase in Habs.
  revert Habs. apply empty_spec.
Qed.

End Compatible_choose.

Definition tgt_lt x y :=
  Rlt (fst (tgt_loc x)) (fst (tgt_loc y))
  \/ fst (tgt_loc x) = fst (tgt_loc y) /\ Rlt (snd (tgt_loc x)) (snd (tgt_loc y))
  \/ tgt_loc x == tgt_loc y /\ tgt_light x = true /\ tgt_light y = false.

Definition tgt_lt_dec : forall x y, {tgt_lt x y} + {~tgt_lt x y}.
Proof using .
intros x y. unfold tgt_lt.
destruct (Rlt_dec (fst (tgt_loc x)) (fst (tgt_loc y))).
* now do 2 left.
* destruct (Req_EM_T (fst (tgt_loc x)) (fst (tgt_loc y)));
  [destruct (Rlt_dec (snd (tgt_loc x)) (snd (tgt_loc y))) |].
  + left. right. left. now split.
  + destruct (tgt_loc x =?= tgt_loc y), (tgt_light x), (tgt_light y);
    solve [ right; intro Habs; decompose [and or] Habs; clear Habs; congruence || lra
          | left; do 2 right; now repeat split ].
  + right. intro Habs; decompose [and or] Habs; clear Habs; congruence || lra.
Defined.

Instance tgt_lt_compat : Proper (equiv ==> equiv ==> iff) tgt_lt.
Proof using. intros [] [] [] [] [] []. simpl in *. now subst. Qed.

Instance tgt_lt_SO : StrictOrder tgt_lt.
Proof using. split.
+ unfold tgt_lt. intros x Habs. decompose [and or] Habs; clear Habs; try congruence; [|];
  eapply (@irreflexivity _ Rlt); eauto; autoclass.
+ unfold tgt_lt. intros x y z Hlt1 Hlt2.
  decompose [and or] Hlt1; decompose [and or] Hlt2; clear Hlt1 Hlt2; simpl in *;
  solve [ lra | left; congruence | right; left; split; congruence ].
Qed.

Lemma tgt_trichotomy : forall x y, {tgt_lt x y} + {x == y} + {tgt_lt y x}.
Proof.
intros x y. destruct (tgt_lt_dec x y) as [| Hxy]; [| destruct (tgt_lt_dec y x) as [| Hyx]].
+ now do 2left.
+ now right.
+ left; right.
  assert (Hfst : fst (tgt_loc x) = fst (tgt_loc y)).
  { destruct (total_order_T (fst (tgt_loc x)) (fst (tgt_loc y))) as [[Habs | Habs] | Habs].
    - exfalso. apply Hxy. now left.
    - assumption.
    - exfalso. apply Hyx. now left. }
  assert (Hsnd : snd (tgt_loc x) = snd (tgt_loc y)).
  { destruct (total_order_T (snd (tgt_loc x)) (snd (tgt_loc y))) as [[Habs | Habs] | Habs].
    - exfalso. apply Hxy. now right; left.
    - assumption.
    - exfalso. apply Hyx. now right; left. }
  simpl. destruct x as [[] []], y as [[] []]; simpl in *; subst; repeat split; exfalso.
  - apply Hxy. do 2 right. repeat split.
  - apply Hyx. do 2 right. repeat split.
Defined.

Local Hint Immediate tgt_trichotomy : core.

Definition choose_min := set_min tgt_lt tgt_lt_dec.

Instance choose_min_compat : Proper (equiv ==> equiv) choose_min.
Proof using. repeat intro. unfold choose_min. apply set_min_compat; autoclass. Qed.

#[refine]
Instance concrete_params : Param := {|
  choose_target obs :=
    match choose_min (filter (fun tgt => negb (tgt_light tgt)) obs) with
      | Some tgt => tgt
      | None =>
          match choose_min (filter (fun tgt => Rle_bool (dist (tgt_loc tgt) origin) Dp) obs) with
            | Some tgt => tgt
            | None => match choose_min obs with
                        | Some tgt => tgt
                        | None => (origin, false) (* dummy value *)
                      end
          end
    end;
  choose_new_pos obs pt :=
    if Rle_bool (dist pt origin) Dp then origin else (D * unitary pt)%VS
|}.
Proof.
all: autoclass.
* (* choose_target_compat *)
  intros obs1 obs2 Hobs.
  assert (Hf1 : Proper (equiv ==> equiv) (fun tgt => negb (tgt_light tgt))).
  { intros tgt1 tgt2 Htgt. now rewrite Htgt. }
  assert (Hf2 : Proper (equiv ==> equiv) (fun tgt => Rle_bool (dist (tgt_loc tgt) 0) Dp)).
  { intros tgt1 tgt2 Htgt. now rewrite Htgt. }
  assert (Heq1 := filter_m). specialize (Heq1 _ Hf1 obs1 obs2 Hobs).
  assert (Heq2 := filter_m). specialize (Heq2 _ Hf2 obs1 obs2 Hobs).
  assert (Heq3 := choose_min_compat Hobs).
  apply choose_min_compat in Heq1. apply choose_min_compat in Heq2.
  repeat destruct_match; auto.
* (* choose_new_pos_compat *)
  intros obs1 obs2 Hobs pt1 pt2 Hpt. rewrite Hpt.
  destruct_match; try reflexivity; []. now rewrite Hpt.
* (* choose_target_spec *)
  assert (Proper (equiv ==> eq) (fun tgt => negb (tgt_light tgt))).
  { intros tgt1 tgt2 Htgt. now rewrite Htgt. }
  assert (Proper (equiv ==> eq) (fun tgt => Rle_bool (dist (tgt_loc tgt) 0) Dp)).
  { intros tgt1 tgt2 Htgt. now rewrite Htgt. }
  intros obs Hobs.
  destruct (choose_min (filter (fun tgt => negb (tgt_light tgt)) obs)) eqn:Hchoose1;
  [| destruct (choose_min (filter (fun tgt => Rle_bool (dist (tgt_loc tgt) 0) Dp) obs))
       eqn:Hchoose2;
  [| destruct (choose_min obs) eqn:Hchoose3]].
  + apply set_min_Some in Hchoose1; autoclass; [].
    rewrite filter_spec in Hchoose1; trivial; []. destruct Hchoose1 as [Hin Hprop].
    repeat split.
    - intuition.
    - intro Habs. rewrite Habs in *. intuition discriminate.
    - intro Habs. rewrite Habs in *. intuition discriminate.
  + apply set_min_Some in Hchoose2; autoclass; [].
    rewrite filter_spec in Hchoose2; trivial; []. destruct Hchoose2 as [Hin Hprop].
    assert (Hall : For_all (fun tgt => tgt_light tgt = true) obs).
    { intros tgt Htgt. apply set_min_None in Hchoose1. specialize (Hchoose1 tgt).
      rewrite <- Bool.not_false_iff_true. intro Habs. apply (empty_spec tgt).
      rewrite <- Hchoose1. now rewrite filter_spec, Habs. }
    repeat split.
    - intuition.
    - intros Hlight. apply Hall.
    - intros Hlight Habs. exfalso. revert Habs.
      apply Rle_not_lt. rewrite dist_sym. now rewrite <- Rle_bool_true_iff.
  + apply set_min_Some in Hchoose3; autoclass; [].
    assert (Hall1 : For_all (fun tgt => tgt_light tgt = true) obs).
    { intros tgt Htgt. apply set_min_None in Hchoose1. specialize (Hchoose1 tgt).
      rewrite <- Bool.not_false_iff_true. intro Habs. apply (empty_spec tgt).
      now rewrite <- Hchoose1, filter_spec, Habs. }
    assert (Hall2 : For_all (fun tgt => dist (tgt_loc tgt) origin > Dp) obs).
    { intros tgt Htgt. apply set_min_None in Hchoose2. specialize (Hchoose2 tgt).
      apply Rnot_le_gt. intro Habs. apply (empty_spec tgt).
      rewrite <- Hchoose2, filter_spec; autoclass; [].
      split; trivial; []. unfold Rle_bool. now destruct_match. }
    repeat split.
    - intuition.
    - intros _. apply Hall1.
    - intros _ _. setoid_rewrite dist_sym. apply Hall2.
  + cbn zeta. exfalso. apply set_min_None in Hchoose3. contradiction.
* (* choose_new_pos_spec *)
  intros obs pt Hle. cbn zeta. unfold Rle_bool. destruct_match.
  + rewrite dist_same, dist_sym. split; trivial; lra.
  + revert_one not. intro Hnot.
    assert (Hnull : pt =/= origin).
    { intro Habs. apply Hnot. rewrite Habs, dist_same. unfold Dp. lra. }
    split.
    - rewrite (unitary_id pt) at 2. rewrite dist_sym, norm_dist, <- minus_morph, add_morph.
      rewrite norm_mul, norm_unitary, Rmult_1_r; trivial; [].
      rewrite norm_dist, opp_origin, add_origin_r in Hnot.
      rewrite Rabs_pos_eq.
      -- unfold Dp. cut (norm pt <= Dmax); try lra; [].
         now rewrite norm_dist, opp_origin, add_origin_r in Hle.
      -- cut (D <= norm pt); try lra; []. apply Rnot_le_lt in Hnot.
         unfold Dp in *. transitivity (Dmax - D); try lra; []. now apply Rlt_le.
    - rewrite norm_dist, opp_origin, add_origin_r, norm_mul, norm_unitary; auto; [].
      rewrite Rabs_pos_eq; lra.
Defined.

End PursuitSetting.
