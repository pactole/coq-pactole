(**************************************************************************)
(*   Mechanised Framework for Local Interactions & Distributed Algorithms *)
(*   T. Balabonski, P. Courtieu, L. Rieg, X. Urbain                       *)
(*   PACTOLE project                                                      *)
(*                                                                        *)
(*   This file is distributed under the terms of the CeCILL-C licence.    *)
(*                                                                        *)
(**************************************************************************)

(**************************************************************************)
(**  Mechanised Framework for Local Interactions & Distributed Algorithms   
                                                                            
     T. Balabonski, P. Courtieu, L. Rieg, X. Urbain                         
                                                                            
     PACTOLE project                                                        
                                                                            
     This file is distributed under the terms of the CeCILL-C licence.      
                                                                          *)
(**************************************************************************)


Require Import Utf8.
Require Import Lia PeanoNat.
Require Import SetoidList Pactole.Util.SetoidDefs.
Require Import Pactole.Util.Bijection.
Require Export Pactole.CaseStudies.Gathering.Definitions.
Require Export Pactole.Observations.MultisetObservation.
Require Import Pactole.Observations.PairObservation.
Close Scope R_scope.
Close Scope VectorSpace_scope.
Set Implicit Arguments.
Typeclasses eauto := (dfs) 5.

Class Lights := {
  L : Type;
  #[export]  L_Setoid :: Setoid L;
  #[export] L_EqDec :: EqDec L_Setoid;
  witness : L;
  l_list : list L;
  L_list_NoDupA : NoDupA equiv l_list;
  L_list_InA : forall li : L, InA equiv li l_list;
}.


(** Gathering Definitions specific to a setting with multiplicities, i.e. a multiset observation. *)

Section MultisetGathering.

(** Here, the state contain the location and the color. *)
Context `{Loc:Location}.
Context {Lght : Lights}.

Local Instance St : State (location*L) := AddInfo L (OnlyLocation (fun _ => True)).
Context {VS : RealVectorSpace location}.
Context {RMS : RealMetricSpace location}.
Context `{Hnames:Names}.
Context `{Robot_ch:robot_choice}.
Context `{U:update_choice}.
Context `{Ina:inactive_choice}.
Context {UpdFun : update_function _ _ _}.
Context {InaFun : inactive_function _}.


Definition get_light (x:(location*L)): L := snd x.

Global Instance get_light_compat : Proper (equiv ==> equiv) get_light := snd_compat_pactole.

Lemma no_info : forall x y, get_location x == get_location y -> get_light x == get_light y -> x == y.
Proof using . now intros. Qed.

(** Auxiliary def: an observation with lights on two locations, the observer's and another one. *)
Definition find_another_location config pt :=
  List.hd pt (List.filter (fun x => if x =?= pt then false else true)
                          (List.map get_location (config_list config))).

Local Instance find_another_location_compat : Proper (equiv ==> equiv ==> equiv) find_another_location.
Proof using Type.
intros config1 config2 Hconfig pt1 pt2 Hpt. unfold find_another_location.
apply hd_eqlistA_compat; trivial; [].
f_equiv.
- repeat intro. do 2 destruct_match; trivial; exfalso;
  apply H0 || apply H1; do 2 (etransitivity; try eassumption; eauto).
- now rewrite Hconfig.
Qed.

Lemma find_another_location_spec : forall config pt,
  (* not gathered *)
  ~gathered_at pt config ->
(*   (exists id1 id2, get_location (config id1) =/= get_location (config id2)) -> *)
  find_another_location config pt =/= pt.
Proof using Type.
intros config pt Hgathered. unfold find_another_location. rewrite config_list_spec, map_map.
intro Habs. apply Hgathered. intro g. assert (Hg := In_names (Good g)).
induction names as [| id names]; cbn -[equiv_dec] in *.
+ tauto.
+ destruct Hg.
  - subst id. revert Habs. now destruct_match.
  - apply IHnames; trivial; [].
    revert Habs. now destruct_match.
Qed.

Lemma find_another_location_map : forall f,
  Proper (equiv ==> equiv) f -> Util.Preliminary.injective equiv equiv f ->
  forall Pf config pt,
  find_another_location (map_config (lift (existT _ f Pf)) config) (f pt)
  == f (find_another_location config pt).
Proof using Type.
intros f Hf Hf_inj Pf config pt. unfold find_another_location.
rewrite config_list_map, map_map.
* induction (config_list config) as [| [x li] l]; cbn.
  + reflexivity.
  + repeat destruct_match; cbn.
    - apply IHl.
    - apply Hf_inj in H. contradiction.
    - rewrite H0 in *. intuition.
    - reflexivity.
(* * apply lift_compat. intros x y Hxy. cbn. now apply Hf. *)
Qed.

Existing Instance eqlistA_Setoid.
Existing Instance eqlistA_EqDec.

Record obsLight := {
    observer_lght: L;           (* color of the observing robot *)
    colors: multiset (location*L);
  }.

Definition obsLight_equiv (o1 o2: obsLight) :=
  colors o1 == colors o2 /\ observer_lght o1 == observer_lght o2.

Instance obsLight_is_equiv: Equivalence obsLight_equiv.
Proof using Type.
  unfold obsLight_equiv.
  repeat split;auto; try now intuition; try etransitivity; eauto.
Qed.

Global Instance obsLight_Setoid : Setoid obsLight := {|
  equiv := obsLight_equiv;
  setoid_equiv := obsLight_is_equiv |}.

Global Instance obsLight_is_dec: EqDec obsLight_Setoid.
Proof.
  red.
  intros o1 o2.
  destruct (MMultisetWMap.eq_dec _ _ _ _ _ (colors o1) (colors o2)).
  - destruct (observer_lght o1 =?= observer_lght o2).
    + left.
      split;auto.
    + right.
      intro abs.
      destruct abs.
      contradiction.
  - right.
    intro abs.
    destruct abs.
    contradiction.
Defined.

#[export] Instance colors_compat2: Proper (equiv ==> equiv) colors.
Proof using Type.
  intros ? ? ?.
  destruct H.
  assumption.
Qed.

#[export] Instance colors_compat3: Proper (equiv ==> equiv ==> eq)
                                     (fun x => (multiplicity (colors x))).
Proof using Type.
  repeat intro.
  destruct H.
  rewrite H.
  apply H0.
Qed.


#[export] Instance observer_lght_compat: Proper (equiv ==> equiv) observer_lght.
Proof using.
  repeat intro.
  now destruct H as [? ?].
Qed.

(* In order to recover [obs_from_config_ignore_snd], pt1 cannot be [get_location state]. *)
(* Yes it must be [get_location state] because the observation needs
   to provide the color of the observing robot AND also which of the
   two columns it occupies. By convention we chose that fst_lght is
   the observing column. *)
Definition obs_from_config2 config (state : location * L) :=
  let cols := make_multiset (config_list config) in
  {| colors := cols;
    observer_lght := get_light state |}.

Local Instance obs_from_config2_compat : Proper (equiv ==> equiv ==> equiv) obs_from_config2.
Proof using Type.
  intros config1 config2 Hconfig state1 state2 Hstate.
  unfold obs_from_config2.
(* assert (Hstate' := @get_location_compat _ _ _ _ _ Hstate). *)
  assert (Hconfig' := config_list_compat Hconfig).
  assert (Hfst := Hconfig').
  apply (hd_eqlistA_compat _ _ Hstate), fst_compat_pactole in Hfst.
  red.
  (* apply (hd_eqlistA_compat _ _ Hstate), fst_compat_pactole in Hfst. *)
  (* red. *)
  unfold obsLight_Setoid.
  cbn -[fst] in Hstate.
  destruct Hstate as [h1 h2].
  split; cbn -[equiv config_list];auto.
  apply make_multiset_compat.
  now apply eqlistA_PermutationA.
Qed.

Lemma obs_from_config2_map : forall f:location -> location,
  Proper (equiv ==> equiv) f -> Preliminary.injective equiv equiv f ->
  forall config st obs obsf ,
    obs == obs_from_config2 config st -> 
    obsf == obs_from_config2 (map_config (fun x => (f (fst x), snd x)) config) (f (fst st), snd st) ->
    observer_lght obsf == observer_lght obs /\
      colors obsf == map (fun x => (f (fst x), snd x)) (colors obs).
Proof using Type.
  intros f Hf Hf_inj config st obs obsf.
  change (fun x => (f (fst x), snd x)) with (lift (existT _ f I)).
  change (f (fst st), snd st) with (lift (existT _ f I) st).
  assert (Proper (equiv (A := location * L) ==> equiv (A := location * L))
            (lift (existT precondition f I))) as h_proper.
  { intros ? ? ?.
    now apply (@lift_compat _ _ St). }
  unfold obs_from_config2. rewrite config_list_map; trivial; [].
  cbn -[equiv].
  intros h_eq_obs h_eq_obsf. 
  split; cbn -[equiv].
  - now rewrite h_eq_obs, h_eq_obsf.
  - rewrite h_eq_obsf, h_eq_obs.
    cbn.
    intros x. 
    now rewrite make_multiset_map.
Qed.

Definition Obs2 : Observation.
Proof.
refine {|
    observation := obsLight;
    obs_from_config := obs_from_config2;
    obs_from_config_compat := obs_from_config2_compat;
    obs_is_ok :=
      fun obs config state =>
        observer_lght obs = (get_light state)
        /\ forall x, (colors obs)[x] = countA_occ equiv equiv_dec x (config_list config);
    obs_from_config_spec := fun _ _ => _
  |}.
Proof.
  split.
  - reflexivity.
  - intros x. 
    apply make_multiset_spec.
Defined.

Local Instance Obs : Observation := pair_observation multiset_observation Obs2.
Local Declare Scope pactole_scope.
Local Notation " '!!' config" := (@obs_from_config _ _ St _ multiset_observation config (origin,witness)) (at level 10):pactole_scope.
Local Notation " '!!!' '(' config ',' st ')'" := (@obs_from_config _ _ St _ Obs config st ) (at level 10):pactole_scope.
(* Notation "!! config" := (@obs_from_config _ _ _ _ Obs config (origin,witness) ) (at level 10). *)
Local Open Scope pactole_scope.

Global Instance obs_from_config_compat : Proper (equiv ==> equiv ==> equiv)
  (@obs_from_config _ _ _ _ Obs) := @obs_from_config_compat _ _ _ _ Obs.

Lemma obs_from_config_fst_ok: forall (st st':(location * L)) (c:configuration),
    (obs_from_config (Observation:=multiset_observation) c st)
    = (fst (obs_from_config (Observation:=@Obs) c st')).
Proof using Type.
  intros st c. 
  reflexivity.
Qed.

Lemma cardinal_fst_obs_from_config : forall config state,
  cardinal (fst (obs_from_config (Observation:=Obs) config state)) = nG + nB.
Proof using Type. intros. cbn -[make_multiset cardinal nB nG location]. apply cardinal_obs_from_config. Qed.

Lemma obs_from_config_ignore_snd_except_observerlight :
  forall (ref_st : location * L) config st,
  let o := obs_from_config config ref_st in
  let o' := obs_from_config config st in
  obs_from_config (Observation := Obs) config st == (fst o, snd o').
  (* The snd part depends on the color of st/ref_st *)
  (* (fst o, Build_obsLight (fst_lght (snd o)) (snd_lght (snd o)) (snd st)). *)
Proof using Type. reflexivity. Qed.

Lemma obs_from_config_fst :forall (ref_st : location * L) config obs1 obs2,
      obs_from_config config ref_st == (obs1, obs2) -> 
      forall st, obs1 == (obs_from_config (Observation:=multiset_observation) config st).
Proof using Lght VS.
  intros ref_st config obs1 obs2 h_eq_obs st.
  specialize (obs_from_config_ignore_snd_except_observerlight st config ref_st) as h.
  rewrite h in h_eq_obs.
  assert (fst (obs_from_config (Observation:=Obs) config st) == obs1) as h_fst.
  {  apply h_eq_obs. }
  rewrite <- h_fst.
  rewrite (obs_from_config_fst_ok _ (origin,witness)).
  reflexivity.
Qed.

Lemma observer_light_get_light: forall config id,
    observer_lght (snd (obs_from_config config (config id))) = get_light (config id).
Proof using Type. reflexivity. Qed.


Lemma obs_from_ok: forall config st,
    (obs_from_config config st) == 
      (obs_from_config (Observation:=multiset_observation) config st,
        snd (obs_from_config config st)).
Proof using Type.
  intros config st. 
  specialize (obs_from_config_ignore_snd_except_observerlight st config st) as h.
  lazy zeta in h.
  setoid_rewrite h.
  reflexivity.
Qed.

Lemma obs_from_ok2: forall config st,
    (!!!(config,st)) == (obs_from_config config (origin,witness), snd (!!!(config,st))).
Proof using Type.
  intros config st. 
  specialize (@obs_from_config_ignore_snd_except_observerlight st config st) as h.
  cbv zeta in h.
  setoid_rewrite h.
  reflexivity.
Qed.

Definition map_light f (obs : observation (Observation := Obs2)) : observation (Observation := Obs2) :=
  {| observer_lght := observer_lght obs;
     colors := map (fun x => (f (fst x), snd x)) (colors obs) |}.

Instance map_light_compat : forall f, Proper (equiv ==> equiv) f -> Proper (equiv ==> equiv) (map_light f).
Proof using Type.
intros f Hf obs1 obs2 Hobs. unfold map_light.
split; cbn -[equiv]; try apply Hobs; [].
apply map_compat.
+ intros ? ? Heq. now split; cbn; rewrite Heq.
+ now f_equiv.
Qed.

Lemma map_light_extensionality_compat : forall f g, Proper (equiv ==> equiv) f ->
  (forall x, g x == f x) -> forall m , map_light g m == map_light f m.
Proof using Type.
intros f g Hf Hfg m. unfold map_light.
split; cbn -[equiv]; try reflexivity; [].
apply map_extensionality_compat.
+ intros ? ? Heq. now rewrite Heq.
+ intro x. now split; try apply Hfg.
Qed.

Lemma map_light_merge : forall f g, Proper (equiv ==> equiv) f -> Proper (equiv ==> equiv) g ->
  forall obs, map_light f (map_light g obs) == map_light (fun x => f (g x)) obs.
Proof using Type.
intros f g Hf Hg obs. split; try reflexivity; [].
unfold map_light. cbn -[equiv]. apply map_merge.
+ intros ? ? Heq. split; try apply Hf; apply Heq.
+ intros ? ? Heq. split; try apply Hg; apply Heq.
Qed.

Lemma map_light_id : forall obs, map_light id obs == obs.
Proof using Type.
intro obs. unfold map_light. split; try reflexivity; []. cbn -[equiv].
transitivity (map Datatypes.id (colors obs)); try apply map_id; [].
apply map_extensionality_compat.
+ now repeat intro.
+ now intros [].
Qed.

Lemma map_light_colors : forall f,
  Proper (equiv ==> equiv) f -> Preliminary.injective equiv equiv f ->
  forall obs pt c, (colors (map_light f obs))[(f pt, c)] = (colors obs)[(pt, c)].
Proof using Type.
intros f Hf Hinj [] pt c. cbn. change (f pt, c) with ((fun x => (f (fst x), snd x)) (pt, c)).
apply map_injective_spec.
+ intros x y Hxy. now rewrite Hxy.
+ intros x y [Heq1 Heq2]. apply Hinj in Heq1. now split.
Qed.

Definition map_obs f (obs : observation) : observation :=
  (map (Bijection.section f) (fst obs), map_light f (snd obs)).

Lemma map_obs_compat : forall f : Bijection.bijection location,
  Proper (equiv ==> equiv) (map_obs f).
Proof using Type. intros f x y Hxy. unfold map_obs. now rewrite Hxy. Qed.

Lemma map_obs_merge : forall (f g : Bijection.bijection location),
  Proper (equiv ==> equiv) f -> Proper (equiv ==> equiv) g ->
  forall obs, map_obs f (map_obs g obs) == map_obs (f ∘ g) obs.
Proof using Type.
intros f g Hf Hg obs. unfold map_obs. split; cbn -[equiv].
+ now apply map_merge.
+ now apply map_light_merge.
Qed.

Lemma map_obs_id : forall obs, map_obs id obs == obs.
Proof using Type.
intro obs. unfold map_obs. split; cbn -[equiv].
+ apply map_id.
+ apply map_light_id.
Qed.

Lemma obs_from_config_map : forall f : Bijection.bijection location,
  Proper (equiv ==> equiv) f ->
  Preliminary.injective equiv equiv f ->
  forall Pf (config:configuration) st,
    (obs_from_config (map_config (lift (existT precondition f Pf)) config)
           (lift (existT precondition f Pf) st))
    == map_obs f (obs_from_config config st).
Proof using Type.
intros f Hf Hf_inj Pf config st. split.
* assert (Hmap := obs_from_config_map Hf I config).
  do 2 (unfold obs_from_config in *; cbn -[equiv] in *).
  now rewrite Hmap.
* do 2 (unfold obs_from_config; cbn -[equiv]).
  split.
  - specialize (@obs_from_config2_map f) as h.
    specialize h with (config:=config) (st:=st).
    edestruct h;auto.
  - reflexivity.
Qed.


(** A configuration with two towers containing the same numbers of robots. *)
Definition bivalent (config : configuration) :=
  let n := nG + nB in
  Nat.Even n /\ n >=2 /\ exists pt1 pt2 : location, pt1 =/= pt2
  /\ (!! config)[pt1] = Nat.div2 n /\ (!! config)[pt2] = Nat.div2 n.

Global Instance bivalent_compat : Proper (equiv ==> iff) bivalent.
Proof using .
intros ? ? Heq. split; intros [HnG [Hle [pt1 [pt2 [Hneq [Hpt1 Hpt2]]]]]];
repeat split; trivial; exists pt1, pt2; split; trivial; [|].
* assert (Heq' : (!! x) == (!! y)).
  { apply obs_from_config_compat in Heq.
    specialize (Heq _ _ (reflexivity (origin, witness))).
    now apply fst_compat_pactole in Heq. }
  split; (rewrite <- Hpt1 + rewrite <- Hpt2); now apply multiplicity_compat.
* assert (Heq' : (!! x) == (!! y)).
  { apply obs_from_config_compat in Heq.
    specialize (Heq _ _ (reflexivity (origin, witness))).
    now apply fst_compat_pactole in Heq. }
  split; (rewrite <- Hpt1 + rewrite <- Hpt2); now apply multiplicity_compat.
Qed.

(* A bivalent configuration with the same number of robots of each color in both towers.
 Is it reasonable to use the observation here (from origin)? *)
Definition color_bivalent (config : configuration) :=
  let n := nG + nB in
  Nat.Even n /\
    n >=2 /\
    exists pt1 pt2: location,
      pt1 =/= pt2 /\
        (!! config)[pt1] = Nat.div2 n /\
        (!! config)[pt2] = Nat.div2 n /\
        (List.Forall
           (fun col:L =>
              (colors (snd (!!! (config, (0%VS,witness)))))[(pt1,col)]
              = (colors (snd (!!! (config, (0%VS,witness)))))[(pt2,col)])
           l_list).

Global Instance color_bivalent_compat : Proper (equiv ==> iff) color_bivalent.
Proof using .
intros ? ? Heq.
split;intros [HnG [Hle [pt1 [pt2 [Hneq [Hpt1 [Hpt2 Hcolors]]]]]]];
  repeat split; trivial; exists pt1, pt2; split; trivial; [|].
- assert (Heq' : (!! x) == (!! y)).
  { apply obs_from_config_compat in Heq.
    specialize (Heq _ _ (reflexivity (origin, witness))).
    now apply fst_compat_pactole in Heq. }
  repeat split.
  + rewrite <- Hpt1.
    try now apply multiplicity_compat.
  + rewrite <- Hpt2.
    try now apply multiplicity_compat.
  + specialize Forall_Permutation_compat as h.
    unfold Proper, respectful in h.
    specialize (h _
                  (λ col : L,
                      (colors (snd (!!! (y, (0%VS, witness)))))[(pt1, col)] =
                        (colors (snd (!!! (y, (0%VS, witness)))))[(pt2, col)])
                  (λ col : L,
                      (colors (snd (!!! (x, (0%VS, witness)))))[(pt1, col)] =
                        (colors (snd (!!! (x, (0%VS, witness)))))[(pt2, col)])).
    rewrite h.
    apply Hcolors.
    intros x0 y0 H.
    split;intros.
    * rewrite <- H.
      rewrite Heq.
      assumption.
    * rewrite H.
      rewrite <- Heq.
      assumption.
    * reflexivity.
- assert (Heq' : (!! x) == (!! y)).
  { apply obs_from_config_compat in Heq.
    specialize (Heq _ _ (reflexivity (origin, witness))).
    now apply fst_compat_pactole in Heq. }
  repeat split.
  + rewrite <- Hpt1.
    try now apply multiplicity_compat.
  + rewrite <- Hpt2.
    try now apply multiplicity_compat.
  + specialize Forall_Permutation_compat as h.
    unfold Proper, respectful in h.
    specialize (h _
                  (λ col : L,
                      (colors (snd (!!! (y, (0%VS, witness)))))[(pt1, col)] =
                        (colors (snd (!!! (y, (0%VS, witness)))))[(pt2, col)])
                  (λ col : L,
                      (colors (snd (!!! (x, (0%VS, witness)))))[(pt1, col)] =
                        (colors (snd (!!! (x, (0%VS, witness)))))[(pt2, col)])).
    rewrite <- h.
    apply Hcolors.
    intros x0 y0 H.
    split;intros.
    * rewrite <- H.
      rewrite Heq.
      assumption.
    * rewrite H.
      rewrite <- Heq.
      assumption.
    * reflexivity.
Qed.

(* Alternative definitions with explicit locations *)
Definition bivalent_on config pt1 pt2 :=
  (forall id, get_location (config id) == pt1 \/ get_location (config id) == pt2)
  /\ length (occupied config) = 2
  /\ length (on_loc pt1 config) = length (on_loc pt2 config).

Typeclasses eauto := (dfs) 5.

Global Instance bivalent_on_compat : Proper (equiv ==> equiv ==> equiv ==> iff) bivalent_on.
Proof using Type.
intros config1 config2 Hconfig pt1 pt1' Hpt1 pt2 pt2' Hpt2.
unfold bivalent_on.
setoid_rewrite Hconfig. setoid_rewrite Hpt1. setoid_rewrite Hpt2.
reflexivity.
Qed.

Lemma bivalent_on_bivalent : forall config pt1 pt2,
  bivalent_on config pt1 pt2 -> bivalent config.
Proof using Type.
intros config pt1 pt2 [Hloc [Hoccupied Hsame]].
rewrite <- 2 (obs_from_config_on_loc _ (origin, witness)) in Hsame.
assert (Hother : forall pt, pt =/= pt1 -> pt =/= pt2 -> (!! config)[pt] = 0).
{ intros pt Hpt1 Hpt2. rewrite <- not_In. intro Habs. rewrite obs_from_config_In in Habs.
  destruct Habs as [id Hid]. destruct (Hloc id).
  - apply Hpt1. now transitivity (get_location (config id)).
  - apply Hpt2. now transitivity (get_location (config id)). }
assert (Hperm : PermutationA equiv (support (!! config)) (cons pt1 (cons pt2 nil))).
{ apply NoDupA_inclA_length_PermutationA.
  + autoclass.
  + apply support_NoDupA.
  + intros pt Hin. rewrite support_spec, obs_from_config_In in Hin.
    destruct Hin as [id Hid]. rewrite InA_cons, InA_singleton.
    destruct (Hloc id); now (left + right); transitivity (get_location (config id)); auto.
  + now rewrite support_occupied, Hoccupied. }
assert (Hn : (!! config)[pt1] + (!! config)[pt2] = nG + nB).
{ erewrite <- cardinal_obs_from_config, (cardinal_fold_support (!! config)).
  rewrite (@fold_left_symmetry_PermutationA _ _ equiv equiv); autoclass.
  + cbn. lia.
  + intros ? ? Heq1 ? ? Heq2. now rewrite Heq1, Heq2.
  + intros. hnf. lia. }
assert (Heven : Nat.Even (nG + nB)).
{ rewrite <- Hn, Hsame (*, <- Even.even_equiv*).
  replace ((!! config)[pt2] + (!! config)[pt2]) with (2 * (!! config)[pt2]) by lia.
 now exists (!! config)[pt2]. }
repeat split.
* assumption.
* rewrite <- Hn, <- Hsame. cut ((!! config)[pt1] > 0); try lia; [].
  change (In pt1 (!! config)). rewrite <- support_spec, Hperm. now left.
* exists pt1, pt2. repeat split.
  + assert (Hnodup := support_NoDupA (!! config)).
    now rewrite Hperm, NoDupA_2 in Hnodup.
  + apply Nat.Even_double in Heven.
    rewrite <- Hsame in Hn. unfold Nat.double in *. rewrite <- Hn in Heven at 1. lia.
  + apply Nat.Even_double in Heven.
    rewrite Hsame in Hn. unfold Nat.double in *. rewrite <- Hn in Heven at 1. lia.
Qed.

Definition color_bivalent_on config pt1 pt2 :=
  bivalent_on config pt1 pt2
 /\ List.Forall
      (fun col:L => (colors (snd (!!! (config, (0%VS,witness)))))[(pt1,col)]
                  = (colors (snd (!!! (config, (0%VS,witness)))))[(pt2,col)])
      l_list.

Global Instance color_bivalent_on_compat : Proper (equiv ==> equiv ==> equiv ==> iff) color_bivalent_on.
Proof using Type.
intros ? ? Heq1 ? ? Heq2 ? ? Heq3.
unfold color_bivalent_on. rewrite 2 Forall_forall.
setoid_rewrite Heq1. setoid_rewrite Heq2. setoid_rewrite Heq3.
reflexivity.
Qed.

(** **  Generic properties  **)

(* We need to unfold [obs_is_ok] for rewriting *)
Lemma obs_from_config_fst_spec : forall (config : configuration) st (pt : location),
  (fst (!!! (config,st)))[pt] = countA_occ _ equiv_dec pt (List.map fst (config_list config)).
Proof using Type. intros. now destruct (obs_from_config_spec config (pt, witness)) as [Hok _]. Qed.

Lemma obs_non_nil : 2 <= nG+nB -> forall config st,
  fst (!!! (config,st)) =/= MMultisetInterface.empty.
Proof using .
simpl obs_from_config. intros HnG config st Heq.
assert (Hlgth:= config_list_length config).
assert (Hl : config_list config = nil).
{ apply (map_eq_nil fst). rewrite <- make_multiset_empty. apply Heq. }
rewrite Hl in Hlgth.
cbn in *. lia.
Qed.

Local Instance obs_compat : forall pt,
  Proper (equiv ==> eq) (fun obs : location * L => if fst obs =?= pt then true else false).
Proof using Type.
intros pt [] [] []. cbn in *.
repeat destruct_match; auto; exfalso; apply H1 || apply H2; etransitivity; try eassumption; eauto.
Qed.

Definition bivalent_obs (obs : observation) : bool :=
  let obs_loc := fst obs in 
  let sup := support obs_loc in
  match sup with
    (cons pt1 (cons pt2 nil)) => obs_loc[pt1] =? obs_loc[pt2]
  | _ => false
  end.

Instance bivalent_obs_compat: Proper (equiv ==> eq) bivalent_obs.
Proof using Type.
intros [o1 o1'] [o2 o2'] [Hfst Hsnd]. unfold bivalent_obs. cbn [fst snd] in *.
assert (Hperm := support_compat Hfst).
assert (Hlen := PermutationA_length Hperm).
repeat destruct_match; auto; cbn in Hlen; try discriminate; [].
rewrite PermutationA_2 in Hperm; [| now autoclass].
destruct Hperm as [[Heq1 Heq2] | [Heq1 Heq2]].
+ now rewrite Hfst, Heq1, Heq2.
+ now rewrite Hfst, Heq1, Heq2, Nat.eqb_sym.
Qed.

Lemma sum2_le_total : forall config st pt1 pt2, pt1 =/= pt2 ->
  (fst (!!! (config,st)))[pt1] + (fst (!!! (config,st)))[pt2] <= nG + nB.
Proof using .
intros config st pt1 pt2 Hpt12.
rewrite <- (cardinal_fst_obs_from_config config st).
rewrite <- (add_remove_id pt1 (fst (!!! (config,st))) (reflexivity _)) at 3.
rewrite cardinal_add.
rewrite <- (add_remove_id pt2 (fst (!!! (config,st))) (reflexivity _)) at 5.
rewrite remove_add_comm, cardinal_add; trivial.
lia.
Qed.

Lemma sum3_le_total : forall config st pt1 pt2 pt3, pt1 =/= pt2 -> pt2 =/= pt3 -> pt1 =/= pt3 ->
  (fst (!!! (config,st)))[pt1] + (fst (!!! (config,st)))[pt2] + (fst (!!! (config,st)))[pt3] <= nG + nB.
Proof using .
intros config st pt1 pt2 pt3 Hpt12 Hpt23 Hpt13.
rewrite <- (cardinal_fst_obs_from_config config st).
rewrite <- (add_remove_id pt1 (fst (!!! (config,st))) (reflexivity _)) at 4.
rewrite cardinal_add.
rewrite <- (add_remove_id pt2 (fst (!!! (config,st))) (reflexivity _)) at 6.
rewrite remove_add_comm, cardinal_add; trivial.
rewrite <- (add_remove_id pt3 (fst (!!! (config,st))) (reflexivity _)) at 8.
rewrite remove_add_comm, remove_add_comm, cardinal_add; trivial; [].
lia.
Qed.

Lemma bivalent_obs_spec : forall config st,
  bivalent_obs (obs_from_config config st) = true <-> bivalent config.
Proof using .
intros config st. unfold bivalent_obs, bivalent.
assert (Hequiv := obs_from_config_ignore_snd_except_observerlight (origin, witness) config st).
destruct (obs_from_config config st) as [obs ?] eqn:Hobs. cbn [fst].
destruct Hequiv as [Hequiv _]. cbn -[equiv] in Hequiv. setoid_rewrite <- Hequiv.
destruct (support obs) as [| e1 [| e2 [| e3 ?]]] eqn:Hsupport.
* split; try discriminate; [].
  intros [Heven [Hn [pt1 [pt2 [Hdiff [Hpt1 Hpt2]]]]]].
  assert (Hin : InA equiv pt1 (support obs)).
  { rewrite support_spec. unfold In. rewrite Hpt1. apply Exp_prop.div2_not_R0. lia. }
  rewrite Hsupport, InA_nil in Hin. tauto.
* split; intro Hcase; try discriminate; [].
  destruct Hcase as [Heven [Hle [pt1 [pt2 [Hdiff [Hpt1 Hpt2]]]]]].
  elim Hdiff. transitivity e1.
  + assert (Hin : InA equiv pt1 (support obs)).
    { rewrite support_spec. unfold In. rewrite Hpt1. apply Exp_prop.div2_not_R0. lia. }
    now rewrite Hsupport, InA_singleton in Hin.
  + assert (Hin : InA equiv pt2 (support obs)).
    { rewrite support_spec. unfold In. rewrite Hpt2. apply Exp_prop.div2_not_R0. lia. }
    now rewrite Hsupport, InA_singleton in Hin.
* (* real case *)
  rewrite Nat.eqb_eq.
  assert (Hcard := cardinal_fst_obs_from_config config st).
  rewrite cardinal_fold_support, Hobs in Hcard. simpl fst in Hcard. rewrite Hsupport in Hcard.
  cbn in Hcard. rewrite Nat.add_0_r in Hcard.
  split; intro Hcase.
  + repeat split.
    - rewrite Hcase in Hcard. exists obs[e2]. lia.
    - cut (obs[e1] > 0); [lia |].
      change (In e1 obs). rewrite <- support_spec, Hsupport. now left.
    - exists e1, e2. rewrite Hcase in *.
      assert (Hnodup := support_NoDupA obs). rewrite Hsupport in Hnodup.
      inv Hnodup. rewrite InA_cons, InA_nil in *. cut (obs[e2] = Nat.div2 (nG+nB)); intuition; [].
      rewrite <- Hcard.
      clear. induction obs[e2]; trivial; [].
      rewrite Nat.add_succ_r. cbn. lia.
  + destruct Hcase as [Heven [Hle [pt1 [pt2 [Hdiff [Hpt1 Hpt2]]]]]].
    assert (Hperm : PermutationA equiv (pt1 :: pt2 :: nil) (e1 :: e2 :: nil)).
    { assert (Hin1 : In pt1 obs). { unfold In. rewrite Hpt1. apply Exp_prop.div2_not_R0. lia. }
      assert (Hin2 : In pt2 obs). { unfold In. rewrite Hpt2. apply Exp_prop.div2_not_R0. lia. }
      rewrite <- Hsupport.
      apply NoDupA_inclA_length_PermutationA; autoclass.
      + repeat constructor.
        - rewrite InA_cons, InA_nil. intuition.
        - now rewrite InA_nil.
      + intros x Hx. rewrite 2 InA_cons, InA_nil in Hx. rewrite support_spec.
        destruct Hx as [Hx | [Hx | Hx]]; tauto || now rewrite Hx.
      + now rewrite Hsupport. }
    apply PermutationA_2 in Hperm; autoclass; [].
    destruct Hperm as [[Heq1 Heq2] | [Heq1 Heq2]]; rewrite <- Heq1, <- Heq2; congruence.
* (* Three inhabited locations, thus too many robots
     as the two towers should already contain all of them *)
  split; intro Hcase; try discriminate; []. exfalso.
  destruct Hcase as [Heven [Hle [pt1 [pt2 [Hdiff [Hpt1 Hpt2]]]]]].
  assert (Hin1 : InA equiv pt1 (support obs)).
  { rewrite support_spec. unfold In. rewrite Hpt1. apply Exp_prop.div2_not_R0. lia. }
  assert (Hin2 : InA equiv pt2 (support obs)).
  { rewrite support_spec. unfold In. rewrite Hpt2. apply Exp_prop.div2_not_R0. lia. }
  rewrite Hsupport in Hin1, Hin2.
  destruct (PermutationA_3_swap _ Hdiff Hin1 Hin2) as [pt [supp Hsupp]].
  rewrite <- Hsupport in *.
  assert (Hneq : pt1 =/= pt /\ pt2 =/= pt).
  { assert (Hnodup := support_NoDupA obs). rewrite Hsupp in Hnodup.
    inv Hnodup. inv H2. repeat rewrite InA_cons in *. intuition. }
  destruct Hneq as [Hneq1 Hneq2].
  assert (Hcard := sum3_le_total config (0%VS, witness) Hdiff Hneq2 Hneq1).
  rewrite <- Hequiv, Hpt1, Hpt2 in Hcard.
  assert (obs[pt] > 0).
  { change (In pt obs). rewrite <- support_spec, Hsupp, 3 InA_cons. clear. intuition. }
  apply Nat.Even_double in Heven.
  unfold Nat.double in *. lia.
Qed.

Lemma bivalent_obs_spec_st : forall config,
  (forall st, bivalent_obs (obs_from_config config st) = true) <-> bivalent config.
Proof using .
intros config.
split.
+ intro Hall.
  specialize (Hall (origin, witness)).
  now rewrite bivalent_obs_spec in Hall.
+ intros Hbivalent st.
  now rewrite bivalent_obs_spec.
Qed.

Lemma bivalent_obs_size : forall obs,
  bivalent_obs obs = true ->
  length (support (elt := location) (fst obs)) = 2.
Proof using Type.
intros [obs_loc ?]. unfold bivalent_obs. cbn [fst].
repeat destruct_match; discriminate || reflexivity.
Qed.

Corollary bivalent_size : forall config st,
  bivalent config ->
  length (support (elt := location) (fst (obs_from_config config st))) = 2.
Proof using Type. intros config st. rewrite <- bivalent_obs_spec. apply bivalent_obs_size. Qed.

Lemma bivalent_support : forall config, bivalent config ->
  forall st (pt1 pt2 : location),
    pt1 =/= pt2 ->
    In pt1 (fst (Obs.(obs_from_config) config st)) ->
    In pt2 (fst (obs_from_config config st)) ->
    PermutationA equiv (support (fst (Obs.(obs_from_config) config st))) (cons pt1 (cons pt2 nil)).
Proof using Type.
intros config Hbivalent st pt1 pt2 Hdiff Hpt1 Hpt2.
symmetry.
apply NoDupA_inclA_length_PermutationA; autoclass.
+ repeat constructor.
  - now rewrite InA_singleton.
  - now rewrite InA_nil.
+ intros x Hin. rewrite InA_cons, InA_singleton in Hin.
  rewrite support_spec.
  destruct Hin as [Hin | Hin]; rewrite Hin; assumption.
+ eapply bivalent_size with (st := st) in Hbivalent.
  now rewrite Hbivalent.
Qed.

Definition color_bivalent_obs (obs : observation) : bool :=
  let obs_loc := fst obs in
  let sup := support obs_loc in
  match sup with
  | nil => false
  | pt1 :: nil => false
  | pt1 :: pt2 :: nil =>
      (obs_loc[pt1] =? obs_loc[pt2])
      && (List.forallb
           (fun col:L => (colors (snd obs))[(pt1,col)] =? (colors (snd obs))[(pt2,col)])
           l_list)
  | pt1 :: pt2 :: _ :: _ => false
  end.

(* TODO: move to the right file *)
Lemma PermutationA_2_gen :
  forall [A : Type] [eqA : relation A],
    Equivalence eqA ->
    forall (l:list A) (x' y' : A),
      PermutationA eqA l (x' :: y' :: nil) ->
      exists x y, (eqA x x' /\ eqA y y' \/ eqA x y' /\ eqA y x')
                  /\ l = (x :: y :: nil).
Proof using Type.
  intros A eqA HequiveqA l x' y' h_permut. 
  specialize (@PermutationA_length _ _ _ _ h_permut) as h_length.
  destruct l as [ | e1 [ | e2 [ | e3 l3]]]; cbn in h_length; try discriminate.
  exists e1, e2.
  split;auto.
  rewrite  (PermutationA_2 HequiveqA e1 e2 x' y') in h_permut.
  destruct h_permut as [[h1 h2] | [h1 h2]];auto.
Qed.

(* TODO: improve proofs *)
Local Lemma bivalent_obs_map: forall o whatever (f : Bijection.bijection _),
    bivalent_obs o = true -> bivalent_obs ((map f (fst o)),whatever) = true.
Proof using Type.
  intros o whatever f H0.
  assert (Hbivalent := H0).

  unfold bivalent_obs in H0 |- *.
  destruct o as [o1 o2].
  cbn [fst] in *.
  assert (PermutationA equiv (List.map f (support o1)) (support (map f o1))) as h_permut.
  { rewrite map_injective_support;autoclass.
    - reflexivity.
    - apply Bijection.injective. }
  assert (length (List.map f (support o1)) = length (support (map f o1))) as h_length.
  { now rewrite h_permut. }

  destruct (support o1) as [ |e1 l1] eqn:heq_supo;try discriminate.
  destruct l1 as [| e2 l2] eqn:heq_supo2;try discriminate.
  destruct l2 as [| e3 l3] eqn:heq_supo3;try discriminate.
  cbn [List.map] in h_length.
  symmetry in h_permut.
  destruct (PermutationA_2_gen _ h_permut) as [ a [b [[[heq_a heq_b] | [heq_a heq_b]] h_map]]].
  - rewrite h_map.
    rewrite Nat.eqb_eq in H0|-*.
    rewrite heq_a,heq_b.
    setoid_rewrite map_injective_spec;auto;autoclass.
    + apply Bijection.injective.
    + apply Bijection.injective.
  - rewrite h_map.
    rewrite Nat.eqb_eq in H0|-*.
    rewrite heq_a,heq_b.
    setoid_rewrite map_injective_spec; autoclass; apply Bijection.injective.
Qed.

Local Lemma bivalent_obs_map_inv: forall o whatever (f : Bijection.bijection _),
    bivalent_obs ((map f (fst o)),whatever) = true -> bivalent_obs o = true.
Proof using Type.
  intros o whatever f h_bivopsmap.
  assert (Proper (equiv ==> equiv) (λ x0 : location, Bijection.retraction f (f x0))).
  { repeat intro.
    now rewrite H. }
  assert (Proper (equiv ==> equiv) f) as Hproper.
  { apply Bijection.section_compat. }
  assert (Preliminary.injective equiv equiv f) as h_proper_f.
  { apply Bijection.injective. }
  assert (  Preliminary.injective equiv equiv (λ x0 : location, Bijection.retraction f (f x0))) as h_proper_finv.
  { repeat intro.
    setoid_rewrite Bijection.retraction_section in H0.
    assumption. }
  
  unfold bivalent_obs in h_bivopsmap|-*.
  destruct o as [o1 o2].
  cbn [fst] in *.
  assert (PermutationA equiv (List.map f (support o1)) (support (map f o1))) as h_permut.
  { rewrite map_injective_support;auto;autoclass.
    reflexivity. }
  assert (length (List.map f (support o1)) = length (support (map f o1))) as h_length.
  { now rewrite h_permut. }
  symmetry in h_permut.

  destruct (support o1) as [ |e1 l1] eqn:heq_supo;
    destruct (support (map f o1));try now (cbn in * ; discriminate).
  destruct l1; destruct l0;try now (cbn in * ; discriminate).
  destruct l2; destruct l3;try now (cbn in * ; discriminate).
  assert (o1 == (map (Bijection.retraction f) (map f o1))).
  { 
    rewrite map_merge;auto.
    2: apply Bijection.retraction_compat;auto.
    repeat intro.
    assert (x == (λ x0 : location, Bijection.retraction f (f x0)) x).
    { symmetry. apply Bijection.retraction_section. }
    rewrite H0 at 2.
    change (Bijection.retraction f (f x)) with ((λ x0 : location, Bijection.retraction f (f x0)) x).
    rewrite map_injective_spec;auto. }
  apply  Nat.eqb_eq in h_bivopsmap.
  apply Nat.eqb_eq.
  destruct (PermutationA_2_gen _ h_permut) as [ a [b [[[heq_a heq_b] | [heq_a heq_b]] h_map]]].
  - inversion h_map.
    rewrite H2,H3,heq_a, heq_b in h_bivopsmap.
    setoid_rewrite map_injective_spec in h_bivopsmap;auto.
  - inversion h_map.
    rewrite H2,H3,heq_a, heq_b in h_bivopsmap.
    setoid_rewrite map_injective_spec in h_bivopsmap;auto.
Qed.

Corollary bivalent_obs_morph_strong : forall obs (f : Bijection.bijection _) whatever,
  bivalent_obs ((map f (fst obs)), whatever) = bivalent_obs obs.
Proof using Type.
intros obs f whatever.
destruct (bivalent_obs obs) eqn:Hcase.
+ now apply bivalent_obs_map.
+ rewrite <- Bool.not_true_iff_false in *.
  intro Habs. eapply Hcase, bivalent_obs_map_inv, Habs.
Qed.

Corollary bivalent_obs_morph : forall f obs,
  bivalent_obs (map_obs f obs) = bivalent_obs obs.
Proof using Type. intros. apply bivalent_obs_morph_strong. Qed.

Lemma permut_forallb_ext:
  (forall A (l: list A) (f g: A -> bool),
      (forall a: A, f a = g a) -> 
      forallb f l = forallb g l).
Proof using Type.
  intros A l f g H.
  induction l;auto.
  cbn.
  now rewrite H,IHl.
Qed.

Lemma permut_forallb:
  (forall A (l1 l2: list A) (f g: A -> bool),
      (forall a: A, f a = g a) -> 
      PermutationA eq l1 l2 -> 
      forallb f l1 = forallb g l2).
Proof using Type.
  intros A l3 l4 f g Hext Hpermut. 
  induction Hpermut.
  + reflexivity.
  + rewrite H.
    cbn.
    rewrite IHHpermut.
    rewrite Hext.
    reflexivity.
  + cbn.
    repeat rewrite Hext.
    repeat rewrite Bool.andb_assoc.
    rewrite (Bool.andb_comm (g y) (g x)).
    rewrite permut_forallb_ext with (g:=g);auto.
  + transitivity (forallb g l₂);auto.
    rewrite permut_forallb_ext with (g:=f);auto.
Qed.

Instance color_bivalent_obs_compat: Proper (equiv ==> eq) color_bivalent_obs.
Proof using Type.
  intros [o1 o1'] [o2 o2'] [Hfst Hsnd].
  unfold color_bivalent_obs.
  cbn [fst snd] in *.
  assert (Hperm := support_compat Hfst).
  assert (Hlen := PermutationA_length Hperm).
  repeat destruct_match; auto; cbn in Hlen; try discriminate; [].
  rewrite PermutationA_2 in Hperm; [| now autoclass].
  assert (Heq : (o1[l] =? o1[l1]) = (o2[l3] =? o2[l5])).
  { destruct Hperm as [[Heq1 Heq2] | [Heq1 Heq2]].
    - now rewrite Hfst, Heq1, Heq2.
    - now rewrite Hfst, Heq1, Heq2, Nat.eqb_sym. }
  rewrite Heq.
  f_equal.
  cbn.
  apply permut_forallb.
  - intros a. 
    destruct Hperm as [[Heq1 Heq2] | [Heq1 Heq2]];auto.
    + f_equal.
      * rewrite (colors_compat2 Hsnd).
        apply MMultisetWMap.pre_multiplicity_compat;auto; try typeclasses eauto.
        reflexivity.
      * rewrite (colors_compat2 Hsnd).
        apply MMultisetWMap.pre_multiplicity_compat;auto; try typeclasses eauto.
        reflexivity.
    + rewrite Nat.eqb_sym at 1.
      f_equal.
      * rewrite (colors_compat2 Hsnd).
        apply MMultisetWMap.pre_multiplicity_compat;auto; try typeclasses eauto.
        reflexivity.
      * rewrite (colors_compat2 Hsnd).
        apply MMultisetWMap.pre_multiplicity_compat;auto; try typeclasses eauto.
        reflexivity.
  - reflexivity.
Qed.

Lemma color_bivalent_obs_bivalent_obs:
  forall o, color_bivalent_obs o = true -> bivalent_obs o = true.
Proof using Type.
  intros o h.
  unfold color_bivalent_obs, bivalent_obs in *.
  destruct (support (fst o)) as [ | e1 [ | e2 [ | e3 l3]] ];try now (contradict h;auto).
  apply andb_prop in h.
  destruct h as [h1 h2].
  assumption.
Qed.


(*
(* In a bivalent configuration, the number of robots on the towers at pt1 and pt2 are the same. *)
Local Lemma color_bivalent_same_cardinal :
  forall config, forall pt1 pt2 : location,
    pt1 =/= pt2 ->
    (forall pt l, pt =/= pt1 -> pt=/= pt2 -> (!! config)[(pt, l)] = 0) ->
    (forall l : L, (!!config) [(pt1, l)] = (!!config) [(pt2, l)]) ->
    cardinal (filter (fun obs => if fst obs =?= pt1 then true else false) (!! config))
    = cardinal (filter (fun obs => if fst obs =?= pt2 then true else false) (!! config)).
Proof.
intros config pt1 pt2 Hdiff Hother Hsame.
rewrite 2 cardinal_filter_elements; try apply obs_compat; [].
assert (Hnodup := elements_NoDupA (!! config)).
assert (Hsame' : forall l n, InA eq_pair (pt1, l, n) (elements (!! config))
                         <-> InA eq_pair (pt2, l, n) (elements (!! config))).
{ intros. rewrite 2 elements_spec. cbn. now rewrite Hsame. }
clear Hsame Hother. rename Hsame' into Hsame. generalize 0 as i.
induction (elements (!! config)) as [l Hrec] using list_len_ind; intro i; trivial; [].
destruct l as [| [[pt li] n] l]; auto; []. cbn.
assert (Hcompat : forall pt, Proper (equiv ==> eq)
          (fun obs : location * L * nat => if fst (fst obs) =?= pt then true else false)).
{ intros x [[] ?] [[] ?] [[] ?]. cbn in *.
  do 2 destruct_match; auto; elim c; etransitivity; try eassumption; eauto. }
assert (Hfold : Proper (PermutationA eq_pair ==> eq ==> eq)
                  (fold_left (fun (acc : nat) (xn : location * L * nat) => snd xn + acc))).
{ apply fold_left_symmetry_PermutationA.
  + intros ? ? ? [] [] []. cbn in *. lia.
  + intros [] [] ?. lia. }
do 2 destruct_match.
* (* Absurd case: pt1 == pt2 *)
  elim Hdiff. etransitivity; try eassumption; eauto.
* (* Real case: pt == pt1 *)
  assert (Hin' : InA eq_pair (pt2, li, n) ((pt, li, n) :: l)).
  { rewrite <- Hsame. left. repeat split; cbn; auto. }
  inversion_clear Hin'; try (now cbn in *; elim c; symmetry); [].
  revert_one InA. intro Hin'. apply PermutationA_split in Hin'; autoclass; [].
  destruct Hin' as [l' Hperm]. cbn. 
  setoid_rewrite (Hfold _ _ (filter_PermutationA_compat (Hcompat pt1) Hperm) (n+i) (n+i) eq_refl).
  setoid_rewrite (Hfold _ _ (filter_PermutationA_compat (Hcompat pt2) Hperm) i i eq_refl).
  cbn. setoid_rewrite (equiv_dec_refl pt2). destruct_match; try (now elim Hdiff); [].
  cbn. apply Hrec.
  + rewrite Hperm. cbn. lia.
(*   + intros x Hx. apply Hin. rewrite Hperm. now do 2 right. *)
  + inv Hnodup. revert_all. intro Hgoal. assert (Hperm' := Hperm).
    apply (PermutationA_subrelation_compat subrelation_pair_elt eq_refl eq_refl) in Hperm'.
    rewrite Hperm' in Hgoal. now inv Hgoal.
  + intros. split; intro Hpt.
    - assert (Hin_l' : InA eq_pair (pt1, l0, n0) ((pt, li, n) :: l)).
      { rewrite Hperm. now do 2 right. }
      rewrite Hsame in Hin_l'. rewrite Hperm in Hin_l'.
      inv Hin_l'.
      ++ elim c. symmetry. hnf in * |-. cbn in *. auto.
      ++ inv H0; trivial; [].
         assert (Hperm' := Hperm).
         apply (PermutationA_subrelation_compat subrelation_pair_elt eq_refl eq_refl) in Hperm'.
         rewrite Hperm' in Hnodup. exfalso.
         inv Hnodup. revert_one not. intro Habs. apply Habs.
         right. apply InA_pair_elt with (n:=n0). revert_one eq_pair. intros [[? Hl] ?]. cbn in *.
         revert Hpt. apply InA_eqA; autoclass; []. now repeat split; cbn.
    - assert (Hin_l' : InA eq_pair (pt2, l0, n0) ((pt, li, n) :: l)).
      { rewrite Hperm. now do 2 right. }
      rewrite <- Hsame in Hin_l'. rewrite Hperm in Hin_l'.
      inv Hin_l'.
      ++ revert_one eq_pair. intros [[? Hl] ?]. cbn in *.
         assert (Hperm' := Hperm).
         apply (PermutationA_subrelation_compat subrelation_pair_elt eq_refl eq_refl) in Hperm'.
         rewrite Hperm' in Hnodup. exfalso.
         inv Hnodup. revert_one not. intros _.
         inv H4. revert_one not. intros Habs. apply Habs.
         apply InA_pair_elt with (n:=n). 
         revert Hpt. apply InA_eqA; autoclass; []. now repeat split; cbn.
      ++ inv H0; trivial; [].
         elim c. symmetry. hnf in * |-. cbn in *. auto.
* (* Real case: pt == pt2 *)
  assert (Hin' : InA eq_pair (pt1, li, n) ((pt, li, n) :: l)).
  { rewrite Hsame. left. repeat split; cbn; auto. }
  inversion_clear Hin'; try (now cbn in *; elim c; symmetry); [].
  revert_one InA. intro Hin'. apply PermutationA_split in Hin'; autoclass; [].
  destruct Hin' as [l' Hperm]. cbn. 
  setoid_rewrite (Hfold _ _ (filter_PermutationA_compat (Hcompat pt1) Hperm) i i eq_refl).
  setoid_rewrite (Hfold _ _ (filter_PermutationA_compat (Hcompat pt2) Hperm) (n+i) (n+i) eq_refl).
  cbn. setoid_rewrite (equiv_dec_refl pt1). destruct_match; try (now elim Hdiff); [].
  cbn. apply Hrec.
  + rewrite Hperm. cbn. lia.
(*   + intros x Hx. apply Hin. rewrite Hperm. now do 2 right. *)
  + inv Hnodup. revert_all. intro Hgoal. assert (Hperm' := Hperm).
    apply (PermutationA_subrelation_compat subrelation_pair_elt eq_refl eq_refl) in Hperm'.
    rewrite Hperm' in Hgoal. now inv Hgoal.
  + intros. split; intro Hpt.
    - assert (Hin_l' : InA eq_pair (pt1, l0, n0) ((pt, li, n) :: l)).
      { rewrite Hperm. now do 2 right. }
      rewrite Hsame in Hin_l'. rewrite Hperm in Hin_l'.
      inv Hin_l'.
      ++ revert_one eq_pair. intros [[? Hl] ?]. cbn in *.
         assert (Hperm' := Hperm).
         apply (PermutationA_subrelation_compat subrelation_pair_elt eq_refl eq_refl) in Hperm'.
         rewrite Hperm' in Hnodup. exfalso.
         inv Hnodup. revert_one not. intros _.
         inv H4. revert_one not. intros Habs. apply Habs.
         apply InA_pair_elt with (n:=n). 
         revert Hpt. apply InA_eqA; autoclass; []. now repeat split; cbn.
      ++ inv H0; trivial; [].
         elim Hdiff. symmetry. hnf in * |-. cbn in *. auto.
    - assert (Hin_l' : InA eq_pair (pt2, l0, n0) ((pt, li, n) :: l)).
      { rewrite Hperm. now do 2 right. }
      rewrite <- Hsame in Hin_l'. rewrite Hperm in Hin_l'.
      inv Hin_l'.
      ++ elim c. symmetry. hnf in * |-. cbn in *. auto.
      ++ inv H0; trivial; [].
         assert (Hperm' := Hperm).
         apply (PermutationA_subrelation_compat subrelation_pair_elt eq_refl eq_refl) in Hperm'.
         rewrite Hperm' in Hnodup. exfalso.
         inv Hnodup. revert_one not. intro Habs. apply Habs.
         right. apply InA_pair_elt with (n:=n0). revert_one eq_pair. intros [[? Hl] ?]. cbn in *.
         revert Hpt. apply InA_eqA; autoclass; []. now repeat split; cbn.
* (* Real case: pt =/= pt1, pt2 *)
  apply Hrec.
  + cbn. lia.
(*   + intros x Hx. apply Hin. now right. *)
  + now inv Hnodup.
  + intros. split; intro Hpt.
    - assert (Hin_l' : InA eq_pair (pt1, l0, n0) ((pt, li, n) :: l)) by now right.
      rewrite Hsame in Hin_l'. inv Hin_l'; trivial; [].
      revert_one eq_pair. intros [[? Hl] ?]. cbn in *. now elim c0.
    - assert (Hin_l' : InA eq_pair (pt2, l0, n0) ((pt, li, n) :: l)) by now right.
      rewrite <- Hsame in Hin_l'. inv Hin_l'; trivial; [].
      revert_one eq_pair. intros [[? Hl] ?]. cbn in *. now elim c.
Qed.
*)
Lemma color_bivalent_bivalent : forall config,
  color_bivalent config -> bivalent config.
Proof using Type.
  intros config H. 
  unfold bivalent.
  unfold color_bivalent in H.
  decompose [and ex] H.
  clear H.
  intuition.
  exists x, x0.
  split;auto.
Qed.

Corollary color_bivalent_even : forall config,
  color_bivalent config -> Nat.Even (nG + nB).
Proof using Type.
intros config Hconfig. apply color_bivalent_bivalent in Hconfig.
now destruct Hconfig.
Qed.

(* Lemma bivalent_size :
  forall config st, bivalent config -> size (fst (!!! (config,st))) = 2.
Proof using . intros. rewrite size_spec. now apply bivalent_support. Qed. *)

Lemma invalid_strengthen : (*nB = 0 -> *) forall config st,
    bivalent config ->
    { pt1 : location &
      { pt2 : location |        (* fixindent *)
        pt1 =/= pt2 &
          fst (!!! (config,st)) == add pt1 (Nat.div2 (nG+nB)) (singleton pt2 (Nat.div2 (nG+nB))) } }.
Proof using .
intros config st Hconfig.
(* Because we want a computational goal and the hypothesis is not,
   we first destruct the support to get the elements and only then
   prove that they are the same as the one given in [invalid]. *)
assert (Hlen := bivalent_size st Hconfig).
destruct (support (fst (!!! (config,st)))) as [| pt1 [| pt2 [| ? ?]]] eqn:Hsupp; try discriminate; [].
(* Transforming sig2 into sig to have only one goal after instanciating pt1 and pt2 *)
cut {pt1 : location & {pt2 : location
    | pt1 =/= pt2 /\ fst (!!! (config,st)) == add pt1 (Nat.div2 (nG+nB)) (singleton pt2 (Nat.div2 (nG+nB)))}}.
{ intros [? [? [? ?]]]. eauto. }
exists pt1, pt2.
destruct Hconfig as [Heven [Hge2 [pt1' [pt2' [Hdiff [Hpt1' Hpt2']]]]]].
(* Both couples of points are the same *)
assert (!!! (config, st) == (fst (!!! (config, st)), snd (!!! (config, st)))) as hsurj.
{ rewrite surjective_pairing at 1.
  reflexivity. }
specialize (@obs_from_config_fst st config (fst (!!! (config, st))) (snd (!!! (config, st))) hsurj (origin,witness)) as hfst.

assert (Hcase : pt1' == pt1 /\ pt2' == pt2 \/ pt1' == pt2 /\ pt2' == pt1).
{ assert (Hin1 : InA equiv pt1' (pt1 :: pt2 :: nil)).
  { rewrite <- Hsupp, support_spec. unfold In.
    rewrite hfst, Hpt1'.
    destruct (nG+nB) as [| [| nG]] eqn:heqnB ; simpl;
    lia || (set (d := (Nat.div2 nG)); lia). }
  assert (Hin2 : InA equiv pt2' (pt1 :: pt2 :: nil)).
  { rewrite <- Hsupp, support_spec. unfold In.
    rewrite hfst, Hpt2'.
    destruct (nG+nB) as [| [| nG]] eqn:heqnB; simpl;
    lia || (set (d := (Nat.div2 nG)); lia). }
  rewrite 2 InA_cons, InA_nil in Hin1, Hin2. clear -Hin1 Hin2 Hdiff.
  decompose [or] Hin1; decompose [or] Hin2; tauto || elim Hdiff; etransitivity; eauto. }
split.
+ intro. apply Hdiff.
  decompose [and or] Hcase; repeat (etransitivity; eauto; symmetry).
+ symmetry. apply cardinal_total_sub_eq.
  - intro pt. rewrite add_spec, singleton_spec.
    repeat destruct_match;
    destruct Hcase as [[Heq1 Heq2] | [Heq1 Heq2]];
      rewrite Heq1,Heq2 in *;
      try match goal with H : pt == _ |- _ => rewrite H in *; clear H end;
      try (now elim Hdiff);
      rewrite ?hfst, ?hsurj, ?Hpt1', ?Hpt2';
      lia.
  - rewrite cardinal_add, cardinal_singleton, (cardinal_obs_from_config config (origin, witness)).
    now apply even_div2.
Qed.

Lemma bivalent_same_location: forall config st pt1 pt2 pt3, bivalent config ->
  In pt1 (fst (!!! (config,st))) -> In pt2 (fst (!!! (config,st))) -> In pt3 (fst (!!! (config,st))) ->
  pt1 =/= pt3 -> pt2 =/= pt3 -> pt1 == pt2.
Proof using .
intros config st pt1 pt2 pt3 Hinvalid Hin1 Hin2 Hin3 Hdiff13 Hdiff23.
destruct (invalid_strengthen st Hinvalid) as [pta [ptb Hdiff Hobs]].
rewrite Hobs, add_In, In_singleton in Hin1, Hin2, Hin3.
destruct Hin1 as [[] | []], Hin2 as [[] | []], Hin3 as [[] | []];
solve [ etransitivity; eauto
      | elim Hdiff13; etransitivity; eauto
      | elim Hdiff23; etransitivity; eauto ].
Qed.
Arguments bivalent_same_location config st {pt1} {pt2} pt3 _ _ _ _ _.

Lemma bivalent_other_locs : forall config, bivalent config ->
  forall pt1 pt2, pt1 =/= pt2 -> In pt1 (!! config) -> In pt2 (!! config) ->
  forall pt, pt =/= pt1 -> pt =/= pt2 -> (!! config)[pt] = 0.
Proof using Type.
intros config Hbivalent pt1 pt2 Hdiff Hpt1 Hpt2 pt Hneq1 Hneq2.
destruct (Nat.eq_0_gt_0_cases (!!config)[pt]) as [| Hlt]; trivial; [].
elim Hdiff. apply (bivalent_same_location _ _ pt Hbivalent Hpt1 Hpt2); intuition.
Qed.

Lemma obs_fst : forall config, !! config == fst (!!!(config, (origin, witness))).
Proof using Type. reflexivity. Qed.

Lemma fold_obs_fst : forall st config, fst (!!!(config, st)) == !! config.
Proof using Type. reflexivity. Qed.

Definition bivalent_extended (config : configuration) :=
  let n := nG + nB in
  Nat.Even n /\ n >=2 /\ exists pt1 pt2 : location, pt1 =/= pt2
  /\ (!! config)[pt1] = Nat.div2 n /\ (!! config)[pt2] = Nat.div2 n
  /\ support (!! config) = cons pt1 (cons pt2 nil)
  /\ (forall pt, In pt (!! config) -> pt == pt1 \/ pt == pt2)
  /\ forall pt, pt =/= pt1 -> pt =/= pt2 -> (!! config)[pt] = 0.

Lemma extend_bivalent : forall config,
  bivalent config <-> bivalent_extended config.
Proof using Type.
intro config. split; intro Hbivalent.
* assert (Hlen := bivalent_size (origin, witness) Hbivalent).
  rewrite <- obs_fst in Hlen.
  assert (Hnodup := support_NoDupA (!! config)).
  destruct Hbivalent as [Heven [Hle [pt1 [pt2 [Hdiff [Hpt1 Hpt2]]]]]].
  repeat split; trivial; [].
  destruct (support (!! config)) as [| pt [| pt' [|]]] eqn:Hsupp; try discriminate; [].
  exists pt, pt'.
  assert (Hperm : PermutationA equiv (cons pt (cons pt' nil)) (cons pt1 (cons pt2 nil))).
  { rewrite <- Hsupp.
    symmetry. apply NoDupA_inclA_length_PermutationA; autoclass.
    + repeat constructor; rewrite ?InA_cons, InA_nil; intuition.
    + intros x Hin. rewrite InA_cons, InA_singleton in Hin.
      rewrite support_spec. unfold In.
      assert (Nat.div2 (nG + nB) > 0). { apply Exp_prop.div2_not_R0. lia. }
      destruct Hin as [Heq | Heq]; rewrite Heq; lia.
    + rewrite Hsupp. reflexivity. }
  rewrite PermutationA_2 in Hperm.
  repeat split.
  + inv Hnodup. now rewrite InA_singleton in *.
  + now destruct Hperm as [[Heq _] | [Heq _]]; rewrite Heq.
  + now destruct Hperm as [[_ Heq] | [_ Heq]]; rewrite Heq.
  + intros x Hin. now rewrite <- support_spec, Hsupp, InA_cons, InA_singleton in Hin.
  + intros x Hpt Hpt'.
    destruct (Nat.eq_0_gt_0_cases (!! config)[x]) as [Heq | Hlt]; trivial; [].
    exfalso. change (In x (!! config)) in Hlt.
    rewrite <- support_spec, Hsupp, InA_cons, InA_singleton in Hlt. intuition.
  + autoclass.
* destruct Hbivalent as [Heven [Hle [pt1 [pt2 [Hdiff [Hpt1 [Hpt2 [Hsupp Hother]]]]]]]].
  repeat split; eauto.
Qed.


Lemma bivalent_min: forall c,
    bivalent c ->
    nG + nB >= 2.
Proof using Type.
  intros c h_biv. 
  unfold bivalent in h_biv.
  now decompose [and] h_biv.
Qed.


Lemma biv_col_size: forall c,
    color_bivalent c -> 
    size (!! c) = 2.
Proof using Type.
  intros c h_bivcol. 
  apply color_bivalent_bivalent in h_bivcol.
  apply bivalent_size with (st := (origin,witness)) in h_bivcol.
  rewrite <- h_bivcol.
  rewrite obs_from_ok2.
  apply size_spec.
Qed.

Lemma biv_col_supp: forall c,
    color_bivalent c -> 
    exists pt1 pt2, support (!! c) = (pt1::pt2::nil).
Proof using Type.
  intros c h_bivcol. 
  specialize (biv_col_size h_bivcol) as h_size.
  rewrite size_spec in h_size.
  destruct (support (!! c)) as [ | x [ | y [ | z l] ]] eqn:heq_supp; try now (exfalso; cbn in h_size;lia).
  eauto.
Qed.

Lemma biv_col_supp2: forall c st,
    color_bivalent c -> 
    exists pt1 pt2, support (fst (!!! (c,st))) = (pt1::pt2::nil).
Proof using Type.
  intros c st h_bivcol.
  specialize (biv_col_size h_bivcol) as h_size.
  rewrite size_spec in h_size.
  destruct (support (!! c)) as [ | x [ | y [ | z l] ]] eqn:heq_supp; try now (exfalso; cbn in h_size;lia).
  eauto.
Qed.

Lemma colors_indep:
  forall c st st',
    (colors (snd (!!! (c, st))))
    == (colors (snd (!!! (c, st')))).
Proof using Type.
  intros c st st'. 
  destruct (!!! (c, st)) eqn:heq.
  destruct (!!! (c, st')) eqn:heq'.
  cbn in *.
  unfold obs_from_config, Obs in *.
  cbn in *.
  inversion heq.
  inversion heq'.
  reflexivity.
Qed.

#[export]
Instance pair_compat_ours {A B} {SA : Setoid A} {SB : Setoid B}: Proper (equiv ==> equiv ==> equiv) (@pair A B).
Proof using Type.
  repeat intro.
  split;cbn;auto.
Qed.

(* Remove Hints pair_compat: typeclass_instances. *)

Lemma color_bivalent_correct : forall config st,
  color_bivalent_obs (obs_from_config config st) = true -> color_bivalent config.
Proof using .
  intros config st h_bivobs. 
  specialize (color_bivalent_obs_bivalent_obs _ h_bivobs) as h_biv.
  rewrite bivalent_obs_spec in h_biv.
  red in h_biv.
  destruct h_biv as [heq_n [h_lt_n _]].
  revert h_bivobs.
  unfold color_bivalent, color_bivalent_obs.
  repeat (destruct_match; subst; try discriminate).
  intros h_and.
  split;auto.
  split;auto.

  apply andb_prop in h_and.
  destruct h_and as [ heq_loc h_forall].
  apply Nat.eqb_eq in heq_loc.
  rewrite <- (obs_from_config_fst_ok (origin, witness)) in heq_loc.
  rewrite  forallb_forall in h_forall.
  setoid_rewrite Nat.eqb_eq in h_forall.

  specialize (obs_from_config_fst_spec config (origin,witness)) as h.
  specialize (h l) as h_l.
  specialize (h l1) as h_l1.
  clear h.
  assert (h_cardinal := cardinal_fst_obs_from_config config (origin,witness)).
  rewrite <- obs_fst in h_cardinal.
  rewrite cardinal_fold_support in h_cardinal.
  rewrite <- obs_from_config_fst_ok with (st:=(0%VS, witness)) in H.
  rewrite H in h_cardinal.
  cbn in h_cardinal.
  rewrite Nat.add_0_r in h_cardinal.
  specialize (support_NoDupA (!! config)) as h.
  rewrite H in h.
  apply NoDupA_2 in h.
  exists l, l1.
  split;auto.
  rewrite heq_loc in h_cardinal.
  match type of h_cardinal with
    ?A + ?A = ?B => replace (A + A) with (2 * A) in h_cardinal
  end.
  2:{ lia. }
  rewrite <- h_cardinal.
  rewrite Nat.div2_double.
  rewrite Forall_forall.
  repeat split;auto.
Qed.



Lemma color_bivalent_complete : forall config st,
  color_bivalent config -> color_bivalent_obs (obs_from_config config st) = true.
Proof using .
  intros c st h_bivcol. 
  specialize (color_bivalent_bivalent h_bivcol) as h_biv.
  assert ((nG + nB) >= 2) as h_n_le_2.
  { apply (bivalent_min h_biv). }
  assert (Nat.div2 (nG + nB) >= 1) as h_div2n_lt_2.
  { apply Exp_prop.div2_not_R0. lia. }
  specialize (bivalent_obs_spec c st) as h_biv_obs.
  destruct h_biv_obs as [h_biv1 h_biv2].
  specialize (h_biv2 h_biv).
  clear h_biv1.
  specialize (invalid_strengthen st h_biv) as h_biv_str.
  destruct h_biv_str as [pt [pt' h_pt heq_obsmulti1]].
  specialize obs_from_config_fst_ok with  (st:=(@origin _ _ _ VS, @witness Lght)) (st':=st) (c:=c) as heq_obsmulti2.

  unfold color_bivalent in h_bivcol.
  unfold color_bivalent_obs.
  destruct h_bivcol as [ h_even [ h_n [ pt1 [pt2 [h_neq [h_mult_pt1 [h_mult_pt2 h_Forall ]]]]]]].

  destruct (!!! (c, st)) eqn:heq_ofc.
  cbn [snd fst] in *.
  assert (PermutationA equiv (support o) (support (add pt (Nat.div2 (nG + nB)) (singleton pt' (Nat.div2 (nG + nB)))))) as h_supp.
  { rewrite heq_obsmulti1.
    reflexivity. }
  rewrite support_add in h_supp;auto.
  destruct (In_dec pt (singleton pt' (Nat.div2 (nG + nB)))).
  { exfalso.
    rewrite In_singleton in i.
    destruct i.
    contradiction. }
  rewrite support_singleton in h_supp;auto.

  assert (PermutationA equiv (pt::pt'::nil) (pt1::pt2::nil) ) as h_ptpt'.
  { rewrite <- h_supp.
    assert (InA equiv pt1 (support o)).
    { rewrite support_spec.
      unfold In.
      rewrite <-  heq_obsmulti2.
      lia. }
    assert (InA equiv pt2 (support o)).
    { rewrite support_spec.
      unfold In.
      rewrite <-  heq_obsmulti2.
      lia. }
    rewrite h_supp in *.
    repeat rewrite InA_cons in *.
    rewrite InA_nil in *.
    symmetry.
    apply PermutationA_2;autoclass.
    clear -H H0 h_neq h_pt.
    intuition.
    - assert (pt1 == pt2).
      { transitivity pt;auto. }
      contradiction.
    - assert (pt1 == pt2).
      { transitivity pt';auto. }
      contradiction. }
  
  specialize (permA_trans h_supp h_ptpt') as h_trans_permA.

  destruct (PermutationA_2_gen _ h_trans_permA) as [ptx [pty [[[h_ptx h_pty]  | [h_ptx h_pty]] h_l]]].
  - rewrite PermutationA_2 in h_ptpt';[|autoclass].
    rewrite h_l.
    apply Bool.andb_true_iff.
    split.
    + rewrite h_ptx,h_pty.
      rewrite heq_obsmulti1.
      apply Nat.eqb_eq.
      destruct h_ptpt' as [ [heq_pt heq_pt'] | [heq_pt heq_pt'] ];
        rewrite <-heq_pt, <-heq_pt', add_same, add_other; (try assumption);
        try (now symmetry);
        rewrite singleton_same,singleton_other;auto.
    + apply forallb_forall.
      intros a h_in_a_l_list. 
      rewrite Forall_forall in h_Forall.
      apply Nat.eqb_eq.
      change (!!! (c, st)) with ( fst (!!! (c, st)), snd (!!! (c, st))) in heq_ofc.
      change o0 with (snd (o,o0)).
      apply h_Forall in h_in_a_l_list.
      rewrite (colors_indep c (0%VS, witness) st) in h_in_a_l_list.
      rewrite  <- heq_ofc.
      destruct h_ptpt' as [ [heq_pt heq_pt'] | [heq_pt heq_pt'] ];
        rewrite h_ptx,h_pty;
        apply h_in_a_l_list.
  - rewrite PermutationA_2 in h_ptpt';[|autoclass].
    rewrite h_l.
    apply Bool.andb_true_iff.
    split.
    + rewrite h_ptx,h_pty.
      rewrite heq_obsmulti1.
      apply Nat.eqb_eq.
      destruct h_ptpt' as [ [heq_pt heq_pt'] | [heq_pt heq_pt'] ];
        rewrite <-heq_pt, <-heq_pt', add_same, add_other; (try assumption);
        try (now symmetry);
        rewrite singleton_same,singleton_other;auto.
    + apply forallb_forall.
      intros a h_in_a_l_list. 
      rewrite Forall_forall in h_Forall.
      apply Nat.eqb_eq.
      change (!!! (c, st)) with ( fst (!!! (c, st)), snd (!!! (c, st))) in heq_ofc.
      change o0 with (snd (o,o0)).
      apply h_Forall in h_in_a_l_list.
      rewrite (colors_indep c (0%VS, witness) st) in h_in_a_l_list.
      rewrite  <- heq_ofc.
      destruct h_ptpt' as [ [heq_pt heq_pt'] | [heq_pt heq_pt'] ];
        rewrite h_ptx,h_pty;
        symmetry;
        apply h_in_a_l_list.
Qed.

Corollary color_bivalent_obs_spec : forall config st,
  color_bivalent_obs (obs_from_config config st) = true <-> color_bivalent config.
Proof using . intros. split; eauto using color_bivalent_correct, color_bivalent_complete. Qed.


Lemma bivalent_dec : forall config, {bivalent config} + {~bivalent config}.
Proof using .
intros config.
destruct (bivalent_obs (!!! (config, (origin, witness)))) eqn:Hcase.
+ left. now rewrite bivalent_obs_spec in Hcase.
+ right. now rewrite <- bivalent_obs_spec, Hcase.
Qed.

Lemma color_bivalent_dec : forall config, {color_bivalent config} + {~color_bivalent config}.
Proof using .
intros config.
destruct (color_bivalent_obs (!!! (config, (origin, witness)))) eqn:Hcase.
+ left. now rewrite color_bivalent_obs_spec in Hcase.
+ right. now rewrite <- color_bivalent_obs_spec, Hcase.
Qed.


Lemma bivalent_sim : forall (sim : similarity location) Psim obs,
  bivalent (map_config (lift (existT precondition sim Psim)) obs) <-> bivalent obs.
Proof using Type.
intros sim Psim obs.
rewrite <- 2 bivalent_obs_spec; trivial; [].
rewrite obs_from_config_map with (st := (origin, witness)).
+ rewrite bivalent_obs_morph. reflexivity.
+ apply Bijection.section_compat.
+ apply injective.
Qed.

Lemma color_bivalent_obs_morph : forall f obs,
  color_bivalent_obs (map_obs f obs) = color_bivalent_obs obs.
Proof using Type.
intros f [obs_loc obs_light].
unfold color_bivalent_obs in *. cbn -[equiv support].
assert (Hperm := map_injective_support (Bijection.section_compat f) (Bijection.injective f) obs_loc).
assert (Hlen := PermutationA_length Hperm).
destruct (support (map f obs_loc)) as [| pt1' [| pt2' []]],
         (support obs_loc) as [| pt1 [| pt2 []]];
cbn in Hlen; try discriminate || reflexivity; [].
cbn in Hperm. rewrite PermutationA_2 in Hperm; [| now autoclass].
destruct Hperm as [[Heq1 Heq2] | [Heq1 Heq2]]; rewrite Heq1, Heq2;
(rewrite 2 map_injective_spec; try apply Bijection.injective; [| now autoclass | now autoclass]).
+ f_equal. f_equiv.
  intros ? c ?. subst. rewrite Heq1, Heq2.
  change (f ?A, c) with ((λ x, (f (fst x), snd x)) (A, c)).
  setoid_rewrite map_injective_spec.
  - reflexivity.
  - intros ? ? H. now rewrite H.
  - intros [] [] [Heq Heq']. split; cbn in *; trivial; []. apply (Bijection.injective f _ _ Heq).
  - intros ? ? H. now rewrite H.
  - intros [] [] [Heq Heq']. split; cbn in *; trivial; []. apply (Bijection.injective f _ _ Heq).
+ rewrite Nat.eqb_sym.
  f_equal. f_equiv.
  intros ? c ?. subst. rewrite Heq1, Heq2, Nat.eqb_sym.
  change (f ?A, c) with ((λ x, (f (fst x), snd x)) (A, c)).
  setoid_rewrite map_injective_spec.
  - reflexivity.
  - intros ? ? H. now rewrite H.
  - intros [] [] [Heq Heq']. split; cbn in *; trivial; []. apply (Bijection.injective f _ _ Heq).
  - intros ? ? H. now rewrite H.
  - intros [] [] [Heq Heq']. split; cbn in *; trivial; []. apply (Bijection.injective f _ _ Heq).
Qed.


Property state_in_config : forall config pt id, In (config id) (colors (snd (Obs.(obs_from_config) config pt))).
Proof using Type.
intros config pt id. unfold obs_from_config. simpl. unfold In.
rewrite make_multiset_spec. rewrite (countA_occ_pos _).
change (@prod_Setoid (@location Loc) (@L Lght) (@location_Setoid Loc) (@L_Setoid Lght))
         with (@state_Setoid _ (prod (@location Loc) (@L Lght)) _).
rewrite (config_list_InA (config id) config). now exists id.
Qed.

Property obs_from_config_In_gen : forall (config:configuration) (pt:location * L) (l:location * L),
  In l (colors (snd (Obs.(obs_from_config) config pt))) <-> exists id, (config id) == l.
Proof using InaFun UpdFun.
intros config pt l. split; intro Hin.
+ assert (Heq := obs_from_config_spec config pt).
  unfold obs_is_ok, obs_from_config, multiset_observation in *.
  unfold In in Hin.
  cbn in *.
  destruct Heq as [ ? [? h]].
  rewrite h in Hin.
  rewrite (countA_occ_pos _ (prod_EqDec location_EqDec L_EqDec)) in Hin.
  rewrite config_list_spec in Hin.
  rewrite (InA_map_iff _ _) in Hin.
  - firstorder.
  - repeat intro. cbn in *. now subst.
+ destruct Hin as [id Hid]. rewrite <- Hid.
  apply state_in_config.
Qed.

End MultisetGathering.
