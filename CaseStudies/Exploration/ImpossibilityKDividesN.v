(**************************************************************************)
(*   Mechanised Framework for Local Interactions & Distributed Algorithms *)
(*   P. Courtieu, R. Pelle, L. Rieg, X. Urbain                            *)
(*   PACTOLE project                                                      *)
(*                                                                        *)
(*   This file is distributed under the terms of the CeCILL-C licence     *)
(*                                                                        *)
(**************************************************************************)

Require Import Utf8.
Require Import Arith Lia.
Require Import SetoidList.
Require Import Pactole.Util.Stream.
Require Import Pactole.Models.NoByzantine.
Require Import Pactole.Models.RingSSync.
Require Import Pactole.CaseStudies.Exploration.ExplorationDefs.

Set Implicit Arguments.

Section Exploration.

(** Given an abitrary ring *)
Context {n : nat} {ltc_2_n : 2 <c n}.
(** There are k good robots and no byzantine ones. *)
Context {k : nat} {ltc_0_k : 0 <c k}.
Instance Robots : Names := Robots k 0.

(** Assumptions on the number of robots: it strictly divides the ring size. *)
Hypothesis kdn : (n mod k = 0).
Hypothesis k_inf_n : (k < n).

Lemma h_not_0: n <> 0.
Proof using ltc_2_n.
  unfold ltc in *.
  apply neq_lt. 
  transitivity 2; auto with arith.
Qed.

Lemma k_not_0: k <> 0.
Proof using ltc_0_k.
  unfold ltc in *.
  now apply neq_lt. 
Qed.

Local Hint Resolve h_not_0 k_not_0: localDB.

Lemma local_subproof1 : n = k * (n / k).
Proof using kdn ltc_0_k. apply Nat.Div0.div_exact. auto with localDB. Qed.

Lemma local_subproof2 : n / k ≠ 0.
Proof using kdn ltc_2_n ltc_0_k.
  intros Habs. eapply @neq_u_0. apply ltc_2_n. rewrite local_subproof1, Habs. clear. lia.
Qed.

Local Hint Resolve local_subproof1 local_subproof2 : localDB.

Lemma local_subproof3 : ∀ m : nat, m < k -> m * (n / k) < n.
Proof using kdn k_inf_n ltc_0_k.
intros * H. pattern n at 2. rewrite (Nat.div_mod_eq n k). (* FIXME: bug in rewrite? *)
rewrite kdn, Nat.add_0_r, <- Nat.mul_lt_mono_pos_r; trivial; [].
apply Nat.div_str_pos. split; auto with arith.
Qed.

Instance NoByz : NoByzantine.
Proof using . now split. Qed.

(** A dummy state used for (inexistant) byzantine robots. *)
Definition dummy_loc : location := fin0. (* could be anything *)

Notation "!! config" := (obs_from_config config dummy_loc) (at level 0).

(** Let us consider an arbirary robogram. *)
Variable r : robogram.

(** The key idea is to prove that we can always make robots think that there are
    in the same configuration.  Thus, is they move at all, then they will never stop.
    If they do not move, they do not explore the ring.
    The configuration to which we will always come back is [NoByz_periodic_config],
    in which robots are evenly spaced on the ring.  *)

(** ***  Definition of the demon used in the proof  **)

Definition create_ref_config (g : G) : location :=
  mod2fin (fin2nat g * (n / k)).

(** The starting configuration where robots are evenly spaced:
    each robot is at a distance of [ring_size / kG] from the previous one,
    starting from the origin. *)
Definition ref_config : configuration :=
  fun id => match id with
              | Good g => create_ref_config g
              | Byz b => dummy_loc
            end.


Lemma ref_config_injective :
  Util.Preliminary.injective eq equiv (fun id => get_location (ref_config id)).
Proof using kdn ltc_0_k k_inf_n.
intros id1 id2. apply (no_byz id2), (no_byz id1). clear id1 id2.
intros g1 g2 Heq. f_equal. unfold ref_config, create_ref_config in Heq.
eapply mod2fin_betweenI, Nat.mul_cancel_r, fin2natI in Heq; trivial;
try (now split; [apply Nat.le_0_l | apply local_subproof3, fin_lt]).
intro Habs. rewrite Nat.div_small_iff in Habs; try lia; []. now rewrite Nat.neq_0_lt_0.
Qed.

(**  Translating [ref_config] by multiples of [n / k]
     does not change its observation. *)
Lemma obs_asbf_ref_config : forall g,
  !! (map_config (λ x, subf x (create_ref_config g)) ref_config) == !! ref_config.
Proof using kdn ltc_0_k k_inf_n.
  unfold obs_from_config, MultisetObservation.multiset_observation,
  MultisetObservation.make_multiset. intro g. f_equiv.
  rewrite 2 config_list_spec, 4 map_map.
  apply NoDupA_equivlistA_PermutationA.
  * autoclass.
  * apply map_injective_NoDupA with eq; autoclass; [|].
    + intros id1 id2 [Heq _]. apply ref_config_injective. apply addIm in Heq.
      rewrite Heq. reflexivity.
    + rewrite NoDupA_Leibniz. apply names_NoDup.
  * apply map_injective_NoDupA with eq; autoclass; [|].
    + intros ? ? []. now apply ref_config_injective.
    + rewrite NoDupA_Leibniz. apply names_NoDup.
  * intro pt. repeat rewrite InA_map_iff; autoclass; [].
    split; intros [id [Hpt _]]; revert Hpt; apply (no_byz id); clear id; intros g' Hpt.
    + pose (id' := subf g' g). change (fin k) with G in id'.
      exists (Good id'). split. 2:{ rewrite InA_Leibniz. apply In_names. }
      rewrite <- Hpt. cbn -[create_ref_config BijectionInverse]. split; trivial; [].
      change G with (fin k) in *. unfold create_ref_config, Datatypes.id, id'. apply fin2natI.
      rewrite 2 subf2nat, 3 mod2fin2nat, Nat.Div0.add_mod_idemp_l, 2 (Nat.mod_small (_ * _));auto with localDB.
      - pattern n at 3 5. rewrite local_subproof1.
        rewrite <- Nat.mul_sub_distr_r, <- Nat.mul_add_distr_r.
        rewrite Nat.Div0.mul_mod_distr_r;try auto with localDB.
      - apply local_subproof3, fin_lt.
      - pattern n at 2. rewrite local_subproof1, <- Nat.mul_lt_mono_pos_r; try apply mod2fin_lt; [].
        apply Nat.neq_0_lt_0, local_subproof2.
    + pose (id' := addf g' g). change (fin k) with G in id'.
      exists (Good id'). split. 2:{ rewrite InA_Leibniz. apply In_names. }
      rewrite <- Hpt. cbn. split. 2: reflexivity. unfold create_ref_config, id', Datatypes.id. apply fin2natI.
      rewrite subf2nat, 3 mod2fin2nat, addf2nat, Nat.Div0.add_mod_idemp_l, 2 (Nat.mod_small (_ * _));
      try (pattern n at 2; rewrite local_subproof1, <- Nat.mul_lt_mono_pos_r; try apply fin_lt;           
           [];
           apply Nat.div_str_pos; split; trivial; []; now apply Nat.lt_le_incl); try lia ;[].
      rewrite local_subproof1 at 2 4.
      rewrite <- Nat.mul_sub_distr_r, <- Nat.mul_add_distr_r, Nat.Div0.mul_mod_distr_r; auto with localDB.
      apply Nat.mul_cancel_r; try apply local_subproof2; [].
      rewrite Nat.Div0.add_mod_idemp_l, <- Nat.add_assoc, (Nat.add_comm (fin2nat g)), Nat.sub_add;
      try apply Nat.lt_le_incl, fin_lt;auto with localDB.
      rewrite <- Nat.Div0.add_mod_idemp_r, Nat.Div0.mod_same, Nat.add_0_r;auto with localDB. apply Nat.mod_small, fin_lt.
Qed.

(** The demon activate all robots and shifts their view to be on 0. *)
Program Definition da : demonic_action := {|
  activate := fun id => true;
  relocate_byz := fun _ _ => dummy_loc;
  change_frame := fun config g => (config (Good g), false);
  choose_update := fun _ _ _ => tt;
  choose_inactive := fun _ _ => tt |}.
Next Obligation. (* activate_compat *) now repeat intro. Qed.
Next Obligation. (* relocate_byz_compat *)
  repeat intro. do 2 f_equal. subst. auto.
Qed.
Next Obligation. (* change_frame_compat *) now repeat intro. Qed.
Next Obligation. (* choose_update_compat *) now repeat intro. Qed.

Definition bad_demon : demon := Stream.constant da.

(** This setting is FFSYNC. *)
Lemma FSYNC_one : FSYNC_da da.
Proof using . split. Qed.
Lemma FYSNC_setting : FSYNC bad_demon.
Proof using . coinduction Hcoind. apply FSYNC_one. Qed.

(** As all robots see the same observation, we take for instance the one at location [origin]. *)
Definition move := pgm r (!! ref_config).
Definition target := move_along fin0 move.

(** Acceptable starting configurations contain no tower,
    that is, all robots are at different locations. *)
Definition Valid_starting_config config : Prop :=
  Util.Preliminary.injective (@Logic.eq ident) (@equiv _ state_Setoid) config.

Definition Explore_and_Stop (r : robogram) :=
  forall d config, Fair d -> Valid_starting_config config ->
  ExplorationStop (execute r d config).

(** **  First case: robots do not move  **)

(** In this case, the configuration stays exactly the same
    hence there is a location which is not explored. *)

Section NoMove.
Hypothesis Hmove : move == SelfLoop.

Lemma round_id : round r da ref_config == ref_config.
Proof using Hmove kdn ltc_0_k k_inf_n.
  unfold ltc in *.
  rewrite FSYNC_round_simplify.
  2: split.
  apply no_byz_eq. intro g.
  cbn-[create_ref_config Bijection.BijectionInverse equiv].
  erewrite transvE, asbfVE, obs_from_config_ignore_snd, obs_asbf_ref_config, Hmove, move_along_0.
  rewrite Bijection.inv_inv, asbfE. apply subfVKV.
Qed.

Lemma NeverVisited_ref_config : ∀ e, e == execute r bad_demon ref_config
  -> exists pt, ~ Will_be_visited pt e.
Proof using Hmove kdn k_inf_n ltc_0_k.
  intros e Heq_e. exists (mod2fin 1). intro Hl.
  induction Hl as [e [g Hvisited] | e Hlater IHvisited].
  * rewrite Heq_e in Hvisited. cbn in Hvisited.
    apply (f_equal (@fin2nat n)) in Hvisited. revert Hvisited.
    cbn-[Nat.modulo Nat.div]. rewrite local_subproof1 at 2.
    rewrite Nat.Div0.mul_mod_distr_r, (Nat.mod_small 1), Nat.mul_eq_1.
    intros [_ Habs].
    apply Nat.lt_nge in k_inf_n.
    contradiction k_inf_n.
    rewrite local_subproof1,
    Habs, Nat.mul_1_r. reflexivity.
    eapply @lt_l_u. apply lt_s_u. auto.
  * apply IHvisited. rewrite Heq_e, execute_tail. rewrite round_id. f_equiv.
Qed.

Lemma never_visited :
  ~(∀ pt, Will_be_visited pt (execute r bad_demon ref_config)).
Proof using Hmove kdn k_inf_n ltc_0_k.
  intros Hw. destruct (NeverVisited_ref_config (reflexivity _)) as [pt Hpt].
  apply Hpt, Hw.
Qed.

Theorem no_exploration_idle : ~ Explore_and_Stop r.
Proof using Hmove k_inf_n kdn ltc_0_k.
  intros Habs. destruct (Habs bad_demon ref_config) as [Hexpl _].
  apply FSYNC_implies_Fair, FYSNC_setting. apply ref_config_injective.
  now apply never_visited.
Qed.

End NoMove.

(** **  Second case: the robots move  **)

(** ***  Equality of configurations up to translation  **)

(** Translating a configuration. *)
Definition f_config config m : configuration := map_config (asbm m) config.

Instance f_config_compat : Proper (equiv ==> equiv ==> equiv) f_config.
Proof using .
  unfold f_config. intros c1 c2 Hc m1 m2 Hm. rewrite Hc, Hm. reflexivity.
Qed.

Lemma f_config_merge : ∀ config m1 m2,
  f_config (f_config config m1) m2 == f_config config (m1 + m2).
Proof using k_inf_n kdn.
  intros. unfold f_config. rewrite map_config_merge; autoclass; [].
  intros id. cbn-[equiv]. rewrite 3 asbmE, <- (addm_mod (config id)), <- mod2fin2nat,
  <- addmA, addm2nat, mod2fin2nat, Nat.Div0.add_mod_idemp_l;auto with localDB.
  apply addm_mod.
Qed.

Lemma f_config_swap : ∀ config m1 m2,
  f_config (f_config config m1) m2 == f_config (f_config config m2) m1.
Proof using k_inf_n kdn.
  intros. rewrite 2 f_config_merge, Nat.add_comm. reflexivity.
Qed.

Lemma f_config_0 : ∀ config, f_config config 0 == config.
Proof using . intros * id. cbn-[equiv]. apply addm0. Qed.

Lemma f_config_injective_config : ∀ m config1 config2,
  f_config config1 m == f_config config2 m -> config1 == config2.
Proof using k_inf_n kdn.
  intros * Heq. eapply map_config_inj'. 2: apply Heq. apply Bijection.injective.
Qed.

Lemma f_config_injective_N : ∀ config m1 m2,
  f_config config m1 == f_config config m2
  -> m1 mod n == m2 mod n.
Proof using kdn k_inf_n ltc_0_k.
  unfold f_config. intros * Heq. specialize (Heq (Good fin0)).
  eapply (@addm_betweenI 2 n ltc_2_n). 3: rewrite 2 addm_mod.
  3: apply Heq. all: split. 1,3: apply Nat.le_0_l. all: apply mod2fin_lt.
Qed.

Lemma f_config_modulo : ∀ config m,
  f_config config (m mod n) == f_config config m.
Proof using . intros * id. apply addm_mod. Qed.

Lemma asbf_f_config : ∀ (config : configuration) (id1 id2 : ident) (m : nat),
  asbf (config id1)⁻¹ (config id2)
  == asbf (f_config config m id1)⁻¹ (f_config config m id2).
Proof using .
  intros. unfold f_config. cbn-[equiv]. symmetry. apply subf_addm_addm.
Qed.

(** Two configurations are equivalent if they are equal up to a global translation. *)
Definition equiv_config_m m config1 config2 : Prop
  := config2 == f_config config1 m.
Definition equiv_config config1 config2 : Prop
  := ∃ m, equiv_config_m m config1 config2.

Lemma equiv_config_m_sym : ∀ m config1 config2,
  equiv_config_m m config1 config2
  -> equiv_config_m (@oppm 2 n ltc_2_n m) config2 config1.
Proof using k_inf_n kdn.
  unfold equiv_config_m. intros * Hequiv. unshelve erewrite Hequiv,
  f_config_merge, <- f_config_modulo, (proj2 (Nat.Lcm0.mod_divide _ _));auto with localDB.
  symmetry. apply f_config_0. apply divide_addn_oppm.
Qed.

Lemma equiv_config_m_trans : ∀ m1 m2 config1 config2 config3,
  equiv_config_m m1 config1 config2 -> equiv_config_m m2 config2 config3 ->
  equiv_config_m (m1 + m2) config1 config3.
Proof using k_inf_n kdn.
  unfold equiv_config_m. intros * Hequiv12 Hequiv23.
  now rewrite Hequiv23, Hequiv12, f_config_merge.
Qed.

Instance equiv_config_equiv : Equivalence equiv_config.
Proof using k_inf_n kdn. split.
  + intro config. exists 0. unfold equiv_config_m. now rewrite f_config_0.
  + intros config1 config2 [m Hm]. exists (@oppm 2 n ltc_2_n m).
    apply (equiv_config_m_sym Hm).
  + intros ? ? ? [m1 Hm1] [m2 Hm2]. exists (m1 + m2).
    revert Hm1 Hm2. apply equiv_config_m_trans.
Qed.

Lemma equiv_config_mod : ∀ (m : nat) (config1 config2 : configuration),
  equiv_config_m (m mod n) config1 config2
  <-> equiv_config_m m config1 config2.
Proof using .
  intros. split; intros H id.
  - rewrite <- f_config_modulo. apply H.
  - rewrite f_config_modulo. apply H.
Qed.

(* It is actually an equivalence. *)
Instance eq_equiv_subrelation : subrelation equiv equiv_config.
Proof using .
  intros. exists 0. unfold equiv_config_m. now rewrite f_config_0.
Qed.

(** Equivalent configurations produce the same observation
    hence the same answer from the robogram. *)

Lemma config1_obs_equiv : ∀ config1 config2, equiv_config config1 config2
  -> ∀ g, !! (map_config (asbf (config1 (Good g))⁻¹) config1)
       == !! (map_config (asbf (config2 (Good g))⁻¹) config2).
Proof using k_inf_n kdn.
  intros config1 config2 [offset Hequiv] g. f_equiv.
  unfold equiv_config_m in Hequiv. rewrite Hequiv. rewrite Hequiv.
  intros id. cbn[map_config]. apply asbf_f_config.
Qed.

Theorem equiv_config_m_round : ∀ m config1 config2,
  equiv_config_m m config1 config2
  -> equiv_config_m m (round r da config1) (round r da config2).
Proof using k_inf_n kdn.
  unfold equiv_config_m. intros * Hequiv. unfold f_config.
  rewrite 2 (FSYNC_round_simplify r _ FSYNC_one). intros id. apply (no_byz id).
  clear id. intro g. cbn-[equiv Bijection.BijectionInverse].
  setoid_rewrite <- config1_obs_equiv. 2,3: reflexivity.
  repeat rewrite Hequiv.
  setoid_rewrite transvVE. rewrite 2 transvE, 2 asbfE, 2 asbfVE, asbmE. cbn-[f_config equiv].
  rewrite <- 2 subf_move_along'. unfold f_config, map_config. cbn. rewrite 2 subfVKV, addm_move_along.
  apply move_along_compat. reflexivity. apply (pgm_compat r),
  obs_from_config_compat. 2: reflexivity. intros id. apply subf_addm_addm.
Qed.

Corollary equiv_config_round : ∀ config1 config2, equiv_config config1 config2
  -> equiv_config (round r da config1) (round r da config2).
Proof using k_inf_n kdn.
  intros config1 config2 [m Hequiv]. exists m. now apply equiv_config_m_round.
Qed.

(** ***  Equality of executions up to translation  **)

Definition AlwaysEquiv m (e1 e2 : execution) : Prop :=
  Stream.forever2 (Stream.instant2 (equiv_config_m m)) e1 e2.

Lemma AlwaysEquiv_refl : ∀ e, AlwaysEquiv 0 e e.
Proof using .
  coinduction Hcoind. unfold Stream.instant2, equiv_config_m.
  now rewrite f_config_0.
Qed.

Lemma AlwaysEquiv_sym : ∀ m (e1 e2 : execution),
  AlwaysEquiv m e1 e2 -> AlwaysEquiv (@oppm 2 n ltc_2_n m) e2 e1.
Proof using k_inf_n kdn.
  cofix Hcoind. intros m1 e1 e2 [Hnow Hlater]. constructor.
  + now apply equiv_config_m_sym.
  + apply Hcoind; auto.
Qed.

Lemma AlwaysEquiv_trans : ∀ m1 m2 (e1 e2 e3 : execution),
  AlwaysEquiv m1 e1 e2 -> AlwaysEquiv m2 e2 e3 -> AlwaysEquiv (m1 + m2) e1 e3.
Proof using k_inf_n kdn.
  cofix Hrec. intros * [Hnow12 Hlater12] [Hnow23 Hnlater23]. constructor.
  + eapply equiv_config_m_trans; eauto.
  + apply Hrec with (tl e2); auto.
Qed.

Instance execute_equiv_compat : ∀ m,
  Proper (equiv_config_m m ==> AlwaysEquiv m) (execute r bad_demon).
Proof using k_inf_n kdn.
  intros. coinduction Hrec; trivial; []. simpl.
  now apply equiv_config_m_round.
Qed.

Lemma AlwaysEquiv_mod : ∀ (m : nat) (e1 e2 : execution),
  AlwaysEquiv (m mod n) e1 e2 <-> AlwaysEquiv m e1 e2.
Proof using .
  intros. split.
  - revert m e1 e2. cofix Hrec. intros * H. constructor.
    setoid_rewrite <- equiv_config_mod. apply H. apply Hrec, H.
  - revert m e1 e2. cofix Hrec. intros * H. constructor.
    setoid_rewrite equiv_config_mod. apply H. apply Hrec, H. 
Qed.

(** Stopping is invariant by this notion of equivalence. *)
Instance Stall_equiv_compat : ∀ m, Proper (AlwaysEquiv m ==> iff) Stall.
Proof using k_inf_n kdn.
  intros m s1 s2 Hequiv. unfold Stall.
  destruct Hequiv as [Hequiv [Hequiv' Hlater]].
  unfold instant2, equiv_config_m in *. rewrite Hequiv, Hequiv'. split.
  - intro Heq. now rewrite Heq.
  - apply f_config_injective_config.
Qed.

Lemma Stopped_equiv_compat_aux : ∀ m e1 e2,
  AlwaysEquiv m e1 e2 -> Stopped e1 -> Stopped e2.
Proof using k_inf_n kdn.
  cofix Hcoind. intros m e1 e2 Hequiv Hstop. constructor.
  + rewrite <- (Stall_equiv_compat Hequiv). apply Hstop.
  + destruct Hequiv. apply (Hcoind _ _ _ Hequiv), Hstop.
Qed.

Instance Stopped_equiv_compat : ∀ m, Proper (AlwaysEquiv m ==> iff) Stopped.
Proof using k_inf_n kdn.
  intros ? ? ? ?.
  split; eapply Stopped_equiv_compat_aux; eauto using AlwaysEquiv_sym.
Qed.

Instance NoStopped_equiv_compat : ∀ m,
  Proper (AlwaysEquiv m ==> iff) (fun e => ~Stopped e).
Proof using k_inf_n kdn.
  intros ? ? ? Hequiv. now rewrite (Stopped_equiv_compat Hequiv).
Qed.

(** An execution that never stops is always moving. *)
Definition AlwaysMoving (e : execution) : Prop :=
  forever (fun e1 => ~Stopped e1) e.

Lemma AlwaysMoving_equiv_compat_aux : ∀ m e1 e2,
  AlwaysEquiv m e1 e2 -> AlwaysMoving e1 -> AlwaysMoving e2.
Proof using k_inf_n kdn.
  cofix Hcoind. intros m e1 e2 Hequiv He. constructor.
  + rewrite <- (NoStopped_equiv_compat Hequiv). apply He.
  + destruct Hequiv. apply (Hcoind _ _ _ Hequiv), He.
Qed.

Instance AlwaysMoving_equiv_compat : ∀ m,
  Proper (AlwaysEquiv m ==> iff) AlwaysMoving.
Proof using k_inf_n kdn.
  intros ? ? ? ?.
  split; eapply AlwaysMoving_equiv_compat_aux; eauto using AlwaysEquiv_sym.
Qed.

Instance AlwaysMoving_execute_compat : ∀ m, Proper (equiv_config_m m ==> iff)
  (λ config, AlwaysMoving (execute r bad_demon config)).
Proof using k_inf_n kdn.
  intros m ? ? Hequiv.
  apply (AlwaysMoving_equiv_compat (execute_equiv_compat Hequiv)).
Qed.

(** ***  Proof when robots move  **)

(** In this case, the configuration is equivalent after a round so the algorithm never stops. *)

Section DoesMove.

Hypothesis Hmove : move =/= SelfLoop.

(** After a round, the configuration obtained from ref_config is equivalent to ref_config. *)
Lemma round_simplify : round r da ref_config == f_config ref_config target.
Proof using k_inf_n kdn ltc_0_k.
  rewrite (FSYNC_round_simplify _ _ FSYNC_one). apply no_byz_eq. intro g.
  cbn-[Bijection.BijectionInverse mod2fin equiv].
  setoid_rewrite transvVE. rewrite transvE, asbfE, asbfVE.
  rewrite obs_from_config_ignore_snd, obs_asbf_ref_config.
  unfold target. rewrite subff, 2 move_along0_, addfC. unfold f_config.
  rewrite asbmE. cbn. rewrite addm_addf, <- dir2nodE. reflexivity.
Qed.

Corollary round_ref_config :
  equiv_config_m target ref_config (round r da ref_config).
Proof using k_inf_n kdn ltc_0_k. apply round_simplify. Qed.

Corollary AlwaysEquiv_ref_config :
  AlwaysEquiv target (execute r bad_demon ref_config)
                     (tl (execute r bad_demon ref_config)).
Proof using k_inf_n kdn ltc_0_k.
  apply execute_equiv_compat, round_simplify.
Qed.

(** An execution that is always moving cannot stop. *)
Lemma AlwaysMoving_not_WillStop : ∀ e, AlwaysMoving e -> ~Will_stop e.
Proof using .
  intros e [Hnow Hmoving] Hstop. induction Hstop as [e Hstop | e Hstop IHstop].
 contradiction. inv Hmoving. now apply IHstop.
Qed.

(** The starting configuration is always moving. *)
Lemma ref_config_AlwaysMoving : AlwaysMoving (execute r bad_demon ref_config).
Proof using Hmove k_inf_n kdn ltc_0_k.
  generalize (AlwaysEquiv_refl (execute r bad_demon ref_config)).
  generalize 0 at 1. generalize (execute r bad_demon ref_config) at 1 3. 
  cofix Hcoind. intros e m Hequiv. constructor.
  + clear Hcoind. rewrite Hequiv. intros [Hstop _]. contradiction Hmove.
    unfold Stall in Hstop. rewrite execute_tail, round_simplify in Hstop.
    simpl hd in Hstop. apply (move_along_I fin0). rewrite move_along_0.
    apply fin2natI. setoid_rewrite <- Nat.mod_small. 2,3: apply fin_lt.
    eapply f_config_injective_N. rewrite fin02nat, f_config_0.
    symmetry. apply Hstop.
  + eapply (Hcoind _ (addm (oppf target) m)). clear Hcoind. rewrite addm2nat,
    Nat.add_comm. apply AlwaysEquiv_mod.
    apply AlwaysEquiv_trans with (tl (execute r bad_demon ref_config)).
    apply Hequiv. rewrite oppf_oppm.
    apply AlwaysEquiv_sym, AlwaysEquiv_ref_config.
Qed.

(** Final theorem when robots move. *)
Theorem no_exploration_moving : ~ Explore_and_Stop r. 
Proof using Hmove k_inf_n kdn ltc_0_k.
  intros Habs. unfold Explore_and_Stop in *.
  destruct (Habs bad_demon ref_config) as [_ Hstop]. apply FSYNC_implies_Fair.
  apply FYSNC_setting. unfold Valid_starting_config. apply ref_config_injective.
  revert Hstop. now apply AlwaysMoving_not_WillStop, ref_config_AlwaysMoving.
Qed.

End DoesMove.

(** Final theorem combining both cases:
    In the asynchronous model, if the number of robots [kG] divides the size [n] of the ring,
    then the exploration with stop of a n-node ring is not possible. *)
Theorem no_exploration : ~ Explore_and_Stop r.
Proof using k_inf_n kdn ltc_0_k.
  destruct (move =?= SelfLoop) as [Hmove | Hmove].
  + now apply no_exploration_idle.
  + now apply no_exploration_moving.
Qed.

End Exploration.

(* Do not leave this here, because it makes "make vos" fail.
 It is done in Impossibilitykdividesn_Assumptions.v instead *)
(* Print Assumptions no_exploration. *)
