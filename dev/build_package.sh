#!/bin/sh

# Remove any previous archive
rm -rf ./package/ package.tgz

# Create all the required (sub-)directories
mkdir ./package
mkdir ./package/Util
mkdir ./package/Util/FSets
mkdir ./package/Util/FMaps
mkdir ./package/Util/MMultiset
mkdir ./package/Core
mkdir ./package/Spaces
mkdir ./package/Observations
mkdir ./package/Models
mkdir ./package/CaseStudies
mkdir ./package/CaseStudies/Convergence
mkdir ./package/CaseStudies/Gathering
mkdir ./package/CaseStudies/Gathering/InR
mkdir ./package/CaseStudies/Gathering/InR2
mkdir ./package/CaseStudies/Exploration
mkdir ./package/CaseStudies/LifeLine
mkdir ./package/minipactole

# Create a fresh Makefile from the _CoqPoject
coq_makefile -f _CoqProject -o Makefile

# Copy files into each directory
cp Util/FSets/FSetInterface.v \
   Util/FSets/FSetFacts.v \
   Util/FSets/FSetList.v \
   ./package/Util/FSets/

cp Util/FMaps/FMapInterface.v \
   Util/FMaps/FMapFacts.v \
   Util/FMaps/FMapList.v \
   ./package/Util/FMaps/

cp Util/MMultiset/Preliminary.v \
   Util/MMultiset/MMultisetInterface.v \
   Util/MMultiset/MMultisetFacts.v \
   Util/MMultiset/MMultisetWMap.v \
   Util/MMultiset/MMultisetExtraOps.v \
   ./package/Util/MMultiset/

cp Util/Coqlib.v \
   Util/Preliminary.v \
   Util/SetoidDefs.v \
   Util/NumberComplements.v \
   Util/ListComplements.v \
   Util/Ratio.v \
   Util/Lexprod.v \
   Util/Stream.v \
   Util/Bijection.v \
   ./package/Util/

cp Core/Identifiers.v \
   Core/State.v \
   Core/Configuration.v \
   Core/Formalism.v \
   ./package/Core/

cp Setting.v \
   Pactole_all.v \
   Makefile \
   _CoqProject \
   ./package/

cp Spaces/RealVectorSpace.v \
   Spaces/RealMetricSpace.v \
   Spaces/RealNormedSpace.v \
   Spaces/EuclideanSpace.v \
   Spaces/Similarity.v \
   Spaces/Isometry.v \
   Spaces/R.v \
   Spaces/R2.v \
   Spaces/Graph.v \
   Spaces/Ring.v \
   Spaces/Grid.v \
   Spaces/Isomorphism.v \
   ./package/Spaces/

cp Observations/Definition.v \
   Observations/MultisetObservation.v \
   Observations/MultisetObservationInfo.v \
   Observations/SetObservation.v \
   Observations/LimitedMultisetObservation.v \
   Observations/LimitedSetObservation.v \
   Observations/PointedObservation.v \
   Observations/PreCompositionObservation.v \
   Observations/PairObservation.v \
   ./package/Observations/

cp Models/NoByzantine.v \
   Models/Rigid.v \
   Models/Flexible.v \
   Models/Isometry.v \
   Models/Similarity.v \
   Models/RigidFlexibleEquivalence.v \
   Models/RigidFlexibleEquivalence_Assumptions.v \
   Models/DiscreteGraph.v \
   Models/ContinuousGraph.v \
   Models/GraphEquivalence.v \
   Models/GraphEquivalence_Assumptions.v \
   ./package/Models/

cp CaseStudies/Convergence/Impossibility_2G_1B.v \
   CaseStudies/Convergence/Impossibility_2G_1B_Assumptions.v \
   CaseStudies/Convergence/Algorithm_noB.v \
   CaseStudies/Convergence/Algorithm_noB_Assumptions.v \
   ./package/CaseStudies/Convergence/

cp CaseStudies/Gathering/Definitions.v \
   CaseStudies/Gathering/WithMultiplicity.v \
   CaseStudies/Gathering/WithMultiplicityLight.v \
   CaseStudies/Gathering/Impossibility.v \
   CaseStudies/Gathering/Impossibility_Assumptions.v \
   ./package/CaseStudies/Gathering/

cp CaseStudies/Gathering/InR/Algorithm.v \
   CaseStudies/Gathering/InR/Algorithm_Assumptions.v \
   CaseStudies/Gathering/InR/Impossibility.v \
   CaseStudies/Gathering/InR/Impossibility_Assumptions.v \
   ./package/CaseStudies/Gathering/InR/

cp CaseStudies/Gathering/InR2/Algorithm.v \
   CaseStudies/Gathering/InR2/Algorithm_Assumptions.v \
   CaseStudies/Gathering/InR2/Algorithm_withLight.v \
   CaseStudies/Gathering/InR2/Algorithm_withLight_Assumptions.v \
   CaseStudies/Gathering/InR2/FSyncFlexNoMultAlgorithm.v \
   CaseStudies/Gathering/InR2/FSyncFlexNoMultAlgorithm_Assumptions.v \
   CaseStudies/Gathering/InR2/Peleg.v \
   CaseStudies/Gathering/InR2/Peleg_Assumptions.v \
   CaseStudies/Gathering/InR2/Viglietta.v \
   CaseStudies/Gathering/InR2/Viglietta_Assumptions.v \
   ./package/CaseStudies/Gathering/InR2/

cp CaseStudies/Exploration/Definitions.v \
   CaseStudies/Exploration/ImpossibilityKDividesN.v \
   CaseStudies/Exploration/ImpossibilityKDividesN_Assumptions.v \
   CaseStudies/Exploration/Tower.v \
   CaseStudies/Exploration/Tower_Assumptions.v \
   ./package/CaseStudies/Exploration/

cp CaseStudies/LifeLine/Algorithm.v \
   ./package/CaseStudies/LifeLine/

cp minipactole/minipactole.v \
   ./package/minipactole/

# Compile the archive to make sure it works
time make -C package -j 3

# Clean the compilation
make -C package cleanall
rm package/Makefile.conf

# Create the actual archive
tar cfz package.tgz package
