



TOLERATED="Pactole_all.v\\|Util/FSets/OrderedType.v\\|CaseStudies/Volumes/NoCollisionAndPath.v"
VFILES=$(find . -name "*.v" | sed -e 's/\.\///' | grep -v "\.#\\|*~" | grep -v $TOLERATED | grep -v CaseStudies)

for i in $VFILES ; do
    echo $i | sed -e 's/\.v/./' | sed -e 's/\//./g' | sed -e 's/^/Require Import Pactole./'
done
