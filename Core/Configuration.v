(**************************************************************************)
(*  Mechanised Framework for Local Interactions & Distributed Algorithms  *)
(*  T. Balabonski, P. Courtieu, L. Rieg, X. Urbain                        *)
(*  PACTOLE project                                                       *)
(*                                                                        *)
(*  This file is distributed under the terms of the CeCILL-C licence      *)
(*                                                                        *)
(**************************************************************************)

(** Mechanised Framework for Local Interactions & Distributed Algorithms    
                                                                            
    T. Balabonski, P. Courtieu, L. Rieg, X. Urbain                          
                                                                            
    PACTOLE project                                                         
                                                                            
    This file is distributed under the terms of the CeCILL-C licence      *)


Require Import Utf8.
Require Import SetoidList.
Require Import SetoidDec.
Require Import Decidable.
Require Import Pactole.Util.Coqlib.
Require Import Pactole.Util.Bijection.
Require Import Pactole.Core.Identifiers.
Require Import Pactole.Core.State.
Set Implicit Arguments.
Typeclasses eauto := (dfs).


(** * Configurations *)

(** A configuration is simply a map fron robot indentifiers to robot states (including their locations). *)
Definition configuration `{State} `{Names} := ident -> info.

Section Configuration.

Context `{State}.
Context `{Names}.

(** Equality of configurations is extensional. *)
Global Instance configuration_Setoid : Setoid configuration := fun_Setoid ident _.

Global Instance configuration_compat : forall config : configuration, Proper (Logic.eq ==> equiv) config.
Proof using . repeat intro. now subst. Qed.

Open Scope program_scope.

(** The lists of positions for good, Byzantine, and all robots. *)
Definition Gpos := fun config : configuration => List.map (config ∘ Good) Gnames.
Definition Bpos := fun config : configuration => List.map (config ∘ Byz) Bnames.
Definition config_list := fun config => Gpos config ++ Bpos config.

Lemma Gpos_spec : forall config, Gpos config = List.map (config ∘ Good) Gnames.
Proof using . reflexivity. Qed.

Lemma Bpos_spec : forall config, Bpos config = List.map (config ∘ Byz) Bnames.
Proof using . reflexivity. Qed.

Close Scope program_scope.

Lemma config_list_spec : forall config, config_list config = List.map config names.
Proof using . intros. unfold config_list, names. rewrite map_app. now do 2 rewrite map_map. Qed.

(** Compatilities with equivalences. *)
Global Instance Gpos_compat : Proper (@equiv _ configuration_Setoid ==> eqlistA equiv) Gpos.
Proof using . intros f g Hfg. rewrite 2Gpos_spec, Hfg. reflexivity. Qed.

Global Instance Bpos_compat : Proper (@equiv _ configuration_Setoid ==> eqlistA equiv) Bpos.
Proof using . intros f g Hfg. rewrite 2Bpos_spec, Hfg. reflexivity. Qed.

Global Instance config_list_compat : Proper (@equiv _ configuration_Setoid ==> eqlistA equiv) config_list.
Proof using . intros f g Hfg. rewrite 2config_list_spec, Hfg. reflexivity. Qed.

(** Properties w.r.t. [InA] and [length]. *)
Lemma Gpos_InA : forall l config, InA equiv l (Gpos config) <-> exists g, equiv l (config (Good g)).
Proof using .
intros. rewrite Gpos_spec, InA_map_iff; autoclass;
try (now repeat intro; cbn in *; now subst); []. (* for 8.16 and 8.17 *)
split; intros [g Hg]; exists g.
  - now symmetry.
  - split; try (now symmetry); []. rewrite InA_Leibniz. apply In_Gnames.
Qed.

Lemma Bpos_InA : forall l config, InA equiv l (Bpos config) <-> exists b, equiv l (config (Byz b)).
Proof using .
intros. rewrite Bpos_spec, InA_map_iff; autoclass;
try (now repeat intro; cbn in *; now subst); []. (* for 8.16 and 8.17 *)
split; intros [b Hb]; exists b.
  - now symmetry.
  - split; try (now symmetry); []. rewrite InA_Leibniz. apply In_Bnames.
Qed.

Lemma config_list_InA : forall l config, InA equiv l (config_list config) <-> exists id, equiv l (config id).
Proof using .
intros l config. rewrite config_list_spec. unfold names. rewrite map_app, (InA_app_iff _).
repeat rewrite InA_map_iff; autoclass || (try now cbn; repeat intro; subst); [].
split; intro Hin.
+ destruct Hin as [[g [Hin _]] | [b [Hin  _]]]; symmetry in Hin; eauto.
+ setoid_rewrite InA_Leibniz. destruct Hin as [[g | b] Hin]; symmetry in Hin.
  - left. exists (Good g). auto using in_map, In_Gnames.
  - right. exists (Byz b). auto using in_map, In_Bnames.
Qed.

Lemma Gpos_length : forall config, length (Gpos config) = nG.
Proof using . intro. rewrite Gpos_spec, map_length. apply Gnames_length. Qed.

Lemma Bpos_length : forall config, length (Bpos config) = nB.
Proof using . intro. rewrite Bpos_spec, map_length. apply Bnames_length. Qed.

Lemma config_list_length : forall config, length (config_list config) = nG + nB.
Proof using . intro. now rewrite config_list_spec, map_length, names_length. Qed.

(** As the number of robots is finite, extensional equality of configurations is decidable. *)
Global Instance configuration_EqDec : @EqDec configuration _.
Proof using .
intros config₁ config₂.
destruct (eqlistA_dec equiv_dec (config_list config₁) (config_list config₂)) as [Heq | Heq];
rewrite 2 config_list_spec in Heq.
+ left. intro x. apply (fun_names_eq _ _ Heq).
+ right. intro Habs. apply Heq. f_equiv. intros ? ? Hpt. hnf in Hpt. subst. apply Habs.
Qed.

(** If two configurations are not equal, then there exists a robot
    that is not located in the same place in both. *)
Theorem config_neq_equiv : forall config₁ config₂ : configuration,
  config₁ =/= config₂ <-> exists id, ~config₁ id == config₂ id.
Proof using .
intros config₁ config₂. split; intro Hneq.
+ assert (Hlist : ~eqlistA equiv (List.map config₁ names) (List.map config₂ names)).
  { intro Habs. apply Hneq. hnf. cbn. intro id.
    assert (Hin : List.In id names) by now apply In_names.
    induction names as [| id' l].
    - inversion Hin.
    - inversion_clear Habs. inversion_clear Hin; solve [now subst | now apply IHl]. }
  induction names as [| id l].
  - now contradiction Hlist.
  - cbn in Hlist. destruct (equiv_dec (config₁ id) (config₂ id)) as [Hid | Hid].
    -- apply IHl. intro Heq. apply Hlist. now constructor.
    -- eauto.
+ destruct Hneq as [id Hneq]. intro Habs. apply Hneq, Habs.
Qed.


Definition on_loc pt (config : configuration) :=
  List.filter (fun id => get_location (config id) ==b pt) names.

Global Instance on_loc_compat : Proper (equiv ==> equiv ==> Logic.eq) on_loc.
Proof using.
intros pt1 pt2 Hpt config1 config2 Hconfig. unfold on_loc.
apply filter_extensionality_compat; trivial; [].
intros x id ?; subst x. unfold equiv_decb.
repeat destruct_match; trivial; rewrite Hconfig, Hpt in *; contradiction.
Qed.

Lemma on_loc_spec : forall pt config id, List.In id (on_loc pt config) <-> get_location (config id) == pt.
Proof using.
intros pt config id. unfold on_loc, equiv_decb. rewrite filter_In.
destruct_match; intuition; apply In_names.
Qed.

Lemma on_loc_NoDup : forall pt config, NoDup (on_loc pt config).
Proof using. intros pt config. apply NoDup_filter, names_NoDup. Qed.

(** The list of occupied locations inside a configuration *)
Definition occupied config :=
  removeA_dups equiv_dec (List.map (fun id => get_location (config id)) names).

Lemma occupied_spec : forall pt config,
  InA equiv pt (occupied config) <-> exists id, get_location (config id) == pt.
Proof using.
intros pt config. unfold occupied.
rewrite (proj1 (removeA_dups_spec equiv_dec _)), InA_map_iff; autoclass; [].
split.
+ intros [x [Hx _]]. now exists x.
+ intros [x Hx]. exists x. split; trivial; [].
  rewrite InA_Leibniz. apply In_names.
Qed.

Global Instance occupied_compat : Proper (equiv ==> eqlistA equiv) occupied.
Proof using.
intros config1 config2 Hconfig. unfold occupied.
induction names as [| id l]; try reflexivity; [].
cbn.
lazymatch goal with |- eqlistA equiv (if ?x then _ else _) (if ?y then _ else _) => assert (Htest : x = y) end.
{ apply mem_compat.
  - now rewrite Hconfig.
  - apply eqlistA_equivlistA; autoclass; [].
    apply (map_extensionalityA_compat (eqA := equiv) setoid_equiv); try reflexivity; [].
    intros id1 id2 Hid. now rewrite Hconfig, Hid. }
rewrite Htest.
destruct_match.
+ apply IHl.
+ constructor; trivial; []. now rewrite Hconfig.
Qed.

Lemma occupied_NoDupA : forall config, NoDupA equiv (occupied config).
Proof using. intro. apply removeA_dups_spec. Qed.

Lemma occupied_config_list : forall config,
  equivlistA equiv (occupied config) (map get_location (config_list config)).
Proof using.
intro.
etransitivity; [apply removeA_dups_spec |].
unfold config_list, Gpos, Bpos, names.
now rewrite 2 map_app, 4 map_map.
Qed.

End Configuration.

(** Injective configurations *)
Definition config_injective `{State} `{Names} :=
  Util.Preliminary.injective (@eq ident) (@equiv _ state_Setoid).

Lemma config_injective_equiv_NoDupA `{State} `{Names} : forall config : configuration,
  config_injective config <-> NoDupA equiv (config_list config).
Proof using .
intros config. rewrite config_list_spec. split; intro Hinj.
+ eapply map_injective_NoDupA; try apply Hinj; autoclass; [].
  rewrite NoDupA_Leibniz. apply names_NoDup.
+ intros id id' Hconfig.
  eapply (map_NoDupA_eq _ _ names_eq_dec _ Hinj); auto; rewrite InA_Leibniz; apply In_names.
Qed.

Lemma config_injective_dec `{State} `{Names} : forall config : configuration,
  {config_injective config} + {~ config_injective config}.
Proof using .
intros config.
destruct (NoDupA_dec equiv equiv_dec (config_list config));
rewrite <- (config_injective_equiv_NoDupA config) in *; tauto.
Qed.

Lemma config_not_injective `{State} `{Names} : forall config : configuration,
  ~ config_injective config <-> exists id id', id <> id' /\ config id == config id'.
Proof using .
intros config. split; intro Hinj. 
+ rewrite config_injective_equiv_NoDupA, not_NoDupA in Hinj; autoclass; try apply state_EqDec; [].
  destruct Hinj as [state [l Hperm]].
  assert (Hid : exists id, state == config id).
  { rewrite <- config_list_InA, Hperm. now left. }
  destruct Hid as [id Hid]. exists id.
  rewrite config_list_spec in Hperm.
  assert (Hin : In id names) by apply In_names.
  rewrite <- InA_Leibniz in Hin.
  apply PermutationA_split in Hin; autoclass; [].
  destruct Hin as [l' Hnames].
  assert (Hl' := Hnames). apply (PermutationA_map _ (f := config)) in Hl'; autoclass; [].
  rewrite Hl' in Hperm. simpl in Hperm. rewrite <- Hid in Hperm.
  apply PermutationA_cons_inv in Hperm; autoclass; [].
  assert (Hid' : InA equiv state (state :: l)) by now left.
  rewrite <- Hperm in Hid'.
  destruct (PermutationA_split _ Hid') as [l'' Hl''].
  rewrite (InA_map_iff (eqA := eq)) in Hid'; autoclass; [].
  destruct Hid' as [id' [Heq Hid']].
  exists id'. rewrite Heq, <- Hid. split; try reflexivity; [].
  intro Habs.
  assert (Hnodup := names_NoDup).
  rewrite <- NoDupA_Leibniz, Hnames in Hnodup. inversion_clear Hnodup. subst. tauto.
+ destruct Hinj as [id [id' [Hid Heq]]]. intro Habs. apply Habs in Heq. contradiction.
Qed.

(** Applying a function on all states of a configuration. *)

Section MapConfig.

Context {L : Location} {info1 info2 : Type}.
Context {St1 : @State _ info1} {St2 : @State _ info2}.
Context {N : Names}.

Definition map_config (f : info1 -> info2) (config : @configuration _ _ St1 _) : configuration :=
  fun id => f (config id).

Global Instance map_config_compat :
    Proper ((equiv ==> equiv) ==> @equiv _ configuration_Setoid ==> @equiv _ configuration_Setoid) map_config.
Proof using . intros f g Hfg ? ? Hconfig id. unfold map. apply Hfg, Hconfig. Qed.

Lemma config_list_map : forall f config,
  config_list (map_config f config) == List.map f (config_list config).
Proof using . intros. now rewrite 2 config_list_spec, map_map. Qed.

Lemma map_config_inj' : ∀ (f : info1 -> info2),
  Preliminary.injective equiv equiv f -> Preliminary.injective equiv equiv (map_config f).
Proof using . intros f H config1 config2 Hc id. apply H, Hc. Qed.

Lemma map_config_inj : ∀ (f : info1 -> info2) (config : @configuration _ _ St1 _),
  Preliminary.injective equiv equiv f -> config_injective config
  -> config_injective (map_config f config).
Proof using .
  unfold map_config. intros * Hf Hc id1 id2 Hm. apply Hc, Hf, Hm.
Qed.

End MapConfig.

Arguments map_config {_} {info1} {info2} {_} {_} {_} f config id /.

Lemma map_config_id `{State} `{Names} : forall config,
  map_config Datatypes.id config == config.
Proof using . now repeat intro. Qed.

Lemma map_config_merge `{Location} {T U V : Type} `{@State _ T} `{@State _ U} `{@State _ V} `{Names} :
  forall (f : T -> U) (g : U -> V), Proper (equiv ==> equiv) g ->
  forall config : configuration, map_config g (map_config f config) == map_config (fun x => g (f x)) config.
Proof using . now repeat intro. Qed.
