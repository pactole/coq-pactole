(**************************************************************************)
(**  Mechanised Framework for Local Interactions & Distributed Algorithms   
                                                                            
     P. Courtieu, L. Rieg, X. Urbain                                        
                                                                            
     PACTOLE project                                                        
                                                                            
     This file is distributed under the terms of the CeCILL-C licence       
                                                                          *)
(**************************************************************************)


Require Import SetoidDec SetoidList.
Require Import Arith_base.
Require Import Lia.
Require Import Pactole.Util.Coqlib.
Require Import Pactole.Util.Fin.
Require Import Pactole.Util.Enum.


Set Implicit Arguments.

(** ** Byzantine Robots *)

(** We have finitely many robots. Some are good, others are Byzantine.
    Both are represented by an abtract type that can be enumerated. *)
Class Names := {
  (** Number of good and Byzantine robots *)
  nG : nat;
  nB : nat;
  (** Types representing good and Byzantine robots *)
  G : Type;
  B : Type;
  (** Enumerations of robots *)
  Gnames : list G;
  Bnames : list B;
  (** The enumerations are complete and without duplicates *)
  In_Gnames : forall g : G, In g Gnames;
  In_Bnames : forall b : B, In b Bnames;
  Gnames_NoDup : NoDup Gnames;
  Bnames_NoDup : NoDup Bnames;
  (** There is the right amount of robots *)
  Gnames_length : length Gnames = nG;
  Bnames_length : length Bnames = nB;
  (** We can tell robots apart *)
  Geq_dec : forall g g' : G, {g = g'} + {g <> g'};
  Beq_dec : forall b b' : B, {b = b'} + {b <> b'};
  (** Being a finite type, extensional function equality is decidable *)
  fun_Gnames_eq : forall {A : Type} eqA f g,
    @eqlistA A eqA (List.map f Gnames) (List.map g Gnames) -> forall x, eqA (f x) (g x);
  fun_Bnames_eq : forall {A : Type} eqA f g,
    @eqlistA A eqA (List.map f Bnames) (List.map g Bnames) -> forall x, eqA (f x) (g x)}.

Global Opaque In_Gnames In_Bnames Gnames_NoDup Bnames_NoDup
              Gnames_length Bnames_length Geq_dec Beq_dec fun_Gnames_eq fun_Bnames_eq.

(** Identifiers make good and Byzantine robots undistinguishable.
    They have their own enumeration without duplicates,
    and both equality and extensional function equality are decidable. *)
Inductive ident `{Names} : Type :=
  | Good (g : G)
  | Byz (b : B).

Definition names `{Names} : list ident := List.map Good Gnames ++ List.map Byz Bnames.

Lemma In_names `{Names} : forall r : ident, In r names.
Proof using .
intro r. cbn. unfold names. rewrite in_app_iff. destruct r as [g | b].
- left. apply in_map, In_Gnames.
- right. apply in_map, In_Bnames.
Qed.

Corollary Forall_ident `{Names} : forall P, Forall P names <-> forall id, P id.
Proof using . intro P. rewrite Forall_forall. generalize In_names. intuition. Qed.

Corollary Exists_ident `{Names} : forall P, List.Exists P names <-> exists id, P id.
Proof using .
intro P.
rewrite Exists_exists.
split; intro Hex; decompose [ex and] Hex; eauto using In_names.
Qed.

Lemma names_NoDup `{Names} : NoDup names.
Proof using .
unfold names. rewrite <- NoDupA_Leibniz. apply (NoDupA_app _).
+ apply (map_injective_NoDupA _ _).
  - now repeat intro; hnf in *; subst.
  - intros ? ? Heq. now inversion Heq.
  - rewrite NoDupA_Leibniz. apply Gnames_NoDup.
+ apply (map_injective_NoDupA _ _).
  - now repeat intro; hnf in *; subst.
  - intros ? ? Heq. now inversion Heq.
  - rewrite NoDupA_Leibniz. apply Bnames_NoDup.
+ intros id HinA HinB. rewrite (InA_map_iff _ _) in HinA. rewrite (InA_map_iff _ _) in HinB.
  - destruct HinA as [? [? ?]], HinB as [? [? ?]]. subst. discriminate.
  - now repeat intro; hnf in *; subst.
  - now repeat intro; hnf in *; subst.
Qed.

Lemma names_length `{Names} : length names = nG + nB.
Proof using . unfold names. now rewrite app_length, map_length, map_length, Gnames_length, Bnames_length. Qed.

Lemma names_eq_dec `{Names} : forall id id' : ident, {id = id'} + { id <> id'}.
Proof using .
intros id id'.
destruct id as [g | b], id' as [g' | b']; try (now right; discriminate); [|].
+ destruct (Geq_dec g g').
  - left; subst; auto.
  - right; intro Habs. now injection Habs.
+ destruct (Beq_dec b b').
  - left; subst; auto.
  - right; intro Habs. now injection Habs.
Qed.

Global Instance ident_Setoid `{Names} : Setoid ident := { equiv := eq; setoid_equiv := eq_equivalence }.
Global Instance ident_EqDec `{Names} : EqDec ident_Setoid := names_eq_dec.

Global Instance fun_refl `{Names} : forall A (f : ident -> A) R,
  Reflexive R -> Proper (@SetoidClass.equiv ident _ ==> R) f.
Proof using . intros A f R HR ? ? Heq. simpl in Heq. now subst. Qed.

Global Instance list_ident_Setoid `{Names} : Setoid (list ident) := { equiv := eq; setoid_equiv := eq_equivalence }.
Global Instance list_ident_Eqdec `{Names} : EqDec list_ident_Setoid := list_eq_dec ident_EqDec.

Lemma fun_names_eq `{Names} : forall {A : Type} eqA f g,
  @eqlistA A eqA (List.map f names) (List.map g names) -> forall x, eqA (f x) (g x).
Proof using .
intros A eqA f h Heq id.
unfold names in Heq. repeat rewrite ?map_app, map_map in Heq. apply eqlistA_app_split in Heq.
+ destruct id as [g | b].
  - change (eqA ((fun x => f (Good x)) g) ((fun x => h (Good x)) g)). apply fun_Gnames_eq, Heq.
  - change (eqA ((fun x => f (Byz x)) b) ((fun x => h (Byz x)) b)). apply fun_Bnames_eq, Heq.
+ now do 2 rewrite map_length.
Qed.

Section Robots.

(** Given a number of correct and Byzantine robots, we can build canonical names.
    It is not declared as a global instance to avoid creating spurious settings. *)
Definition Robots (n m : nat) : Names.
Proof using .
  refine {|
    nG := n;
    nB := m;
    G := fin n;
    B := fin m;
    Gnames := enum n;
    Bnames := enum m |}.
  + abstract (intro g; apply In_enum, fin_lt).
  + abstract (intro b; apply In_enum, fin_lt).
  + apply enum_NoDup.
  + apply enum_NoDup.
  + apply enum_length.
  + apply enum_length.
  + apply fin_dec.
  + apply fin_dec.
  + intros ? ?. apply enum_eq.
  + intros ? ?. apply enum_eq.
Defined.

Global Opaque G B.
(* TODO: discuss this
Section NM.

Variables n m: nat.

Notation GRob := (@G (Robots n m)).
Notation BRob := (@B (Robots n m)).

Lemma GRob_Robots : GRob = fin n.
Proof using . reflexivity. Qed.

Lemma BRob_Robots : BRob = fin m.
Proof using . reflexivity. Qed.

Lemma GRob_Robots_eq_iff : forall g1 g2 : GRob, g1 = g2 :> GRob <-> g1 = g2 :> fin n.
Proof using . reflexivity. Qed.

Lemma BRob_Robots_eq_iff : forall b1 b2 : BRob, b1 = b2 :> BRob <-> b1 = b2 :> fin m.
Proof using . reflexivity. Qed.

Definition good0 : GRob := fin0.

Definition byz0 : BRob := fin0.

Lemma all_good0 : forall g : GRob, n = 1 -> g = good0.
Proof using . intros * H. rewrite GRob_Robots_eq_iff. apply all_fin0, H. Qed.

Lemma all_good_eq : forall g1 g2 : GRob, n = 1 -> g1 = g2.
Proof using ltc_l_n. intros * H. rewrite GRob_Robots_eq_iff. apply all_eq, H. Qed.

Lemma all_byz0 : forall b : BRob, m = 1 -> b = byz0.
Proof using . intros * H. rewrite BRob_Robots_eq_iff. apply all_fin0, H. Qed.

Lemma all_byz_eq : forall b1 b2 : BRob, m = 1 -> b1 = b2.
Proof using ltc_l_m. intros * H. rewrite BRob_Robots_eq_iff. apply all_eq, H. Qed.
End NM.

*)

End Robots.
